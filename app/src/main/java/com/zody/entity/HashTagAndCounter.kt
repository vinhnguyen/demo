package com.zody.entity

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, September 25
 */
class HashTagAndCounter(@SerializedName("name")
                        val name: String?,
                        @SerializedName("counter")
                        val counter: Int?) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is HashTagAndCounter) return false

        if (name != other.name) return false
        if (counter != other.counter) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name?.hashCode() ?: 0
        result = 31 * result + (counter ?: 0)
        return result
    }
}