package com.zody.entity

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity

/**
 * Created by vinhnguyen.it.vn on 2018, July 31
 */
@Entity(tableName = "post_photos_of_users", primaryKeys = ["photo_post_id"])
data class PostPhotoItem(@Embedded val photo: Photo,
                         @ColumnInfo(name = "photo_author_id") val authorId: String,
                         @ColumnInfo(name = "photo_post_id") val postId: String,
                         @ColumnInfo(name = "photo_next") val next: String?,
                         @ColumnInfo(name = "photo_order") val order: Int)