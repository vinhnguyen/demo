package com.zody.entity

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, July 20
 */
class Photo() : Parcelable {
    @SerializedName("_id")
    @ColumnInfo(name = "photo_id")
    lateinit var id: String
    @SerializedName("url")
    @ColumnInfo(name = "photo_url")
    var url: String? = null
    @SerializedName("width")
    @ColumnInfo(name = "photo_width")
    var width: Int? = null
    @SerializedName("height")
    @ColumnInfo(name = "photo_height")
    var height: Int? = null

    fun isHorizontal(): Boolean? {
        width?.let { w ->
            height?.let { h ->
                return w >= h
            }
        }
        return null
    }

    fun ratio(): Float? {
        width?.let { w ->
            height?.let { h ->
                return w.toFloat() / h.toFloat()
            }
        }
        return null
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Photo) return false

        if (id != other.id) return false
        if (url != other.url) return false
        if (width != other.width) return false
        if (height != other.height) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (url?.hashCode() ?: 0)
        result = 31 * result + (width ?: 0)
        result = 31 * result + (height ?: 0)
        return result
    }

    constructor(parcel: Parcel) : this() {
        id = parcel.readString() ?: ""
        url = parcel.readString()
        width = parcel.readValue(Int::class.java.classLoader) as? Int
        height = parcel.readValue(Int::class.java.classLoader) as? Int
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(url)
        parcel.writeValue(width)
        parcel.writeValue(height)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Photo> {
        override fun createFromParcel(parcel: Parcel): Photo {
            return Photo(parcel)
        }

        override fun newArray(size: Int): Array<Photo?> {
            return arrayOfNulls(size)
        }
    }


}