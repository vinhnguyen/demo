package com.zody.entity

import com.google.gson.annotations.SerializedName
import com.zody.entity.business.BusinessRating

/**
 * Created by vinhnguyen.it.vn on 2018, August 12
 */
open class BusinessList(id: String,
                        name: String?,
                        address: String?,
                        distance: Double?,

                        @SerializedName("chain")
                        val chain: String?,
                        @SerializedName("rating")
                        val rating: BusinessRating?,
                        @SerializedName("categories")
                        val categories: List<BusinessCategory>?) : BusinessCompat(id, name, address, distance) {

    @SerializedName("percentDiscount")
    var percentDiscount: String? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is BusinessList) return false
        if (!super.equals(other)) return false

        if (chain != other.chain) return false
        if (rating != other.rating) return false
        if (categories != other.categories) return false
        if (percentDiscount != other.percentDiscount) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + (chain?.hashCode() ?: 0)
        result = 31 * result + (rating?.hashCode() ?: 0)
        result = 31 * result + (categories?.hashCode() ?: 0)
        result = 31 * result + (percentDiscount?.hashCode() ?: 0)
        return result
    }
}