package com.zody.entity

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, July 18
 */
class LeaderBoardResult {
    @SerializedName("currentUserRank")
    var currentUserRank: Int? = null
    @SerializedName("users")
    var users: List<LeaderBoardItem>? = null
}