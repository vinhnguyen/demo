package com.zody.entity

import androidx.room.Embedded
import androidx.room.Entity
import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, July 18
 */
@Entity(tableName = "leader_board", primaryKeys = ["user_id", "type"])
class LeaderBoardItem {
    @SerializedName("user")
    @Embedded
    lateinit var user: UserCompat

    @SerializedName("rank")
    var rank: Int? = null

    @SerializedName("statistic")
    @Embedded
    var statistic: UserStatistic? = null


    companion object {
        const val TYPE_VIEW = "views"
        const val TYPE_TIPS = "tips"
        const val TYPE_COIN = "coins"

        val types = arrayOf(TYPE_VIEW, TYPE_TIPS, TYPE_COIN)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is LeaderBoardItem) return false

        if (user != other.user) return false
        if (rank != other.rank) return false
        if (statistic != other.statistic) return false

        return true
    }

    override fun hashCode(): Int {
        var result = user.hashCode()
        result = 31 * result + (rank ?: 0)
        result = 31 * result + (statistic?.hashCode() ?: 0)
        return result
    }


}