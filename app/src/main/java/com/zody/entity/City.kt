package com.zody.entity

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, July 12
 */
@Entity(tableName = "cities")
data class City(@PrimaryKey
                @SerializedName("_id")
                @ColumnInfo(name = "city_id")
                val id: String,

                @SerializedName("name")
                @ColumnInfo(name = "city_name")
                val name: String?,

                @SerializedName("location")
                @Embedded(prefix = "city_")
                val location: Coordinates?,

                @Transient
                @ColumnInfo(name = "city_is_filter")
                val isFilter: Boolean?) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is City) return false

        if (id != other.id) return false
        if (name != other.name) return false
        if (location != other.location) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + (location?.hashCode() ?: 0)
        return result
    }
}