package com.zody.entity

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, July 11
 */
class ProfileReferral(@SerializedName("code")
                      @ColumnInfo(name = "referral_code")
                      val code: String?,
                      @SerializedName("canEdit")
                      @ColumnInfo(name = "referral_can_edit")
                      val canEdit: Boolean?,
                      @SerializedName("shareText")
                      @ColumnInfo(name = "referral_share_text")
                      val shareText: String?,
                      @SerializedName("downloadURL")
                      @ColumnInfo(name = "referral_download_url")
                      val downloadURL: String?)