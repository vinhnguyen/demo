package com.zody.entity

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2019, January 18
 */
class BudweiserItem(@SerializedName("name")
                    val name: String?,
                    @SerializedName("quantity")
                    val quantity: Int?,
                    @SerializedName("coin")
                    val coin: Int?)