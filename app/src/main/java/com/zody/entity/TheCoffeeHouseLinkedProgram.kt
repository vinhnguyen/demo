package com.zody.entity

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, October 02
 */
class TheCoffeeHouseLinkedProgram(
        @SerializedName("cover")
        val cover: String?,
        @SerializedName("header")
        val header: String?,
        @SerializedName("footer")
        val footer: String?,
        @SerializedName("step1")
        val step1: String?,
        @SerializedName("step2")
        val step2: String?,
        @SerializedName("step3")
        val step3: String?,
        @SerializedName("tchId")
        val tchId: String?)