package com.zody.entity

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, September 04
 */
class Banner(@SerializedName("_id")
             val id: String,
             @SerializedName("photo")
             val photo: String?,
             @SerializedName("name")
             val name: String?,
             @SerializedName("type")
             val type: String?,
             @SerializedName("target")
             val target: String?,
             @SerializedName("url")
             val url: String?,
             @SerializedName("order")
             val order: Int?)