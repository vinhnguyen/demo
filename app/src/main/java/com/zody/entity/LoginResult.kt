package com.zody.entity

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, July 10
 */
class LoginResult(@SerializedName("isNew")
                  val isNew: Boolean?,
                  @SerializedName("token")
                  val token: String?)