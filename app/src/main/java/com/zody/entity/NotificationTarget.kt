package com.zody.entity

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, August 22
 */
class NotificationTarget(@SerializedName("_id")
                         @ColumnInfo(name = "notification_target_id")
                         val id: String,
                         @SerializedName("action")
                         @ColumnInfo(name = "notification_target_action")
                         val action: String?) {


    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is NotificationTarget) return false

        if (id != other.id) return false
        if (action != other.action) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (action?.hashCode() ?: 0)
        return result
    }
}