package com.zody.entity

import android.text.Spannable
import android.text.SpannableString
import android.text.util.Linkify
import androidx.core.text.util.LinkifyCompat
import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Ignore
import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by vinhnguyen.it.vn on 2018, July 27
 */
@Entity(tableName = "comments", primaryKeys = ["comment_id"])
class Comment {
    @SerializedName("_id")
    @ColumnInfo(name = "comment_id")
    lateinit var id: String
    @SerializedName("user")
    @Embedded(prefix = "comment_")
    var author: Author? = null
    @SerializedName("comment_review")
    var review: String? = null
    @SerializedName("createdAt")
    @ColumnInfo(name = "comment_created")
    var created: Date? = null
    @SerializedName("isAuthor")
    @ColumnInfo(name = "comment_is_author")
    var isAuthor: Boolean? = null
    @SerializedName("content")
    @ColumnInfo(name = "content")
    var content: String? = null
        set(value) {
            field = value
            if (value == null) {
                contentkWithLinkify = null
                return
            }
            val spannable = SpannableString(value)
            LinkifyCompat.addLinks(spannable, Linkify.ALL)
            contentkWithLinkify = spannable
        }
    @SerializedName("statistic")
    @Embedded
    var statistic: CommentStatistic? = null
    @SerializedName("photos")
    @ColumnInfo(name = "comment_photos")
    var photos: List<Photo>? = null

    @Ignore
    var contentkWithLinkify: Spannable? = null
        private set

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Comment) return false

        if (id != other.id) return false
        if (author != other.author) return false
        if (review != other.review) return false
        if (created != other.created) return false
        if (isAuthor != other.isAuthor) return false
        if (content != other.content) return false
        if (statistic != other.statistic) return false
        if (photos != other.photos) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (author?.hashCode() ?: 0)
        result = 31 * result + (review?.hashCode() ?: 0)
        result = 31 * result + (created?.hashCode() ?: 0)
        result = 31 * result + (isAuthor?.hashCode() ?: 0)
        result = 31 * result + (content?.hashCode() ?: 0)
        result = 31 * result + (statistic?.hashCode() ?: 0)
        result = 31 * result + (photos?.hashCode() ?: 0)
        return result
    }


}