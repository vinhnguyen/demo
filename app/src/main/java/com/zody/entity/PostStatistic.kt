package com.zody.entity

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, July 20
 */
class PostStatistic {
    @SerializedName("comments")
    @ColumnInfo(name = "post_comments")
    var comments: Int? = null
    @SerializedName("shares")
    @ColumnInfo(name = "post_shares")
    var shares: Int? = null
    @SerializedName("views")
    @ColumnInfo(name = "post_views")
    var views: Int? = null
    @SerializedName("tipped")
    @ColumnInfo(name = "post_tipped")
    var tipped: Int? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is PostStatistic) return false

        if (comments != other.comments) return false
        if (shares != other.shares) return false
        if (views != other.views) return false
        if (tipped != other.tipped) return false

        return true
    }

    override fun hashCode(): Int {
        var result = comments ?: 0
        result = 31 * result + (shares ?: 0)
        result = 31 * result + (views ?: 0)
        result = 31 * result + (tipped ?: 0)
        return result
    }


}