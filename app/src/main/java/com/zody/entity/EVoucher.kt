package com.zody.entity

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by vinhnguyen.it.vn on 2018, August 15
 */
class EVoucher(@SerializedName("_id")
               val id: String,
               @SerializedName("title")
               val title: String?,
               @SerializedName("desc")
               val desc: String?,
               @SerializedName("cover")
               val cover: String?,
               @SerializedName("expireAt")
               val expire: Date?,
               @SerializedName("usedAt")
               val used: Date?,
               @SerializedName("wayToRedeemReward")
               val wayToRedeemReward: String?,
               @SerializedName("mainName")
               val mainName: String?,
               @SerializedName("subName")
               val subName: String?,
               @SerializedName("expired")
               val expired: Boolean?,
               @SerializedName("isUsed")
               val isUsed: Boolean?,
               @SerializedName("applyFor")
               val applyFor: List<BusinessCompat>?) {

    @Transient
    var status: String? = null
        get() = field ?: when {
            isUsed == true -> STATUS_USED
            expired == true -> STATUS_EXPIRED
            else -> STATUS_UNUSED
        }

    companion object {
        const val WAY_TO_REDEEM_STAFF_CONFIRM = "staffConfirm"
        const val WAY_TO_REDEEM_SELF_REMOVE = "selfRemove"

        const val STATUS_USED = "used"
        const val STATUS_UNUSED = "unused"
        const val STATUS_EXPIRED = "expired"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is EVoucher) return false

        if (id != other.id) return false
        if (title != other.title) return false
        if (desc != other.desc) return false
        if (cover != other.cover) return false
        if (expire != other.expire) return false
        if (wayToRedeemReward != other.wayToRedeemReward) return false
        if (mainName != other.mainName) return false
        if (subName != other.subName) return false
        if (expired != other.expired) return false
        if (status != other.status) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (title?.hashCode() ?: 0)
        result = 31 * result + (desc?.hashCode() ?: 0)
        result = 31 * result + (cover?.hashCode() ?: 0)
        result = 31 * result + (expire?.hashCode() ?: 0)
        result = 31 * result + (wayToRedeemReward?.hashCode() ?: 0)
        result = 31 * result + (mainName?.hashCode() ?: 0)
        result = 31 * result + (subName?.hashCode() ?: 0)
        result = 31 * result + (expired?.hashCode() ?: 0)
        result = 31 * result + (status?.hashCode() ?: 0)
        return result
    }

}