package com.zody.entity

import com.google.gson.annotations.SerializedName
import com.zody.entity.explore.Category

/**
 * Created by vinhnguyen.it.vn on 2018, July 12
 */
class Configuration(@SerializedName("cities")
                    val cities: List<City>?,
                    @SerializedName("zcoinCenters")
                    val zcoinCenter: ZcoinCenter?,
                    @SerializedName("categories")
                    val categories: List<Category>?)