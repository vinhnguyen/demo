package com.zody.entity

import androidx.room.ColumnInfo
import androidx.room.Entity

/**
 * Created by vinhnguyen.it.vn on 2018, August 08
 */
@Entity(tableName = "notification_item", primaryKeys = ["ref_notification_id"])
data class NotificationItem(
        @ColumnInfo(name = "ref_notification_id")
        val id: String,
        @ColumnInfo(name = "page_next")
        val next: String?,
        @ColumnInfo(name = "item_order")
        val order: Int
)