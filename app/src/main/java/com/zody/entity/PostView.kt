package com.zody.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import java.util.*

/**
 * Created by vinhnguyen.it.vn on 2018, August 07
 */
@Entity(tableName = "post_views", primaryKeys = ["post_id"])
data class PostView(@ColumnInfo(name = "post_id")
                    val postId: String,
                    @ColumnInfo(name = "time")
                    val lastTime: Date)