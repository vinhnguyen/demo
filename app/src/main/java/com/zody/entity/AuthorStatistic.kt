package com.zody.entity

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, July 19
 */
class AuthorStatistic {
    @SerializedName("followers")
    @ColumnInfo(name = "author_followers")
    var followers: Int? = null
    @SerializedName("followings")
    @ColumnInfo(name = "author_followings")
    var followings: Int? = null
    @SerializedName("reviews")
    @ColumnInfo(name = "author_reviews")
    var reviews: Int? = null
    @SerializedName("coin")
    @ColumnInfo(name = "author_coin")
    var coin: Int? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is AuthorStatistic) return false

        if (followers != other.followers) return false
        if (reviews != other.reviews) return false
        if (coin != other.coin) return false

        return true
    }

    override fun hashCode(): Int {
        var result = followers ?: 0
        result = 31 * result + (reviews ?: 0)
        result = 31 * result + (coin ?: 0)
        return result
    }


}