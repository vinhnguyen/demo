package com.zody.entity.explore

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, September 08
 */
class OnlineShoppingOrderProduct(@SerializedName("id")
                                 val id: String,
                                 @SerializedName("name")
                                 val name: String?,
                                 @SerializedName("price")
                                 val price: Double?)