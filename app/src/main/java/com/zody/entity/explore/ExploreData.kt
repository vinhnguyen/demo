package com.zody.entity.explore

import com.google.gson.annotations.SerializedName
import com.zody.entity.Banner

/**
 * Created by vinhnguyen.it.vn on 2018, September 04
 */
class ExploreData(@SerializedName("banners")
                  val banners: List<Banner>?,
                  @SerializedName("featuredBrands")
                  val featuredBrands: List<BusinessFeatured>?,
                  @SerializedName("affiliatePrograms")
                  val affiliatePrograms: List<AffiliateProgram>?)