package com.zody.entity.explore

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, September 04
 */
class ExploreTop<T>(@SerializedName("data")
                    val data: List<T>,
                    @SerializedName("hasMore")
                    val hasMore: Boolean?,
                    @SerializedName("categories")
                    val categories: List<Category>?)