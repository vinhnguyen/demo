package com.zody.entity.explore

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, September 06
 */
class BusinessFeatured(@SerializedName("name")
                       val name: String,
                       @SerializedName("logo")
                       val logo: String?,
                       @SerializedName("business")
                       val business: String?,
                       @SerializedName("chain")
                       val chain: String?,
                       @SerializedName("order")
                       val order: Int?,
                       @SerializedName("totalBusinesses")
                       val totalBusinesses: Int?)