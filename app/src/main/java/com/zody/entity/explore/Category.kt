package com.zody.entity.explore

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, September 06
 */
@Entity(tableName = "categories", primaryKeys = ["category_id"])
class Category(@SerializedName("_id")
               @ColumnInfo(name = "category_id")
               val id: String,
               @SerializedName("name")
               @ColumnInfo(name = "category_name")
               val name: String?,
               @SerializedName("cover")
               @ColumnInfo(name = "category_cover")
               val cover: String?,
               @SerializedName("order")
               @ColumnInfo(name = "category_order")
               val order: Int?) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Category) return false

        if (id != other.id) return false
        if (name != other.name) return false
        if (cover != other.cover) return false
        if (order != other.order) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + (cover?.hashCode() ?: 0)
        result = 31 * result + (order ?: 0)
        return result
    }
}