package com.zody.entity.explore

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by vinhnguyen.it.vn on 2018, September 08
 */
class OnlineShoppingOrder(@SerializedName("_id")
                          val id: String,
                          @SerializedName("transactionId")
                          val transactionId: String?,
                          @SerializedName("transactionTime")
                          val transactionTime: Date?,
                          @SerializedName("coin")
                          val coin: Int?,
                          @SerializedName("status")
                          val status: OnlineShoppingOrderStatus?,
                          @SerializedName("products")
                          val products: List<OnlineShoppingOrderProduct>?)