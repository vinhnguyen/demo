package com.zody.entity.explore

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, September 04
 */
class ExploreMenu(@SerializedName("_id")
                  val id: String,
                  @SerializedName("shortDisplayName")
                  val name: String?,
                  @SerializedName("cover")
                  val cover: String?,
                  @SerializedName("child")
                  val children: List<MenuOnline>?,
                  @SerializedName("displayName")
                  val displayName: String?)