package com.zody.entity.explore

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, September 07
 */
class OnlineShoppingCategory(@SerializedName("_id")
                          val id: String,
                             @SerializedName("title")
                          val title: String?,
                             @SerializedName("desc")
                          val desc: String?,
                             @SerializedName("discount")
                          val discount: String?)