package com.zody.entity.explore

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, November 17
 */
class AffiliateProgram(@SerializedName("_id")
                       val id: String,
                       @SerializedName("title")
                       val name: String?,
                       @SerializedName("logo")
                       val logo: String?,
                       @SerializedName("discount")
                       val discount: String?,
                       @SerializedName("totalTransactions")
                       val totalTransactions: Int?) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is AffiliateProgram) return false

        if (id != other.id) return false
        if (name != other.name) return false
        if (logo != other.logo) return false
        if (discount != other.discount) return false
        if (totalTransactions != other.totalTransactions) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + (logo?.hashCode() ?: 0)
        result = 31 * result + (discount?.hashCode() ?: 0)
        result = 31 * result + (totalTransactions ?: 0)
        return result
    }
}