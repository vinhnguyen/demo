package com.zody.entity.explore

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, September 07
 */
class OnlineShoppingFaq(@SerializedName("_id")
                     val id: String,
                        @SerializedName("question")
                     val question: String?,
                        @SerializedName("answer")
                     val answer: String?)