package com.zody.entity.explore

import com.google.gson.annotations.SerializedName
import com.zody.entity.business.BusinessChainCompat
import java.util.*

/**
 * Created by vinhnguyen.it.vn on 2018, September 04
 */
class Promotion(@SerializedName("_id")
                val id: String,
                @SerializedName("name")
                val name: String?,
                @SerializedName("title")
                val title: String?,
                @SerializedName("cover")
                val cover: String?,
                @SerializedName("rating")
                val rating: Double?,
                @SerializedName("address")
                val address: String?,
                @SerializedName("endTime")
                val endTime: Date?,
                @SerializedName("businessId")
                val businessId: String?,
                @SerializedName("categories")
                val categories: List<String>?,
                @SerializedName("chain")
                val chain: BusinessChainCompat?) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Promotion) return false

        if (id != other.id) return false
        if (name != other.name) return false
        if (title != other.title) return false
        if (cover != other.cover) return false
        if (rating != other.rating) return false
        if (address != other.address) return false
        if (endTime != other.endTime) return false
        if (categories != other.categories) return false
        if (chain != other.chain) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + (title?.hashCode() ?: 0)
        result = 31 * result + (cover?.hashCode() ?: 0)
        result = 31 * result + (rating?.hashCode() ?: 0)
        result = 31 * result + (address?.hashCode() ?: 0)
        result = 31 * result + (endTime?.hashCode() ?: 0)
        result = 31 * result + (categories?.hashCode() ?: 0)
        result = 31 * result + (chain?.hashCode() ?: 0)
        return result
    }
}