package com.zody.entity.explore

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, September 07
 */
class OnlineShopping(@SerializedName("_id")
                     val id: String,
                     @SerializedName("title")
                     val title: String?,
                     @SerializedName("cover")
                     val cover: String?,
                     @SerializedName("desc")
                     val desc: String?,
                     @SerializedName("buttonText")
                     val buttonText: String?,
                     @SerializedName("url")
                     val url: String?,
                     @SerializedName("discount")
                     val discount: String?,
                     @SerializedName("openingVia")
                     val openingVia: String?,
                     @SerializedName("faqs")
                     val faq: List<OnlineShoppingFaq>?)