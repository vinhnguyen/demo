package com.zody.entity.explore

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, September 08
 */
class OnlineShoppingOrderStatus(@SerializedName("type")
                                val type: String?,
                                @SerializedName("title")
                                val title: String?,
                                @SerializedName("desc")
                                val desc: String?,
                                @SerializedName("color")
                                val color: String?)