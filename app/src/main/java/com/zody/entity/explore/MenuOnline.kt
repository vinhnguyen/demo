package com.zody.entity.explore

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, September 07
 */
class MenuOnline(@SerializedName("_id")
                 val id: String,
                 @SerializedName("name")
                 val name: String?,
                 @SerializedName("logo")
                 val logo: String?,
                 @SerializedName("kind")
                 val kind: String?)