package com.zody.entity

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, September 27
 */
class ProfileFacebook(@SerializedName("id")
                      @ColumnInfo(name = "facebook_id")
                      val id: String?,
                      @SerializedName("profileURL")
                      @ColumnInfo(name = "facebook_url")
                      val url: String?)