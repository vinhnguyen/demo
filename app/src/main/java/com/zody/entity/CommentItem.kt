package com.zody.entity

import androidx.room.ColumnInfo
import androidx.room.Entity

/**
 * Created by vinhnguyen.it.vn on 2018, July 27
 */
@Entity(tableName = "comment_items", primaryKeys = ["ref_comment_id"])
data class CommentItem(
        @ColumnInfo(name = "ref_comment_id")
        val commentId: String,
        @ColumnInfo(name = "comment_post_id")
        val postId: String,
        @ColumnInfo(name = "comment_next")
        val next: String?,
        @ColumnInfo(name = "comment_order")
        val order: Int)