package com.zody.entity.luckydraw

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by vinhnguyen.it.vn on 2018, December 03
 */
class LuckyDrawHistory(@SerializedName("_id")
                       val id: String,
                       @SerializedName("prize")
                       val prize: LuckyDrawPrize?,
                       @SerializedName("drawTime")
                       val time: Date?) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is LuckyDrawHistory) return false

        if (id != other.id) return false
        if (prize != other.prize) return false
        if (time != other.time) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (prize?.hashCode() ?: 0)
        result = 31 * result + (time?.hashCode() ?: 0)
        return result
    }
}