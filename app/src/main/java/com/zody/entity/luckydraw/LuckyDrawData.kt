package com.zody.entity.luckydraw

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, December 03
 */
class LuckyDrawData(@SerializedName("luckyDraw")
                      val luckyDraw: LuckyDraw?,
                    @SerializedName("userTurnLeft")
                      val userTurnLeft: Int?,
                    @SerializedName("todayWinners")
                      val winners: List<LuckyDrawWinner>?)