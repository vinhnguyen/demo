package com.zody.entity.luckydraw

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.zody.entity.BusinessCompat
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

/**
 * Created by vinhnguyen.it.vn on 2018, November 30
 */
@Parcelize
class LuckyDrawPrize(@SerializedName("_id")
                     val id: String,
                     @SerializedName("rewardTitle")
                     val rewardTitle: String?,
                     @SerializedName("reward")
                     val reward: String?,
                     @SerializedName("order")
                     val order: Int?,
                     @SerializedName("color")
                     val color: String?,
                     @SerializedName("covers")
                     val covers: List<String>?,
                     @SerializedName("business")
                     val business: BusinessCompat?,
                     @SerializedName("type")
                     val type: String?,
                     @SerializedName("price")
                     val price: Double?,
                     @SerializedName("quantity")
                     val quantity: Int?,
                     @SerializedName("isLuckyPrize")
                     val isLuckyPrize: Boolean?) : Parcelable {

    @IgnoredOnParcel
    val cover: String? = null
        get() = field ?: covers?.firstOrNull()

    companion object {
        const val TYPE_COIN = "coin"
        const val TYPE_VOUCHER = "voucher"
        const val TYPE_GOOD_LUCK = "goodLuck"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is LuckyDrawPrize) return false

        if (id != other.id) return false
        if (rewardTitle != other.rewardTitle) return false
        if (reward != other.reward) return false
        if (order != other.order) return false
        if (color != other.color) return false
        if (covers != other.covers) return false
        if (business != other.business) return false
        if (type != other.type) return false
        if (price != other.price) return false
        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (rewardTitle?.hashCode() ?: 0)
        result = 31 * result + (reward?.hashCode() ?: 0)
        result = 31 * result + (order ?: 0)
        result = 31 * result + (color?.hashCode() ?: 0)
        result = 31 * result + (covers?.hashCode() ?: 0)
        result = 31 * result + (business?.hashCode() ?: 0)
        result = 31 * result + (type?.hashCode() ?: 0)
        result = 31 * result + (price?.hashCode() ?: 0)
        return result
    }
}