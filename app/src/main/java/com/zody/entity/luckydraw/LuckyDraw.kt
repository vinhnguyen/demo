package com.zody.entity.luckydraw

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by vinhnguyen.it.vn on 2018, November 30
 */
class LuckyDraw(@SerializedName("_id")
                val id: String,
                @SerializedName("desc")
                val desc: String?,
                @SerializedName("name")
                val name: String?,
                @SerializedName("startAt")
                val start: Date?,
                @SerializedName("endAt")
                val end: Date?,
                @SerializedName("cover")
                val cover: String?,
                @SerializedName("introductionLink")
                val introductionLink: String?,
                @SerializedName("prizes")
                val prizes: LuckyDrawPrizes?,
                @SerializedName("shareBonus")
                val shareBonus: LuckyDrawShareBonus?,
                @SerializedName("type")
                val type: String?,
                @SerializedName("haveBallot")
                val haveBallot: Boolean?,
                @SerializedName("haveLeaderBoard")
                val haveLeaderBoard: Boolean?,
                @SerializedName("howToGetTurns")
                val guide: List<LuckyDrawGuide>?) {

    val hasSupport: Boolean
        get() = type == TYPE_LUCKY_DRAW || type == TYPE_LUCKY_MONEY

    companion object {
        const val TYPE_LUCKY_DRAW = "luckyDraw"
        const val TYPE_LUCKY_MONEY = "luckyMoney"
    }
}