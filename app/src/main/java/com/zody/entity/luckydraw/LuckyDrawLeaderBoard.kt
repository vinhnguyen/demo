package com.zody.entity.luckydraw

import com.google.gson.annotations.SerializedName
import com.zody.entity.UserCompat

/**
 * Created by vinhnguyen.it.vn on 2019, January 29
 */
class LuckyDrawLeaderBoard(@SerializedName("_id")
                           val id: String,
                           @SerializedName("count")
                           val count: Int?,
                           @SerializedName("user")
                           val user: UserCompat?)