package com.zody.entity.luckydraw

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by vinhnguyen.it.vn on 2018, December 07
 */
class LuckyDrawBallot(@SerializedName("_id")
                      val id: String,
                      @SerializedName("code")
                      val code: String?,
                      @SerializedName("createdAt")
                      val created: Date?) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is LuckyDrawBallot) return false

        if (id != other.id) return false
        if (code != other.code) return false
        if (created != other.created) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (code?.hashCode() ?: 0)
        result = 31 * result + (created?.hashCode() ?: 0)
        return result
    }
}