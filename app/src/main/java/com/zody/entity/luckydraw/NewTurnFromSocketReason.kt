package com.zody.entity.luckydraw

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2019, January 29
 */
class NewTurnFromSocketReason(@SerializedName("title")
                              val title: String?,
                              @SerializedName("message")
                              val message: String?)