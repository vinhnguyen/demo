package com.zody.entity.luckydraw

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, November 30
 */
class LuckyDrawShareBonus(@SerializedName("type")
                          val type: String?,
                          @SerializedName("value")
                          val value: Int?,
                          @SerializedName("message")
                          val message: String?,
                          @SerializedName("link")
                          val link: String?) {

    val canShare: Boolean
        get() = type == TYPE_TURN && link?.isNotEmpty() == true

    companion object {
        const val TYPE_TURN = "turn"
    }
}