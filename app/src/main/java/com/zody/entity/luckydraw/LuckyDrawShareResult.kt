package com.zody.entity.luckydraw

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2019, January 30
 */
class LuckyDrawShareResult(@SerializedName("value")
                           val value: Int?,
                           @SerializedName("reason")
                           val reason: String?)