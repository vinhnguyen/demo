package com.zody.entity.luckydraw

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2019, January 30
 */
class LuckyDrawGuide(@SerializedName("title")
                     val title: String?,
                     @SerializedName("desc")
                     val desc: String?,
                     @SerializedName("type")
                     val type: String?,
                     @SerializedName("value")
                     val value: Int?,
                     @SerializedName("extraData")
                     val extraData: String?) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeString(desc)
        parcel.writeString(type)
        parcel.writeValue(value)
        parcel.writeString(extraData)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object {
        const val TYPE_INVITE = "invite"

        @JvmField
        val CREATOR = object : Parcelable.Creator<LuckyDrawGuide> {
            override fun createFromParcel(parcel: Parcel): LuckyDrawGuide {
                return LuckyDrawGuide(parcel)
            }

            override fun newArray(size: Int): Array<LuckyDrawGuide?> {
                return arrayOfNulls(size)
            }
        }
    }
}