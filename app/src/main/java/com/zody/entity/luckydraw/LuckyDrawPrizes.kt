package com.zody.entity.luckydraw

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, November 30
 */
class LuckyDrawPrizes(@SerializedName("totalPrize")
                      val totalPrize: Int?,
                      @SerializedName("totalItems")
                      val totalItems: Int?,
                      @SerializedName("data")
                      val data: List<LuckyDrawPrize>?)