package com.zody.entity.luckydraw

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, December 07
 */
class LuckyDrawResult(@SerializedName("prize")
                      val prize: LuckyDrawPrize?,
                      @SerializedName("ballotData")
                      val ballot: LuckyDrawBallot?)