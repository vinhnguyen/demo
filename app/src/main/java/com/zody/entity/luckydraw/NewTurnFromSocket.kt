package com.zody.entity.luckydraw

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2019, January 29
 */
class NewTurnFromSocket(@SerializedName("turns")
                        val turns: Int?,
                        @SerializedName("reason")
                        val reason: NewTurnFromSocketReason?,
                        @SerializedName("luckyDraw")
                        val luckyDraw: LuckyDraw?)