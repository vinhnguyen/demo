package com.zody.entity.luckydraw

import com.google.gson.annotations.SerializedName
import com.zody.entity.UserCompat
import java.util.*

/**
 * Created by vinhnguyen.it.vn on 2018, December 03
 */
class LuckyDrawWinner(@SerializedName("_id")
                      val id: String,
                      @SerializedName("prize")
                      val prize: String?,
                      @SerializedName("drawTime")
                      val time: Date?,
                      @SerializedName("user")
                      val user: UserCompat?)