package com.zody.entity.voucher

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by vinhnguyen.it.vn on 2019, January 30
 */
class VoucherBanner(@SerializedName("_id")
                    val id: String,
                    @SerializedName("cover")
                    val cover: String?,
                    @SerializedName("linkTo")
                    val linkTo: String?,
                    @SerializedName("linkValue")
                    val linkValue: String?,
                    @SerializedName("order")
                    val order: String?,
                    @SerializedName("startAt")
                    val start: Date?,
                    @SerializedName("endAt")
                    val end: Date?) {
    companion object {
        const val LINK_TO_VOUCHER = "voucher"
        const val LINK_TO_CATEGORY = "voucherCategory"
    }
}