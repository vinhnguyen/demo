package com.zody.entity.voucher

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by vinhnguyen.it.vn on 2019, January 30
 */
class Voucher(@SerializedName("_id")
              val id: String,
              @SerializedName("name")
              val name: String?,
              @SerializedName("coin")
              val coin: Int?,
              @SerializedName("discountPercent")
              val discountPercent: Double?,
              @SerializedName("rest")
              val rest: Int?,
              @SerializedName("isHot")
              val isHot: Boolean?,
              @SerializedName("desc")
              val desc: String?,
              @SerializedName("covers")
              val covers: List<String>?,
              @SerializedName("canExchangeAfterDate")
              val canExchangeAfterDate: Date?) {

    val cover: String?
        get() = covers?.firstOrNull()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Voucher) return false

        if (id != other.id) return false
        if (name != other.name) return false
        if (coin != other.coin) return false
        if (discountPercent != other.discountPercent) return false
        if (rest != other.rest) return false
        if (isHot != other.isHot) return false
        if (desc != other.desc) return false
        if (covers != other.covers) return false
        if (canExchangeAfterDate != other.canExchangeAfterDate) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + (coin ?: 0)
        result = 31 * result + (discountPercent?.hashCode() ?: 0)
        result = 31 * result + (rest ?: 0)
        result = 31 * result + (isHot?.hashCode() ?: 0)
        result = 31 * result + (desc?.hashCode() ?: 0)
        result = 31 * result + (covers?.hashCode() ?: 0)
        result = 31 * result + (canExchangeAfterDate?.hashCode() ?: 0)
        return result
    }
}