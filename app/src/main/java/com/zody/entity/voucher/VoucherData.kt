package com.zody.entity.voucher

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2019, January 30
 */
class VoucherData(@SerializedName("banners")
                  val banners: List<VoucherBanner>?,
                  @SerializedName("categories")
                  val categories: List<VoucherCategory>?,
                  @SerializedName("list")
                  val list: List<VoucherList>?)