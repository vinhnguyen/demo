package com.zody.entity.voucher

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2019, January 30
 */
class VoucherList(@SerializedName("_id")
                  val id: String,
                  @SerializedName("name")
                  val name: String?,
                  @SerializedName("order")
                  val order: Int?,
                  @SerializedName("hasLoadMore")
                  var hasLoadMore: Boolean? = null,
                  @SerializedName("type")
                  var type: String? = null,
                  @SerializedName("vouchers")
                  var vouchers: List<Voucher>? = null) {

    val hasSupport: Boolean
        get() = type in arrayOf(TYPE_CATEGORY, TYPE_NEW_UPDATE, TYPE_YOU_MY_LIKE)

    companion object {
        const val TYPE_CATEGORY = "category"
        const val TYPE_YOU_MY_LIKE = "youMayLike"
        const val TYPE_NEW_UPDATE = "newUpdate"
    }
}