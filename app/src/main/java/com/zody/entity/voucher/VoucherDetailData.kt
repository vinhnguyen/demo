package com.zody.entity.voucher

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2019, February 15
 */
class VoucherDetailData(@SerializedName("_id")
                        val id: String,
                        @SerializedName("name")
                        val name: String?,
                        @SerializedName("showListVoucherGroup")
                        val showListVoucherGroup: Boolean?,
                        @SerializedName("group")
                        val vouchers: List<Voucher>?)