package com.zody.entity.voucher

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2019, January 30
 */
@Entity(tableName = "voucher_category", primaryKeys = ["voucher_category_id"])
class VoucherCategory(@SerializedName("_id")
                      @ColumnInfo(name = "voucher_category_id")
                      val id: String,
                      @SerializedName("name")
                      @ColumnInfo(name = "voucher_category_name")
                      val name: String?,
                      @SerializedName("order")
                      @ColumnInfo(name = "voucher_category_order")
                      val order: Int?) 