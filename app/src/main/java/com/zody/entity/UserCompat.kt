package com.zody.entity

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, August 08
 */
class UserCompat(@SerializedName("_id")
                 @ColumnInfo(name = "user_id")
                 val id: String,
                 @SerializedName("name")
                 @ColumnInfo(name = "user_name")
                 val name: String?,
                 @SerializedName("avatar")
                 @ColumnInfo(name = "user_avatar")
                 val avatar: String?) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is UserCompat) return false

        if (id != other.id) return false
        if (name != other.name) return false
        if (avatar != other.avatar) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + (avatar?.hashCode() ?: 0)
        return result
    }
}