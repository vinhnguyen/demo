package com.zody.entity

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, December 17
 */
class ScanQrCodeSummary(@SerializedName("bill")
                        val bill: Int?,
                        @SerializedName("other")
                        val other: Int?,
                        @SerializedName("luckyDrawTurns")
                        val luckyDrawTurns: Int?)