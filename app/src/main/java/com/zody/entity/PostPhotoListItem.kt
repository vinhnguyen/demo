package com.zody.entity

/**
 * Created by vinhnguyen.it.vn on 2018, July 30
 */
class PostPhotoListItem(val photo: Photo, data: Post, next: String?, order: Int, type: Int = 0) : ListItem<Post>(data, next, order, type)