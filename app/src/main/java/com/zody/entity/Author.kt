package com.zody.entity

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, July 18
 */
@Entity(tableName = "authors", primaryKeys = ["author_id"])
class Author(@SerializedName("_id")
             @ColumnInfo(name = "author_id")
             val id: String,
             @SerializedName("name")
             @ColumnInfo(name = "author_name")
             val name: String?,
             @SerializedName("avatar")
             @ColumnInfo(name = "author_avatar")
             val avatar: String?,
             @SerializedName("isFollowed")
             @ColumnInfo(name = "author_followed")
             val isFollowed: Boolean?,
             @SerializedName("isLocalExpert")
             @ColumnInfo(name = "author_is_expert")
             val isExpert: Boolean?,
             @SerializedName("statistic")
             @Embedded
             val statistic: AuthorStatistic?,
             @SerializedName("nickname")
             @ColumnInfo(name = "author_nickname")
             val nickname: String?,
             @SerializedName("desc")
             @ColumnInfo(name = "author_desc")
             val desc: String?) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Author) return false

        if (id != other.id) return false
        if (name != other.name) return false
        if (avatar != other.avatar) return false
        if (isFollowed != other.isFollowed) return false
        if (isExpert != other.isExpert) return false
        if (statistic != other.statistic) return false
        if (nickname != other.nickname) return false
        if (desc != other.desc) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + (avatar?.hashCode() ?: 0)
        result = 31 * result + (isFollowed?.hashCode() ?: 0)
        result = 31 * result + (isExpert?.hashCode() ?: 0)
        result = 31 * result + (statistic?.hashCode() ?: 0)
        result = 31 * result + (nickname?.hashCode() ?: 0)
        result = 31 * result + (desc?.hashCode() ?: 0)
        return result
    }
}