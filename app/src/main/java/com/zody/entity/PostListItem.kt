package com.zody.entity

/**
 * Created by vinhnguyen.it.vn on 2018, July 24
 */
class PostListItem {

    companion object {
        const val TYPE_AUTHOR_AND_TIME = 11
        const val TYPE_BUSINESS_AND_POINT = 12
        const val TYPE_FEEDBACK = 13
        const val TYPE_PHOTO_ONE = 14
        const val TYPE_PHOTO_TWO_SPECIAL = 15
        const val TYPE_PHOTO_MULTI_HORIZONTAL = 16
        const val TYPE_PHOTO_MULTI_VERTICAL = 17
        const val TYPE_PHOTO_FIVE_AND_OVER = 18
        const val TYPE_PHOTO_MULTI = 19
        const val TYPE_STATISTIC = 20
        const val TYPE_ACTION = 21
    }
}