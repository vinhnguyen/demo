package com.zody.entity

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, July 18
 */
class UserStatistic {
    @SerializedName("coin")
    @ColumnInfo(name = "coin")
    var coin: Int? = null
    @SerializedName("bills")
    @ColumnInfo(name = "bills")
    var bills: Int? = null
    @SerializedName("views")
    @ColumnInfo(name = "views")
    var views: Int? = null
    @SerializedName("reviews")
    @ColumnInfo(name = "reviews")
    var reviews: Int? = null
    @SerializedName("tips")
    @ColumnInfo(name = "tips")
    var tips: Int? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is UserStatistic) return false

        if (coin != other.coin) return false
        if (bills != other.bills) return false
        if (views != other.views) return false
        if (reviews != other.reviews) return false
        if (tips != other.tips) return false

        return true
    }

    override fun hashCode(): Int {
        var result = coin ?: 0
        result = 31 * result + (bills ?: 0)
        result = 31 * result + (views ?: 0)
        result = 31 * result + (reviews ?: 0)
        result = 31 * result + (tips ?: 0)
        return result
    }


}