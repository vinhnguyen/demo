package com.zody.entity

import androidx.room.Embedded

/**
 * Created by vinhnguyen.it.vn on 2018, July 25
 */
data class PostQueryItem(@Embedded val item: PostItem?, @Embedded val post: Post?, @Embedded val author: Author?) {
    init {
        post?.author = author
    }
}