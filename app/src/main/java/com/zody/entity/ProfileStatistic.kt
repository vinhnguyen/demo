package com.zody.entity

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, July 11
 */
class ProfileStatistic(@SerializedName("followers")
                       @ColumnInfo(name = "statistic_followers")
                       val followers: Int?,
                       @SerializedName("followings")
                       @ColumnInfo(name = "statistic_followings")
                       val followings: Int?,
                       @SerializedName("reviews")
                       @ColumnInfo(name = "statistic_reviews")
                       val reviews: Int?,
                       @SerializedName("coin")
                       @ColumnInfo(name = "statistic_coin")
                       val coin: Int?,
                       @SerializedName("expense")
                       @ColumnInfo(name = "statistic_expense")
                       val expense: Long?,
                       @SerializedName("reward")
                       @ColumnInfo(name = "statistic_reward")
                       val reward: Int?)