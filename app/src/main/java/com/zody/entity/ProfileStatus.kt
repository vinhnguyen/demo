package com.zody.entity

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, August 14
 */
class ProfileStatus(@SerializedName("verified")
                    @ColumnInfo(name = "status_verified")
                    val verified: Boolean?)