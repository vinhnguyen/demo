package com.zody.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by vinhnguyen.it.vn on 2018, July 18
 */
@Entity(tableName = "current_rank")
class LeaderBoardCurrentRank(@ColumnInfo(name = "views") val views: Int?,
                             @ColumnInfo(name = "tips") val tips: Int?,
                             @ColumnInfo(name = "coins") val coins: Int?) {
    @PrimaryKey
    @ColumnInfo(name = "id")
    var id: Int = 1
}