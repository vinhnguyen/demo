package com.zody.entity

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, July 27
 */
class CommentStatistic {
    @SerializedName("likes")
    @ColumnInfo(name = "comment_like")
    var likes: Int? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is CommentStatistic) return false

        if (likes != other.likes) return false

        return true
    }

    override fun hashCode(): Int {
        return likes ?: 0
    }


}