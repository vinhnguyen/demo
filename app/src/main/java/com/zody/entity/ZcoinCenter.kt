package com.zody.entity

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, August 14
 */
class ZcoinCenter(@SerializedName("url")
                  val link: String?,
                  @SerializedName("items")
                  var items: List<ZcoinCenterItem>?)