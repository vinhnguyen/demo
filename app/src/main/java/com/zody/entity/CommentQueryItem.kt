package com.zody.entity

import androidx.room.Embedded

/**
 * Created by vinhnguyen.it.vn on 2018, July 25
 */
data class CommentQueryItem(@Embedded val item: CommentItem, @Embedded val comment: Comment)