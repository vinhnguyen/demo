package com.zody.entity

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, July 13
 */
class BusinessCategory {
    @SerializedName("_id")
    lateinit var id: String
    @SerializedName("name")
    var name: String? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is BusinessCategory) return false

        if (id != other.id) return false
        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (name?.hashCode() ?: 0)
        return result
    }


}