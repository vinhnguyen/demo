package com.zody.entity

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by vinhnguyen.it.vn on 2018, August 08
 */
@Entity(tableName = "notifications", primaryKeys = ["notification_id"])
class Notification(@SerializedName("_id")
                   @ColumnInfo(name = "notification_id")
                   val id: String,
                   @SerializedName("type")
                   @ColumnInfo(name = "notification_type")
                   val type: String?,
                   @SerializedName("user")
                   @Embedded
                   val user: UserCompat?,
                   @SerializedName("timestamp")
                   @ColumnInfo(name = "notification_created")
                   val created: Date?,
                   @SerializedName("text")
                   @ColumnInfo(name = "notification_content")
                   val content: String?,
                   @SerializedName("isRead")
                   @ColumnInfo(name = "notification_read")
                   var read: Boolean?,
                   @SerializedName("target")
                   @Embedded
                   val target: NotificationTarget?,
                   @SerializedName("from")
                   @Embedded(prefix = "notification_from_")
                   val from: UserCompat) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Notification) return false

        if (id != other.id) return false
        if (type != other.type) return false
        if (user != other.user) return false
        if (created != other.created) return false
        if (content != other.content) return false
        if (read != other.read) return false
        if (target != other.target) return false
        if (from != other.from) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (type?.hashCode() ?: 0)
        result = 31 * result + (user?.hashCode() ?: 0)
        result = 31 * result + (created?.hashCode() ?: 0)
        result = 31 * result + (content?.hashCode() ?: 0)
        result = 31 * result + (read?.hashCode() ?: 0)
        result = 31 * result + (target?.hashCode() ?: 0)
        result = 31 * result + from.hashCode()
        return result
    }
}