package com.zody.entity

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, August 16
 */
class LocalExpert(@SerializedName("lastReview")
                  val post: Post?)