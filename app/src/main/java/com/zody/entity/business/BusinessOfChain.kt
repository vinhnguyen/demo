package com.zody.entity.business

import com.google.gson.annotations.SerializedName
import com.zody.entity.BusinessCompat

/**
 * Created by vinhnguyen.it.vn on 2018, September 07
 */
class BusinessOfChain(@SerializedName("cover")
                      val cover: String?,
                      @SerializedName("name")
                      val name: String?,
                      @SerializedName("businesses")
                      val businesses: List<BusinessCompat>)