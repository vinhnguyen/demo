package com.zody.entity.business

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, August 12
 */
class BusinessTopUsers(
        @SerializedName("me")
        val me: BusinessTopUser?,
        @SerializedName("list")
        val data: List<BusinessTopUser>?)