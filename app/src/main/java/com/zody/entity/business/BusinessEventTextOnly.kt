package com.zody.entity.business

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by vinhnguyen.it.vn on 2018, August 12
 */
class BusinessEventTextOnly(
        @SerializedName("_id")
        val id: String,
        @SerializedName("desc")
        val desc: String?,
        @SerializedName("beginTime")
        val begin: Date,
        @SerializedName("endTime")
        val end: Date,
        @SerializedName("title")
        val title: String,
        @SerializedName("shortDesc")
        val shortDesc: String?) : BusinessEvent