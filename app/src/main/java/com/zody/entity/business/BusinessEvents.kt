package com.zody.entity.business

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, August 12
 */
class BusinessEvents(
        @SerializedName("firstBill")
        val firstBill: BusinessEventFirstBill?,
        @SerializedName("coinEachBill")
        val coinEachBill: BusinessEventCoinEachBill?,
        @SerializedName("billStack")
        val billStack: List<BusinessEventBillStack>?,
        @SerializedName("happyHours")
        val happyHour: BusinessEventHappyHour?,
        @SerializedName("textOnly")
        val textOnly: List<BusinessEventTextOnly>?) {

    val fromZody: List<BusinessEvent>
        get() = mutableListOf<BusinessEvent>().apply {
            if (firstBill != null) add(firstBill)
            if (coinEachBill != null) add(coinEachBill)
        }

    val fromMerchant: List<BusinessEvent>
        get() = mutableListOf<BusinessEvent>().apply {
            if (happyHour != null) add(happyHour)
            textOnly?.forEach {
                add(it)
            }
        }

}