package com.zody.entity.business

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by vinhnguyen.it.vn on 2018, August 12
 */
class BusinessEventPromotion(
        @SerializedName("_id")
        val id: String,
        @SerializedName("text")
        val text: String,
        @SerializedName("content")
        val content: String,
        @SerializedName("endTime")
        val end: Date) : BusinessEvent