package com.zody.entity.business

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, August 12
 */
class BusinessEventHappyHourTime(
        @SerializedName("startHour")
        val startHour: String?,
        @SerializedName("startMinute")
        val startMinute: String?,
        @SerializedName("endHour")
        val endHour: String?,
        @SerializedName("endMinute")
        val endMinute: String?) {

    override fun toString(): String {
        return String.format("%s:%s - %s:%s", startHour, startMinute, endHour, endMinute)
    }
}
