package com.zody.entity.business

import com.google.gson.annotations.SerializedName
import com.zody.entity.BusinessCategory
import com.zody.entity.BusinessList
import com.zody.entity.Coordinates

/**
 * Created by vinhnguyen.it.vn on 2018, August 10
 */
class Business(id: String,
               name: String?,
               address: String?,
               distance: Double?,
               rating: BusinessRating?,
               categories: List<BusinessCategory>?,

               @SerializedName("email")
               val email: String?,
               @SerializedName("phone")
               val phone: String?,
               @SerializedName("socialUrls")
               val social: BusinessSocial?,
               @SerializedName("isOpening")
               val opening: Boolean?,
               @SerializedName("workingHours")
               val hours: List<BusinessHour>?,
               @SerializedName("location")
               val location: Coordinates?,
               @SerializedName("hasMenu")
               val hasMenu: Boolean?,
               @SerializedName("shareURL")
               val shareLink: String?,
               @SerializedName("desc")
               val desc: String?,
               @SerializedName("events")
               val events: BusinessEvents?,
               @SerializedName("topMembers")
               val topUsers: BusinessTopUsers?,
               @SerializedName("myStatistic")
               val statistic: BusinessUserStatistic?,
               @SerializedName("hasScanPrinterQRCode")
               val hasScanPrinterQRCode: Boolean?) : BusinessList(id, name, address, distance, null, rating, categories) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Business) return false
        if (!super.equals(other)) return false

        if (email != other.email) return false
        if (phone != other.phone) return false
        if (social != other.social) return false
        if (opening != other.opening) return false
        if (hours != other.hours) return false
        if (location != other.location) return false
        if (hasMenu != other.hasMenu) return false
        if (shareLink != other.shareLink) return false
        if (desc != other.desc) return false
        if (events != other.events) return false
        if (topUsers != other.topUsers) return false
        if (statistic != other.statistic) return false
        if (hasScanPrinterQRCode != other.hasScanPrinterQRCode) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + (email?.hashCode() ?: 0)
        result = 31 * result + (phone?.hashCode() ?: 0)
        result = 31 * result + (social?.hashCode() ?: 0)
        result = 31 * result + (opening?.hashCode() ?: 0)
        result = 31 * result + (hours?.hashCode() ?: 0)
        result = 31 * result + (location?.hashCode() ?: 0)
        result = 31 * result + (hasMenu?.hashCode() ?: 0)
        result = 31 * result + (shareLink?.hashCode() ?: 0)
        result = 31 * result + (desc?.hashCode() ?: 0)
        result = 31 * result + (events?.hashCode() ?: 0)
        result = 31 * result + (topUsers?.hashCode() ?: 0)
        result = 31 * result + (statistic?.hashCode() ?: 0)
        result = 31 * result + (hasScanPrinterQRCode?.hashCode() ?: 0)
        return result
    }
}