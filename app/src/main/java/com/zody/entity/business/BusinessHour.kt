package com.zody.entity.business

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, August 10
 */
class BusinessHour(
        @SerializedName("openHour")
        val openHour: Int?,
        @SerializedName("openMinute")
        val openMinute: Int?,
        @SerializedName("closeHour")
        val closeHour: Int?,
        @SerializedName("closeMinute")
        val closeMinute: Int?
) {
    override fun toString(): String {
        return String.format("%1$02d:%2$02d - %3$02d:%4$02d", openHour, openMinute, closeHour, closeMinute)
    }
}