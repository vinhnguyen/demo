package com.zody.entity.business

import androidx.room.ColumnInfo
import androidx.room.Ignore
import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, September 04
 */
open class BusinessChainCompat(@SerializedName("_id")
                          @ColumnInfo(name = "chain_id")
                          val id: String,
                          @SerializedName("name")
                          @ColumnInfo(name = "chain_name")
                          val name: String?,
                          @SerializedName("totalBusinessInChain")
                          @ColumnInfo(name = "chain_total_business")
                          val totalBusiness: Int?,
                          @SerializedName("average")
                          @ColumnInfo(name = "chain_average_rate")
                          val averageRate: Double?,
                          @SerializedName("totalRatings")
                          @ColumnInfo(name = "chain_total_rate")
                          val totalRate: Int?) {

    @SerializedName("cover")
    @ColumnInfo(name = "chain_cover")
    var cover: String? = null
        get() = field ?: covers?.firstOrNull()

    @SerializedName("covers")
    @Ignore
    var covers: List<String>? = null


    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is BusinessChainCompat) return false

        if (id != other.id) return false
        if (name != other.name) return false
        if (totalBusiness != other.totalBusiness) return false
        if (averageRate != other.averageRate) return false
        if (totalRate != other.totalRate) return false
        if (cover != other.cover) return false
        if (covers != other.covers) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + (totalBusiness ?: 0)
        result = 31 * result + (averageRate?.hashCode() ?: 0)
        result = 31 * result + (totalRate ?: 0)
        result = 31 * result + (cover?.hashCode() ?: 0)
        result = 31 * result + (covers?.hashCode() ?: 0)
        return result
    }
}