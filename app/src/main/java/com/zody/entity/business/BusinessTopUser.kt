package com.zody.entity.business

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, August 12
 */
class BusinessTopUser(
        @SerializedName("_id")
        val id: String,
        @SerializedName("name")
        val name: String?,
        @SerializedName("avatar")
        val avatar: String?,
        @SerializedName("statistic")
        val statistic: BusinessTopUserStatistic?) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is BusinessTopUser) return false

        if (id != other.id) return false
        if (name != other.name) return false
        if (avatar != other.avatar) return false
        if (statistic != other.statistic) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + (avatar?.hashCode() ?: 0)
        result = 31 * result + (statistic?.hashCode() ?: 0)
        return result
    }
}