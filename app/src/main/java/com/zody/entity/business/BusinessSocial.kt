package com.zody.entity.business

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, August 10
 */
class BusinessSocial(
        @SerializedName("website")
        val website: String?,
        @SerializedName("facebook")
        val facebook: String?,
        @SerializedName("instagram")
        val instagram: String?
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is BusinessSocial) return false

        if (website != other.website) return false
        if (facebook != other.facebook) return false
        if (instagram != other.instagram) return false

        return true
    }

    override fun hashCode(): Int {
        var result = website?.hashCode() ?: 0
        result = 31 * result + (facebook?.hashCode() ?: 0)
        result = 31 * result + (instagram?.hashCode() ?: 0)
        return result
    }
}