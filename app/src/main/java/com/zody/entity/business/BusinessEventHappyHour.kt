package com.zody.entity.business

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, August 12
 */
class BusinessEventHappyHour(
        @SerializedName("_id")
        val id: String,
        @SerializedName("days")
        val days: Array<Int>?,
        @SerializedName("multiplier")
        val multiplier: Double?,
        @SerializedName("desc")
        val desc: String?,
        @SerializedName("hours")
        val hours: List<BusinessEventHappyHourTime>?) : BusinessEvent