package com.zody.entity.business

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, August 12
 */
class BusinessEventBillStack(
        @SerializedName("_id")
        val id: String,
        @SerializedName("amount")
        val amount: Long?,
        @SerializedName("coin")
        val coin: Int?,
        @SerializedName("bonusDiscount")
        val bonusDiscount: Double?,
        @SerializedName("desc")
        val desc: String?) : BusinessEvent