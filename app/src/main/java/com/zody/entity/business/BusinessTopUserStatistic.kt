package com.zody.entity.business

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, September 19
 */
class BusinessTopUserStatistic(@SerializedName("bill")
                               val bill: Int?,
                               @SerializedName("coin")
                               val coin: Int?) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is BusinessTopUserStatistic) return false

        if (bill != other.bill) return false
        if (coin != other.coin) return false

        return true
    }

    override fun hashCode(): Int {
        var result = bill ?: 0
        result = 31 * result + (coin ?: 0)
        return result
    }
}