package com.zody.entity.business

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, August 12
 */
class BusinessEventFirstBill(
        @SerializedName("_id")
        val id: String,
        @SerializedName("discount")
        val discount: Double?,
        @SerializedName("desc")
        val desc: String?) : BusinessEvent