package com.zody.entity.business

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, August 12
 */
class BusinessLastRate(
        @SerializedName("_id")
        val id: String,
        @SerializedName("point")
        val point: Int?,
        @SerializedName("feedback")
        val feedback: String?)