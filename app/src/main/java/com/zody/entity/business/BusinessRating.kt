package com.zody.entity.business

import com.google.gson.JsonDeserializer
import timber.log.Timber

/**
 * Created by vinhnguyen.it.vn on 2018, August 13
 */
class BusinessRating(val average: Double?,
                     val times: Int?,
                     val countByPoint: Map<Int, Int>?) {

    val maxCount: Int = countByPoint?.maxBy { it.value }?.value ?: 0

    companion object {
        val deserializer = JsonDeserializer { json, _, _ ->
            try {
                json.asJsonObject.apply {
                    return@JsonDeserializer BusinessRating(average = get("average")?.asDouble,
                            times = get("times")?.asInt,
                            countByPoint = get("detail")?.asJsonArray?.mapNotNull {
                                val point = it.asJsonObject.get("point")?.asInt
                                val total = it.asJsonObject.get("total")?.asInt
                                if (point != null && total != null) {
                                    point to total
                                } else {
                                    null
                                }
                            }?.toMap())
                }
            } catch (e: Exception) {
                Timber.tag("JsonException").e(e)
                return@JsonDeserializer null
            }

        }
    }
}