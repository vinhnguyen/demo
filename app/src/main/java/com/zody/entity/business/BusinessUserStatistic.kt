package com.zody.entity.business

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, August 23
 */
class BusinessUserStatistic(@SerializedName("expense")
                            val expense: Long?,
                            @SerializedName("lastReview")
                            val lastReview: BusinessLastRate?)