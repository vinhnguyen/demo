package com.zody.entity

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Ignore
import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, August 12
 */
open class BusinessCompat(@SerializedName("_id")
                          @ColumnInfo(name = "business_id")
                          val id: String,
                          @SerializedName("name")
                          @ColumnInfo(name = "business_name")
                          val name: String?,
                          @SerializedName("address")
                          @ColumnInfo(name = "business_address")
                          val address: String?,
                          @SerializedName("distance")
                          @ColumnInfo(name = "business_distance")
                          val distance: Double?) : Parcelable {

    @SerializedName("cover")
    @ColumnInfo(name = "business_cover")
    var cover: String? = null
        get() = field ?: covers?.firstOrNull()

    @SerializedName("covers")
    @Ignore
    var covers: List<String>? = null

    @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Double::class.java.classLoader) as? Double) {
        cover = parcel.readString()
        covers = parcel.createStringArrayList()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(address)
        parcel.writeValue(distance)
        parcel.writeString(cover)
        parcel.writeStringList(covers)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is BusinessCompat) return false

        if (id != other.id) return false
        if (name != other.name) return false
        if (address != other.address) return false
        if (distance != other.distance) return false
        if (covers != other.covers) return false
        if (cover != other.cover) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + (address?.hashCode() ?: 0)
        result = 31 * result + (distance?.hashCode() ?: 0)
        result = 31 * result + (covers?.hashCode() ?: 0)
        result = 31 * result + (cover?.hashCode() ?: 0)
        return result
    }

    companion object CREATOR : Parcelable.Creator<BusinessCompat> {
        override fun createFromParcel(parcel: Parcel): BusinessCompat {
            return BusinessCompat(parcel)
        }

        override fun newArray(size: Int): Array<BusinessCompat?> {
            return arrayOfNulls(size)
        }
    }
}