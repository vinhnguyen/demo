package com.zody.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, August 14
 */
@Entity(tableName = "zcoin_center_items", primaryKeys = ["zcoin_center_item_id"])
class ZcoinCenterItem(@SerializedName("_id")
                      @ColumnInfo(name = "zcoin_center_item_id")
                      val id: String,
                      @SerializedName("icon")
                      @ColumnInfo(name = "zcoin_center_item_icon")
                      val icon: String?,
                      @SerializedName("title")
                      @ColumnInfo(name = "zcoin_center_item_title")
                      val title: String?,
                      @SerializedName("type")
                      @ColumnInfo(name = "zcoin_center_item_type")
                      val type: String?,
                      @SerializedName("order")
                      @ColumnInfo(name = "zcoin_center_item_order")
                      val order: Int?,
                      @SerializedName("content")
                      @ColumnInfo(name = "zcoin_center_item_content")
                      val content: String?) {

    @Transient
    @ColumnInfo(name = "zcoin_center_item_link")
    var link: String? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ZcoinCenterItem) return false

        if (id != other.id) return false
        if (icon != other.icon) return false
        if (title != other.title) return false
        if (type != other.type) return false
        if (order != other.order) return false
        if (content != other.content) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (icon?.hashCode() ?: 0)
        result = 31 * result + (title?.hashCode() ?: 0)
        result = 31 * result + (type?.hashCode() ?: 0)
        result = 31 * result + (order ?: 0)
        result = 31 * result + (content?.hashCode() ?: 0)
        return result
    }
}