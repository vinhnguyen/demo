package com.zody.entity

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, October 30
 */
class ScanQrCodeResult(@SerializedName("coin")
                       val coin: Int?,
                       @SerializedName("price")
                       val price: Double?,
                       @SerializedName("business")
                       val business: BusinessCompat?,
                       @SerializedName("summary")
                       val summary: ScanQrCodeSummary?,
                       @SerializedName("budweiserItems")
                       val budweiserItems: List<BudweiserItem>?) {

    val total: Int
        get() = (summary?.bill ?: 0) +
                (summary?.other ?: 0) +
                (budweiserItems?.sumBy {
                    it.coin ?: 0
                } ?: 0)
}