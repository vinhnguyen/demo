package com.zody.entity

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by vinhnguyen.it.vn on 2018, July 10
 */
@Entity(tableName = "my_profile", primaryKeys = ["user_id"])
class Profile(@SerializedName("_id")
              @ColumnInfo(name = "user_id")
              val id: String,
              @SerializedName("name")
              @ColumnInfo(name = "user_name")
              val name: String?,
              @SerializedName("avatar")
              @ColumnInfo(name = "user_avatar")
              val avatar: String?,
              @SerializedName("city")
              @ColumnInfo(name = "user_city")
              val city: String?,
              @SerializedName("gender")
              @ColumnInfo(name = "user_gender")
              val gender: String?,
              @SerializedName("phone")
              @ColumnInfo(name = "user_phone")
              val phone: String?,
              @SerializedName("birthday")
              @ColumnInfo(name = "user_birthday")
              val birthday: Date?,
              @SerializedName("isLocalExpert")
              @ColumnInfo(name = "user_is_expert")
              val isExpert: Boolean?,
              @SerializedName("statistic")
              @Embedded(prefix = "user_")
              val statistic: ProfileStatistic?,
              @SerializedName("referral")
              @Embedded(prefix = "user_")
              val referral: ProfileReferral?,
              @SerializedName("statuses")
              @Embedded(prefix = "user_")
              val statuses: ProfileStatus?,
              @SerializedName("nickname")
              @ColumnInfo(name = "user_nickname")
              val nickname: String?,
              @SerializedName("desc")
              @ColumnInfo(name = "user_desc")
              val desc: String?,
              @SerializedName("isForceUpdateData")
              @ColumnInfo(name = "user_force_update_data")
              var isForceUpdateData: Boolean?,
              @SerializedName("facebook")
              @Embedded(prefix = "user_")
              val facebook: ProfileFacebook?,

              @ColumnInfo(name = "user_is_new")
              var isNew: Boolean?) {

    companion object {
        const val GENDER_MALE = "male"
        const val GENDER_FEMALE = "female"
    }
}