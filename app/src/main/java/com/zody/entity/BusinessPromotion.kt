package com.zody.entity

import com.google.gson.annotations.SerializedName
import com.zody.entity.business.BusinessRating

/**
 * Created by vinhnguyen.it.vn on 2018, November 21
 */
class BusinessPromotion(id: String,
                        name: String?,
                        address: String?,
                        distance: Double?,
                        chain: String?,
                        rating: BusinessRating?,
                        categories: List<BusinessCategory>?) : BusinessList(id, name, address, distance, chain, rating, categories) {

    @SerializedName("promotionText")
    var promotionText: String? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is BusinessPromotion) return false
        if (!super.equals(other)) return false

        if (promotionText != other.promotionText) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + (promotionText?.hashCode() ?: 0)
        return result
    }


}