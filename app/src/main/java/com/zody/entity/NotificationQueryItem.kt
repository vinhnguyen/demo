package com.zody.entity

import androidx.room.ColumnInfo
import androidx.room.Embedded

/**
 * Created by vinhnguyen.it.vn on 2018, August 08
 */
data class NotificationQueryItem(
        @Embedded
        val data: Notification,
        @ColumnInfo(name = "page_next")
        val next: String?,
        @ColumnInfo(name = "item_order")
        val order: Int)