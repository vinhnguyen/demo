package com.zody.entity

import androidx.room.ColumnInfo
import androidx.room.Entity

/**
 * Created by vinhnguyen.it.vn on 2018, July 24
 */
@Entity(tableName = "post_items", primaryKeys = ["post_filter", "ref_post_id", "post_sort"])
class PostItem(
        @ColumnInfo(name = "ref_post_id")
        val postId: String,
        @ColumnInfo(name = "ref_author_id")
        val authorId: String?,
        @ColumnInfo(name = "post_filter")
        val filter: String,
        @ColumnInfo(name = "post_sort")
        val sort: String,
        @ColumnInfo(name = "post_next")
        val next: String?,
        @ColumnInfo(name = "post_order")
        val order: Int) {

    companion object {
        const val FILTER_CITY = "nearby-reviews"
        const val FILTER_FOLLOWING = "following-reviews"
        const val FILTER_LOCAL_EXPERT = "local_expert"

        const val SORT_TRENDING = "trending"
        const val SORT_NEARBY = "nearby"
        const val SORT_NEWEST = "newest"
    }
}