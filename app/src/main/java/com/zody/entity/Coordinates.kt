package com.zody.entity

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, July 12
 */
data class Coordinates(@SerializedName("latitude")
                       @ColumnInfo(name = "latitude")
                       val latitude: Double?,
                       @SerializedName("longitude")
                       @ColumnInfo(name = "longitude")
                       val longitude: Double?) {

    fun distanceTo(lat: Double, lng: Double): Double? {
        if (latitude == null || longitude == null) return null

        val R = 6371
        val latDistance = Math.toRadians(lat - latitude)
        val lngDistance = Math.toRadians(lng - longitude)

        val a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(Math.toRadians(latitude)) * Math.cos(Math.toRadians(lat)) * Math.sin(lngDistance / 2) * Math.sin(lngDistance / 2)
        val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
        return R * c * 1000
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Coordinates) return false

        if (latitude != other.latitude) return false
        if (longitude != other.longitude) return false

        return true
    }

    override fun hashCode(): Int {
        var result = latitude?.hashCode() ?: 0
        result = 31 * result + (longitude?.hashCode() ?: 0)
        return result
    }


}