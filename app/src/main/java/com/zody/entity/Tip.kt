package com.zody.entity

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by vinhnguyen.it.vn on 2018, July 28
 */
class Tip {
    @SerializedName("user")
    var author: Author? = null
    @SerializedName("coin")
    var coin: Int? = null
    @SerializedName("createdAt")
    var created: Date? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Tip) return false

        if (author != other.author) return false
        if (coin != other.coin) return false
        if (created != other.created) return false

        return true
    }

    override fun hashCode(): Int {
        var result = author?.hashCode() ?: 0
        result = 31 * result + (coin ?: 0)
        result = 31 * result + (created?.hashCode() ?: 0)
        return result
    }


}