package com.zody.entity

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Ignore
import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by vinhnguyen.it.vn on 2018, July 14
 */
@Entity(tableName = "posts", primaryKeys = ["post_id"])
class Post {
    @SerializedName("_id")
    @ColumnInfo(name = "post_id")
    lateinit var id: String
    @SerializedName("business")
    @Embedded(prefix = "post_")
    var business: BusinessCompat? = null
    @SerializedName("createdAt")
    @ColumnInfo(name = "post_created")
    var createdAt: Date? = null
    @SerializedName("statistic")
    @Embedded
    var statistic: PostStatistic? = null
    @SerializedName("feedback")
    @ColumnInfo(name = "post_feedback")
    var feedback: String? = null
    @SerializedName("point")
    @ColumnInfo(name = "post_point")
    var point: Int? = null
    @SerializedName("photos")
    @ColumnInfo(name = "post_photos")
    var photos: List<Photo>? = null
    @SerializedName("currentUserTipped")
    @ColumnInfo(name = "post_user_tipped")
    var currentUserTipped: Int? = null
    @SerializedName("isAuthor")
    @ColumnInfo(name = "post_is_author")
    var isAuthor: Boolean? = null
    @SerializedName("shareURL")
    @ColumnInfo(name = "post_share_url")
    var shareUrl: String? = null
    @SerializedName("hashtags")
    @ColumnInfo(name = "post_hash_tags")
    var hashtags: List<String>? = null
    @SerializedName("distance")
    @ColumnInfo(name = "post_distance")
    var distance: Double? = null

    @SerializedName("user")
    @Ignore
    var author: Author? = null
    @ColumnInfo(name = "post_author_id")
    var authorId: String? = null
        get() = field ?: author?.id

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Post) return false

        if (id != other.id) return false
        if (author != other.author) return false
        if (business != other.business) return false
        if (createdAt != other.createdAt) return false
        if (statistic != other.statistic) return false
        if (feedback != other.feedback) return false
        if (point != other.point) return false
        if (photos != other.photos) return false
        if (currentUserTipped != other.currentUserTipped) return false
        if (isAuthor != other.isAuthor) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + (author?.hashCode() ?: 0)
        result = 31 * result + (business?.hashCode() ?: 0)
        result = 31 * result + (createdAt?.hashCode() ?: 0)
        result = 31 * result + (statistic?.hashCode() ?: 0)
        result = 31 * result + (feedback?.hashCode() ?: 0)
        result = 31 * result + (point ?: 0)
        result = 31 * result + (photos?.hashCode() ?: 0)
        result = 31 * result + (currentUserTipped ?: 0)
        result = 31 * result + (isAuthor?.hashCode() ?: 0)
        return result
    }


}