package com.zody.entity

/**
 * Created by vinhnguyen.it.vn on 2018, July 30
 */
open class ListItem<T>(val data: T?, val next: String?, val order: Int, val type: Int)