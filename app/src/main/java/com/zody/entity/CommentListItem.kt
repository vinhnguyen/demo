package com.zody.entity

/**
 * Created by vinhnguyen.it.vn on 2018, July 27
 */
class CommentListItem(val photo: Photo, next: String?, order: Int, type: Int = 0) : ListItem<Post>(null, next, order, type) {

    companion object {
        const val TYPE_NORMAL = 1
    }
}