package com.zody.entity

import com.google.gson.annotations.SerializedName
import com.zody.entity.voucher.Voucher

/**
 * Created by vinhnguyen.it.vn on 2018, September 22
 */
class SearchResult(@SerializedName("businesses")
                   val businesses: List<BusinessCompat>?,
                   @SerializedName("users")
                   val users: List<Author>?,
                   @SerializedName("vouchers")
                   val vouchers: List<Voucher>?)