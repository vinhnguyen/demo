package com.zody.firebase

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.core.content.edit
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.zody.R
import com.zody.entity.Notification
import com.zody.livedata.UnreadNotificationLiveData
import com.zody.utils.SPKey
import dagger.android.AndroidInjection
import org.json.JSONException
import java.util.concurrent.atomic.AtomicInteger
import javax.inject.Inject


/**
 * Created by vinhnguyen.it.vn on 2018, August 17
 */
class MyFirebaseMessageService : FirebaseMessagingService() {

    companion object {
        private val notificationId = AtomicInteger()
    }

    @Inject
    lateinit var sp: SharedPreferences

    @Inject
    lateinit var gson: Gson

    @Inject
    lateinit var unreadNotification: UnreadNotificationLiveData

    override fun onCreate() {
        super.onCreate()
        AndroidInjection.inject(this)
    }

    override fun onNewToken(token: String?) {
        super.onNewToken(token)
        sp.edit {
            putString(SPKey.PREF_DEVICE_TOKEN, token)
        }
    }

    override fun onMessageReceived(message: RemoteMessage?) {
        super.onMessageReceived(message)
        message?.notification?.let {
            try {
                val notification = gson.fromJson<Notification>(message.data["data"], Notification::class.java)
                if (notification?.read == false) {
                    unreadNotification.add(notification.id)
                }
            } catch (e: JSONException) {

            }

            val intent = Intent(it.clickAction).apply {
                setPackage(packageName)
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                putExtras(message.toIntent())
            }
            val pendingIntent = PendingIntent.getActivity(this@MyFirebaseMessageService, notificationId.get(), intent, PendingIntent.FLAG_UPDATE_CURRENT)

            val notification = NotificationCompat.Builder(this@MyFirebaseMessageService, getString(R.string.app_name))
                    .setSmallIcon(R.drawable.ic_stat_ic_notification)
                    .setColor(ContextCompat.getColor(this, R.color.orange))
                    .setPriority(message.priority)
                    .setContentText(it.body)
                    .setContentTitle(it.title)
                    .setContentIntent(pendingIntent)
                    .setDefaults(NotificationCompat.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .build()
            val notificationManager = (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)
            notificationManager.notify(notificationId.getAndIncrement(), notification)
        }
    }
}