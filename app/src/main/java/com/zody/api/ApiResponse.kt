package com.zody.api

import com.google.gson.annotations.SerializedName

/**
 * Created by vinhnguyen.it.vn on 2018, July 10
 */
data class ApiResponse<T>(@SerializedName("success")
                          val success: Boolean = false,
                          @SerializedName("message")
                          val message: String? = null,
                          @SerializedName("data")
                          val data: T? = null,
                          @SerializedName("code")
                          val code: Int? = null)