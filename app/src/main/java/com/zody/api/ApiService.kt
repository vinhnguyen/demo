package com.zody.api

import androidx.lifecycle.LiveData
import com.google.gson.JsonElement
import com.zody.entity.*
import com.zody.entity.business.Business
import com.zody.entity.business.BusinessOfChain
import com.zody.entity.explore.ExploreData
import com.zody.entity.explore.OnlineShopping
import com.zody.entity.voucher.Voucher
import com.zody.entity.voucher.VoucherData
import com.zody.entity.voucher.VoucherDetailData
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by vinhnguyen.it.vn on 2018, July 13
 */
interface ApiService {

    //Configuration
    @GET("app-data")
    fun loadConfiguration(): Call<ApiResponse<JsonElement>>

    //User
    @POST("login-facebook")
    fun loginWithFacebook(@Body body: RequestBody): LiveData<ApiResponse<LoginResult>>

    //Profile
    @GET("me")
    fun loadProfile(): LiveData<ApiResponse<ApiData<Profile>>>

    //Profile
    @GET("me")
    fun loadProfile(@Header("Authorization") token: String): LiveData<ApiResponse<ApiData<Profile>>>

    @PUT("me")
    fun updateProfile(@Body body: RequestBody): LiveData<ApiResponse<Unit>>

    @GET("me/reviews")
    fun loadOwnerPost(@Query("pageToken") next: String?, @Query("type") type: String = "detail"): Call<ApiResponse<ApiPaging<Post>>>

    //Evoucher
    @GET("rewards/{id}")
    fun loadEvoucher(@Path("id") id: String): LiveData<ApiResponse<ApiData<EVoucher>>>

    @GET("me/rewards")
    fun loadEvouchers(@Query("status") status: String?, @Query("sort") sort: String?, @Query("pageToken") next: String?): Call<ApiResponse<ApiPaging<EVoucher>>>

    @PUT("rewards/{id}")
    fun useEvoucherWithBusiness(@Path("id") evoucherId: String, @Body body: RequestBody): LiveData<ApiResponse<Unit>>

    @PUT("rewards/{id}")
    fun useEvoucher(@Path("id") evoucherId: String): LiveData<ApiResponse<Unit>>

    @PATCH("me/rewards-stat")
    fun updateNumberReward(@Body body: RequestBody): Call<ApiResponse<Unit>>

    //Search
    @GET("search/business-with-google-place")
    fun searchBusiness(@Query("keyword") keyword: String = "",
                       @Query("latitude") latitude: Double?, @Query("longitude") longitude: Double?,
                       @Query("city") city: String?): LiveData<ApiResponse<ApiPaging<BusinessCompat>>>

    //Leader board
    @GET("leaderboard")
    fun loadLeaderBoard(@Query("type") type: String = "coins", @Query("time") time: String = "all", @Query("city") city: String): LiveData<ApiResponse<LeaderBoardResult>>

    //Author
    @GET("users/{id}")
    fun loadAuthorProfile(@Path("id") id: String): LiveData<ApiResponse<ApiData<Author>>>

    @GET("users/{id}/reviews")
    fun loadAuthorPost(@Path("id") authorId: String,
                       @Query("pageToken") next: String?, @Query("type") type: String = "detail"): Call<ApiResponse<ApiPaging<Post>>>

    @POST("follow/{id}")
    fun followAuthor(@Path("id") id: String): LiveData<ApiResponse<Unit>>

    @DELETE("follow/{id}")
    fun unFollowAuthor(@Path("id") id: String): LiveData<ApiResponse<Unit>>

    //Post
    @GET("check-content")
    fun checkContentForWritePost(@Query("content") content: String?): LiveData<ApiResponse<Unit>>

    @GET("hashtags/suggested")
    fun loadHashTag(@Query("keyword") keyword: String?): LiveData<ApiResponse<ApiPaging<HashTagAndCounter>>>

    @GET("hashtags/trending")
    fun loadTrendingHashTag(): LiveData<ApiResponse<ApiPaging<String>>>

    @POST("reviews")
    fun writePost(@Body body: RequestBody): LiveData<ApiResponse<ApiData<Post>>>

    @PUT("reviews/{id}")
    fun editPost(@Path("id") postId: String, @Body body: RequestBody): LiveData<ApiResponse<ApiData<Post>>>

    @GET("nearby-reviews")
    fun loadNearBy(@Query("pageToken") next: String?,
                   @Query("latitude") latitude: Double?, @Query("longitude") longitude: Double?,
                   @Query("city") city: String?,
                   @Query("sort") sort: String?): Call<ApiResponse<ApiPaging<Post>>>

    @GET("following-reviews")
    fun loadFollowingPost(@Query("pageToken") next: String?,
                          @Query("sort") sort: String? = "newest"): Call<ApiResponse<ApiPaging<Post>>>

    @GET("search/reviews")
    fun loadPostOfHashTag(@Query("hashtag") tag: String?,
                          @Query("pageToken") next: String?,
                          @Query("latitude") latitude: Double?, @Query("longitude") longitude: Double?,
                          @Query("city") city: String?,
                          @Query("type") type: String?): Call<ApiResponse<ApiPaging<Post>>>

    @GET("suggested-local-experts")
    fun loadSuggestionLocalExpert(): Call<ApiResponse<ApiPaging<LocalExpert>>>

    @GET("reviews/{id}")
    fun loadPostDetail(@Path("id") id: String): LiveData<ApiResponse<ApiData<Post>>>

    @GET("reviews/{postId}/comments")
    fun loadComment(@Path("postId") postId: String, @Query("pageToken") next: String?): Call<ApiResponse<ApiPaging<Comment>>>

    @POST("reviews/{postId}/comments")
    fun sendComment(@Path("postId") postId: String, @Body body: RequestBody): LiveData<ApiResponse<ApiData<Comment>>>

    @POST("tips")
    fun tips(@Body body: RequestBody): Call<ApiResponse<Unit>>

    @DELETE("reviews/{postId}")
    fun deletePost(@Path("postId") postId: String): LiveData<ApiResponse<Unit>>

    @GET("tips/review/{postId}")
    fun loadPostTipped(@Path("postId") postId: String, @Query("pageToken") after: String?): Call<ApiResponse<ApiPaging<Tip>>>

    @POST("reviews/{id}/views")
    fun increaseView(@Path("id") id: String?, @Body body: RequestBody): Call<ApiResponse<Unit>>

    //Notification
    @GET("notifications")
    fun loadNotification(@Query("pageToken") next: String?): Call<ApiResponse<ApiPaging<Notification>>>

    @PATCH("notifications/{id}")
    fun readNotification(@Path("id") id: String): Call<ApiResponse<Unit>>

    @PATCH("me/notifications-stat")
    fun forceReadNotification(): Call<ApiResponse<Unit>>

    //Business
    @GET("businesses/{id}")
    fun loadBusinessDetail(@Path("id") id: String): LiveData<ApiResponse<ApiData<Business>>>

    @GET("/businesses/{id}/brief-info")
    fun loadBusinessCompat(@Path("id") id: String): LiveData<ApiResponse<ApiData<Business>>>

    @GET("businesses/{id}/reviews")
    fun loadBusinessPost(@Path("id") businessId: String, @Query("pageToken") next: String?): Call<ApiResponse<ApiPaging<Post>>>

    @GET("businesses/{id}/photos")
    fun loadBusinessPhoto(@Path("id") businessId: String, @Query("pageToken") next: String?): Call<ApiResponse<ApiPaging<Photo>>>

    @GET("businesses/{id}/photos")
    fun loadBusinessPhotoFirstPage(@Path("id") businessId: String): LiveData<ApiResponse<ApiPaging<Photo>>>

    @POST("businesses")
    fun createBusiness(@Body body: RequestBody): LiveData<ApiResponse<ApiData<BusinessCompat>>>

    //Search
    @GET("search/all")
    fun search(@Query("keyword") keyword: String?,
               @Query("latitude") latitude: Double?, @Query("longitude") longitude: Double?,
               @Query("city") city: String?): LiveData<ApiResponse<SearchResult>>


    //Integrated
    @GET("integrated/theCoffeeHouse")
    fun loadTheCoffeeHouse(): LiveData<ApiResponse<TheCoffeeHouseLinkedProgram>>

    @PUT("integrated/theCoffeeHouse")
    fun updateTheCoffeeHouse(@Body body: RequestBody): LiveData<ApiResponse<Unit>>

    @DELETE("integrated/theCoffeeHouse")
    fun deleteTheCoffeeHouse(): LiveData<ApiResponse<Unit>>

    //Explore
    @GET("explorer/data")
    fun loadExplorerData(@Query("city") city: String?, @Query("latitude") latitude: Double?, @Query("longitude") longitude: Double?): LiveData<ApiResponse<ExploreData>>

    @GET("explorer/qr-code-businesses")
    fun loadExploreQrCodeBusiness(@Query("pageToken") next: String?,
                                  @Query("city") city: String?,
                                  @Query("latitude") latitude: Double?, @Query("longitude") longitude: Double?,
                                  @Query("view") view: String = "explorer"): LiveData<ApiResponse<ApiPaging<BusinessList>>>

    @GET("explorer/new-businesses")
    fun loadExploreNewBusiness(@Query("pageToken") next: String?,
                               @Query("city") city: String?,
                               @Query("latitude") latitude: Double?, @Query("longitude") longitude: Double?,
                               @Query("view") view: String = "explorer"): LiveData<ApiResponse<ApiPaging<BusinessList>>>

    @GET("explorer/trending-businesses")
    fun loadExploreTrendingBusiness(@Query("pageToken") next: String?,
                                    @Query("city") city: String?,
                                    @Query("latitude") latitude: Double?, @Query("longitude") longitude: Double?,
                                    @Query("view") view: String = "explorer"): LiveData<ApiResponse<ApiPaging<BusinessList>>>

    @GET("explorer/promotion-businesses")
    fun loadExplorePromotionBusiness(@Query("pageToken") next: String?,
                                     @Query("city") city: String?,
                                     @Query("latitude") latitude: Double?, @Query("longitude") longitude: Double?,
                                     @Query("view") view: String = "explorer"): LiveData<ApiResponse<ApiPaging<BusinessPromotion>>>

    @GET("affiliate-programs/{id}")
    fun loadAffiliate(@Path("id") id: String): LiveData<ApiResponse<OnlineShopping>>

    @GET("explorer/businesses-by-category")
    fun loadBusinessOfCategory(@Query("category") categoryId: String?,
                               @Query("pageToken") next: String?,
                               @Query("city") city: String?,
                               @Query("latitude") latitude: Double?, @Query("longitude") longitude: Double?): Call<ApiResponse<ApiPaging<BusinessList>>>

    @GET("explorer/qr-code-businesses")
    fun loadQrCodeBusiness(@Query("pageToken") next: String?,
                           @Query("city") city: String?,
                           @Query("latitude") latitude: Double?, @Query("longitude") longitude: Double?,
                           @Query("view") view: String = "all"): Call<ApiResponse<ApiPaging<BusinessList>>>

    @GET("explorer/new-businesses")
    fun loadNewBusiness(@Query("pageToken") next: String?,
                        @Query("city") city: String?,
                        @Query("latitude") latitude: Double?, @Query("longitude") longitude: Double?,
                        @Query("view") view: String = "all"): Call<ApiResponse<ApiPaging<BusinessList>>>

    @GET("explorer/trending-businesses")
    fun loadTrendingBusiness(@Query("pageToken") next: String?,
                             @Query("city") city: String?,
                             @Query("latitude") latitude: Double?, @Query("longitude") longitude: Double?,
                             @Query("view") view: String = "all"): Call<ApiResponse<ApiPaging<BusinessList>>>

    @GET("explorer/promotion-businesses")
    fun loadPromotionBusiness(@Query("pageToken") next: String?,
                              @Query("city") city: String?,
                              @Query("latitude") latitude: Double?, @Query("longitude") longitude: Double?,
                              @Query("view") view: String = "all"): Call<ApiResponse<ApiPaging<BusinessPromotion>>>

    //Chain
    @GET("chains/{id}/businesses")
    fun loadBusinessOfChain(@Path("id") id: String?): LiveData<ApiResponse<BusinessOfChain>>

    //Voucher
    @GET("vouchers")
    fun loadVouchers(@Query("pageToken") next: String?,
                     @Query("city") city: String?,
                     @Query("latitude") latitude: Double?,
                     @Query("longitude") longitude: Double?): LiveData<ApiResponse<ApiData<VoucherData>>>

    @GET("vouchers-by-category")
    fun loadVoucherOfCategory(@Query("category") category: String?,
                              @Query("sort") sort: String?,
                              @Query("pageToken") next: String?,
                              @Query("city") city: String?,
                              @Query("latitude") latitude: Double?,
                              @Query("longitude") longitude: Double?): Call<ApiResponse<ApiPaging<Voucher>>>

    @GET("vouchers-you-may-like")
    fun loadVoucherYouMayLike(@Query("pageToken") next: String?,
                              @Query("city") city: String?,
                              @Query("latitude") latitude: Double?,
                              @Query("longitude") longitude: Double?): Call<ApiResponse<ApiPaging<Voucher>>>

    @GET("vouchers-new-update")
    fun loadVoucherNewUpdate(@Query("pageToken") next: String?,
                             @Query("city") city: String?,
                             @Query("latitude") latitude: Double?,
                             @Query("longitude") longitude: Double?): Call<ApiResponse<ApiPaging<Voucher>>>

    @GET("vouchers/{id}")
    fun loadVoucherDetail(@Path("id") id: String): LiveData<ApiResponse<ApiData<VoucherDetailData>>>

    @POST("vouchers/{id}/exchange")
    fun exchangeVoucher(@Path("id") id: String): LiveData<ApiResponse<Unit>>
}