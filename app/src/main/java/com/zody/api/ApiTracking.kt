package com.zody.api

import retrofit2.Call
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.Path

/**
 * Created by vinhnguyen.it.vn on 2018, August 17
 */
interface ApiTracking {

    @FormUrlEncoded
    @POST("{type}")
    fun tracking(@Path("type") type: String, @FieldMap params: Map<String, @JvmSuppressWildcards Any?>): Call<Any>
}