package com.zody.api

import com.google.gson.JsonDeserializer
import timber.log.Timber
import java.lang.reflect.ParameterizedType

/**
 * Created by vinhnguyen.it.vn on 2018, July 27
 */
data class ApiData<T>(val data: T?) {

    companion object {
        val deserializer = JsonDeserializer { json, typeApiData, context ->
            try {
                json?.asJsonObject?.entrySet()?.firstOrNull { entry ->
                    val typeOfT = (typeApiData as ParameterizedType).actualTypeArguments[0]
                    return@JsonDeserializer ApiData<Any>(context.deserialize(entry.value, typeOfT))
                }
            } catch (e: Exception) {
                Timber.tag("JsonException").e(e)
                return@JsonDeserializer null
            }
        }
    }
}