package com.zody.api

import androidx.lifecycle.LiveData
import com.zody.BuildConfig
import retrofit2.*
import timber.log.Timber
import java.lang.reflect.Type
import java.util.concurrent.atomic.AtomicBoolean

/**
 * Created by vinhnguyen.it.vn on 2018, July 10
 */
class LiveDataCallAdapter<R>(private val responseType: Type, private val retrofit: Retrofit) :
        CallAdapter<ApiResponse<R>, LiveData<ApiResponse<R>>> {

    override fun responseType() = responseType

    override fun adapt(call: Call<ApiResponse<R>>): LiveData<ApiResponse<R>> {
        return object : LiveData<ApiResponse<R>>() {
            private var started = AtomicBoolean(false)
            override fun onActive() {
                super.onActive()
                if (started.compareAndSet(false, true)) {
                    call.enqueue(object : Callback<ApiResponse<R>> {
                        override fun onResponse(call: Call<ApiResponse<R>>, response: Response<ApiResponse<R>>) {
                            if (response.isSuccessful) {
                                postValue(response.body())
                            } else {
                                val converter = retrofit.responseBodyConverter<ApiResponse<R>>(ApiResponse::class.java, ApiResponse::class.java.annotations)
                                val body = response.errorBody()
                                try {
                                    postValue(if (body != null) converter.convert(body) else ApiResponse<R>(false, null, null))
                                } catch (e: Exception) {
                                    postValue(ApiResponse(false, if (BuildConfig.DEBUG) e.message else null, null))
                                }
                            }
                        }

                        override fun onFailure(call: Call<ApiResponse<R>>, throwable: Throwable) {
                            Timber.e(throwable)
                            postValue(ApiResponse(false, throwable.message, null))
                        }
                    })
                }
            }
        }
    }
}
