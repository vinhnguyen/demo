package com.zody.api

import com.google.gson.JsonDeserializer
import com.google.gson.annotations.SerializedName
import java.lang.reflect.ParameterizedType

/**
 * Created by vinhnguyen.it.vn on 2018, July 14
 */
class ApiPaging<T> {
    @SerializedName("data")
    var data: List<T>? = null
    @SerializedName("endData")
    var endData: Boolean? = null
    @SerializedName("nextPageToken")
    var next: String? = null
    @SerializedName("total")
    var total: Int? = null

    companion object {
        val deserializer = JsonDeserializer { json, typeApiPaging, context ->
            json?.let { it ->
                if (it.isJsonObject) {
                    val paging = ApiPaging<Any>()
                    it.asJsonObject.entrySet().forEach { entry ->
                        when {
                            entry.key == "endData" || entry.key == "isEnd" -> paging.endData = entry.value?.asBoolean
                            entry.key == "nextPageToken" -> paging.next = entry.value?.asString
                            entry.key == "total" -> paging.total = entry.value?.asInt
                            entry.value.isJsonArray -> {
                                val typeOfT = (typeApiPaging as ParameterizedType).actualTypeArguments[0]
                                val type = object : ParameterizedType {
                                    override fun getRawType() = List::class.java

                                    override fun getOwnerType() = null

                                    override fun getActualTypeArguments() = arrayOf(typeOfT)
                                }
                                paging.data = context.deserialize(entry.value, type)
                            }
                        }
                    }
                    return@JsonDeserializer paging
                }
            }
            return@JsonDeserializer null
        }
    }
}