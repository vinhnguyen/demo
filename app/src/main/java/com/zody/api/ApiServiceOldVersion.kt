package com.zody.api

import androidx.lifecycle.LiveData
import com.zody.entity.LoginResult
import com.zody.entity.Photo
import com.zody.entity.ScanQrCodeResult
import com.zody.entity.explore.OnlineShoppingOrder
import com.zody.entity.luckydraw.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*


/**
 * Created by vinhnguyen.it.vn on 2018, July 09
 */
interface ApiServiceOldVersion {

    //User
    @FormUrlEncoded
    @POST("loginWithPhoneNumberFromAccountKit")
    fun loginWithAccountKit(@Field("accessToken") token: String): LiveData<ApiResponse<LoginResult>>

    @FormUrlEncoded
    @PUT("users/verifyPhoneNumberWithAccountKit")
    fun updatePhone(@Field("accessToken") token: String): LiveData<ApiResponse<Unit>>

    @Multipart
    @POST("photos/userAvatar")
    fun updateAvatar(@Part part: MultipartBody.Part): LiveData<ApiResponse<Photo>>

    @Multipart
    @POST("photos")
    fun uploadPhoto(@Part part: MultipartBody.Part): Call<ApiResponse<Photo>>

    @FormUrlEncoded
    @POST("v2.5/users/inputReferralCode")
    fun inputReferralCode(@Field("code") code: String, @Field("deviceInfo[deviceOS]") deviceOs: String? = "android", @Field("deviceInfo[deviceId]") deviceId: String?): LiveData<ApiResponse<ApiData<Int>>>

    @GET("{type}/{id}/history")
    fun loadOnlineShoppingOrder(@Path("type") type: String, @Path("id") id: String, @Query("page") page: Int): Call<ApiResponse<ApiPaging<OnlineShoppingOrder>>>

    //Qr code
    @POST("bills/hookPOS")
    fun checkQrCode(@Body body: RequestBody): LiveData<ApiResponse<ScanQrCodeResult>>

    //LuckyDraw
    @GET("v3.0/luckyDraws/currentActive")
    fun getCurrentActiveLuckyDraw(@Query("city") city: String?,
                                  @Query("latitude") latitude: Double?,
                                  @Query("longitude") longitude: Double?): LiveData<ApiResponse<LuckyDrawData>>

    @GET("luckyDraws/{id}")
    fun getLuckyDraw(@Path("id") id: String): LiveData<ApiResponse<LuckyDrawData>>

    @GET("v3.0/luckyDraws/{id}/history")
    fun getLuckyDrawHistory(@Path("id") id: String, @Query("page") page: Int, @Query("haveGoodLuckPrize") haveGoodLuckPrize: Boolean): Call<ApiResponse<ApiPaging<LuckyDrawHistory>>>

    @GET("v3.0/luckyDraws/{id}/draw")
    fun draw(@Path("id") id: String): LiveData<ApiResponse<LuckyDrawResult>>

    @GET("v3.0/luckyDraws/{id}/winners")
    fun getWinner(@Path("id") id: String, @Query("page") page: Int): Call<ApiResponse<ApiPaging<LuckyDrawWinner>>>

    @GET("v3.0/luckyDraws/{id}/leaderBoard")
    fun getLeaderBoard(@Path("id") id: String): LiveData<ApiResponse<ApiPaging<LuckyDrawLeaderBoard>>>

    @GET("luckyDraws/{id}/codes")
    fun getCode(@Path("id") id: String, @Query("page") page: Int): Call<ApiResponse<ApiPaging<LuckyDrawBallot>>>

    @PUT("v3.0/luckyDraws/{id}/share")
    fun getRewardAfterShared(@Path("id") id: String): LiveData<ApiResponse<LuckyDrawShareResult>>
}