package com.zody.di

import com.zody.ui.post.list.following.PostOfFollowingFragment
import com.zody.ui.post.list.nearby.PostNearByFragment
import com.zody.ui.post.list.newest.PostNewestFragment
import com.zody.ui.post.list.trending.PostTrendingFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by vinhnguyen.it.vn on 2018, July 11
 */
@Module
abstract class HomeFragmentChildModule {

    @ContributesAndroidInjector
    abstract fun contributePostTrendingFragment(): PostTrendingFragment

    @ContributesAndroidInjector
    abstract fun contributePostNewestFragment(): PostNewestFragment

    @ContributesAndroidInjector
    abstract fun contributePostNearByFragment(): PostNearByFragment

    @ContributesAndroidInjector
    abstract fun contributePostFromFollowingFragment(): PostOfFollowingFragment
}