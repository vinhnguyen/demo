package com.zody.di

import com.zody.ui.writepost.WritePostFragment
import com.zody.ui.writepost.search.SearchForWritePostFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by vinhnguyen.it.vn on 2018, July 11
 */
@Module
abstract class WritePostActivityFragmentModule {
    @ContributesAndroidInjector
    abstract fun contributeSearchForWritePostFragment(): SearchForWritePostFragment

    @ContributesAndroidInjector
    abstract fun contributeWritePostFragment(): WritePostFragment
}