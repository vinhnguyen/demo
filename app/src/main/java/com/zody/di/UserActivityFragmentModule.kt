package com.zody.di

import com.zody.ui.author.AuthorProfileFragment
import com.zody.ui.post.PostOptionsDialogFragment
import com.zody.ui.profile.ProfileFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by vinhnguyen.it.vn on 2018, July 11
 */
@Module
abstract class UserActivityFragmentModule {
    @ContributesAndroidInjector(modules = [ProfileFragmentChildModule::class])
    abstract fun contributeProfileFragment(): ProfileFragment

    @ContributesAndroidInjector(modules = [AuthorProfileFragmentChildModule::class])
    abstract fun contributeAuthorProfileFragment(): AuthorProfileFragment

    @ContributesAndroidInjector
    abstract fun contributePostOptionsDialogFragment(): PostOptionsDialogFragment
}