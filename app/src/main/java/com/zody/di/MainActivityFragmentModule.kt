package com.zody.di

import com.zody.ui.explore.ExploreFragment
import com.zody.ui.home.HomeFragment
import com.zody.ui.post.PostOptionsDialogFragment
import com.zody.ui.profile.ProfileFragment
import com.zody.ui.voucher.VoucherTabFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by vinhnguyen.it.vn on 2018, July 11
 */
@Module
abstract class MainActivityFragmentModule {
    @ContributesAndroidInjector(modules = [HomeFragmentChildModule::class])
    abstract fun contributeHomeFragment(): HomeFragment

    @ContributesAndroidInjector
    abstract fun contributeExploreFragment(): ExploreFragment

    @ContributesAndroidInjector(modules = [VoucherTabFragmentChildModule::class])
    abstract fun contributeVoucherFragment(): VoucherTabFragment

    @ContributesAndroidInjector(modules = [ProfileFragmentChildModule::class])
    abstract fun contributeProfileFragment(): ProfileFragment

    @ContributesAndroidInjector
    abstract fun contributePostOptionsDialogFragment(): PostOptionsDialogFragment
}