package com.zody.di

import com.zody.firebase.MyFirebaseMessageService
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by vinhnguyen.it.vn on 2018, August 17
 */
@Module
internal abstract class ServiceModule {
    @ContributesAndroidInjector
    internal abstract fun contributeMyFirebaseMessageService(): MyFirebaseMessageService
}