package com.zody.di

import com.zody.ui.leaderboard.LeaderBoardFilterFragment
import com.zody.ui.leaderboard.LeaderBoardFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by vinhnguyen.it.vn on 2018, July 11
 */
@Module
abstract class LeaderBoardActivityFragmentModule {
    @ContributesAndroidInjector
    abstract fun contributeLeaderBoardFragment(): LeaderBoardFragment

    @ContributesAndroidInjector
    abstract fun contributeLeaderBoardFilterFragment(): LeaderBoardFilterFragment
}