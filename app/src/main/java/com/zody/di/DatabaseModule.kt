package com.zody.di

import android.app.Application
import androidx.room.Room
import com.zody.db.*
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by vinhnguyen.it.vn on 2018, July 11
 */
@Module
class DatabaseModule {

    //Config

    @Singleton
    @Provides
    fun provideConfigDb(app: Application): ConfigDb {
        return Room
                .databaseBuilder(app, ConfigDb::class.java, "configuration.db")
                .fallbackToDestructiveMigration()
                .build()
    }

    @Singleton
    @Provides
    fun profileConfigDao(db: ConfigDb): ConfigDao {
        return db.configDao()
    }

    //User

    @Singleton
    @Provides
    fun provideProfileDb(app: Application): ProfileDb {
        return Room
                .databaseBuilder(app, ProfileDb::class.java, "profile.db")
                .fallbackToDestructiveMigration()
                .build()
    }

    @Provides
    fun provideProfileDao(db: ProfileDb): ProfileDao {
        return db.profileDao()
    }

    //Post
    @Singleton
    @Provides
    fun providePostDb(app: Application): PostDb {
        return Room
                .inMemoryDatabaseBuilder(app, PostDb::class.java)
                .fallbackToDestructiveMigration()
                .build()
    }

    @Provides
    fun providePostDao(db: PostDb): PostDao {
        return db.postDao()
    }

    @Provides
    fun provideCommentDao(db: PostDb): CommentDao {
        return db.commentDao()
    }

    @Provides
    fun provideAuthDao(db: PostDb): AuthorDao {
        return db.authorDao()
    }

    @Provides
    fun provideNotificationDb(app: Application): NotificationDb {
        return Room.inMemoryDatabaseBuilder(app, NotificationDb::class.java)
                .fallbackToDestructiveMigration()
                .build()
    }

    @Provides
    fun provideVoucherDb(app: Application): VoucherDb {
        return Room.inMemoryDatabaseBuilder(app, VoucherDb::class.java)
                .fallbackToDestructiveMigration()
                .build()
    }
}