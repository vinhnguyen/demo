package com.zody.di

import android.app.Application
import com.zody.App
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
        modules = [AndroidInjectionModule::class,
            AppModule::class,
            ApiModule::class,
            DatabaseModule::class,
            ViewModelModule::class,
            ActivityModule::class,
            ServiceModule::class])
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: App)
}
