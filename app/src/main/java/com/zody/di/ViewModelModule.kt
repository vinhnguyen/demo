/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zody.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.zody.ui.author.AuthorProfileViewModel
import com.zody.ui.business.BusinessDetailViewModel
import com.zody.ui.business.photo.BusinessPhotoViewModel
import com.zody.ui.evoucher.EVoucherViewModel
import com.zody.ui.evoucher.detail.EVoucherDetailViewModel
import com.zody.ui.expert.AuthorViewModel
import com.zody.ui.expert.LocalExpertViewModel
import com.zody.ui.explore.ExploreViewModel
import com.zody.ui.explore.chain.ChainViewModel
import com.zody.ui.explore.newbusiness.NewBusinessViewModel
import com.zody.ui.explore.offline.BusinessViewModel
import com.zody.ui.explore.online.OnlineShoppingViewModel
import com.zody.ui.explore.promotion.PromotionViewModel
import com.zody.ui.explore.qrcode.QrCodeBusinessViewModel
import com.zody.ui.explore.trending.TrendingViewModel
import com.zody.ui.hashtag.SearchHashTagViewModel
import com.zody.ui.header.HeaderViewModel
import com.zody.ui.help.ZcoinCenterDetailViewModel
import com.zody.ui.help.ZcoinCenterViewModel
import com.zody.ui.home.HomeViewModel
import com.zody.ui.leaderboard.LeaderBoardViewModel
import com.zody.ui.login.LoginViewModel
import com.zody.ui.luckydraw.LuckyDrawViewModel
import com.zody.ui.luckydraw.code.LuckyDrawCodeViewModel
import com.zody.ui.luckydraw.history.LuckyDrawHistoryViewModel
import com.zody.ui.luckydraw.leaderboard.LuckyDrawLeaderBoardViewModel
import com.zody.ui.luckydraw.winner.LuckyDrawWinnerViewModel
import com.zody.ui.main.MainViewModel
import com.zody.ui.notification.NotificationViewModel
import com.zody.ui.permission.AskPermissionViewModel
import com.zody.ui.post.PostActionViewModel
import com.zody.ui.post.detail.PostDetailViewModel
import com.zody.ui.post.hashtag.PostOfHashTagViewModel
import com.zody.ui.post.list.following.PostOfFollowingViewModel
import com.zody.ui.post.list.nearby.PostNearByViewModel
import com.zody.ui.post.list.newest.PostNewestViewModel
import com.zody.ui.post.list.trending.PostTrendingViewModel
import com.zody.ui.post.tip.PostTippedViewModel
import com.zody.ui.profile.ProfileViewModel
import com.zody.ui.profile.edit.EditProfileViewModel
import com.zody.ui.profile.referral.ReferralViewModel
import com.zody.ui.qrcode.ScanQrCodeViewModel
import com.zody.ui.search.SearchViewModel
import com.zody.ui.setting.SettingViewModel
import com.zody.ui.splash.SplashViewModel
import com.zody.ui.tch.TheCoffeeHouseViewModel
import com.zody.ui.user.UserViewModel
import com.zody.ui.verify.VerifyPhoneViewModel
import com.zody.ui.voucher.VoucherOfCategoryViewModel
import com.zody.ui.voucher.VoucherOfOtherCategoryViewModel
import com.zody.ui.voucher.VoucherTabViewModel
import com.zody.ui.voucher.VoucherViewModel
import com.zody.ui.voucher.detail.VoucherDetailViewModel
import com.zody.ui.writepost.WritePostViewModel
import com.zody.ui.writepost.add.AddManualViewModel
import com.zody.ui.writepost.edit.EditPostViewModel
import com.zody.ui.writepost.hashtag.AddHashTagViewModel
import com.zody.ui.writepost.media.SelectMediaViewModel
import com.zody.ui.writepost.search.SearchForWritePostViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    abstract fun bindSplashViewModel(splashViewModel: SplashViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(loginViewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(mainViewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VerifyPhoneViewModel::class)
    abstract fun bindVerifyPhoneViewModel(verifyPhoneViewModel: VerifyPhoneViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProfileViewModel::class)
    abstract fun bindProfileViewModel(profileViewModel: ProfileViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindHomeViewModel(homeViewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HeaderViewModel::class)
    abstract fun bindHeaderViewModel(headerViewModel: HeaderViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ExploreViewModel::class)
    abstract fun bindExploreViewModel(exploreViewModel: ExploreViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(QrCodeBusinessViewModel::class)
    abstract fun bindQrCodeBusinessViewModel(qrCodeBusinessViewModel: QrCodeBusinessViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SearchForWritePostViewModel::class)
    abstract fun bindSearchForWritePostViewModel(searchForWritePostViewModel: SearchForWritePostViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VoucherTabViewModel::class)
    abstract fun bindVoucherTabViewModel(voucherTabViewModel: VoucherTabViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VoucherViewModel::class)
    abstract fun bindVoucherViewModel(voucherViewModel: VoucherViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VoucherOfCategoryViewModel::class)
    abstract fun bindVoucherOfCategoryViewModel(voucherOfCategoryViewModel: VoucherOfCategoryViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VoucherOfOtherCategoryViewModel::class)
    abstract fun bindVoucherOfOtherCategoryViewModel(voucherOfOtherCategoryViewModel: VoucherOfOtherCategoryViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(WritePostViewModel::class)
    abstract fun bindWritePostViewModel(writePostViewModel: WritePostViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EditPostViewModel::class)
    abstract fun bindEditPostViewModel(editPostViewModel: EditPostViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddManualViewModel::class)
    abstract fun bindAddManualViewModel(addManualViewModel: AddManualViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SelectMediaViewModel::class)
    abstract fun bindSelectMediaViewModel(selectMediaViewModel: SelectMediaViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddHashTagViewModel::class)
    abstract fun bindHasTagViewModel(addHashTagViewModel: AddHashTagViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LeaderBoardViewModel::class)
    abstract fun bindLeaderBoardViewModel(leaderBoardViewModel: LeaderBoardViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PostNearByViewModel::class)
    abstract fun bindPostNearByViewModel(postNearByViewModel: PostNearByViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PostTrendingViewModel::class)
    abstract fun bindPostTrendingViewModel(postTrendingViewModel: PostTrendingViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PostNewestViewModel::class)
    abstract fun bindPostNewestViewModel(postNewestViewModel: PostNewestViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PostOfFollowingViewModel::class)
    abstract fun bindPostOfFollowingViewMode(postOfFollowingViewModel: PostOfFollowingViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LocalExpertViewModel::class)
    abstract fun bindLocalExpertViewModel(localExpertViewModel: LocalExpertViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AuthorViewModel::class)
    abstract fun bindAuthorViewModel(authorViewModel: AuthorViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PostDetailViewModel::class)
    abstract fun bindPostDetailViewModel(postDetailViewModel: PostDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PostActionViewModel::class)
    abstract fun bindPostActionViewModel(postActionViewModel: PostActionViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PostTippedViewModel::class)
    abstract fun bindPostTippedViewModel(postTippedViewModel: PostTippedViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AuthorProfileViewModel::class)
    abstract fun bindAuthorProfileViewModel(authorProfileViewModel: AuthorProfileViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NotificationViewModel::class)
    abstract fun bindNotificationViewModel(notificationViewModel: NotificationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SettingViewModel::class)
    abstract fun bindSettingViewModel(settingViewModel: SettingViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EditProfileViewModel::class)
    abstract fun bindEditProfileViewModel(editProfileViewModel: EditProfileViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VoucherDetailViewModel::class)
    abstract fun bindVoucherDetailViewModel(voucherDetailViewModel: VoucherDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BusinessDetailViewModel::class)
    abstract fun bindBusinessDetailViewModel(businessDetailViewModel: BusinessDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BusinessPhotoViewModel::class)
    abstract fun bindBusinessPhotoViewModel(businessPhotoViewModel: BusinessPhotoViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(UserViewModel::class)
    abstract fun bindUserViewModel(userViewModel: UserViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ZcoinCenterViewModel::class)
    abstract fun bindZcoinCenterViewModel(zcoinCenterViewModel: ZcoinCenterViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ZcoinCenterDetailViewModel::class)
    abstract fun bindZcoinCenterDetailViewModel(zcoinCenterDetailViewModel: ZcoinCenterDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EVoucherViewModel::class)
    abstract fun bindEVoucherViewModel(eVoucherViewModel: EVoucherViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EVoucherDetailViewModel::class)
    abstract fun bindEVoucherDetailViewModel(eVoucherDetailViewModel: EVoucherDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PromotionViewModel::class)
    abstract fun bindPromotionViewModel(promotionViewModel: PromotionViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NewBusinessViewModel::class)
    abstract fun bindNewBusinessViewModel(newBusinessViewModel: NewBusinessViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TrendingViewModel::class)
    abstract fun bindTrendingViewModel(trendingViewModel: TrendingViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BusinessViewModel::class)
    abstract fun bindBusinessViewModel(businessViewModel: BusinessViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChainViewModel::class)
    abstract fun bindChainViewModel(chainViewModel: ChainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OnlineShoppingViewModel::class)
    abstract fun bindInstructionViewModel(onlineShoppingViewModel: OnlineShoppingViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SearchHashTagViewModel::class)
    abstract fun bindSearchHashTagViewModel(searchHashTagViewModel: SearchHashTagViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PostOfHashTagViewModel::class)
    abstract fun bindPostOfHashTagViewModel(postOfHashTagViewModel: PostOfHashTagViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SearchViewModel::class)
    abstract fun bindSearchViewModel(searchViewModel: SearchViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ReferralViewModel::class)
    abstract fun bindReferralViewModel(referralViewModel: ReferralViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TheCoffeeHouseViewModel::class)
    abstract fun bindTheCoffeeHouseViewModel(theCoffeeHouseViewModel: TheCoffeeHouseViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ScanQrCodeViewModel::class)
    abstract fun bindScanQrCodeViewModel(scanQrCodeViewModel: ScanQrCodeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AskPermissionViewModel::class)
    abstract fun bindAskPermissionViewModel(askPermissionViewModel: AskPermissionViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LuckyDrawViewModel::class)
    abstract fun bindLuckyDrawViewModel(luckyDrawViewModel: LuckyDrawViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LuckyDrawHistoryViewModel::class)
    abstract fun bindLuckyDrawHistoryViewModel(luckyDrawHistoryViewModel: LuckyDrawHistoryViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LuckyDrawWinnerViewModel::class)
    abstract fun bindLuckyDrawWinnerViewModel(luckyDrawWinnerViewModel: LuckyDrawWinnerViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LuckyDrawCodeViewModel::class)
    abstract fun bindLuckyDrawCodeViewModel(luckyDrawCodeViewModel: LuckyDrawCodeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LuckyDrawLeaderBoardViewModel::class)
    abstract fun bindLuckyDrawLeaderBoardViewModel(luckyDrawLeaderBoardViewModel: LuckyDrawLeaderBoardViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}
