package com.zody.di

import com.zody.ui.writepost.add.AddManualFragment
import com.zody.ui.writepost.add.AddManualSuccessFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by vinhnguyen.it.vn on 2018, July 11
 */
@Module
abstract class AddManualActivityFragmentModule {

    @ContributesAndroidInjector
    abstract fun contributeAddManualFragment(): AddManualFragment

    @ContributesAndroidInjector
    abstract fun contributeAddManualSuccessFragment(): AddManualSuccessFragment
}