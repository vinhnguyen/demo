package com.zody.di

import com.zody.ui.author.AuthorProfilePhotoFragment
import com.zody.ui.author.AuthorProfilePostFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by vinhnguyen.it.vn on 2018, July 11
 */
@Module
abstract class AuthorProfileFragmentChildModule {
    @ContributesAndroidInjector
    abstract fun contributeAuthorProfilePhotoFragment(): AuthorProfilePhotoFragment

    @ContributesAndroidInjector
    abstract fun contributeAuthorProfilePostFragment(): AuthorProfilePostFragment
}