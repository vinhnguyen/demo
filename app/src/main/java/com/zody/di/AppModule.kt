package com.zody.di

import android.annotation.SuppressLint
import android.app.Application
import android.content.SharedPreferences
import android.provider.Settings
import com.google.android.gms.analytics.GoogleAnalytics
import com.google.android.gms.analytics.Tracker
import com.zody.R
import com.zody.utils.sp
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class AppModule {

    @Singleton
    @Provides
    fun createSharePreference(app: Application): SharedPreferences {
        return app.sp
    }

    @SuppressLint("HardwareIds")
    @Provides
    @Named("deviceId")
    fun profileDeviceId(app: Application): String {
        return Settings.Secure.getString(app.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    @Singleton
    @Provides
    fun provideGoogleAnalytics(app: Application): GoogleAnalytics {
        return GoogleAnalytics.getInstance(app)
    }

    @Singleton
    @Provides
    fun provideTracker(analytics: GoogleAnalytics): Tracker {
        return analytics.newTracker(R.xml.global_tracker)
    }
}
