package com.zody.di

import com.zody.ui.profile.ProfilePhotoFragment
import com.zody.ui.profile.ProfilePostFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by vinhnguyen.it.vn on 2018, July 11
 */
@Module
abstract class ProfileFragmentChildModule {
    @ContributesAndroidInjector
    abstract fun contributeProfilePhotoFragment(): ProfilePhotoFragment

    @ContributesAndroidInjector
    abstract fun contributeProfilePostFragment(): ProfilePostFragment
}