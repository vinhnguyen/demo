package com.zody.di

import com.zody.ui.post.PostOptionsDialogFragment
import com.zody.ui.post.hashtag.PostOfHashTagFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by vinhnguyen.it.vn on 2018, July 11
 */
@Module
abstract class PostOfHashTagActivityFragmentModule {

    @ContributesAndroidInjector
    abstract fun contributePostOfHashTagFragment(): PostOfHashTagFragment

    @ContributesAndroidInjector
    abstract fun contributePostOptionsDialogFragment(): PostOptionsDialogFragment
}