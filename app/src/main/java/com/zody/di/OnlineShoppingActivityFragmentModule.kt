package com.zody.di

import com.zody.ui.explore.online.OnlineShoppingFaqFragment
import com.zody.ui.explore.online.OnlineShoppingIntroduceFragment
import com.zody.ui.explore.online.OnlineShoppingOrderFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by vinhnguyen.it.vn on 2018, July 11
 */
@Module
abstract class OnlineShoppingActivityFragmentModule {

    @ContributesAndroidInjector
    abstract fun contributeOnlineShoppingIntroduceFragment(): OnlineShoppingIntroduceFragment

    @ContributesAndroidInjector
    abstract fun contributeOnlineShoppingOrderFragment(): OnlineShoppingOrderFragment

    @ContributesAndroidInjector
    abstract fun contributeOnlineShoppingFaqFragment(): OnlineShoppingFaqFragment
}