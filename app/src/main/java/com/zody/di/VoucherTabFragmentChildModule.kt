package com.zody.di

import com.zody.ui.evoucher.EVoucherFragment
import com.zody.ui.voucher.VoucherFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by vinhnguyen.it.vn on 2018, July 11
 */
@Module
abstract class VoucherTabFragmentChildModule {

    @ContributesAndroidInjector
    abstract fun contributeVoucherFragment(): VoucherFragment

    @ContributesAndroidInjector
    abstract fun contributeEVoucherFragment(): EVoucherFragment
}