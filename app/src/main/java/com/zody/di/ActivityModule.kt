package com.zody.di

import com.zody.ui.business.BusinessDetailActivity
import com.zody.ui.business.photo.BusinessPhotoActivity
import com.zody.ui.evoucher.EVoucherActivity
import com.zody.ui.evoucher.detail.EVoucherDetailActivity
import com.zody.ui.expert.LocalExpertActivity
import com.zody.ui.explore.chain.ChainActivity
import com.zody.ui.explore.newbusiness.NewBusinessActivity
import com.zody.ui.explore.offline.BusinessActivity
import com.zody.ui.explore.online.OnlineShoppingActivity
import com.zody.ui.explore.promotion.PromotionActivity
import com.zody.ui.explore.qrcode.QrCodeBusinessActivity
import com.zody.ui.explore.trending.TrendingActivity
import com.zody.ui.hashtag.SearchHashTagActivity
import com.zody.ui.help.ZcoinCenterDetailActivity
import com.zody.ui.help.ZcoinCentreActivity
import com.zody.ui.leaderboard.LeaderBoardActivity
import com.zody.ui.login.LoginActivity
import com.zody.ui.luckydraw.LuckyDrawActivity
import com.zody.ui.luckydraw.code.LuckyDrawCodeActivity
import com.zody.ui.luckydraw.guide.LuckyDrawGuideActivity
import com.zody.ui.luckydraw.history.LuckyDrawHistoryActivity
import com.zody.ui.luckydraw.leaderboard.LuckyDrawLeaderBoardActivity
import com.zody.ui.luckydraw.prize.LuckyDrawPrizeActivity
import com.zody.ui.luckydraw.prize.LuckyDrawPrizeDetailActivity
import com.zody.ui.luckydraw.winner.LuckyDrawWinnerActivity
import com.zody.ui.main.MainActivity
import com.zody.ui.notification.NotificationActivity
import com.zody.ui.permission.AskLocationActivity
import com.zody.ui.photo.PhotoActivity
import com.zody.ui.post.detail.PostDetailActivity
import com.zody.ui.post.hashtag.PostOfHashTagActivity
import com.zody.ui.post.tip.PostTippedActivity
import com.zody.ui.profile.avatar.SelectAvatarActivity
import com.zody.ui.profile.edit.EditProfileActivity
import com.zody.ui.profile.referral.ReferralActivity
import com.zody.ui.qrcode.ScanQrCodeActivity
import com.zody.ui.search.SearchActivity
import com.zody.ui.setting.ChangeHostActivity
import com.zody.ui.setting.SettingActivity
import com.zody.ui.splash.SplashActivity
import com.zody.ui.tch.TheCoffeeHouseActivity
import com.zody.ui.user.UserActivity
import com.zody.ui.verify.VerifyPhoneNumberActivity
import com.zody.ui.voucher.VoucherOfCategoryActivity
import com.zody.ui.voucher.VoucherOfOtherCategoryActivity
import com.zody.ui.voucher.detail.VoucherDetailActivity
import com.zody.ui.writepost.WritePostActivity
import com.zody.ui.writepost.add.AddManualActivity
import com.zody.ui.writepost.edit.EditPostActivity
import com.zody.ui.writepost.hashtag.AddHashTagActivity
import com.zody.ui.writepost.media.SelectMediaActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Suppress("unused")
@Module
internal abstract class ActivityModule {
    @ContributesAndroidInjector
    internal abstract fun contributeSplashActivity(): SplashActivity

    @ContributesAndroidInjector
    internal abstract fun contributeLoginActivity(): LoginActivity

    @ContributesAndroidInjector(modules = [MainActivityFragmentModule::class])
    internal abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [WritePostActivityFragmentModule::class])
    internal abstract fun contributeWritePostActivity(): WritePostActivity

    @ContributesAndroidInjector
    internal abstract fun contributeEditPostActivity(): EditPostActivity

    @ContributesAndroidInjector(modules = [AddManualActivityFragmentModule::class])
    internal abstract fun contributeAddManualActivity(): AddManualActivity

    @ContributesAndroidInjector
    internal abstract fun contributeAddHashTagActivity(): AddHashTagActivity

    @ContributesAndroidInjector
    internal abstract fun contributeSelectMediaActivity(): SelectMediaActivity

    @ContributesAndroidInjector(modules = [LeaderBoardActivityFragmentModule::class])
    internal abstract fun contributeLeaderBoardActivity(): LeaderBoardActivity

    @ContributesAndroidInjector
    internal abstract fun contributeLocalExpertActivity(): LocalExpertActivity

    @ContributesAndroidInjector(modules = [PostDetailActivityFragmentModule::class])
    internal abstract fun contributePostDetailActivity(): PostDetailActivity

    @ContributesAndroidInjector
    internal abstract fun contributePostTippedActivity(): PostTippedActivity

    @ContributesAndroidInjector
    internal abstract fun contributePhotoActivity(): PhotoActivity

    @ContributesAndroidInjector(modules = [UserActivityFragmentModule::class])
    internal abstract fun contributeUserActivity(): UserActivity

    @ContributesAndroidInjector
    internal abstract fun contributeNotificationActivity(): NotificationActivity

    @ContributesAndroidInjector
    internal abstract fun contributePostSettingActivity(): SettingActivity

    @ContributesAndroidInjector
    internal abstract fun contributeEditProfileActivity(): EditProfileActivity

    @ContributesAndroidInjector
    internal abstract fun contributeChangeHostActivity(): ChangeHostActivity

    @ContributesAndroidInjector
    internal abstract fun contributeSelectAvatarActivity(): SelectAvatarActivity

    @ContributesAndroidInjector(modules = [BusinessDetailActivityFragmentModule::class])
    internal abstract fun contributeBusinessDetailActivity(): BusinessDetailActivity

    @ContributesAndroidInjector
    internal abstract fun contributeBusinessPhotoActivity(): BusinessPhotoActivity

    @ContributesAndroidInjector
    internal abstract fun contributeVoucherOfCategoryActivity(): VoucherOfCategoryActivity

    @ContributesAndroidInjector
    internal abstract fun contributeVoucherOfOtherCategoryActivity(): VoucherOfOtherCategoryActivity

    @ContributesAndroidInjector
    internal abstract fun contributeVoucherDetailActivity(): VoucherDetailActivity

    @ContributesAndroidInjector(modules = [SearchActivityFragmentModule::class])
    internal abstract fun contributeSearchActivity(): SearchActivity

    @ContributesAndroidInjector
    internal abstract fun contributeZcoinCenterActivity(): ZcoinCentreActivity

    @ContributesAndroidInjector
    internal abstract fun contributeZcoinCenterDetailActivity(): ZcoinCenterDetailActivity

    @ContributesAndroidInjector(modules = [EVoucherActivityFragmentModule::class])
    internal abstract fun contributeEVoucherActivity(): EVoucherActivity

    @ContributesAndroidInjector
    internal abstract fun contributeEVoucherDetailActivity(): EVoucherDetailActivity

    @ContributesAndroidInjector
    internal abstract fun contributePromotionActivity(): PromotionActivity

    @ContributesAndroidInjector
    internal abstract fun contributeNewBusinessActivity(): NewBusinessActivity

    @ContributesAndroidInjector
    internal abstract fun contributeTrendingActivity(): TrendingActivity

    @ContributesAndroidInjector
    internal abstract fun contributeQrCodeBusinessActivity(): QrCodeBusinessActivity

    @ContributesAndroidInjector
    internal abstract fun contributeBusinessActivity(): BusinessActivity

    @ContributesAndroidInjector
    internal abstract fun contributeChainActivity(): ChainActivity

    @ContributesAndroidInjector(modules = [OnlineShoppingActivityFragmentModule::class])
    internal abstract fun contributeOnlineShoppingActivity(): OnlineShoppingActivity

    @ContributesAndroidInjector
    internal abstract fun contributeSearchHashTagActivity(): SearchHashTagActivity

    @ContributesAndroidInjector(modules = [PostOfHashTagActivityFragmentModule::class])
    internal abstract fun contributePostOfHashTagActivity(): PostOfHashTagActivity

    @ContributesAndroidInjector
    internal abstract fun contributeVerifyPhoneActivity(): VerifyPhoneNumberActivity

    @ContributesAndroidInjector
    internal abstract fun contributeReferralActivity(): ReferralActivity

    @ContributesAndroidInjector
    internal abstract fun contributeTheCoffeeHouseActivity(): TheCoffeeHouseActivity

    @ContributesAndroidInjector
    internal abstract fun contributeAskLocationActivity(): AskLocationActivity

    @ContributesAndroidInjector
    internal abstract fun contributeScanQrCodeActivity(): ScanQrCodeActivity

    @ContributesAndroidInjector(modules = [LuckyDrawActivityFragmentModule::class])
    internal abstract fun contributeLuckyDrawActivity(): LuckyDrawActivity

    @ContributesAndroidInjector
    internal abstract fun contributeLuckyDrawPrizeActivity(): LuckyDrawPrizeActivity

    @ContributesAndroidInjector
    internal abstract fun contributeLuckyDrawPrizeDetailActivity(): LuckyDrawPrizeDetailActivity

    @ContributesAndroidInjector
    internal abstract fun contributeLuckyDrawHistoryActivity(): LuckyDrawHistoryActivity

    @ContributesAndroidInjector
    internal abstract fun contributeLuckyDrawWinnerActivity(): LuckyDrawWinnerActivity

    @ContributesAndroidInjector
    internal abstract fun contributeLuckyDrawCodeActivity(): LuckyDrawCodeActivity

    @ContributesAndroidInjector
    internal abstract fun contributeLuckyDrawLeaderBoardActivity(): LuckyDrawLeaderBoardActivity

    @ContributesAndroidInjector
    internal abstract fun contributeLuckyDrawGuideActivity(): LuckyDrawGuideActivity
}