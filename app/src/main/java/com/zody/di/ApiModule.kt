package com.zody.di

import android.app.Application
import android.content.SharedPreferences
import android.net.Uri
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializer
import com.zody.R
import com.zody.api.*
import com.zody.entity.business.BusinessRating
import com.zody.utils.SPKey
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.joda.time.format.ISODateTimeFormat
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton


@Suppress("unused")
@Module
class ApiModule {

    @Singleton
    @Provides
    fun createOkHttpClient(sp: SharedPreferences): OkHttpClient {
        return OkHttpClient.Builder()
                .addInterceptor { chain ->
                    val requestBuilder = chain.request().newBuilder()
                    sp.getString(SPKey.PREF_TOKEN, null)?.let { token ->
                        requestBuilder.addHeader("Authorization", "Bearer $token")
                    }
                    return@addInterceptor chain.proceed(requestBuilder.build())
                }
                .readTimeout(3, TimeUnit.MINUTES)
                .writeTimeout(10, TimeUnit.MINUTES)
                .connectTimeout(2, TimeUnit.MINUTES)
                .addInterceptor(HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { message -> Timber.tag("ApiService").d(message) }).apply {
                    level = HttpLoggingInterceptor.Level.BODY
                })
                .build()
    }

    @Provides
    fun createGson(): Gson {
        return GsonBuilder()
                .registerTypeAdapter(Date::class.java, JsonDeserializer<Date> { json, _, _ ->
                    json?.let {
                        try {
                            return@JsonDeserializer ISODateTimeFormat.dateTime().parseDateTime(json.asString).toDate()
                        } catch (e: Exception) {
                            Timber.tag("JsonException").e(e)
                        }
                    }
                    null
                })
                .registerTypeAdapter(ApiPaging::class.java, ApiPaging.deserializer)
                .registerTypeAdapter(ApiData::class.java, ApiData.deserializer)
                .registerTypeAdapter(BusinessRating::class.java, BusinessRating.deserializer)
                .create()
    }

    @Singleton
    @Provides
    @Named("oldVersion")
    fun createRetrofitOldVersions(sp: SharedPreferences, app: Application, client: OkHttpClient, gson: Gson): Retrofit {
        val url = Uri.parse(sp.getString(SPKey.PREF_OLD_HOST, app.getString(R.string.host_old_version)))
                .buildUpon()
                .appendPath("")
                .build().toString()
        return Retrofit.Builder()
                .baseUrl(url)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(LiveDataCallAdapterFactory())
                .build()
    }

    @Singleton
    @Provides
    fun createRetrofit(sp: SharedPreferences, app: Application, client: OkHttpClient, gson: Gson): Retrofit {
        val url = Uri.parse(sp.getString(SPKey.PREF_HOST, app.getString(R.string.host)))
                .buildUpon()
                .appendPath("")
                .build().toString()
        return Retrofit.Builder()
                .baseUrl(url)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(LiveDataCallAdapterFactory())
                .build()
    }

    @Singleton
    @Provides
    @Named("tracking")
    fun createRetrofitTracking(sp: SharedPreferences, app: Application, client: OkHttpClient, gson: Gson): Retrofit {
        val url = Uri.parse(sp.getString(SPKey.PREF_TRACKING_HOST, app.getString(R.string.host_tracking)))
                .buildUpon()
                .appendPath("")
                .build().toString()
        return Retrofit.Builder()
                .baseUrl(url)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(LiveDataCallAdapterFactory())
                .build()
    }

    @Singleton
    @Provides
    fun createServiceOldVersion(@Named("oldVersion") retrofit: Retrofit): ApiServiceOldVersion {
        return retrofit.create(ApiServiceOldVersion::class.java)
    }

    @Singleton
    @Provides
    fun createService(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }

    @Singleton
    @Provides
    fun createServiceTracking(@Named("tracking") retrofit: Retrofit): ApiTracking {
        return retrofit.create(ApiTracking::class.java)
    }
}
