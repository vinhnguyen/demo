package com.zody.di

import com.zody.ui.evoucher.EVoucherFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by vinhnguyen.it.vn on 2018, July 11
 */
@Module
abstract class EVoucherActivityFragmentModule {
    @ContributesAndroidInjector
    abstract fun contributeEVoucherFragment(): EVoucherFragment
}