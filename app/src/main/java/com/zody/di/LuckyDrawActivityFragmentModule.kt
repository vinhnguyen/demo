package com.zody.di

import com.zody.ui.luckydraw.dialog.LuckyDrawResultDialog
import com.zody.ui.luckydraw.draw.LuckyDrawFragment
import com.zody.ui.luckydraw.money.LuckyMoneyFragment
import com.zody.ui.luckydraw.money.LuckyMoneyShakeDialog
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by vinhnguyen.it.vn on 2018, July 11
 */
@Module
abstract class LuckyDrawActivityFragmentModule {

    @ContributesAndroidInjector
    abstract fun contributeLuckyDrawFragment(): LuckyDrawFragment

    @ContributesAndroidInjector
    abstract fun contributeLuckyMoneyFragment(): LuckyMoneyFragment

    @ContributesAndroidInjector
    abstract fun contributeLuckyDrawResultDialog(): LuckyDrawResultDialog

    @ContributesAndroidInjector
    abstract fun contributeLuckyMoneyShakeDialog(): LuckyMoneyShakeDialog
}