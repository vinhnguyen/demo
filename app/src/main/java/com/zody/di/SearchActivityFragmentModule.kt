package com.zody.di

import com.zody.ui.search.SearchAuthorFragment
import com.zody.ui.search.SearchBusinessFragment
import com.zody.ui.search.SearchVoucherFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by vinhnguyen.it.vn on 2018, July 11
 */
@Module
abstract class SearchActivityFragmentModule {
    @ContributesAndroidInjector
    abstract fun contributeSearchBusinessFragment(): SearchBusinessFragment

    @ContributesAndroidInjector
    abstract fun contributeSearchAuthorFragment(): SearchAuthorFragment

    @ContributesAndroidInjector
    abstract fun contributeSearchVoucherFragment(): SearchVoucherFragment
}