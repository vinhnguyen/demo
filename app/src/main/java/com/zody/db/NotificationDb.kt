package com.zody.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.zody.entity.Notification
import com.zody.entity.NotificationItem

/**
 * Created by vinhnguyen.it.vn on 2018, August 08
 */
@Database(
        entities = [NotificationItem::class, Notification::class],
        version = 6,
        exportSchema = false
)
@TypeConverters(DateConverter::class)
abstract class NotificationDb : RoomDatabase() {

    abstract fun notificationDao(): NotificationDao
}