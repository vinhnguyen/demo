package com.zody.db

import androidx.room.TypeConverter
import java.util.*


/**
 * Created by vinhnguyen.it.vn on 2018, July 11
 */
class DateConverter {
    @TypeConverter
    fun fromDate(value: Date?): Long? {
        return value?.time
    }

    @TypeConverter
    fun toDate(value: Long?): Date? {
        return if (value == null) null else Date(value)
    }
}