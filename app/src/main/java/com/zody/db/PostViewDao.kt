package com.zody.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.zody.entity.PostView
import java.util.*

/**
 * Created by vinhnguyen.it.vn on 2018, August 07
 */
@Dao
abstract class PostViewDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(item: PostView)

    @Query("SELECT time FROM post_views WHERE post_id=:id LIMIT 1")
    abstract fun get(id: String): Date

    @Query("SELECT * FROM post_views")
    abstract fun load(): List<PostView>
}