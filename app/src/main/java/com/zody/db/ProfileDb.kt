package com.zody.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.zody.entity.Profile

/**
 * Created by vinhnguyen.it.vn on 2018, July 11
 */
@Database(
        entities = [Profile::class],
        version = 6,
        exportSchema = false
)
@TypeConverters(DateConverter::class)
abstract class ProfileDb : RoomDatabase() {

    abstract fun profileDao(): ProfileDao
}