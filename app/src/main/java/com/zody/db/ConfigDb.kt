package com.zody.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.zody.entity.City
import com.zody.entity.ZcoinCenterItem
import com.zody.entity.explore.Category

/**
 * Created by vinhnguyen.it.vn on 2018, July 12
 */
@Database(
        entities = [City::class, ZcoinCenterItem::class, Category::class],
        version = 6,
        exportSchema = false)
abstract class ConfigDb : RoomDatabase() {

    abstract fun configDao(): ConfigDao
}