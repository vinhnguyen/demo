package com.zody.db

import androidx.room.TypeConverter


/**
 * Created by vinhnguyen.it.vn on 2018, July 11
 */
class StringListConverter {
    @TypeConverter
    fun fromList(value: List<String>?): String? {
        return value?.joinToString(separator = ",")
    }

    @TypeConverter
    fun toList(value: String?): List<String>? {
        return if (value?.isEmpty() == true) emptyList() else value?.split(",")
    }
}