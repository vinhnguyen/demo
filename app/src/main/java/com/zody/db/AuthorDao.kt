package com.zody.db

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.*
import com.zody.entity.Author

/**
 * Created by vinhnguyen.it.vn on 2018, July 24
 */
@Dao
abstract class AuthorDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(item: Author)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(list: List<Author>)

    @Query("SELECT * FROM authors")
    abstract fun query(): DataSource.Factory<Int, Author>

    @Query("SELECT * FROM authors WHERE author_id=:id LIMIT 1")
    abstract fun loadAuthor(id: String): Author?

    @Query("SELECT * FROM authors WHERE author_id=:id LIMIT 1")
    abstract fun loadAuthorAsLiveData(id: String): LiveData<Author>

    @Query("UPDATE authors SET author_followed=:followed WHERE author_id=:id")
    abstract fun updateFollowed(id: String, followed: Boolean)

    @Query("UPDATE authors SET author_followers=:number WHERE author_id=:id")
    abstract fun updateNumFollow(id: String, number: Int)

    @Transaction
    open fun follow(id: String) {
        loadAuthor(id)?.let {
            updateFollowed(id, true)
            updateNumFollow(id, Math.max(0, (it.statistic?.followers ?: 0) + 1))
        }
    }

    @Transaction
    open fun unFollow(id: String) {
        loadAuthor(id)?.let {
            updateFollowed(id, false)
            updateNumFollow(id, Math.max(0, (it.statistic?.followers ?: 0) - 1))
        }
    }
}