package com.zody.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.zody.entity.City
import com.zody.entity.ZcoinCenterItem
import com.zody.entity.explore.Category

/**
 * Created by vinhnguyen.it.vn on 2018, July 12
 */
@Dao
interface ConfigDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCity(cities: List<City>)

    @Query("UPDATE cities SET city_is_filter=1 WHERE city_id IN (:ids)")
    fun updateIsFilter(ids: List<String>)

    @Query("SELECT * FROM cities")
    fun loadCity(): List<City>

    @Query("SELECT * FROM cities")
    fun loadCityAsLiveData(): LiveData<List<City>>

    @Query("SELECT * FROM cities WHERE city_id=:id")
    fun loadCity(id: String): City?

    @Query("SELECT * FROM cities WHERE city_id=:id")
    fun loadCityAsLiveData(id: String): LiveData<City>

    @Query("DELETE FROM cities")
    fun deleteAllCity()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertZcoinCenters(items: List<ZcoinCenterItem>)

    @Query("SELECT * FROM zcoin_center_items ORDER BY zcoin_center_item_order DESC")
    fun loadZcoinCenters(): LiveData<List<ZcoinCenterItem>>

    @Query("SELECT * FROM zcoin_center_items WHERE zcoin_center_item_id=:id")
    fun loadZcoinCenterItem(id: String): LiveData<ZcoinCenterItem>

    @Query("DELETE from zcoin_center_items")
    fun deleteZcoinCenters()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCategories(items: List<Category>)

    @Query("SELECT * FROM categories ORDER BY category_order")
    fun loadCategories(): LiveData<List<Category>>

    @Query("DELETE FROM categories")
    fun deleteCategories()
}