package com.zody.db

import androidx.room.TypeConverter
import com.google.gson.reflect.TypeToken
import com.zody.entity.BusinessCategory

/**
 * Created by vinhnguyen.it.vn on 2018, July 24
 */
class BusinessCategoryConverter {
    @TypeConverter
    fun fromBusinessCategories(value: List<BusinessCategory>?): String? {
        return if (value == null) null else gsonForConverters.toJson(value)
    }

    @TypeConverter
    fun toBusinessCategories(value: String?): List<BusinessCategory>? {
        return if (value == null) null
        else gsonForConverters.fromJson(value, object : TypeToken<List<BusinessCategory>>() {}.type)
    }
}