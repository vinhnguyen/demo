package com.zody.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.zody.entity.Profile
import java.util.*

/**
 * Created by vinhnguyen.it.vn on 2018, July 11
 */
@Dao
interface ProfileDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProfile(profile: Profile)

    @Query("SELECT * FROM my_profile LIMIT 1")
    fun getProfile(): Profile?

    @Query("SELECT * FROM my_profile LIMIT 1")
    fun getProfileAsLiveData(): LiveData<Profile>

    @Query("UPDATE my_profile SET user_statistic_coin=:coin")
    fun updateCoin(coin: Int)

    @Query("UPDATE my_profile SET user_statistic_reward=:reward")
    fun updateReward(reward: Int)

    @Query("UPDATE my_profile SET user_avatar=:avatar")
    fun updateAvatar(avatar: String)

    @Query("SELECT user_statistic_reward FROM my_profile")
    fun getNumberReward(): Int?

    @Query("UPDATE my_profile SET user_phone=:phone, user_status_verified=:verify")
    fun updatePhoneAndVerify(phone: String?, verify: Boolean)

    @Transaction
    fun increaseCoin(number: Int) {
        getProfile()?.let {
            updateCoin(Math.max(0, it.statistic?.coin ?: 0) + number)
        }
    }

    @Transaction
    fun updateNumberReward(number: Int) {
        getProfile()?.let {
            updateReward(Math.max(0, (it.statistic?.reward ?: 0) + number))
        }
    }

    @Query("UPDATE my_profile SET user_name=:name, user_nickname=:nickname, user_gender=:gender, user_city=:city, user_desc=:desc, user_birthday=:birthday, user_is_new=0, user_force_update_data=0")
    fun updateProfile(name: String?, nickname: String?, birthday: Date?, gender: String?, city: String?, desc: String?)
}