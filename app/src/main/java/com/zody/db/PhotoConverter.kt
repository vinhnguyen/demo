package com.zody.db

import androidx.room.TypeConverter
import com.google.gson.reflect.TypeToken
import com.zody.entity.Photo

/**
 * Created by vinhnguyen.it.vn on 2018, July 24
 */
class PhotoConverter {
    @TypeConverter
    fun fromPhotos(value: List<Photo>?): String? {
        return if (value == null) null else gsonForConverters.toJson(value)
    }

    @TypeConverter
    fun toPhotos(value: String?): List<Photo>? {
        return if (value == null) null else gsonForConverters.fromJson(value, object : TypeToken<List<Photo>>() {}.type)
    }
}