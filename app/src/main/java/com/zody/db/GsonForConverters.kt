package com.zody.db

import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializer
import org.joda.time.format.ISODateTimeFormat
import timber.log.Timber
import java.util.*

/**
 * Created by vinhnguyen.it.vn on 2018, July 24
 */
val gsonForConverters = GsonBuilder().registerTypeAdapter(Date::class.java, JsonDeserializer<Date> { json, _, _ ->
    json?.let {
        try {
            return@JsonDeserializer ISODateTimeFormat.dateTime().parseDateTime(json.asString).toDate()
        } catch (e: Exception) {
            Timber.tag("JsonException").e(e)
        }
    }
    null
}).create()