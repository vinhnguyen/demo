package com.zody.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.zody.entity.*

/**
 * Created by vinhnguyen.it.vn on 2018, July 24
 */
@Database(
        entities = [PostItem::class, Post::class, Author::class, CommentItem::class, Comment::class, PostPhotoItem::class, PostView::class],
        version = 6,
        exportSchema = false
)
@TypeConverters(DateConverter::class, BusinessCategoryConverter::class, PhotoConverter::class, StringListConverter::class)
abstract class PostDb : RoomDatabase() {
    abstract fun postDao(): PostDao

    abstract fun postPhotoDao(): PostPhotoDao

    abstract fun commentDao(): CommentDao

    abstract fun authorDao(): AuthorDao

    abstract fun postViewDao(): PostViewDao
}