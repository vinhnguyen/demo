package com.zody.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.zody.entity.Author
import com.zody.entity.Post
import com.zody.entity.PostItem
import com.zody.entity.PostQueryItem

/**
 * Created by vinhnguyen.it.vn on 2018, July 24
 */
@Dao
abstract class PostDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertItem(item: PostItem)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertItems(list: List<PostItem>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertPost(list: List<Post>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertPost(post: Post)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAuthor(list: List<Author>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAuthor(author: Author)

    @Transaction
    open fun insertPostAndAuthor(post: Post) {
        post.authorId = post.author?.id
        insertPost(post)
        post.author?.let { insertAuthor(it) }
    }

    @Query("SELECT COUNT(*) FROM post_items WHERE post_filter=:filter AND post_sort=:sort")
    abstract fun getCount(filter: String, sort: String): Int

    @Query("SELECT post_order FROM post_items WHERE post_filter=:filter AND post_sort=:sort ORDER BY post_order DESC LIMIT 1")
    abstract fun getLastedOrder(filter: String, sort: String): Int?

    @Query("DELETE FROM post_items WHERE post_filter=:filter")
    abstract fun clearItems(filter: String)

    @Query("SELECT * FROM post_items LEFT JOIN posts ON ref_post_id = post_id LEFT JOIN authors ON ref_author_id = author_id WHERE post_filter=:filter AND post_sort=:sort ORDER BY post_order LIMIT :limit")
    abstract fun loadInitial(filter: String, sort: String, limit: Int): List<PostQueryItem>

    @Query("SELECT * FROM post_items LEFT JOIN posts ON ref_post_id = post_id LEFT JOIN authors ON ref_author_id = author_id WHERE post_filter=:filter AND post_sort=:sort AND post_order >= :order ORDER BY post_order LIMIT :limit")
    abstract fun loadInitial(filter: String, sort: String, order: Int, limit: Int): List<PostQueryItem>

    @Query("SELECT * FROM post_items LEFT JOIN posts ON ref_post_id = post_id LEFT JOIN authors ON ref_author_id = author_id WHERE post_filter=:filter AND post_sort=:sort AND post_order > :order ORDER BY post_order LIMIT :limit")
    abstract fun loadAfter(filter: String, sort: String, order: Int, limit: Int): List<PostQueryItem>

    @Query("SELECT * FROM post_items LEFT JOIN posts ON ref_post_id = post_id LEFT JOIN authors ON ref_author_id = author_id WHERE post_filter=:filter AND post_sort=:sort AND post_order < :order ORDER BY post_order DESC LIMIT :limit")
    abstract fun loadBefore(filter: String, sort: String, order: Int, limit: Int): List<PostQueryItem>

    @Query("SELECT * FROM posts LEFT JOIN authors ON post_author_id = author_id WHERE post_id=:id LIMIT 1")
    abstract fun loadPostDetail(id: String): LiveData<PostQueryItem>

    @Query("SELECT * FROM posts WHERE post_id = :id")
    abstract fun loadPost(id: String): Post?

    @Update(onConflict = OnConflictStrategy.REPLACE)
    abstract fun updatePost(post: Post)

    @Query("DELETE FROM post_items WHERE ref_post_id=:id")
    abstract fun deletePostItem(id: String)

    @Query("DELETE FROM posts WHERE post_id=:id")
    abstract fun deletePost(id: String)

    @Transaction
    open fun increaseCommentOfPost(postId: String) {
        loadPost(postId)?.let {
            it.statistic?.comments = (it.statistic?.comments ?: 0) + 1
            updatePost(it)
        }
    }

    @Transaction
    open fun increaseTipsOfPost(postId: String, number: Int) {
        loadPost(postId)?.let {
            it.statistic?.tipped = Math.max((it.statistic?.tipped ?: 0) + number, 0)
            it.currentUserTipped = Math.max((it.currentUserTipped ?: 0) + number, 0)
            updatePost(it)
        }
    }

    @Transaction
    open fun deletePostAndItem(postId: String) {
        deletePostItem(postId)
        deletePost(postId)
    }

    @Transaction
    open fun increaseViewOfPost(postId: String, number: Int = 1) {
        loadPost(postId)?.let {
            it.statistic?.views = Math.max((it.statistic?.views ?: 0) + number, 0)
            updatePost(it)
        }
    }
}