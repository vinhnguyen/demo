package com.zody.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.zody.entity.Comment
import com.zody.entity.CommentItem
import com.zody.entity.CommentQueryItem

/**
 * Created by vinhnguyen.it.vn on 2018, July 27
 */
@Dao
abstract class CommentDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertItem(item: CommentItem)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertItems(list: List<CommentItem>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertComment(list: List<Comment>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertComment(comment: Comment)

    @Query("SELECT comment_order FROM comment_items WHERE comment_post_id=:postId ORDER BY comment_order DESC LIMIT 1")
    abstract fun getLastedOrder(postId: String): Int?

    @Query("SELECT comment_order FROM comment_items WHERE comment_post_id=:postId ORDER BY comment_order LIMIT 1")
    abstract fun getNewestOrder(postId: String): Int?

    @Query("DELETE FROM comment_items WHERE comment_post_id=:postId")
    abstract fun clearItems(postId: String)

    @Query("SELECT * FROM comment_items LEFT JOIN comments ON ref_comment_id = comment_id WHERE comment_post_id=:postId ORDER BY comment_order LIMIT :limit")
    abstract fun loadInitial(postId: String, limit: Int): List<CommentQueryItem>

    @Query("SELECT * FROM comment_items LEFT JOIN comments ON ref_comment_id = comment_id WHERE comment_post_id=:postId AND comment_order >= :order ORDER BY comment_order LIMIT :limit")
    abstract fun loadInitial(postId: String, order: Int, limit: Int): List<CommentQueryItem>

    @Query("SELECT * FROM comment_items LEFT JOIN comments ON ref_comment_id = comment_id WHERE comment_post_id=:postId AND comment_order > :order ORDER BY comment_order LIMIT :limit")
    abstract fun loadAfter(postId: String, order: Int, limit: Int): List<CommentQueryItem>

    @Query("SELECT * FROM comment_items LEFT JOIN comments ON ref_comment_id = comment_id WHERE comment_post_id=:postId AND comment_order < :order ORDER BY comment_order DESC LIMIT :limit")
    abstract fun loadBefore(postId: String, order: Int, limit: Int): List<CommentQueryItem>
}