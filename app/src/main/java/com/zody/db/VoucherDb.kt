package com.zody.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.zody.entity.voucher.VoucherCategory

/**
 * Created by vinhnguyen.it.vn on 2019, February 17
 */
@Database(
        entities = [VoucherCategory::class],
        version = 6,
        exportSchema = false)
abstract class VoucherDb : RoomDatabase() {

    abstract fun voucherCategoryDao(): VoucherCategoryDao
}