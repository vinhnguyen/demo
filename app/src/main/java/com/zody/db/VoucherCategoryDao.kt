package com.zody.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.zody.entity.voucher.VoucherCategory

/**
 * Created by vinhnguyen.it.vn on 2019, February 17
 */
@Dao
interface VoucherCategoryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(list: List<VoucherCategory>)

    @Query("SELECT * FROM voucher_category ORDER BY voucher_category_order, voucher_category_name")
    fun load(): LiveData<List<VoucherCategory>>

    @Query("DELETE FROM voucher_category")
    fun deleteAll()
}