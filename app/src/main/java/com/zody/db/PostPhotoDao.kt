package com.zody.db

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.zody.entity.PostPhotoItem

/**
 * Created by vinhnguyen.it.vn on 2018, July 31
 */
@Dao
abstract class PostPhotoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(item: PostPhotoItem)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertItems(list: List<PostPhotoItem>)

    @Query("SELECT photo_order FROM post_photos_of_users WHERE photo_author_id=:authorId ORDER BY photo_order DESC LIMIT 1")
    abstract fun getLastedOrder(authorId: String): Int?

    @Query("DELETE FROM post_photos_of_users WHERE photo_author_id=:authorId")
    abstract fun clearItems(authorId: String)

    @Query("SELECT * FROM post_photos_of_users WHERE photo_author_id=:authorId ORDER BY photo_order")
    abstract fun loadPhotoOf(authorId: String): DataSource.Factory<Int, PostPhotoItem>

    @Query("DELETE FROM post_photos_of_users WHERE photo_post_id=:id")
    abstract fun deletePhotoOfPost(id: String)
}