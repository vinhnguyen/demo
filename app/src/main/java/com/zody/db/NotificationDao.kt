package com.zody.db

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.zody.entity.Notification
import com.zody.entity.NotificationItem
import com.zody.entity.NotificationQueryItem

/**
 * Created by vinhnguyen.it.vn on 2018, August 08
 */
@Dao
abstract class NotificationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(item: Notification)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(items: List<Notification>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertItem(item: NotificationItem)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertItems(items: List<NotificationItem>)

    @Query("SELECT * FROM notifications WHERE notification_id =:id LIMIT 1")
    abstract fun load(id: String): Notification?

    @Query("UPDATE notifications SET notification_read=:read WHERE notification_id=:id")
    abstract fun setRead(id: String, read: Boolean)

    @Query("SELECT item_order FROM notification_item ORDER BY item_order DESC LIMIT 1")
    abstract fun getLastedOrder(): Int?

    @Query("DELETE FROM notifications")
    abstract fun deleteAll()

    @Query("DELETE FROM notification_item")
    abstract fun deleteAllItems()

    @Query("SELECT * FROM notification_item LEFT JOIN notifications ON ref_notification_id = notification_id ORDER BY item_order")
    abstract fun load(): DataSource.Factory<Int, NotificationQueryItem>
}