package com.zody

import android.app.*
import android.content.Context
import android.os.Build
import androidx.core.provider.FontRequest
import androidx.emoji.text.EmojiCompat
import androidx.emoji.text.FontRequestEmojiCompatConfig
import androidx.multidex.MultiDex
import com.crashlytics.android.Crashlytics
import com.zody.di.AppInjector
import com.zody.utils.AlphaTree
import com.zody.utils.ReleaseTree
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.HasServiceInjector
import io.fabric.sdk.android.Fabric
import timber.log.Timber
import javax.inject.Inject


/**
 * Created by vinhnguyen.it.vn on 2018, June 27
 */
class App : Application(), HasActivityInjector, HasServiceInjector {
    @Inject
    lateinit var dispatchingAndroidActivityInjector: DispatchingAndroidInjector<Activity>

    @Inject
    lateinit var dispatchingAndroidServiceInjector: DispatchingAndroidInjector<Service>

    override fun onCreate() {
        super.onCreate()
        if (resources.getBoolean(R.bool.fabric_enable)) {
            Fabric.with(this, Crashlytics())
        }

        Timber.plant(
                when (BuildConfig.BUILD_TYPE) {
                    "release" -> ReleaseTree()
                    "alpha" -> AlphaTree()
                    else -> Timber.DebugTree()
                }
        )

        AppInjector.init(this)

        initNotificationChannel()
        initEmoji()
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this);
    }

    override fun activityInjector() = dispatchingAndroidActivityInjector

    override fun serviceInjector() = dispatchingAndroidServiceInjector

    private fun initNotificationChannel() {
        val appName = getString(R.string.app_name)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(appName, appName, NotificationManager.IMPORTANCE_HIGH)
            (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).createNotificationChannel(channel)
        }
    }

    private fun initEmoji() {
        val fontRequest = FontRequest(
                "com.google.android.gms.fonts",
                "com.google.android.gms",
                "Noto Color Emoji Compat",
                R.array.com_google_android_gms_fonts_certs)
        val config = FontRequestEmojiCompatConfig(applicationContext, fontRequest)
                .setReplaceAll(true)
        EmojiCompat.init(config)
    }
}
