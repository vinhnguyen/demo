package com.zody.livedata

import androidx.lifecycle.LiveData
import com.zody.entity.City
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by vinhnguyen.it.vn on 2018, August 29
 */
@Singleton
class ManualCityLiveData @Inject constructor() : LiveData<City>() {

    init {
        value = null
    }

    fun manualUpdate(city: City?) {
        if (value?.id != city?.id) {
            value = city
        }
    }
}