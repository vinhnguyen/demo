package com.zody.livedata

import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.lifecycle.LiveData
import com.zody.utils.SPKey
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by vinhnguyen.it.vn on 2018, August 28
 */
@Singleton
class UnreadNotificationLiveData @Inject constructor(private val sp: SharedPreferences) : LiveData<MutableSet<String>>() {

    init {
        value = sp.getStringSet(SPKey.PREF_UNREAD_NOTIFICATION, emptySet())
    }

    fun init(set: MutableSet<String>) {
        postValue(set)
        save(set)
    }

    fun add(id: String) {
        val set = value ?: mutableSetOf()
        if (!set.contains(id)) {
            set.add(id)
            postValue(set)
            save(set)
        }
    }

    fun remove(id: String) {
        val set = value
        if (set?.remove(id) == true) {
            postValue(set)
            save(set)
        }
    }

    fun clear() {
        val empty = mutableSetOf<String>()
        postValue(empty)
        save(empty)
    }

    private fun save(set: Set<String>) {
        sp.edit {
            putStringSet(SPKey.PREF_UNREAD_NOTIFICATION, set)
        }
    }
}