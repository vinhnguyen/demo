package com.zody.livedata

import org.json.JSONException
import org.json.JSONObject
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by vinhnguyen.it.vn on 2018, August 17
 */
@Singleton
class MessageFromSocketLiveData @Inject constructor(socketLiveData: SocketLiveData) : SingleLiveEvent<String>() {

    init {
        addSource(socketLiveData) { socket ->
            socket?.on("show-popup") {
                Timber.tag(SocketLiveData.TAG).d("socket show popup ${it.joinToString()}")
                if (it.isNotEmpty()) {
                    try {
                        postValue(JSONObject(it[0].toString()).getString("message"))
                    } catch (e: JSONException) {

                    }
                }
            }
        }
    }
}