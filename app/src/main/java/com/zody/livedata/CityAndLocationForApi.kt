package com.zody.livedata

import androidx.lifecycle.MediatorLiveData
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, November 13
 */
class CityAndLocationForApi @Inject constructor(city: CityLiveData,
                                                location: LocationForApiLiveData) : MediatorLiveData<CityAndLocationForApi.Data>() {

    init {
        addSource(city) { c ->
            removeSource(location)
            addSource(location) { l ->
                if (value?.city != c?.id || value?.latitude != l?.latitude || value?.longitude != l?.longitude) {
                    value = Data(c?.id, l?.latitude, l?.longitude)
                }
            }
        }
    }

    data class Data(val city: String?, val latitude: Double?, val longitude: Double?)
}