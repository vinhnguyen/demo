package com.zody.livedata

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Transformations
import com.zody.entity.City
import com.zody.repository.ConfigurationRepository
import com.zody.repository.ProfileRepository
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by vinhnguyen.it.vn on 2018, August 29
 */
@Singleton
class CityLiveData @Inject constructor(currentCity: CurrentCityLiveData,
                                       manualCity: ManualCityLiveData,
                                       profileRepository: ProfileRepository,
                                       configurationRepository: ConfigurationRepository) : MediatorLiveData<City?>() {

    init {
        val profile = profileRepository.loadProfileFromDB()
        val profileCity = Transformations.switchMap(profile) {
            configurationRepository.getCity(it?.city)
        }

        addSource(manualCity) { manual ->
            removeSource(currentCity)
            if (manual != null) {
                valueIfDifferent(manual)
            } else {
                addSource(currentCity) { current ->
                    removeSource(profileCity)
                    if (current != null) {
                        valueIfDifferent(current)
                    } else {
                        addSource(profileCity) {
                            valueIfDifferent(it)
                        }
                    }
                }
            }
        }
    }

    private fun valueIfDifferent(city: City?) {
        if (value?.id != city?.id) {
            value = city
        }
    }
}