package com.zody.livedata

import androidx.lifecycle.MediatorLiveData
import com.zody.entity.City
import com.zody.repository.ConfigurationRepository
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by vinhnguyen.it.vn on 2018, November 13
 */
@Singleton
class CurrentCityLiveData @Inject constructor(location: LocationLiveData,
                                              configurationRepository: ConfigurationRepository) : MediatorLiveData<City>() {

    init {
        addSource(location) {
            if (it == null) {
                value = null
            } else {
                val nearestCity = configurationRepository.getNearestCity(it.latitude, it.longitude)
                addSource(nearestCity) { city ->
                    removeSource(nearestCity)
                    if (value?.id != city?.id) {
                        value = city
                    }
                }
            }
        }
    }
}