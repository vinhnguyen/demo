package com.zody.livedata

import com.google.gson.Gson
import com.zody.entity.luckydraw.NewTurnFromSocket
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2019, January 29
 */
class LuckyDrawTurnFromSocketLiveData @Inject constructor(socketLiveData: SocketLiveData,
                                                          gson: Gson) : SingleLiveEvent<NewTurnFromSocket>() {

    init {
        addSource(socketLiveData) { socket ->
            socket?.on("newLuckyMoneyTurn") {
                Timber.tag(SocketLiveData.TAG).d("socket new lucky turn ${it?.joinToString()}")
                if (it?.isNotEmpty() == true) {
                    gson.fromJson<NewTurnFromSocket>(it[0].toString(), NewTurnFromSocket::class.java)?.let { data ->
                        if (data.luckyDraw?.hasSupport == true) {
                            postValue(data)
                        }
                    }
                }
            }
        }
    }

}