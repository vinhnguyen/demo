package com.zody.livedata

import android.location.Location
import androidx.lifecycle.MediatorLiveData
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by vinhnguyen.it.vn on 2018, October 18
 */
@Singleton
class LocationForApiLiveData @Inject constructor(manualCity: ManualCityLiveData,
                                                 currentCity: CurrentCityLiveData,
                                                 location: LocationLiveData) : MediatorLiveData<Location>() {

    init {
        addSource(manualCity) { manual ->
            removeSource(currentCity)
            removeSource(location)
            if (manual != null) {
                addSource(currentCity) { current ->
                    removeSource(location)
                    if (manual.id == current?.id) {
                        addSource(location) {
                            value = it
                        }
                    } else {
                        value = null
                    }
                }
            } else {
                addSource(location) {
                    value = it
                }
            }
        }
    }
}