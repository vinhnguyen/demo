package com.zody.livedata

import android.Manifest
import android.app.Application
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by vinhnguyen.it.vn on 2018, November 09
 */
@Singleton
class LocationEnableLiveData @Inject constructor(private val app: Application) : LiveData<Boolean>() {

    override fun onActive() {
        super.onActive()
        if (value == null) {
            update()
        }
    }

    fun update() {
        if (ContextCompat.checkSelfPermission(app, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (value != false) value = false
            return
        }
        val locationRequest = LocationRequest().apply {
            interval = 0
            fastestInterval = 0
            smallestDisplacement = 1000F
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
        val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest)
        val client: SettingsClient = LocationServices.getSettingsClient(app)
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())
        task.addOnCompleteListener {
            val enabled = it.isSuccessful
            if (value != enabled) value = enabled
        }
    }
}