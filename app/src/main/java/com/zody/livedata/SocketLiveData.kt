package com.zody.livedata

import android.app.Application
import android.content.SharedPreferences
import android.os.Handler
import androidx.lifecycle.MediatorLiveData
import com.zody.BuildConfig
import com.zody.R
import com.zody.repository.ProfileRepository
import com.zody.utils.SPKey
import com.zody.utils.TimeManager
import io.socket.client.IO
import io.socket.client.Manager
import io.socket.client.Socket
import io.socket.engineio.client.Transport
import org.json.JSONException
import org.json.JSONObject
import timber.log.Timber
import java.util.concurrent.atomic.AtomicBoolean
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by vinhnguyen.it.vn on 2018, August 17
 */
@Singleton
class SocketLiveData @Inject constructor(private val app: Application,
                                         private val sp: SharedPreferences,
                                         @Named("deviceId")
                                         private val deviceId: String,
                                         private val unreadNotification: UnreadNotificationLiveData,
                                         private val profileRepository: ProfileRepository,
                                         private val timeManager: TimeManager) : MediatorLiveData<Socket>() {

    companion object {
        const val TAG = "AppSocket"
    }

    private val handler = Handler()
    private val disconnect = Runnable {
        value?.disconnect()
    }
    private val delayToDisconnect = 5 * 1000L
    private val connecting = AtomicBoolean(false)

    init {
        addSource(profileRepository.onLogin) {
            Timber.tag(TAG).d("socket login")
            initAndConnect()
        }
        addSource(profileRepository.onLogout) {
            Timber.tag(TAG).d("socket logout")
            emitLogout()
        }
    }

    override fun onActive() {
        super.onActive()
        initAndConnect()
    }

    override fun onInactive() {
        super.onInactive()
        handler.postDelayed(disconnect, delayToDisconnect)
    }

    private fun initAndConnect() {
        if (value == null) {
            sp.getString(SPKey.PREF_TOKEN, null)?.let { token ->
                val opts = IO.Options()
                opts.forceNew = true
                opts.secure = true
                opts.transports = arrayOf("websocket")
                opts.query = String.format("token=%s", token)
                opts.query += String.format("&version=%s", BuildConfig.VERSION_NAME)
                opts.query += String.format("&deviceId=%s", deviceId)
                opts.query += String.format("&useFCMKeyId=%s", app.getString(R.string.project_id))

                sp.getString(SPKey.PREF_DEVICE_TOKEN, null)?.let { deviceToken ->
                    opts.query += String.format("&registrationToken=%s", deviceToken)
                }

                val host = sp.getString(SPKey.PREF_HOST, app.getString(R.string.host))
                value = IO.socket(host, opts).apply {
                    io().on(Manager.EVENT_TRANSPORT) { args ->
                        if (args.isNotEmpty()) {
                            (args[0] as Transport).on(Transport.EVENT_REQUEST_HEADERS) {
                                if (it.isNotEmpty()) {
                                    @Suppress("UNCHECKED_CAST")
                                    (it[0] as MutableMap<String, List<String>>).apply {
                                        this["user-agent"] = listOf(System.getProperty("http.agent"))
                                    }
                                }
                            }
                        }
                    }
                    on(Socket.EVENT_CONNECTING) {
                        Timber.tag(TAG).d("socket connecting ${it.joinToString()}")
                        emit("data")
                        connecting.set(true)
                    }
                    on(Socket.EVENT_CONNECT) {
                        Timber.tag(TAG).d("socket connected ${it.joinToString()}")
                        connecting.set(false)
                    }
                    on(Socket.EVENT_DISCONNECT) {
                        Timber.tag(TAG).d("socket disconnected ${it.joinToString()}")
                        postValue(null)
                        connecting.set(false)
                    }
                    on(Socket.EVENT_CONNECT_ERROR) {
                        Timber.tag(TAG).d("socket connect error ${it.joinToString()}")
                        connecting.set(false)
                    }
                    on(Socket.EVENT_CONNECT_TIMEOUT) {
                        Timber.tag(TAG).d("socket connect timeout ${it.joinToString()}")
                        connecting.set(false)
                    }
                    on(Socket.EVENT_ERROR) {
                        Timber.tag(TAG).d("socket error ${it.joinToString()}")
                    }
                    on("data") {
                        if (it.isNotEmpty()) {
                            receiveData(it[0].toString())
                        }
                        Timber.tag(TAG).d("socket data ${it.joinToString()}")
                    }
                    on("request-new-token") {
                        Timber.tag(TAG).d("socket request new token ${it.joinToString()}")
                        profileRepository.requestNewToken()
                    }
                }
            }
        }

        value?.apply {
            handler.removeCallbacks(disconnect)
            if (!connecting.get() && !connected()) {
                connecting.set(true)
                connect()
            }
        }
    }

    private fun emitLogout() {
        value?.apply {
            val json = JSONObject()
            try {
                json.put("deviceId", deviceId)
                sp.getString(SPKey.PREF_TOKEN, null)?.let { token ->
                    json.put("registrationToken", token)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            emit("logout", json.toString())
            emit("recent-logout", json.toString())

            disconnect()
        }
    }

    private fun receiveData(data: String) {
        try {
            val json = JSONObject(data)
            json.optJSONArray("unreadNotifications")?.apply {
                val set = mutableSetOf<String>().apply {
                    for (i in 0 until length()) {
                        optString(i)?.let {
                            add(it)
                        }
                    }
                }
                unreadNotification.init(set)
            }
            json.optString("now")?.let { now ->
                timeManager.setTimeFromServer(now)
            }
        } catch (e: Exception) {

        }
    }
}