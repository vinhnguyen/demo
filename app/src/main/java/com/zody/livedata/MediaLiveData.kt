package com.zody.livedata

import android.Manifest
import android.app.Application
import android.content.ContentUris
import android.content.pm.PackageManager
import android.database.ContentObserver
import android.net.Uri
import android.os.Handler
import android.provider.MediaStore.Images.Media.*
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 17
 */
class MediaLiveData @Inject constructor(val app: Application) : LiveData<List<Uri>>() {

    private val observer = object : ContentObserver(Handler()) {
        override fun onChange(selfChange: Boolean, uri: Uri?) {
            super.onChange(selfChange, uri)
            updateValue()
        }
    }

    override fun onActive() {
        super.onActive()
        updateValue()

        app.contentResolver.registerContentObserver(EXTERNAL_CONTENT_URI, true, observer)
    }

    override fun onInactive() {
        super.onInactive()
        app.contentResolver.unregisterContentObserver(observer)
    }

    private fun updateValue() {
        if (ContextCompat.checkSelfPermission(app, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) return
        val list = mutableListOf<Uri>()
        val what = arrayOf(_ID, DATE_TAKEN)
        val where = "$MIME_TYPE LIKE 'image/%'"
        val cursor = query(app.contentResolver, EXTERNAL_CONTENT_URI, what, where, null, "$DATE_TAKEN DESC")
        while (cursor.moveToNext()) {
            val id = cursor.getLong(cursor.getColumnIndex(_ID))
            list.add(ContentUris.withAppendedId(EXTERNAL_CONTENT_URI, id))
        }
        cursor.close()

        postValue(list)
    }
}