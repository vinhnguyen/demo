package com.zody.livedata

import android.annotation.SuppressLint
import android.app.Application
import android.location.Location
import androidx.lifecycle.MediatorLiveData
import com.google.android.gms.location.*
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by vinhnguyen.it.vn on 2018, July 22
 */
@Singleton
class LocationLiveData @Inject constructor(app: Application,
                                           locationEnable: LocationEnableLiveData) : MediatorLiveData<Location?>() {

    private val fusedLocationClient: FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(app)

    private val callback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            super.onLocationResult(locationResult)
            locationResult?.locations?.let {
                if (it.isNotEmpty()) {
                    val location = it.last()
                    postValue(location)
                    fusedLocationClient.removeLocationUpdates(this)
                }
            }
        }
    }

    init {
        addSource(locationEnable) {
            if (it && value == null) {
                requestLocation()
            } else {
                value = null
            }
        }
    }

    override fun onInactive() {
        super.onInactive()
        fusedLocationClient.removeLocationUpdates(callback)
    }

    @SuppressLint("MissingPermission")
    private fun requestLocation() {
        fusedLocationClient.lastLocation.addOnCompleteListener {
            val location = it.result
            postValue(location)
        }
        val locationRequest = LocationRequest().apply {
            interval = 0
            fastestInterval = 0
            smallestDisplacement = 1000F
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
        fusedLocationClient.requestLocationUpdates(locationRequest, callback, null)
    }
}