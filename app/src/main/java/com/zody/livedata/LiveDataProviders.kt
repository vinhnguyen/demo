package com.zody.livedata

import androidx.lifecycle.LiveData

/**
 * Created by vinhnguyen.it.vn on 2018, September 08
 */
object LiveDataProviders {

    fun <T> createAbsent(): LiveData<T> {
        return createSingleValue(null)
    }

    fun <T> createSingleValue(value: T?): LiveData<T> {
        return object : LiveData<T>() {
            init {
                postValue(value)
            }
        }
    }
}