package com.zody.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.paging.PagedList

/**
 * Created by vinhnguyen.it.vn on 2018, July 30
 */
data class ListingData<T>(val pagedList: PagedList<T>?, val endData: Boolean) {

    companion object {
        fun <T> createLiveData(pagedList: LiveData<PagedList<T>>, endData: LiveData<Boolean>): LiveData<ListingData<T>> {
            val result = MediatorLiveData<ListingData<T>>()
            result.addSource(pagedList) {
                result.value = ListingData(it, endData.value ?: false)
            }
            result.addSource(endData) {
                result.value = ListingData(pagedList.value, it)
            }
            return result
        }
    }
}