package com.zody.repository

import android.app.Application
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.zody.api.ApiData
import com.zody.api.ApiResponse
import com.zody.api.ApiService
import com.zody.api.ApiServiceOldVersion
import com.zody.db.PostDb
import com.zody.entity.Photo
import com.zody.entity.Post
import com.zody.entity.PostPhotoItem
import com.zody.utils.AppExecutors
import com.zody.utils.bodyOf
import com.zody.utils.toPart
import retrofit2.Retrofit
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by vinhnguyen.it.vn on 2018, July 27
 */
@Singleton
class WritePostRepository @Inject constructor(private val app: Application,
                                              private val appExecutors: AppExecutors,
                                              private val retrofit: Retrofit,
                                              private val apiService: ApiService,
                                              private val apiServiceOldVersion: ApiServiceOldVersion,
                                              private val postDb: PostDb) {

    companion object {
        private const val TAG = "WritePost"
    }

    private fun sendPost(businessId: String?, point: Int?, feedback: String?, media: List<String>?, tags: List<String>?): LiveData<Result<Post>> {
        return object : NetworkResource<Post, ApiData<Post>>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<ApiData<Post>>> {
                return apiService.writePost(bodyOf(
                        "business" to businessId,
                        "feedback" to feedback,
                        "point" to point,
                        "photos" to media,
                        "hashtags" to tags
                ))
            }

            override fun transfer(item: ApiData<Post>?): Post? {
                return item?.data
            }
        }.asLiveData()
    }

    fun writePost(businessId: String?, point: Int?, feedback: String?, media: List<Uri>?, tags: List<String>?): LiveData<Result<Post>> {
        val result = MediatorLiveData<Result<Post>>()
        if (media?.isNotEmpty() != true) {
            Timber.tag(TAG).d("send post without media")
            return sendPost(businessId, point, feedback, null, tags)
        }
        result.postValue(Result.loading())
        appExecutors.networkIO.execute {
            val ids = mutableListOf<String>()
            try {
                Timber.tag(TAG).d("compress all images")
                val parts = media.map {
                    it.toPart(context = app)
                }.filterNotNull()
                Timber.tag(TAG).d("start upload")
                for (i in 0 until parts.size) {
                    Timber.tag(TAG).d("uploading image ${i + 1}")
                    val response = apiServiceOldVersion.uploadPhoto(parts[i]).execute()
                    if (!response.isSuccessful) {
                        val body = response.errorBody()
                        if (body != null) {
                            val converter = retrofit.responseBodyConverter<ApiResponse<Photo>>(ApiResponse::class.java, ApiResponse::class.java.annotations)
                            result.postValue(Result.error(message = converter.convert(body).message))
                        } else {
                            result.postValue(Result.error(message = ""))
                        }
                        Timber.tag(TAG).d("upload error ${result.value?.message}")
                        break
                    }
                    response.body()?.data?.let {
                        Timber.tag(TAG).d("upload success ${it.id} ${it.url}")
                        ids.add(it.id)
                    }
                }
                appExecutors.mainThread.execute {
                    Timber.tag(TAG).d("send post with media")
                    val writePostResult = sendPost(businessId, point = point, feedback = feedback, media = ids, tags = tags)
                    result.addSource(writePostResult) {
                        if (!it.loading) {
                            result.removeSource(writePostResult)
                        }
                        result.postValue(it)
                    }
                }
            } catch (e: Exception) {
                Timber.tag(TAG).d("error $e")
                result.postValue(Result.error(message = e.message))
            }
        }
        return result
    }

    private fun sendEditPost(postId: String, point: Int?, feedback: String?, media: List<String>?, tags: List<String>?): LiveData<Result<Post>> {
        return object : NetworkResource<Post, ApiData<Post>>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<ApiData<Post>>> {
                return apiService.editPost(postId, bodyOf(
                        "feedback" to feedback,
                        "point" to point,
                        "photos" to media,
                        "hashtags" to tags
                ))
            }

            override fun transfer(item: ApiData<Post>?): Post? {
                return item?.data
            }

            override fun saveCallResult(item: ApiData<Post>?) {
                item?.data?.let { post ->
                    appExecutors.diskIO.execute {
                        postDb.postDao().insertPostAndAuthor(post)
                        if (post.photos?.isNotEmpty() == true) {
                            val postPhotoItem = PostPhotoItem(post.photos!![0], postId = post.id, authorId = "me", next = null, order = -1)
                            postDb.postPhotoDao().insert(postPhotoItem)
                        } else {
                            postDb.postPhotoDao().deletePhotoOfPost(post.id)
                        }
                    }
                }
            }
        }.asLiveData()
    }

    fun editPost(postId: String, point: Int?, feedback: String?, uploaded: List<String>?, media: List<Uri>?, tags: List<String>?): LiveData<Result<Post>> {
        val result = MediatorLiveData<Result<Post>>()
        if (media?.isNotEmpty() != true) {
            Timber.tag(TAG).d("send post without media")
            return sendEditPost(postId, point, feedback, uploaded, tags)
        }
        result.postValue(Result.loading())
        appExecutors.networkIO.execute {
            val ids = mutableListOf<String>()
            if (uploaded != null) ids.addAll(uploaded)
            try {
                Timber.tag(TAG).d("compress all images")
                val parts = media.map {
                    it.toPart(context = app)
                }.filterNotNull()
                Timber.tag(TAG).d("start upload")
                for (i in 0 until parts.size) {
                    Timber.tag(TAG).d("uploading image ${i + 1}")
                    val response = apiServiceOldVersion.uploadPhoto(parts[i]).execute()
                    if (!response.isSuccessful) {
                        val body = response.errorBody()
                        if (body != null) {
                            val converter = retrofit.responseBodyConverter<ApiResponse<Photo>>(ApiResponse::class.java, ApiResponse::class.java.annotations)
                            result.postValue(Result.error(message = converter.convert(body).message))
                        } else {
                            result.postValue(Result.error(message = ""))
                        }
                        Timber.tag(TAG).d("upload error ${result.value?.message}")
                        break
                    }
                    response.body()?.data?.let {
                        Timber.tag(TAG).d("upload success ${it.id} ${it.url}")
                        ids.add(it.id)
                    }
                }
                appExecutors.mainThread.execute {
                    Timber.tag(TAG).d("send post with media")
                    val editPostResult = sendEditPost(postId, point = point, feedback = feedback, media = ids, tags = tags)
                    result.addSource(editPostResult) {
                        if (!it.loading) {
                            result.removeSource(editPostResult)
                        }
                        result.postValue(it)
                    }
                }
            } catch (e: Exception) {
                Timber.tag(TAG).d("error $e")
                result.postValue(Result.error(message = e.message))
            }
        }
        return result
    }

    fun checkContent(content: String?): LiveData<Result<Unit>> {
        return object : NetworkResource<Unit, Unit>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<Unit>> {
                return apiService.checkContentForWritePost(content)
            }

        }.asLiveData()
    }
}