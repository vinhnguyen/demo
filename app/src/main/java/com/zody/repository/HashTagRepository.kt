package com.zody.repository

import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.lifecycle.LiveData
import com.zody.api.ApiPaging
import com.zody.api.ApiResponse
import com.zody.api.ApiService
import com.zody.entity.HashTagAndCounter
import com.zody.livedata.LiveDataProviders
import com.zody.utils.AppExecutors
import com.zody.utils.SPKey
import com.zody.utils.VietnameseComparator
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 19
 */
class HashTagRepository @Inject constructor(private val appExecutors: AppExecutors,
                                            private val sp: SharedPreferences,
                                            private val apiService: ApiService) {

    fun loadSuggestionHashTag(keyword: String?): LiveData<Result<List<String>>> {
        return object : NetworkResource<List<String>, ApiPaging<HashTagAndCounter>>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<ApiPaging<HashTagAndCounter>>> {
                return apiService.loadHashTag(keyword = keyword)
            }

            override fun transfer(item: ApiPaging<HashTagAndCounter>?): List<String>? {
                //Must sort with alphabet
                return item?.data?.asSequence()?.mapNotNull { it.name }?.sortedWith(VietnameseComparator())?.toList()
            }
        }.asLiveData()
    }

    fun loadTrendingHashTag(): LiveData<Result<List<String>>> {
        return object : NetworkBoundResource<List<String>, ApiPaging<String>>(appExecutors) {
            override fun saveCallResult(item: ApiPaging<String>) {
                sp.edit {
                    putStringSet(SPKey.PREF_TRENDING_HASH_TAG, item.data?.toSet())
                }
            }

            override fun loadFromDb(): LiveData<List<String>> {
                return LiveDataProviders.createSingleValue(sp.getStringSet(SPKey.PREF_TRENDING_HASH_TAG, null)?.toList())
            }

            override fun createCall(): LiveData<ApiResponse<ApiPaging<String>>> {
                return apiService.loadTrendingHashTag()
            }

            override fun shouldFetch(data: List<String>?): Boolean {
                return true
            }

        }.asLiveData()
    }

    fun searchHashTag(keyword: String?): LiveData<Result<List<HashTagAndCounter>>> {
        return object : NetworkResource<List<HashTagAndCounter>, ApiPaging<HashTagAndCounter>>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<ApiPaging<HashTagAndCounter>>> {
                return apiService.loadHashTag(keyword = keyword)
            }

            override fun transfer(item: ApiPaging<HashTagAndCounter>?): List<HashTagAndCounter>? {
                return item?.data
            }

        }.asLiveData()
    }

}