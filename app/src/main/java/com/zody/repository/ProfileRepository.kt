package com.zody.repository

import android.annotation.SuppressLint
import android.app.Application
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import androidx.core.content.edit
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import com.crashlytics.android.Crashlytics
import com.google.firebase.iid.FirebaseInstanceId
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.zody.R
import com.zody.api.*
import com.zody.db.NotificationDb
import com.zody.db.PostDb
import com.zody.db.ProfileDb
import com.zody.entity.EVoucher
import com.zody.entity.LoginResult
import com.zody.entity.Photo
import com.zody.entity.Profile
import com.zody.livedata.LiveDataProviders
import com.zody.livedata.SingleLiveEvent
import com.zody.ui.login.LoginActivity
import com.zody.utils.*
import retrofit2.Call
import java.util.*
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton


/**
 * Created by vinhnguyen.it.vn on 2018, July 10
 */
@Singleton
class ProfileRepository @Inject constructor(private val app: Application,
                                            private val appExecutors: AppExecutors,
                                            private val apiService: ApiService,
                                            private val apiServiceOldVersion: ApiServiceOldVersion,
                                            private val profileDb: ProfileDb,
                                            private val postDb: PostDb,
                                            private val notificationDb: NotificationDb,
                                            private val sp: SharedPreferences,
                                            @Named("deviceId")
                                            private val deviceId: String) {

    companion object {
        const val LOAD_EVOUCHER_SIZE_PER_PAGE = 20
    }

    val onLogout = SingleLiveEvent<Unit>()
    val onLogin = SingleLiveEvent<Unit>()

    fun isMe(userId: String): LiveData<Boolean> {
        return MutableLiveData<Boolean>().apply {
            appExecutors.diskIO.execute {
                val profile = profileDb.profileDao().getProfile()
                postValue(profile?.id == userId)
            }
        }
    }

    private fun login(api: LiveData<ApiResponse<LoginResult>>): LiveData<Result<Unit>> {
        val result = MediatorLiveData<Result<Unit>>()
        result.value = Result.loading()

        result.addSource(api) { loginResult ->
            result.removeSource(api)
            if (loginResult.success) {
                val profile = apiService.loadProfile("Bearer ${loginResult.data?.token}")
                result.addSource(profile) { profileResult ->
                    when (profileResult.success) {
                        false -> result.value = Result.error(data = null, message = profileResult.message)
                        true -> {
                            appExecutors.diskIO.execute {
                                sp.edit {
                                    putString(SPKey.PREF_TOKEN, loginResult.data?.token)
                                }
                                onLogin.call()
                                profileResult.data?.data?.let {
                                    it.isNew = loginResult.data?.isNew
                                    profileDb.profileDao().insertProfile(it)
                                }

                                result.postValue(Result.success())
                            }
                        }
                    }
                }
            } else {
                result.value = Result.error(data = null, message = loginResult.message)
            }
        }

        return result
    }

    fun loginWithFacebook(token: String): LiveData<Result<Unit>> {
        return login(apiService.loginWithFacebook(bodyOf("token" to token)))
    }

    fun loginWithAccountKit(token: String): LiveData<Result<Unit>> {
        return login(apiServiceOldVersion.loginWithAccountKit(token))
    }

    fun needUpdatePhoneNumber(): LiveData<Boolean> {
        val profile = loadProfileFromDB()
        return Transformations.map(profile) {
            it != null && (it.phone?.isNotEmpty() != true || it.statuses?.verified != true)
        }
    }

    fun updatePhone(token: String, phone: String): LiveData<Result<Unit>> {
        return object : NetworkResource<Unit, Unit>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<Unit>> {
                return apiServiceOldVersion.updatePhone(token)
            }

            override fun saveCallResult(item: Unit?) {
                profileDb.profileDao().updatePhoneAndVerify(phone, true)
            }

        }.asLiveData()
    }

    fun loadProfile(): LiveData<Result<Profile>> {
        return object : NetworkBoundResource<Profile, ApiData<Profile>>(appExecutors) {
            override fun saveCallResult(item: ApiData<Profile>) {
                item.data?.let {
                    profileDb.profileDao().insertProfile(it)
                    if (app.resources.getBoolean(R.bool.fabric_enable)) {
                        appExecutors.mainThread.execute {
                            Crashlytics.setUserIdentifier(it.id)
                            Crashlytics.setUserName(it.name)
                            Crashlytics.setString("phone", it.phone)
                            Crashlytics.setString("facebook", "${it.facebook?.id} ${it.facebook?.url}")
                            Crashlytics.setString("city", it.city)
                            Crashlytics.setString("gender", it.gender)
                        }
                    }
                }
            }

            override fun shouldFetch(data: Profile?): Boolean {
                return data == null || data.isNew != true || data.isForceUpdateData != true
            }

            override fun loadFromDb(): LiveData<Profile> {
                return profileDb.profileDao().getProfileAsLiveData()
            }

            override fun createCall(): LiveData<ApiResponse<ApiData<Profile>>> {
                return apiService.loadProfile()
            }

        }.asLiveData()
    }

    fun updateProfile(name: String?, nickname: String?, birthday: Date?, gender: String?, city: String?, desc: String?): LiveData<Result<Unit>> {
        return object : NetworkResource<Unit, Unit>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<Unit>> {
                return apiService.updateProfile(if (nickname?.isNotEmpty() == true) bodyOf(
                        "name" to name,
                        "nickname" to nickname,
                        "birthday" to birthday?.isoFormat(),
                        "gender" to gender,
                        "city" to city,
                        "desc" to desc
                ) else bodyOf(
                        "name" to name,
                        "birthday" to birthday?.isoFormat(),
                        "gender" to gender,
                        "city" to city,
                        "desc" to desc
                ))
            }

            override fun saveCallResult(item: Unit?) {
                profileDb.profileDao().updateProfile(name, nickname, birthday, gender, city, desc)
            }
        }.asLiveData()
    }


    fun updateAvatar(uri: Uri): LiveData<Result<Unit>> {
        val part = uri.toPart(context = app) ?: return LiveDataProviders.createAbsent()
        return object : NetworkResource<Unit, Photo>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<Photo>> {
                return apiServiceOldVersion.updateAvatar(part)
            }

            override fun saveCallResult(item: Photo?) {
                item?.url?.let {
                    profileDb.profileDao().updateAvatar(it)
                }
            }

        }.asLiveData()
    }

    fun loadProfileFromDB(): LiveData<Profile> {
        return profileDb.profileDao().getProfileAsLiveData()
    }

    fun createBarCodeImage(code: String, width: Int, height: Int): LiveData<Bitmap> {
        return MutableLiveData<Bitmap>().apply {
            appExecutors.diskIO.execute {
                try {
                    postValue(createBarCode(code = code, width = width, height = height))
                } catch (e: WriterException) {

                }
            }
        }
    }

    fun loadEvoucher(status: String?, sort: String?): Listing<EVoucher> {
        val sourceFactory = object : DataSource.Factory<String, EVoucher>() {

            val sourceLiveData = MutableLiveData<PagingWithoutCacheDataSource<EVoucher, EVoucher>>()

            override fun create(): DataSource<String, EVoucher> {
                val dataSource = object : PagingWithoutCacheDataSource<EVoucher, EVoucher>(appExecutors) {
                    override fun transfer(data: List<EVoucher>): List<EVoucher> {
                        return data.apply {
                            forEach { it.status = status }
                        }
                    }

                    override fun createApi(after: String?): Call<ApiResponse<ApiPaging<EVoucher>>> {
                        return apiService.loadEvouchers(status = status, sort = sort, next = after)
                    }
                }
                sourceLiveData.postValue(dataSource)
                return dataSource
            }
        }
        val pageList = LivePagedListBuilder<String, EVoucher>(sourceFactory, 20).build()
        val endData = Transformations.switchMap(sourceFactory.sourceLiveData) {
            it.endData
        }
        val refreshState = Transformations.switchMap(sourceFactory.sourceLiveData) {
            it.initialLoad
        }
        return Listing(
                data = ListingData.createLiveData(pageList, endData),
                networkState = Transformations.switchMap(sourceFactory.sourceLiveData) {
                    it.networkState
                },
                retry = {
                    sourceFactory.sourceLiveData.value?.retryAllFailed()
                },
                refresh = {
                    sourceFactory.sourceLiveData.value?.invalidate()
                },
                refreshState = refreshState
        )
    }

    fun loadEvoucherDetail(id: String): LiveData<Result<EVoucher>> {
        return object : NetworkResource<EVoucher, ApiData<EVoucher>>(appExecutors) {

            override fun transfer(item: ApiData<EVoucher>?): EVoucher? {
                return item?.data
            }

            override fun createCall(): LiveData<ApiResponse<ApiData<EVoucher>>> {
                return apiService.loadEvoucher(id)
            }

        }.asLiveData()
    }

    fun useEvoucher(id: String, businessId: String?): LiveData<Result<Unit>> {
        return object : NetworkResource<Unit, Unit>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<Unit>> {
                return if (businessId != null) {
                    apiService.useEvoucherWithBusiness(id, bodyOf(
                            "business" to businessId
                    ))
                } else {
                    apiService.useEvoucher(id)
                }
            }

            override fun saveCallResult(item: Unit?) {
                profileDb.runInTransaction {
                    profileDb.profileDao().updateNumberReward(-1)
                }
            }
        }.asLiveData()
    }

    @Throws(WriterException::class)
    private fun createBarCode(code: String, width: Int, height: Int,
                              format: BarcodeFormat = BarcodeFormat.CODE_128, backgroundColor: Int = Color.TRANSPARENT, codeColor: Int = Color.BLACK): Bitmap? {
        var encoding: String? = null
        for (i in 0 until code.length) {
            if (code[i].toInt() > 0xFF) {
                encoding = "UTF-8"
                break
            }
        }
        val hints: MutableMap<EncodeHintType, Any>? = if (encoding != null) mutableMapOf(Pair(EncodeHintType.CHARACTER_SET, encoding)) else null
        val writer = MultiFormatWriter()

        return try {
            val result = writer.encode(code, format, width, height, hints)
            val pixels = IntArray(result.width * result.height)
            for (y in 0 until result.height) {
                val offset = y * result.width
                for (x in 0 until result.width) {
                    pixels[offset + x] = if (result.get(x, y)) codeColor else backgroundColor
                }
            }
            val bitmap = Bitmap.createBitmap(result.width, result.height, Bitmap.Config.ARGB_8888)
            bitmap.setPixels(pixels, 0, result.width, 0, 0, result.width, result.height)
            bitmap
        } catch (e: Exception) {
            null
        }
    }

    @SuppressLint("ApplySharedPref")
    fun logout() {
        appExecutors.diskIO.execute {
            onLogout.call()

            sp.edit().remove(SPKey.PREF_TOKEN).commit()

            profileDb.clearAllTables()
            postDb.clearAllTables()
            notificationDb.clearAllTables()

            appExecutors.mainThread.execute {
                app.startActivity(Intent(app, LoginActivity::class.java).apply {
                    addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                })
            }
        }
    }

    fun requestNewToken() {
        FirebaseInstanceId.getInstance().deleteInstanceId()
        sp.edit {
            remove(SPKey.PREF_DEVICE_TOKEN)
        }
        logout()
    }

    fun inputReferralCode(code: String): LiveData<Result<Int>> {
        return object : NetworkResource<Int, ApiData<Int>>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<ApiData<Int>>> {
                return apiServiceOldVersion.inputReferralCode(code, deviceId = deviceId)
            }

            override fun transfer(item: ApiData<Int>?): Int? {
                return item?.data
            }

            override fun saveCallResult(item: ApiData<Int>?) {
                super.saveCallResult(item)
                item?.data?.let {
                    profileDb.profileDao().updateCoin(it)
                }
            }
        }.asLiveData()

    }

    fun increaseCoin(number: Int) {
        profileDb.profileDao().increaseCoin(number)
    }
}