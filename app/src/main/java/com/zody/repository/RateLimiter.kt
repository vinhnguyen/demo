package com.zody.repository

import android.os.SystemClock
import androidx.collection.ArrayMap
import java.util.concurrent.TimeUnit

/**
 * Created by vinhnguyen.it.vn on 2018, July 11
 */
class RateLimiter<in KEY>(timeout: Long, timeUnit: TimeUnit) {
    private val timestamps = ArrayMap<KEY, Long>()
    private val timeout = timeUnit.toMillis(timeout)

    @Synchronized
    fun shouldFetch(key: KEY): Boolean {
        val lastFetched = timestamps[key]
        val now = now()
        if (lastFetched == null) {
            return true
        }
        if (now - lastFetched > timeout) {
            return true
        }
        return false
    }

    @Synchronized
    fun shouldFetchAndUpdate(key: KEY): Boolean {
        val lastFetched = timestamps[key]
        val now = now()
        if (lastFetched == null) {
            timestamps[key] = now
            return true
        }
        if (now - lastFetched > timeout) {
            timestamps[key] = now
            return true
        }
        return false
    }

    @Synchronized
    fun init(key: KEY, last: Long) {
        timestamps[key] = last
    }

    @Synchronized
    fun reset(key: KEY) {
        timestamps.remove(key)
    }

    fun setCurrent(key: KEY) {
        timestamps[key] = now()
    }

    private fun now() = SystemClock.uptimeMillis()
}