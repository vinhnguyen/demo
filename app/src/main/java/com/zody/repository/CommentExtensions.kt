package com.zody.repository

import com.zody.entity.Comment
import com.zody.entity.CommentListItem
import com.zody.entity.CommentQueryItem
import com.zody.entity.ListItem

/**
 * Created by vinhnguyen.it.vn on 2018, July 30
 */
fun Comment.toItemTypes(): List<Int> {
    return listOf(CommentListItem.TYPE_NORMAL)
}

fun CommentQueryItem.toListItems(): List<ListItem<Comment>> {
    return this.comment.toItemTypes().map {
        ListItem(data = this.comment,
                next = this.item.next,
                order = this.item.order,
                type = it)
    }
}

fun List<CommentQueryItem>.toListItems(): List<ListItem<Comment>> {
    return this.flatMap { it.toListItems() }
}
