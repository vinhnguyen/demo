package com.zody.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import com.zody.api.ApiData
import com.zody.api.ApiPaging
import com.zody.api.ApiResponse
import com.zody.api.ApiService
import com.zody.entity.BusinessCompat
import com.zody.entity.Photo
import com.zody.utils.AppExecutors
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 13
 */
class BusinessRepository @Inject constructor(private val appExecutors: AppExecutors,
                                             private val apiService: ApiService) {

    fun searchBusiness(keyword: String, latitude: Double?, longitude: Double?, city: String?): LiveData<Result<List<BusinessCompat>>> {
        return object : NetworkResource<List<BusinessCompat>, ApiPaging<BusinessCompat>>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<ApiPaging<BusinessCompat>>> {
                return apiService.searchBusiness(keyword = keyword, latitude = latitude, longitude = longitude, city = city)
            }

            override fun transfer(item: ApiPaging<BusinessCompat>?): List<BusinessCompat>? {
                return item?.data
            }

        }.asLiveData()
    }

    fun loadBusinessDetail(businessId: String): LiveData<Result<com.zody.entity.business.Business>> {
        return object : NetworkResource<com.zody.entity.business.Business, ApiData<com.zody.entity.business.Business>>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<ApiData<com.zody.entity.business.Business>>> {
                return apiService.loadBusinessDetail(id = businessId)
            }

            override fun transfer(item: ApiData<com.zody.entity.business.Business>?): com.zody.entity.business.Business? {
                return item?.data
            }
        }.asLiveData()
    }

    fun loadBusinessCompat(businessId: String): LiveData<BusinessCompat> {
        return MediatorLiveData<BusinessCompat>().apply {
            addSource(apiService.loadBusinessCompat(businessId)) { apiResponse ->
                apiResponse.data?.data?.let {
                    value = it
                }
            }
        }
    }

    fun loadBusinessPhotoFirstPage(businessId: String): LiveData<Result<List<Photo>>> {
        return object : NetworkResource<List<Photo>, ApiPaging<Photo>>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<ApiPaging<Photo>>> {
                return apiService.loadBusinessPhotoFirstPage(businessId)
            }

            override fun transfer(item: ApiPaging<Photo>?): List<Photo>? {
                return item?.data
            }

        }.asLiveData()
    }

    fun loadBusinessPhoto(businessId: String): Listing<Photo> {
        val sourceFactory = object : DataSource.Factory<String, Photo>() {

            val sourceLiveData = MutableLiveData<PagingWithoutCacheDataSource<Photo, Photo>>()

            override fun create(): DataSource<String, Photo> {
                val dataSource = object : PagingWithoutCacheDataSource<Photo, Photo>(appExecutors) {
                    override fun transfer(data: List<Photo>): List<Photo> {
                        return data
                    }

                    override fun createApi(after: String?): Call<ApiResponse<ApiPaging<Photo>>> {
                        return apiService.loadBusinessPhoto(businessId = businessId, next = after)
                    }
                }
                sourceLiveData.postValue(dataSource)
                return dataSource
            }
        }
        val pageList = LivePagedListBuilder<String, Photo>(sourceFactory, 20).build()
        val endData = Transformations.switchMap(sourceFactory.sourceLiveData) {
            it.endData
        }
        val refreshState = Transformations.switchMap(sourceFactory.sourceLiveData) {
            it.initialLoad
        }
        return Listing(
                data = ListingData.createLiveData(pageList, endData),
                networkState = Transformations.switchMap(sourceFactory.sourceLiveData) {
                    it.networkState
                },
                retry = {
                    sourceFactory.sourceLiveData.value?.retryAllFailed()
                },
                refresh = {
                    sourceFactory.sourceLiveData.value?.invalidate()
                },
                refreshState = refreshState
        )
    }

    fun addBusiness(data: Map<String, Any?>): LiveData<Result<BusinessCompat>> {
        return object : NetworkResource<BusinessCompat, ApiData<BusinessCompat>>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<ApiData<BusinessCompat>>> {
                return apiService.createBusiness(
                        RequestBody.create(MediaType.parse("application/json"),
                                JSONObject(data.filterNot { it.value == null }).toString())
                )
            }

            override fun transfer(item: ApiData<BusinessCompat>?): BusinessCompat? {
                return item?.data
            }

        }.asLiveData()
    }
}