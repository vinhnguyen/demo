package com.zody.repository

import androidx.lifecycle.LiveData
import com.zody.api.ApiResponse
import com.zody.api.ApiService
import com.zody.db.PostDb
import com.zody.utils.AppExecutors
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 24
 */
class AuthorRepository @Inject constructor(private val appExecutors: AppExecutors,
                                           private val apiService: ApiService,
                                           private val postDb: PostDb) {

    fun followAuthor(id: String): LiveData<Result<Unit>> {
        return object : NetworkResource<Unit, Unit>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<Unit>> {
                return apiService.followAuthor(id)
            }

            override fun saveCallResult(item: Unit?) {
                postDb.authorDao().follow(id)
            }
        }.asLiveData()
    }

    fun unFollowAuthor(id: String): LiveData<Result<Unit>> {
        return object : NetworkResource<Unit, Unit>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<Unit>> {
                return apiService.unFollowAuthor(id)
            }

            override fun saveCallResult(item: Unit?) {
                postDb.authorDao().unFollow(id)
            }

        }.asLiveData()
    }
}