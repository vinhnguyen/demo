package com.zody.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import com.zody.api.ApiPaging
import com.zody.api.ApiResponse
import com.zody.api.ApiServiceOldVersion
import com.zody.entity.ListItem
import com.zody.entity.luckydraw.*
import com.zody.utils.AppExecutors
import retrofit2.Call
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, December 03
 */
class LuckyDrawRepository @Inject constructor(private val appExecutors: AppExecutors,
                                              private val apiServiceOldVersion: ApiServiceOldVersion) {

    fun loadCurrentActive(city: String?, latitude: Double?, longitude: Double?): LiveData<Result<LuckyDrawData>> {
        return object : NetworkResource<LuckyDrawData, LuckyDrawData>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<LuckyDrawData>> {
                return apiServiceOldVersion.getCurrentActiveLuckyDraw(city, latitude, longitude)
            }
        }.asLiveData()
    }

    fun loadLuckyDraw(id: String): LiveData<Result<LuckyDrawData>> {
        return object : NetworkResource<LuckyDrawData, LuckyDrawData>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<LuckyDrawData>> {
                return apiServiceOldVersion.getLuckyDraw(id)
            }

        }.asLiveData()
    }

    fun loadLuckyDrawHistory(id: String): Listing<ListItem<LuckyDrawHistory>> {
        val sourceFactory = object : DataSource.Factory<Int, ListItem<LuckyDrawHistory>>() {

            val sourceLiveData = MutableLiveData<PagingWithPageDataSource<ListItem<LuckyDrawHistory>, LuckyDrawHistory>>()

            override fun create(): DataSource<Int, ListItem<LuckyDrawHistory>> {
                val dataSource = object : PagingWithPageDataSource<ListItem<LuckyDrawHistory>, LuckyDrawHistory>(appExecutors) {
                    override fun transfer(data: List<LuckyDrawHistory>): List<ListItem<LuckyDrawHistory>> {
                        return data.map { ListItem(it, null, -1, -1) }
                    }

                    override fun createApi(page: Int): Call<ApiResponse<ApiPaging<LuckyDrawHistory>>> {
                        return apiServiceOldVersion.getLuckyDrawHistory(id, page, true)
                    }
                }
                sourceLiveData.postValue(dataSource)
                return dataSource
            }
        }
        val pageList = LivePagedListBuilder<Int, ListItem<LuckyDrawHistory>>(sourceFactory, 20).build()
        val endData = Transformations.switchMap(sourceFactory.sourceLiveData) {
            it.endData
        }
        val refreshState = Transformations.switchMap(sourceFactory.sourceLiveData) {
            it.initialLoad
        }
        return Listing(
                data = ListingData.createLiveData(pageList, endData),
                networkState = Transformations.switchMap(sourceFactory.sourceLiveData) {
                    it.networkState
                },
                retry = {
                    sourceFactory.sourceLiveData.value?.retryAllFailed()
                },
                refresh = {
                    sourceFactory.sourceLiveData.value?.invalidate()
                },
                refreshState = refreshState
        )
    }

    fun draw(id: String): LiveData<Result<LuckyDrawResult>> {
        return object : NetworkResource<LuckyDrawResult, LuckyDrawResult>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<LuckyDrawResult>> {
                return apiServiceOldVersion.draw(id)
            }
        }.asLiveData()
    }

    fun loadWinner(id: String): Listing<ListItem<LuckyDrawWinner>> {
        val sourceFactory = object : DataSource.Factory<Int, ListItem<LuckyDrawWinner>>() {

            val sourceLiveData = MutableLiveData<PagingWithPageDataSource<ListItem<LuckyDrawWinner>, LuckyDrawWinner>>()

            override fun create(): DataSource<Int, ListItem<LuckyDrawWinner>> {
                val dataSource = object : PagingWithPageDataSource<ListItem<LuckyDrawWinner>, LuckyDrawWinner>(appExecutors) {
                    override fun transfer(data: List<LuckyDrawWinner>): List<ListItem<LuckyDrawWinner>> {
                        return data.map { ListItem(it, null, 0, 0) }
                    }

                    override fun createApi(page: Int): Call<ApiResponse<ApiPaging<LuckyDrawWinner>>> {
                        return apiServiceOldVersion.getWinner(id, page)
                    }
                }
                sourceLiveData.postValue(dataSource)
                return dataSource
            }
        }
        val pageList = LivePagedListBuilder<Int, ListItem<LuckyDrawWinner>>(sourceFactory, 20).build()
        val endData = Transformations.switchMap(sourceFactory.sourceLiveData) {
            it.endData
        }
        val refreshState = Transformations.switchMap(sourceFactory.sourceLiveData) {
            it.initialLoad
        }
        return Listing(
                data = ListingData.createLiveData(pageList, endData),
                networkState = Transformations.switchMap(sourceFactory.sourceLiveData) {
                    it.networkState
                },
                retry = {
                    sourceFactory.sourceLiveData.value?.retryAllFailed()
                },
                refresh = {
                    sourceFactory.sourceLiveData.value?.invalidate()
                },
                refreshState = refreshState
        )
    }

    fun loadLeaderBoard(id: String): LiveData<Result<List<LuckyDrawLeaderBoard>>> {
        return object : NetworkResource<List<LuckyDrawLeaderBoard>, ApiPaging<LuckyDrawLeaderBoard>>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<ApiPaging<LuckyDrawLeaderBoard>>> {
                return apiServiceOldVersion.getLeaderBoard(id)
            }

            override fun transfer(item: ApiPaging<LuckyDrawLeaderBoard>?): List<LuckyDrawLeaderBoard>? {
                return item?.data
            }

        }.asLiveData()
    }

    fun loadCode(id: String): Listing<ListItem<LuckyDrawBallot>> {
        val sourceFactory = object : DataSource.Factory<Int, ListItem<LuckyDrawBallot>>() {

            val sourceLiveData = MutableLiveData<PagingWithPageDataSource<ListItem<LuckyDrawBallot>, LuckyDrawBallot>>()

            override fun create(): DataSource<Int, ListItem<LuckyDrawBallot>> {
                val dataSource = object : PagingWithPageDataSource<ListItem<LuckyDrawBallot>, LuckyDrawBallot>(appExecutors) {
                    override fun transfer(data: List<LuckyDrawBallot>): List<ListItem<LuckyDrawBallot>> {
                        return data.map { ListItem(it, null, 0, 0) }
                    }

                    override fun createApi(page: Int): Call<ApiResponse<ApiPaging<LuckyDrawBallot>>> {
                        return apiServiceOldVersion.getCode(id, page)
                    }
                }
                sourceLiveData.postValue(dataSource)
                return dataSource
            }
        }
        val pageList = LivePagedListBuilder<Int, ListItem<LuckyDrawBallot>>(sourceFactory, 20).build()
        val endData = Transformations.switchMap(sourceFactory.sourceLiveData) {
            it.endData
        }
        val refreshState = Transformations.switchMap(sourceFactory.sourceLiveData) {
            it.initialLoad
        }
        return Listing(
                data = ListingData.createLiveData(pageList, endData),
                networkState = Transformations.switchMap(sourceFactory.sourceLiveData) {
                    it.networkState
                },
                retry = {
                    sourceFactory.sourceLiveData.value?.retryAllFailed()
                },
                refresh = {
                    sourceFactory.sourceLiveData.value?.invalidate()
                },
                refreshState = refreshState
        )
    }

    fun getRewardAfterShared(id: String): LiveData<Result<LuckyDrawShareResult>> {
        return object : NetworkResource<LuckyDrawShareResult, LuckyDrawShareResult>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<LuckyDrawShareResult>> {
                return apiServiceOldVersion.getRewardAfterShared(id)
            }
        }.asLiveData()
    }
}