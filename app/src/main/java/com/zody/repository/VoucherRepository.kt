package com.zody.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import com.zody.api.ApiData
import com.zody.api.ApiPaging
import com.zody.api.ApiResponse
import com.zody.api.ApiService
import com.zody.db.ProfileDb
import com.zody.db.VoucherDb
import com.zody.entity.voucher.*
import com.zody.utils.AppExecutors
import retrofit2.Call
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by vinhnguyen.it.vn on 2018, August 09
 */
@Singleton
class VoucherRepository @Inject constructor(private val appExecutors: AppExecutors,
                                            private val apiService: ApiService,
                                            private val profileDb: ProfileDb,
                                            private val voucherDb: VoucherDb) {

    fun loadVoucherData(city: String?, latitude: Double?, longitude: Double?): LiveData<Result<VoucherData>> {
        return object : NetworkResource<VoucherData, ApiData<VoucherData>>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<ApiData<VoucherData>>> {
                return apiService.loadVouchers(next = null, city = city, latitude = latitude, longitude = longitude)
            }

            override fun saveCallResult(item: ApiData<VoucherData>?) {
                item?.data?.categories?.let { categories ->
                    voucherDb.voucherCategoryDao().apply {
                        deleteAll()
                        insert(categories)
                    }
                }
            }

            override fun transfer(item: ApiData<VoucherData>?): VoucherData? {
                return item?.data
            }
        }.asLiveData()
    }

    fun loadVoucherCategory(): LiveData<List<VoucherCategory>> {
        return voucherDb.voucherCategoryDao().load()
    }

    fun loadVoucherOfCategory(id: String?, sort: String?, city: String?, latitude: Double?, longitude: Double?): Listing<Voucher> {
        val sourceFactory = object : DataSource.Factory<String, Voucher>() {

            val sourceLiveData = MutableLiveData<PagingWithoutCacheDataSource<Voucher, Voucher>>()

            override fun create(): DataSource<String, Voucher> {
                val dataSource = object : PagingWithoutCacheDataSource<Voucher, Voucher>(appExecutors) {
                    override fun transfer(data: List<Voucher>): List<Voucher> {
                        return data
                    }

                    override fun createApi(after: String?): Call<ApiResponse<ApiPaging<Voucher>>> {
                        return apiService.loadVoucherOfCategory(category = id, sort = sort, next = after,
                                city = city, latitude = latitude, longitude = longitude)
                    }
                }
                sourceLiveData.postValue(dataSource)
                return dataSource
            }
        }
        val pageList = LivePagedListBuilder<String, Voucher>(sourceFactory, 20).build()
        val endData = Transformations.switchMap(sourceFactory.sourceLiveData) {
            it.endData
        }
        val refreshState = Transformations.switchMap(sourceFactory.sourceLiveData) {
            it.initialLoad
        }
        return Listing(
                data = ListingData.createLiveData(pageList, endData),
                networkState = Transformations.switchMap(sourceFactory.sourceLiveData) {
                    it.networkState
                },
                retry = {
                    sourceFactory.sourceLiveData.value?.retryAllFailed()
                },
                refresh = {
                    sourceFactory.sourceLiveData.value?.invalidate()
                },
                refreshState = refreshState
        )
    }

    fun loadVoucherOfOtherCategory(type: String, city: String?, latitude: Double?, longitude: Double?): Listing<Voucher> {
        val sourceFactory = object : DataSource.Factory<String, Voucher>() {

            val sourceLiveData = MutableLiveData<PagingWithoutCacheDataSource<Voucher, Voucher>>()

            override fun create(): DataSource<String, Voucher> {
                val dataSource = object : PagingWithoutCacheDataSource<Voucher, Voucher>(appExecutors) {
                    override fun transfer(data: List<Voucher>): List<Voucher> {
                        return data
                    }

                    override fun createApi(after: String?): Call<ApiResponse<ApiPaging<Voucher>>> {
                        return when (type) {
                            VoucherList.TYPE_YOU_MY_LIKE -> apiService.loadVoucherYouMayLike(
                                    next = after, city = city, latitude = latitude, longitude = longitude)
                            else -> apiService.loadVoucherNewUpdate(//VoucherList.TYPE_NEW_UPDATE
                                    next = after, city = city, latitude = latitude, longitude = longitude)
                        }
                    }
                }
                sourceLiveData.postValue(dataSource)
                return dataSource
            }
        }
        val pageList = LivePagedListBuilder<String, Voucher>(sourceFactory, 20).build()
        val endData = Transformations.switchMap(sourceFactory.sourceLiveData) {
            it.endData
        }
        val refreshState = Transformations.switchMap(sourceFactory.sourceLiveData) {
            it.initialLoad
        }
        return Listing(
                data = ListingData.createLiveData(pageList, endData),
                networkState = Transformations.switchMap(sourceFactory.sourceLiveData) {
                    it.networkState
                },
                retry = {
                    sourceFactory.sourceLiveData.value?.retryAllFailed()
                },
                refresh = {
                    sourceFactory.sourceLiveData.value?.invalidate()
                },
                refreshState = refreshState
        )
    }

    fun loadVoucherDetail(id: String): LiveData<Result<VoucherDetailData>> {
        return object : NetworkResource<VoucherDetailData, ApiData<VoucherDetailData>>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<ApiData<VoucherDetailData>>> {
                return apiService.loadVoucherDetail(id)
            }

            override fun transfer(item: ApiData<VoucherDetailData>?): VoucherDetailData? {
                return item?.data
            }

        }.asLiveData()
    }

    fun exchangeVoucher(voucher: Voucher): LiveData<Result<Unit>> {
        return object : NetworkResource<Unit, Unit>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<Unit>> {
                return apiService.exchangeVoucher(voucher.id)
            }

            override fun saveCallResult(item: Unit?) {
                profileDb.profileDao().increaseCoin(-1 * (voucher.coin ?: 0))
                profileDb.profileDao().updateNumberReward(1)
            }
        }.asLiveData()
    }

}