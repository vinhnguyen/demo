package com.zody.repository

import android.content.SharedPreferences
import android.os.Handler
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import com.zody.BuildConfig
import com.zody.api.ApiData
import com.zody.api.ApiPaging
import com.zody.api.ApiResponse
import com.zody.api.ApiService
import com.zody.db.PostDb
import com.zody.db.ProfileDao
import com.zody.entity.*
import com.zody.ui.writepost.WritePostData
import com.zody.utils.AppExecutors
import com.zody.utils.SPKey
import com.zody.utils.bodyOf
import com.zody.utils.sha1
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by vinhnguyen.it.vn on 2018, July 19
 */
@Singleton
class PostRepository @Inject constructor(private val appExecutors: AppExecutors,
                                         sp: SharedPreferences,
                                         private val apiService: ApiService,
                                         private val postDb: PostDb,
                                         private val profileDao: ProfileDao,
                                         writePostData: WritePostData,
                                         private val trackingRepository: TrackingRepository) {

    private val rateLimiter = RateLimiter<String>(sp.getLong(SPKey.PREF_REVIEW_TIME_TO_INCREASE_VIEW, 6 * 60 * 60 * 1000), TimeUnit.MILLISECONDS)
    private var userId: String? = null

    init {
        writePostData.result.observeForever { result ->
            result.data?.let { post ->
                appExecutors.diskIO.execute {
                    postDb.postDao().insertItem(PostItem(postId = post.id, authorId = post.authorId,
                            next = null, filter = "me-detail", sort = PostItem.SORT_NEWEST, order = -1))
                    postDb.postDao().insertPostAndAuthor(post)

                    if (post.photos?.isNotEmpty() == true) {
                        val postPhotoItem = PostPhotoItem(post.photos!![0], postId = post.id, authorId = "me", next = null, order = -1)
                        postDb.postPhotoDao().insert(postPhotoItem)
                    }
                }
            }
        }

        appExecutors.diskIO.execute {
            userId = profileDao.getProfile()?.id
            postDb.postViewDao().load().forEach {
                rateLimiter.init(it.postId, it.lastTime.time)
            }
        }
    }

    companion object {
        const val LOAD_POST_SIZE_PER_PAGE = 20
        const val LOAD_PHOTO_SIZE_PER_PAGE = 15
        const val LOAD_COMMENT_SIZE_PER_PAGE = 20
    }

    fun loadTrendingPost(city: String?): Listing<ListItem<Post>> {
        return loadPost(filter = "${PostItem.FILTER_CITY}-${PostItem.SORT_TRENDING}-$city") { next ->
            apiService.loadNearBy(next = next, latitude = null, longitude = null, city = city, sort = PostItem.SORT_TRENDING)
        }
    }

    fun loadNearByPost(latitude: Double?, longitude: Double?): Listing<ListItem<Post>> {
        return loadPost(filter = "${PostItem.FILTER_CITY}-${PostItem.SORT_NEARBY}-$latitude-$longitude") { next ->
            apiService.loadNearBy(next = next, latitude = latitude, longitude = longitude, city = null, sort = PostItem.SORT_NEARBY)
        }
    }

    fun loadNewestPost(city: String?): Listing<ListItem<Post>> {
        return loadPost(filter = "${PostItem.FILTER_CITY}-${PostItem.SORT_NEWEST}-$city") { next ->
            apiService.loadNearBy(next = next, latitude = null, longitude = null, city = city, sort = PostItem.SORT_NEWEST)
        }
    }

    fun loadFollowingPost(): Listing<ListItem<Post>> {
        return loadPost(filter = PostItem.FILTER_FOLLOWING) { next ->
            apiService.loadFollowingPost(next = next)
        }
    }

    private fun loadPost(filter: String, api: (next: String?) -> Call<ApiResponse<ApiPaging<Post>>>): Listing<ListItem<Post>> {
        val sort = ""
        val dataSourceFactory = object : DataSource.Factory<ListItem<Post>, ListItem<Post>>() {
            override fun create(): DataSource<ListItem<Post>, ListItem<Post>> {
                return object : PagingDataSource<Post>(postDb, "post_items", "posts", "authors") {

                    val postDao = postDb.postDao()

                    override fun loadInitial(limit: Int): List<ListItem<Post>> {
                        return postDao.loadInitial(filter, sort, limit).toListItems()
                    }

                    override fun loadCurrentAndAfter(order: Int, limit: Int): List<ListItem<Post>> {
                        return postDao.loadInitial(filter, sort, order, limit).toListItems()
                    }

                    override fun loadAfter(order: Int, limit: Int): List<ListItem<Post>> {
                        return postDao.loadAfter(filter, sort, order, limit).toListItems()
                    }

                    override fun loadBefore(order: Int, limit: Int): List<ListItem<Post>> {
                        return postDao.loadBefore(filter, sort, order, limit).reversed().toListItems()
                    }
                }
            }
        }
        val boundaryCallback = object : PagingBoundaryCallback<Post>(appExecutors) {
            override fun createApi(next: String?): Call<ApiResponse<ApiPaging<Post>>> {
                return api(next)
            }

            override fun insertToDatabase(paging: ApiPaging<Post>?, current: String?) {
                postDb.runInTransaction {
                    if (current == null) {
                        postDb.postDao().clearItems(filter)
                    }

                    paging?.data?.let { data ->
                        var lastedOrder = postDb.postDao().getLastedOrder(filter = filter, sort = sort)
                                ?: -1
                        val postItems = data.map {
                            PostItem(postId = it.id, authorId = it.author?.id, filter = filter, sort = sort, next = paging.next, order = ++lastedOrder)
                        }

                        data.forEach { it.authorId = it.author?.id }
                        postDb.postDao().insertItems(postItems)
                        postDb.postDao().insertPost(data)

                        val authors = data.flatMap { if (it.author != null) listOf(it.author!!) else emptyList() }
                        postDb.postDao().insertAuthor(authors)
                    }
                }
            }
        }

        val pageList = LivePagedListBuilder<ListItem<Post>, ListItem<Post>>(dataSourceFactory, LOAD_POST_SIZE_PER_PAGE)
                .setBoundaryCallback(boundaryCallback)
                .build()
        val refreshTrigger = MutableLiveData<Unit>()
        val refreshState = Transformations.switchMap(refreshTrigger) {
            boundaryCallback.refresh()
        }

        return Listing(
                data = ListingData.createLiveData(pageList, boundaryCallback.endData),
                networkState = boundaryCallback.networkState,
                retry = {
                    boundaryCallback.helper.retryAllFailed()
                },
                refresh = {
                    refreshTrigger.value = null
                },
                refreshState = refreshState)
    }

    fun loadPostOfHashTag(tag: String?, latitude: Double?, longitude: Double?, city: String?, sort: String): Listing<ListItem<Post>> {
        val filter = "tag: $tag-$sort"
        val dataSourceFactory = object : DataSource.Factory<ListItem<Post>, ListItem<Post>>() {
            override fun create(): DataSource<ListItem<Post>, ListItem<Post>> {
                return object : PagingDataSource<Post>(postDb, "post_items", "posts", "authors") {

                    val postDao = postDb.postDao()

                    override fun loadInitial(limit: Int): List<ListItem<Post>> {
                        return postDao.loadInitial(filter, sort, limit).toListItems()
                    }

                    override fun loadCurrentAndAfter(order: Int, limit: Int): List<ListItem<Post>> {
                        return postDao.loadInitial(filter, sort, order, limit).toListItems()
                    }

                    override fun loadAfter(order: Int, limit: Int): List<ListItem<Post>> {
                        return postDao.loadAfter(filter, sort, order, limit).toListItems()
                    }

                    override fun loadBefore(order: Int, limit: Int): List<ListItem<Post>> {
                        return postDao.loadBefore(filter, sort, order, limit).reversed().toListItems()
                    }

                }
            }

        }
        val boundaryCallback = object : PagingBoundaryCallback<Post>(appExecutors) {
            override fun createApi(next: String?): Call<ApiResponse<ApiPaging<Post>>> {
                return apiService.loadPostOfHashTag(tag = tag, next = next, latitude = latitude, longitude = longitude, city = city, type = sort)
            }

            override fun insertToDatabase(paging: ApiPaging<Post>?, current: String?) {
                postDb.runInTransaction {
                    if (current == null) {
                        postDb.postDao().clearItems(filter)
                    }

                    paging?.data?.let { data ->
                        var lastedOrder = postDb.postDao().getLastedOrder(filter = filter, sort = sort)
                                ?: -1
                        val postItems = data.map {
                            PostItem(postId = it.id, authorId = it.author?.id, filter = filter, sort = sort, next = paging.next, order = ++lastedOrder)
                        }

                        data.forEach { it.authorId = it.author?.id }
                        postDb.postDao().insertItems(postItems)
                        postDb.postDao().insertPost(data)

                        val authors = data.flatMap { if (it.author != null) listOf(it.author!!) else emptyList() }
                        postDb.postDao().insertAuthor(authors)
                    }
                }
            }
        }

        val pageList = LivePagedListBuilder<ListItem<Post>, ListItem<Post>>(dataSourceFactory, LOAD_POST_SIZE_PER_PAGE)
                .setBoundaryCallback(boundaryCallback)
                .build()
        val refreshTrigger = MutableLiveData<Unit>()
        val refreshState = Transformations.switchMap(refreshTrigger) {
            boundaryCallback.refresh()
        }

        return Listing(
                data = ListingData.createLiveData(pageList, boundaryCallback.endData),
                networkState = boundaryCallback.networkState,
                retry = {
                    boundaryCallback.helper.retryAllFailed()
                },
                refresh = {
                    refreshTrigger.value = null
                },
                refreshState = refreshState)
    }

    fun loadLocalExpertOnPost(): Listing<ListItem<Author>> {
        val filter = "local_experts"
        val sort = PostItem.SORT_NEWEST

        val dataSourceFactory = object : DataSource.Factory<ListItem<Author>, ListItem<Author>>() {
            override fun create(): DataSource<ListItem<Author>, ListItem<Author>> {
                return object : PagingDataSource<Author>(postDb, "post_items") {

                    val postDao = postDb.postDao()

                    override fun loadInitial(limit: Int): List<ListItem<Author>> {
                        return postDao.loadInitial(filter, sort, limit).toListAuthors()
                    }

                    override fun loadCurrentAndAfter(order: Int, limit: Int): List<ListItem<Author>> {
                        return postDao.loadInitial(filter, sort, order, limit).toListAuthors()
                    }

                    override fun loadAfter(order: Int, limit: Int): List<ListItem<Author>> {
                        return postDao.loadAfter(filter, sort, order, limit).toListAuthors()
                    }

                    override fun loadBefore(order: Int, limit: Int): List<ListItem<Author>> {
                        return postDao.loadBefore(filter, sort, order, limit).reversed().toListAuthors()
                    }

                }
            }

        }
        val boundaryCallback = object : PagingRequestBoundaryCallback<Author, LocalExpert>(appExecutors) {
            override fun createApi(next: String?): Call<ApiResponse<ApiPaging<LocalExpert>>> {
                return apiService.loadSuggestionLocalExpert()
            }

            override fun insertToDatabase(paging: ApiPaging<LocalExpert>?, current: String?) {
                paging?.data?.sortedBy { it.post?.author?.isFollowed }
                postDb.runInTransaction {
                    if (current == null) {
                        postDb.postDao().clearItems(filter)
                    }

                    paging?.data?.let { experts ->
                        val data = experts.asSequence().mapNotNull { it.post }.sortedBy { it.author?.isFollowed }.toList()
                        var lastedOrder = postDb.postDao().getLastedOrder(filter = filter, sort = sort)
                                ?: -1
                        val postItems = data.map {
                            PostItem(postId = it.id, authorId = it.author?.id, filter = filter, sort = sort, next = paging.next, order = ++lastedOrder)
                        }

                        data.forEach { it.authorId = it.author?.id }
                        postDb.postDao().insertItems(postItems)
                        postDb.postDao().insertPost(data)

                        val authors = data.flatMap { if (it.author != null) listOf(it.author!!) else emptyList() }
                        postDb.postDao().insertAuthor(authors)
                    }
                }
            }
        }

        val pageList = LivePagedListBuilder<ListItem<Author>, ListItem<Author>>(dataSourceFactory, LOAD_POST_SIZE_PER_PAGE)
                .setBoundaryCallback(boundaryCallback)
                .build()
        val refreshTrigger = MutableLiveData<Unit>()
        val refreshState = Transformations.switchMap(refreshTrigger) {
            boundaryCallback.refresh()
        }

        return Listing(
                data = ListingData.createLiveData(pageList, boundaryCallback.endData),
                networkState = boundaryCallback.networkState,
                retry = {
                    boundaryCallback.helper.retryAllFailed()
                },
                refresh = {
                    refreshTrigger.value = null
                },
                refreshState = refreshState)
    }

    fun loadLocalExpert(): Listing<ListItem<Post>> {
        val filter = "local_experts"
        val sort = PostItem.SORT_NEWEST

        val dataSourceFactory = object : DataSource.Factory<ListItem<Post>, ListItem<Post>>() {
            override fun create(): DataSource<ListItem<Post>, ListItem<Post>> {
                return object : PagingDataSource<Post>(postDb, "post_items", "posts", "authors") {

                    val postDao = postDb.postDao()

                    override fun loadInitial(limit: Int): List<ListItem<Post>> {
                        return postDao.loadInitial(filter, sort, limit).toListItems()
                    }

                    override fun loadCurrentAndAfter(order: Int, limit: Int): List<ListItem<Post>> {
                        return postDao.loadInitial(filter, sort, order, limit).toListItems()
                    }

                    override fun loadAfter(order: Int, limit: Int): List<ListItem<Post>> {
                        return postDao.loadAfter(filter, sort, order, limit).toListItems()
                    }

                    override fun loadBefore(order: Int, limit: Int): List<ListItem<Post>> {
                        return postDao.loadBefore(filter, sort, order, limit).reversed().toListItems()
                    }

                }
            }

        }
        val boundaryCallback = object : PagingRequestBoundaryCallback<Post, LocalExpert>(appExecutors) {
            override fun createApi(next: String?): Call<ApiResponse<ApiPaging<LocalExpert>>> {
                return apiService.loadSuggestionLocalExpert()
            }

            override fun insertToDatabase(paging: ApiPaging<LocalExpert>?, current: String?) {
                postDb.runInTransaction {
                    if (current == null) {
                        postDb.postDao().clearItems(filter)
                    }

                    paging?.data?.let { experts ->
                        val data = experts.mapNotNull { it.post }.sortedBy { it.author?.isFollowed }
                        var lastedOrder = postDb.postDao().getLastedOrder(filter = filter, sort = sort)
                                ?: -1
                        val postItems = data.map {
                            PostItem(postId = it.id, authorId = it.author?.id, filter = filter, sort = sort, next = paging.next, order = ++lastedOrder)
                        }

                        data.forEach { it.authorId = it.author?.id }
                        postDb.postDao().insertItems(postItems)
                        postDb.postDao().insertPost(data)

                        val authors = data.flatMap { if (it.author != null) listOf(it.author!!) else emptyList() }
                        postDb.postDao().insertAuthor(authors)
                    }
                }
            }
        }

        val pageList = LivePagedListBuilder<ListItem<Post>, ListItem<Post>>(dataSourceFactory, LOAD_POST_SIZE_PER_PAGE)
                .setBoundaryCallback(boundaryCallback)
                .build()
        val refreshTrigger = MutableLiveData<Unit>()
        val refreshState = Transformations.switchMap(refreshTrigger) {
            boundaryCallback.refresh()
        }

        return Listing(
                data = ListingData.createLiveData(pageList, boundaryCallback.endData),
                networkState = boundaryCallback.networkState,
                retry = {
                    boundaryCallback.helper.retryAllFailed()
                },
                refresh = {
                    refreshTrigger.value = null
                },
                refreshState = refreshState)
    }

    fun loadOwnerPost(): Listing<ListItem<Post>> {
        val filter = "me-detail"
        val sort = PostItem.SORT_NEWEST

        val dataSourceFactory = object : DataSource.Factory<ListItem<Post>, ListItem<Post>>() {
            override fun create(): DataSource<ListItem<Post>, ListItem<Post>> {
                return object : PagingDataSource<Post>(postDb, "post_items", "posts") {

                    val postDao = postDb.postDao()

                    override fun loadInitial(limit: Int): List<ListItem<Post>> {
                        return postDao.loadInitial(filter, sort, limit).toListItems()
                    }

                    override fun loadCurrentAndAfter(order: Int, limit: Int): List<ListItem<Post>> {
                        return postDao.loadInitial(filter, sort, order, limit).toListItems()
                    }

                    override fun loadAfter(order: Int, limit: Int): List<ListItem<Post>> {
                        return postDao.loadAfter(filter, sort, order, limit).toListItems()
                    }

                    override fun loadBefore(order: Int, limit: Int): List<ListItem<Post>> {
                        return postDao.loadBefore(filter, sort, order, limit).reversed().toListItems()
                    }
                }
            }

        }
        val boundaryCallback = object : PagingBoundaryCallback<Post>(appExecutors) {
            override fun createApi(next: String?): Call<ApiResponse<ApiPaging<Post>>> {
                return apiService.loadOwnerPost(next = next, type = "detail")
            }

            override fun insertToDatabase(paging: ApiPaging<Post>?, current: String?) {
                postDb.runInTransaction {
                    if (current == null) {
                        postDb.postDao().clearItems(filter)
                    }
                    paging?.data?.let { data ->
                        var lastedOrder = postDb.postDao().getLastedOrder(filter = filter, sort = sort)
                                ?: -1
                        val postItems = data.map {
                            PostItem(postId = it.id, authorId = it.author?.id, filter = filter, sort = sort, next = paging.next, order = ++lastedOrder)
                        }

                        data.forEach { it.authorId = it.author?.id }

                        postDb.postDao().insertItems(postItems)
                        postDb.postDao().insertPost(data)

                        val authors = data.flatMap { if (it.author != null) listOf(it.author!!) else emptyList() }
                        postDb.postDao().insertAuthor(authors)
                    }
                }
            }
        }
        val pageList = LivePagedListBuilder<ListItem<Post>, ListItem<Post>>(dataSourceFactory, LOAD_POST_SIZE_PER_PAGE)
                .setBoundaryCallback(boundaryCallback)
                .build()
        val refreshTrigger = MutableLiveData<Unit>()
        val refreshState = Transformations.switchMap(refreshTrigger) {
            boundaryCallback.refresh()
        }

        return Listing(
                data = ListingData.createLiveData(pageList, boundaryCallback.endData),
                networkState = boundaryCallback.networkState,
                retry = {
                    boundaryCallback.helper.retryAllFailed()
                },
                refresh = {
                    refreshTrigger.value = null
                },
                refreshState = refreshState)
    }

    fun loadOwnerPhoto(): Listing<ListItem<Post>> {
        val authorId = "me"

        val dataSourceFactory: DataSource.Factory<Int, ListItem<Post>> = postDb.postPhotoDao().loadPhotoOf(authorId)
                .map { it.toListPhotoItem() }
        val boundaryCallback = object : PagingBoundaryCallback<Post>(appExecutors) {
            override fun createApi(next: String?): Call<ApiResponse<ApiPaging<Post>>> {
                return apiService.loadOwnerPost(next = next, type = "photo")
            }

            override fun insertToDatabase(paging: ApiPaging<Post>?, current: String?) {
                postDb.runInTransaction {
                    if (current == null) {
                        postDb.postPhotoDao().clearItems(authorId = authorId)
                    }
                    paging?.data?.let { data ->
                        var lastedOrder = postDb.postPhotoDao().getLastedOrder(authorId) ?: -1
                        val postPhotoItems = data.mapNotNull { post ->
                            if (post.photos?.isNotEmpty() == true) {
                                PostPhotoItem(post.photos!![0], postId = post.id, authorId = authorId, next = paging.next, order = ++lastedOrder)
                            } else {
                                null
                            }
                        }
                        postDb.postPhotoDao().insertItems(postPhotoItems)
                    }
                }
            }
        }
        val pageList = LivePagedListBuilder<Int, ListItem<Post>>(dataSourceFactory, LOAD_PHOTO_SIZE_PER_PAGE)
                .setBoundaryCallback(boundaryCallback)
                .build()
        val refreshTrigger = MutableLiveData<Unit>()
        val refreshState = Transformations.switchMap(refreshTrigger) {
            boundaryCallback.refresh()
        }

        return Listing(
                data = ListingData.createLiveData(pageList, boundaryCallback.endData),
                networkState = boundaryCallback.networkState,
                retry =
                {
                    boundaryCallback.helper.retryAllFailed()
                },
                refresh =
                {
                    refreshTrigger.value = null
                },
                refreshState = refreshState)
    }

    fun loadAuthorPost(authorId: String): Listing<ListItem<Post>> {
        val filter = "$authorId-detail"
        val sort = PostItem.SORT_NEWEST

        val dataSourceFactory = object : DataSource.Factory<ListItem<Post>, ListItem<Post>>() {
            override fun create(): DataSource<ListItem<Post>, ListItem<Post>> {
                return object : PagingDataSource<Post>(postDb, "post_items", "posts", "authors") {

                    val postDao = postDb.postDao()

                    override fun loadInitial(limit: Int): List<ListItem<Post>> {
                        return postDao.loadInitial(filter, sort, limit).toListItems()
                    }

                    override fun loadCurrentAndAfter(order: Int, limit: Int): List<ListItem<Post>> {
                        return postDao.loadInitial(filter, sort, order, limit).toListItems()
                    }

                    override fun loadAfter(order: Int, limit: Int): List<ListItem<Post>> {
                        return postDao.loadAfter(filter, sort, order, limit).toListItems()
                    }

                    override fun loadBefore(order: Int, limit: Int): List<ListItem<Post>> {
                        return postDao.loadBefore(filter, sort, order, limit).reversed().toListItems()
                    }
                }
            }

        }
        val boundaryCallback = object : PagingBoundaryCallback<Post>(appExecutors) {
            override fun createApi(next: String?): Call<ApiResponse<ApiPaging<Post>>> {
                return apiService.loadAuthorPost(authorId = authorId, next = next, type = "detail")
            }

            override fun insertToDatabase(paging: ApiPaging<Post>?, current: String?) {
                postDb.runInTransaction {
                    if (current == null) {
                        postDb.postDao().clearItems(filter)
                    }
                    paging?.data?.let { data ->
                        var lastedOrder = postDb.postDao().getLastedOrder(filter = filter, sort = sort)
                                ?: -1
                        val postItems = data.map {
                            PostItem(postId = it.id, authorId = it.author?.id, filter = filter, sort = sort, next = paging.next, order = ++lastedOrder)
                        }

                        data.forEach { it.authorId = it.author?.id }

                        postDb.postDao().insertItems(postItems)
                        postDb.postDao().insertPost(data)

                        val authors = data.flatMap { if (it.author != null) listOf(it.author!!) else emptyList() }
                        postDb.postDao().insertAuthor(authors)
                    }
                }
            }
        }
        val pageList = LivePagedListBuilder<ListItem<Post>, ListItem<Post>>(dataSourceFactory, LOAD_POST_SIZE_PER_PAGE)
                .setBoundaryCallback(boundaryCallback)
                .build()
        val refreshTrigger = MutableLiveData<Unit>()
        val refreshState = Transformations.switchMap(refreshTrigger) {
            boundaryCallback.refresh()
        }

        return Listing(
                data = ListingData.createLiveData(pageList, boundaryCallback.endData),
                networkState = boundaryCallback.networkState,
                retry = {
                    boundaryCallback.helper.retryAllFailed()
                },
                refresh = {
                    refreshTrigger.value = null
                },
                refreshState = refreshState)
    }

    fun loadAuthorPhoto(authorId: String): Listing<ListItem<Post>> {
        val dataSourceFactory: DataSource.Factory<Int, ListItem<Post>> = postDb.postPhotoDao().loadPhotoOf(authorId)
                .map { it.toListPhotoItem() }
        val boundaryCallback = object : PagingBoundaryCallback<Post>(appExecutors) {
            override fun createApi(next: String?): Call<ApiResponse<ApiPaging<Post>>> {
                return apiService.loadAuthorPost(authorId = authorId, next = next, type = "photo")
            }

            override fun insertToDatabase(paging: ApiPaging<Post>?, current: String?) {
                postDb.runInTransaction {
                    if (current == null) {
                        postDb.postPhotoDao().clearItems(authorId = authorId)
                    }
                    paging?.data?.let { data ->
                        var lastedOrder = postDb.postPhotoDao().getLastedOrder(authorId) ?: -1
                        val postPhotoItems = data.mapNotNull { post ->
                            if (post.photos?.isNotEmpty() == true) {
                                PostPhotoItem(post.photos!![0], postId = post.id, authorId = authorId, next = paging.next, order = ++lastedOrder)
                            } else {
                                null
                            }
                        }
                        postDb.postPhotoDao().insertItems(postPhotoItems)
                    }
                }
            }
        }
        val pageList = LivePagedListBuilder<Int, ListItem<Post>>(dataSourceFactory, LOAD_PHOTO_SIZE_PER_PAGE)
                .setBoundaryCallback(boundaryCallback)
                .build()
        val refreshTrigger = MutableLiveData<Unit>()
        val refreshState = Transformations.switchMap(refreshTrigger) {
            boundaryCallback.refresh()
        }

        return Listing(
                data = ListingData.createLiveData(pageList, boundaryCallback.endData),
                networkState = boundaryCallback.networkState,
                retry =
                {
                    boundaryCallback.helper.retryAllFailed()
                },
                refresh =
                {
                    refreshTrigger.value = null
                },
                refreshState = refreshState)
    }

    fun loadBusinessPost(businessId: String): Listing<ListItem<Post>> {
        val filter = businessId
        val sort = PostItem.SORT_NEWEST

        val dataSourceFactory = object : DataSource.Factory<ListItem<Post>, ListItem<Post>>() {
            override fun create(): DataSource<ListItem<Post>, ListItem<Post>> {
                return object : PagingDataSource<Post>(postDb, "post_items", "posts", "authors") {

                    val postDao = postDb.postDao()

                    override fun loadInitial(limit: Int): List<ListItem<Post>> {
                        return postDao.loadInitial(filter, sort, limit).toListItems()
                    }

                    override fun loadCurrentAndAfter(order: Int, limit: Int): List<ListItem<Post>> {
                        return postDao.loadInitial(filter, sort, order, limit).toListItems()
                    }

                    override fun loadAfter(order: Int, limit: Int): List<ListItem<Post>> {
                        return postDao.loadAfter(filter, sort, order, limit).toListItems()
                    }

                    override fun loadBefore(order: Int, limit: Int): List<ListItem<Post>> {
                        return postDao.loadBefore(filter, sort, order, limit).reversed().toListItems()
                    }
                }
            }

        }
        val boundaryCallback = object : PagingBoundaryCallback<Post>(appExecutors) {
            override fun createApi(next: String?): Call<ApiResponse<ApiPaging<Post>>> {
                return apiService.loadBusinessPost(businessId = businessId, next = next)
            }

            override fun insertToDatabase(paging: ApiPaging<Post>?, current: String?) {
                postDb.runInTransaction {
                    if (current == null) {
                        postDb.postDao().clearItems(filter)
                    }
                    paging?.data?.let { data ->
                        var lastedOrder = postDb.postDao().getLastedOrder(filter = filter, sort = sort)
                                ?: -1
                        val postItems = data.map {
                            PostItem(postId = it.id, authorId = it.author?.id, filter = filter, sort = sort, next = paging.next, order = ++lastedOrder)
                        }

                        data.forEach { it.authorId = it.author?.id }

                        postDb.postDao().insertItems(postItems)
                        postDb.postDao().insertPost(data)

                        val authors = data.flatMap { if (it.author != null) listOf(it.author!!) else emptyList() }
                        postDb.postDao().insertAuthor(authors)
                    }
                }
            }
        }
        val pageList = LivePagedListBuilder<ListItem<Post>, ListItem<Post>>(dataSourceFactory, LOAD_POST_SIZE_PER_PAGE)
                .setBoundaryCallback(boundaryCallback)
                .build()
        val refreshTrigger = MutableLiveData<Unit>()
        val refreshState = Transformations.switchMap(refreshTrigger) {
            boundaryCallback.refresh()
        }

        return Listing(
                data = ListingData.createLiveData(pageList, boundaryCallback.endData),
                networkState = boundaryCallback.networkState,
                retry = {
                    boundaryCallback.helper.retryAllFailed()
                },
                refresh = {
                    refreshTrigger.value = null
                },
                refreshState = refreshState)
    }

    fun loadPostDetail(postId: String): LiveData<Result<Post>> {
        return object : NetworkBoundResource<Post, ApiData<Post>>(appExecutors) {
            override fun saveCallResult(item: ApiData<Post>) {
                item.data?.let { postDb.postDao().insertPostAndAuthor(it) }
            }

            override fun loadFromDb(): LiveData<Post> {
                return MediatorLiveData<Post>().apply {
                    addSource(postDb.postDao().loadPostDetail(postId)) {
                        postValue(it?.post)
                    }
                }
            }

            override fun createCall(): LiveData<ApiResponse<ApiData<Post>>> {
                return apiService.loadPostDetail(postId)
            }

            override fun shouldFetch(data: Post?): Boolean {
                return true
            }
        }.asLiveData()
    }

    fun loadComment(postId: String): Listing<ListItem<Comment>> {
        val dataSourceFactory = object : DataSource.Factory<ListItem<Comment>, ListItem<Comment>>() {
            override fun create(): DataSource<ListItem<Comment>, ListItem<Comment>> {
                return object : PagingDataSource<Comment>(postDb, "comment_items", "comments") {

                    val commentDao = postDb.commentDao()

                    override fun loadInitial(limit: Int): List<ListItem<Comment>> {
                        return commentDao.loadInitial(postId, limit).toListItems()
                    }

                    override fun loadCurrentAndAfter(order: Int, limit: Int): List<ListItem<Comment>> {
                        return commentDao.loadInitial(postId, order, limit).toListItems()
                    }

                    override fun loadAfter(order: Int, limit: Int): List<ListItem<Comment>> {
                        return commentDao.loadAfter(postId, order, limit).toListItems()
                    }

                    override fun loadBefore(order: Int, limit: Int): List<ListItem<Comment>> {
                        return commentDao.loadBefore(postId, order, limit).reversed().toListItems()
                    }

                }
            }

        }
        val boundaryCallback = object : PagingBoundaryCallback<Comment>(appExecutors) {
            override fun createApi(next: String?): Call<ApiResponse<ApiPaging<Comment>>> {
                return apiService.loadComment(postId, next)
            }

            override fun insertToDatabase(paging: ApiPaging<Comment>?, current: String?) {
                postDb.runInTransaction {
                    if (current == null) {
                        postDb.commentDao().clearItems(postId)
                    }
                    paging?.data?.let { data ->
                        var lastedOrder = postDb.commentDao().getLastedOrder(postId = postId) ?: -1
                        val commentItems = data.map {
                            CommentItem(commentId = it.id, postId = postId, next = paging.next, order = ++lastedOrder)
                        }
                        postDb.commentDao().insertItems(commentItems)
                        postDb.commentDao().insertComment(data)
                    }
                }
            }

        }

        val pageList = LivePagedListBuilder<ListItem<Comment>, ListItem<Comment>>(dataSourceFactory, LOAD_COMMENT_SIZE_PER_PAGE)
                .setBoundaryCallback(boundaryCallback)
                .build()
        val refreshTrigger = MutableLiveData<Unit>()
        val refreshState = Transformations.switchMap(refreshTrigger) {
            boundaryCallback.refresh()
        }

        return Listing(
                data = ListingData.createLiveData(pageList, boundaryCallback.endData),
                networkState = boundaryCallback.networkState,
                retry = {
                    boundaryCallback.helper.retryAllFailed()
                },
                refresh = {
                    refreshTrigger.value = null
                },
                refreshState = refreshState)
    }

    fun sendComment(postId: String, content: String): LiveData<Result<Comment>> {
        return object : NetworkResource<Comment, ApiData<Comment>>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<ApiData<Comment>>> {
                return apiService.sendComment(postId, bodyOf("content" to content))
            }

            override fun saveCallResult(item: ApiData<Comment>?) {
                item?.data?.let {
                    val newestOrder = postDb.commentDao().getNewestOrder(postId = postId) ?: 0
                    postDb.runInTransaction {
                        postDb.commentDao().insertItem(CommentItem(it.id, postId = postId, next = null, order = newestOrder - 1))
                        postDb.commentDao().insertComment(it)
                        postDb.postDao().increaseCommentOfPost(postId)
                    }
                }
            }

            override fun transfer(item: ApiData<Comment>?): Comment? {
                return item?.data
            }
        }.asLiveData()
    }

    fun deletePost(postId: String): LiveData<Result<Unit>> {
        return object : NetworkResource<Unit, Unit>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<Unit>> {
                return apiService.deletePost(postId)
            }

            override fun saveCallResult(item: Unit?) {
                postDb.postDao().deletePostAndItem(postId)
                postDb.postPhotoDao().deletePhotoOfPost(postId)
            }
        }.asLiveData()
    }

    fun loadPostTipped(postId: String): Listing<Tip> {
        val sourceFactory = object : DataSource.Factory<String, Tip>() {

            val sourceLiveData = MutableLiveData<PagingWithoutCacheDataSource<Tip, Tip>>()

            override fun create(): DataSource<String, Tip> {
                val dataSource = object : PagingWithoutCacheDataSource<Tip, Tip>(appExecutors) {
                    override fun transfer(data: List<Tip>): List<Tip> {
                        return data
                    }

                    override fun createApi(after: String?): Call<ApiResponse<ApiPaging<Tip>>> {
                        return apiService.loadPostTipped(postId = postId, after = after)
                    }
                }
                sourceLiveData.postValue(dataSource)
                return dataSource
            }
        }
        val pageList = LivePagedListBuilder<String, Tip>(sourceFactory, 20).build()
        val endData = Transformations.switchMap(sourceFactory.sourceLiveData) {
            it.endData
        }
        val refreshState = Transformations.switchMap(sourceFactory.sourceLiveData) {
            it.initialLoad
        }
        return Listing(
                data = ListingData.createLiveData(pageList, endData),
                networkState = Transformations.switchMap(sourceFactory.sourceLiveData) {
                    it.networkState
                },
                retry = {
                    sourceFactory.sourceLiveData.value?.retryAllFailed()
                },
                refresh = {
                    sourceFactory.sourceLiveData.value?.invalidate()
                },
                refreshState = refreshState
        )
    }

    fun increaseView(postId: String) {
        appExecutors.diskIO.execute {
            //don't increase review if owner
            if (postDb.postDao().loadPost(id = postId)?.isAuthor == true) return@execute
            //limit time
            if (!rateLimiter.shouldFetchAndUpdate(postId)) return@execute

            @Suppress("ConstantConditionIf")
            val secret = if (BuildConfig.BUILD_TYPE == "release") "bi07y6mR5nt65w8lj9BNxNW7" else "zody-dev-checksum-secret"
            val checksum = "$postId$userId$secret".sha1
            apiService.increaseView(postId, bodyOf("checksum" to checksum)).enqueue(object : Callback<ApiResponse<Unit>> {
                override fun onResponse(call: Call<ApiResponse<Unit>>?, response: Response<ApiResponse<Unit>>?) {
                    appExecutors.diskIO.execute {
                        if (response?.body()?.success == true) {
                            postDb.postViewDao().insert(PostView(postId, Date()))
                            postDb.postDao().increaseViewOfPost(postId)
                            rateLimiter.setCurrent(postId)

                            trackingRepository.trackAfterView(postId)
                        }
                    }
                }

                override fun onFailure(call: Call<ApiResponse<Unit>>?, t: Throwable?) {

                }
            })
        }
    }

    private inner class SendTipRunnable(val postId: String) : LiveData<Result<Unit>>(), Runnable {

        var coin: Int = 0

        init {
            postValue(Result.loading())
        }

        override fun run() {
            sendTipRunnables.remove(postId)
            apiService.tips(bodyOf(
                    "type" to "review",
                    "coin" to coin,
                    "review" to postId
            )).enqueue(object : Callback<ApiResponse<Unit>> {
                override fun onFailure(call: Call<ApiResponse<Unit>>?, t: Throwable?) {
                    onFetchFailed()
                }

                override fun onResponse(call: Call<ApiResponse<Unit>>?, response: Response<ApiResponse<Unit>>?) {
                    if (response?.body()?.success == true) {
                        Result.success(data = null)
                        trackingRepository.trackAfterTip(postId, coin = coin)
                    } else {
                        onFetchFailed()
                    }
                }

                private fun onFetchFailed() {
                    postValue(Result.error())
                    appExecutors.diskIO.execute {
                        postDb.postDao().increaseTipsOfPost(postId, -coin)
                        profileDao.increaseCoin(coin)
                    }
                }
            })
        }
    }

    private val sendTipHandler = Handler()
    private val sendTipRunnables = mutableMapOf<String, SendTipRunnable>()
    private val timeToDelaySendTip = sp.getLong(SPKey.PREF_TIP_DELAY_TIME, 1000)

    fun sendTip(postId: String, coin: Int = 1): LiveData<Result<Unit>> {
        appExecutors.diskIO.execute {
            postDb.postDao().increaseTipsOfPost(postId, coin)
            profileDao.increaseCoin(-coin)
        }
        val runnable = sendTipRunnables[postId] ?: SendTipRunnable(postId)
        runnable.coin += coin
        sendTipHandler.removeCallbacks(runnable)
        sendTipHandler.postDelayed(runnable, timeToDelaySendTip)
        sendTipRunnables[postId] = runnable
        return runnable
    }

    fun clearPostOfTag(tag: String?) {
        appExecutors.diskIO.execute {
            postDb.postDao().clearItems("tag: $tag-${PostItem.SORT_NEARBY}")
            postDb.postDao().clearItems("tag: $tag-${PostItem.SORT_NEWEST}")
            postDb.postDao().clearItems("tag: $tag-${PostItem.SORT_TRENDING}")
        }
    }

}