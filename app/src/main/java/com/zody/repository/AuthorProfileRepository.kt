package com.zody.repository

import android.app.Application
import androidx.lifecycle.LiveData
import com.zody.api.ApiData
import com.zody.api.ApiResponse
import com.zody.api.ApiService
import com.zody.db.PostDb
import com.zody.entity.Author
import com.zody.utils.AppExecutors
import javax.inject.Inject
import javax.inject.Singleton


/**
 * Created by vinhnguyen.it.vn on 2018, July 10
 */
@Singleton
class AuthorProfileRepository @Inject constructor(private val app: Application,
                                                  private val appExecutors: AppExecutors,
                                                  private val apiService: ApiService,
                                                  private val postDb: PostDb) {

    fun loadProfile(id: String): LiveData<Result<Author>> {
        return object : NetworkBoundResource<Author, ApiData<Author>>(appExecutors) {
            override fun saveCallResult(item: ApiData<Author>) {
                item.data?.let { postDb.authorDao().insert(it) }
            }

            override fun loadFromDb(): LiveData<Author> {
                return postDb.authorDao().loadAuthorAsLiveData(id)
            }

            override fun shouldFetch(data: Author?): Boolean {
                return true
            }

            override fun createCall(): LiveData<ApiResponse<ApiData<Author>>> {
                return apiService.loadAuthorProfile(id)
            }

        }.asLiveData()
    }
}