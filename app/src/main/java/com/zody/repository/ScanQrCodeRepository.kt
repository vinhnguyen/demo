package com.zody.repository

import androidx.lifecycle.LiveData
import com.zody.api.ApiResponse
import com.zody.api.ApiServiceOldVersion
import com.zody.entity.ScanQrCodeResult
import com.zody.utils.AppExecutors
import com.zody.utils.SPStorage
import com.zody.utils.bodyOf
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by vinhnguyen.it.vn on 2018, October 30
 */
@Singleton
class ScanQrCodeRepository @Inject constructor(private val appExecutors: AppExecutors,
                                               private val spStorage: SPStorage,
                                               private val apiServiceOldVersion: ApiServiceOldVersion) {

    fun checkQrCode(code: String): LiveData<Result<ScanQrCodeResult>> {
        return object : NetworkResource<ScanQrCodeResult, ScanQrCodeResult>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<ScanQrCodeResult>> {
                return apiServiceOldVersion.checkQrCode(bodyOf("data" to code,
                        "isBudweiser" to true))
            }
        }.asLiveData()
    }

    fun getQrCodeTimeout(): Long {
        return spStorage.qrCodeTimeOut ?: 15 * 60 * 1000
    }
}