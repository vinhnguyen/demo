/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zody.repository

import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.zody.api.ApiResponse
import com.zody.utils.AppExecutors

/**
 * A generic class that can provide a resource backed by both the sqlite database and the network.
 *
 *
 * You can read more about it in the [Architecture
 * Guide](https://developer.android.com/arch).
 * @param <ResultType>
 * @param <RequestType>
</RequestType></ResultType> */
abstract class NetworkResource<ResultType, RequestType>
@MainThread constructor(private val appExecutors: AppExecutors) {

    private val result = MediatorLiveData<Result<ResultType>>()

    init {
        result.value = Result.loading()
        fetchFromNetwork()
    }

    @MainThread
    private fun setValue(newValue: Result<ResultType>) {
        if (result.value != newValue) {
            result.value = newValue
        }
    }

    private fun fetchFromNetwork() {
        val apiResponse = createCall()
        setValue(Result.loading())
        result.addSource(apiResponse) { response ->
            result.removeSource(apiResponse)
            if (response.success) {
                val data = response.data
                if (data != null) {
                    appExecutors.diskIO.execute {
                        saveCallResult(data)
                        appExecutors.mainThread.execute {
                            setValue(Result.success(message = response.message, data = transfer(data)))
                        }
                    }
                }
            } else {
                onFetchFailed()
                appExecutors.mainThread.execute {
                    setValue(Result.error(message = response.message, code = response.code))
                }
            }
        }
    }

    protected open fun onFetchFailed() {}

    fun asLiveData() = result as LiveData<Result<ResultType>>

    @WorkerThread
    protected open fun saveCallResult(item: RequestType?) {

    }

    protected open fun transfer(item: RequestType?): ResultType? {
        @Suppress("UNCHECKED_CAST")
        return if (item == null) null else item as ResultType
    }

    @MainThread
    protected abstract fun createCall(): LiveData<ApiResponse<RequestType>>
}
