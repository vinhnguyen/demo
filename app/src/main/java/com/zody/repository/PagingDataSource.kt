package com.zody.repository

import androidx.paging.ItemKeyedDataSource
import androidx.room.InvalidationTracker
import androidx.room.RoomDatabase
import com.zody.entity.ListItem

/**
 * Created by vinhnguyen.it.vn on 2018, July 30
 */
abstract class PagingDataSource<T>(database: RoomDatabase, firstTable: String, vararg rest: String) : ItemKeyedDataSource<ListItem<T>, ListItem<T>>() {

    init {
        database.invalidationTracker.addObserver(object : InvalidationTracker.Observer(firstTable, *rest) {
            override fun onInvalidated(tables: MutableSet<String>) {
                database.invalidationTracker.removeObserver(this)
                invalidate()
            }
        })
    }


    override fun loadInitial(params: LoadInitialParams<ListItem<T>>, callback: LoadInitialCallback<ListItem<T>>) {
        val size = params.requestedLoadSize
        val key = params.requestedInitialKey
        if (key != null) {
            val order = key.order
            val halfSize = size / 2
            val before = loadBefore(order = order, limit = halfSize)

            val remainingSize = size - before.size
            val currentAndAfter = loadCurrentAndAfter(order = order, limit = remainingSize)

            val data = mutableListOf<ListItem<T>>()
            data.addAll(before)
            data.addAll(currentAndAfter)
            callback.onResult(data, 0, data.size)
        } else {
            val data = loadInitial(limit = size)
            callback.onResult(data, 0, data.size)
        }
    }

    override fun loadAfter(params: LoadParams<ListItem<T>>, callback: LoadCallback<ListItem<T>>) {
        val data = loadAfter(order = params.key.order, limit = params.requestedLoadSize)
        callback.onResult(data)
    }

    override fun loadBefore(params: LoadParams<ListItem<T>>, callback: LoadCallback<ListItem<T>>) {
        val data = loadBefore(order = params.key.order, limit = params.requestedLoadSize)
        callback.onResult(data)
    }

    override fun getKey(item: ListItem<T>): ListItem<T> {
        return item
    }

    abstract fun loadInitial(limit: Int): List<ListItem<T>>

    abstract fun loadCurrentAndAfter(order: Int, limit: Int): List<ListItem<T>>

    abstract fun loadAfter(order: Int, limit: Int): List<ListItem<T>>

    abstract fun loadBefore(order: Int, limit: Int): List<ListItem<T>>

}