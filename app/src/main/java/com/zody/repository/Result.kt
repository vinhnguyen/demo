package com.zody.repository

import com.zody.api.ApiResponse

/**
 * Created by vinhnguyen.it.vn on 2018, July 15
 */
data class Result<T>(val loading: Boolean, val success: Boolean?, val data: T?, val message: String?, val code: Int?) {

    companion object {
        fun <T> loading(data: T? = null, message: String? = null, code: Int? = null) = Result(true, null, data, message, code)

        fun <T> success(data: T? = null, message: String? = null, code: Int? = null) = Result(false, true, data, message, code)

        fun <T> error(data: T? = null, message: String? = null, code: Int? = null) = Result(false, false, data, message, code)

        fun <T> result(apiResponse: ApiResponse<T>) = Result(false, apiResponse.success, apiResponse.data, apiResponse.message, apiResponse.code)
    }
}