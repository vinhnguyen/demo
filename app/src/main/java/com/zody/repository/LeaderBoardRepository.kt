package com.zody.repository

import androidx.lifecycle.LiveData
import com.zody.api.ApiResponse
import com.zody.api.ApiService
import com.zody.entity.LeaderBoardItem
import com.zody.entity.LeaderBoardResult
import com.zody.utils.AppExecutors
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 18
 */
class LeaderBoardRepository @Inject constructor(private val appExecutors: AppExecutors,
                                                private val appService: ApiService) {

    fun loadLeaderBoard(type: String, time: String, city: String): LiveData<Result<List<LeaderBoardItem>>> {

        return object : NetworkResource<List<LeaderBoardItem>, LeaderBoardResult>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<LeaderBoardResult>> {
                return appService.loadLeaderBoard(type, time, city)
            }

            override fun transfer(item: LeaderBoardResult?): List<LeaderBoardItem>? {
                return item?.users
            }
        }.asLiveData()
    }


}