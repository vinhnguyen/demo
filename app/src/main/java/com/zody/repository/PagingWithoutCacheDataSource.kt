package com.zody.repository

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.zody.api.ApiPaging
import com.zody.api.ApiResponse
import com.zody.utils.AppExecutors
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

/**
 * Created by vinhnguyen.it.vn on 2018, August 17
 */
abstract class PagingWithoutCacheDataSource<ResultType, RequestType>(private val appExecutors: AppExecutors) : PageKeyedDataSource<String, ResultType>() {
    // keep a function reference for the retry event
    private var retry: (() -> Any)? = null

    /**
     * There is no sync on the state because paging will always call loadInitial first then wait
     * for it to return some success value before calling loadAfter.
     */
    val networkState = MutableLiveData<NetworkState>()

    val initialLoad = MutableLiveData<NetworkState>()

    var endData = MutableLiveData<Boolean>()

    private var isEnd: Boolean = false
        set(value) {
            field = value
            endData.postValue(value)
        }

    fun retryAllFailed() {
        val prevRetry = retry
        retry = null
        prevRetry?.let {
            appExecutors.networkIO.execute {
                it.invoke()
            }
        }
    }

    override fun loadInitial(params: LoadInitialParams<String>, callback: LoadInitialCallback<String, ResultType>) {
        networkState.postValue(NetworkState.loading())
        initialLoad.postValue(NetworkState.loading())
        isEnd = false

        // triggered by a refresh, we better execute sync
        try {
            val response = createApi(after = null).execute()
            val paging = response.body()?.data
            val items = paging?.data ?: emptyList()
            retry = null
            networkState.postValue(NetworkState.success())
            initialLoad.postValue(NetworkState.success())
            isEnd = paging?.endData != false
            callback.onResult(transfer(items), null, paging?.next)
        } catch (ioException: IOException) {
            retry = {
                loadInitial(params, callback)
            }
            val error = NetworkState.error(ioException.message)
            networkState.postValue(error)
            initialLoad.postValue(error)
            isEnd = false
        }
    }

    override fun loadAfter(params: LoadParams<String>, callback: LoadCallback<String, ResultType>) {
        if (isEnd) return
        networkState.postValue(NetworkState.loading())
        createApi(after = params.key)
                .enqueue(object : Callback<ApiResponse<ApiPaging<RequestType>>> {
                    override fun onFailure(call: Call<ApiResponse<ApiPaging<RequestType>>>?, t: Throwable?) {
                        retry = {
                            loadAfter(params, callback)
                        }
                        networkState.postValue(NetworkState.error(t?.message))
                    }

                    override fun onResponse(call: Call<ApiResponse<ApiPaging<RequestType>>>?, response: Response<ApiResponse<ApiPaging<RequestType>>>?) {
                        if (response?.isSuccessful == true && response.body()?.success == true) {
                            val paging = response.body()?.data
                            val items = paging?.data ?: emptyList()
                            retry = null
                            callback.onResult(transfer(items), paging?.next)
                            networkState.postValue(NetworkState.success())
                            isEnd = paging?.endData != false
                        } else {
                            retry = { loadAfter(params, callback) }
                            networkState.postValue(NetworkState.error(null))
                        }
                    }
                })
    }

    override fun loadBefore(params: LoadParams<String>, callback: LoadCallback<String, ResultType>) {
        //do nothing
    }

    abstract fun createApi(after: String?): Call<ApiResponse<ApiPaging<RequestType>>>

    abstract fun transfer(data: List<RequestType>): List<ResultType>
}