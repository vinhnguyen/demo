package com.zody.repository

import android.os.Build
import com.zody.api.ApiTracking
import com.zody.db.ProfileDb
import com.zody.utils.AppExecutors
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by vinhnguyen.it.vn on 2018, August 17
 */
@Singleton
class TrackingRepository @Inject constructor(private val appExecutors: AppExecutors,
                                             private val profileDb: ProfileDb,
                                             private val apiTracking: ApiTracking,
                                             @Named("deviceId")
                                             private val deviceId: String) {

    private val defaultParams = mutableMapOf<String, Any?>()


    init {
        appExecutors.diskIO.execute {
            val userId = profileDb.profileDao().getProfile()?.id
            defaultParams.apply {
                put("user", userId)
                put("timestamp", System.currentTimeMillis())
                put("device", mapOf(
                        "os" to "android",
                        "version" to Build.VERSION.SDK_INT,
                        "deviceId" to deviceId))
            }
        }
    }

    fun trackAfterView(postId: String) {
        track("views", mapOf(
                "review" to postId
        ))
    }

    fun trackAfterTip(postId: String, coin: Int) {
        track("tips", mapOf(
                "review" to postId,
                "coin" to coin))
    }

    private fun track(type: String, params: Map<String, Any?>) {
        val sendParams = mutableMapOf<String, Any?>().apply {
            putAll(params)
            putAll(defaultParams)
        }
        apiTracking.tracking(type, sendParams).enqueue(object : Callback<Any> {
            override fun onFailure(call: Call<Any>?, t: Throwable?) {

            }

            override fun onResponse(call: Call<Any>?, response: Response<Any>?) {

            }
        })
    }
}