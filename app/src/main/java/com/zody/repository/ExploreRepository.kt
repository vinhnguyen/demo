package com.zody.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import com.zody.api.ApiPaging
import com.zody.api.ApiResponse
import com.zody.api.ApiService
import com.zody.api.ApiServiceOldVersion
import com.zody.db.ConfigDb
import com.zody.entity.BusinessList
import com.zody.entity.BusinessPromotion
import com.zody.entity.ListItem
import com.zody.entity.business.BusinessOfChain
import com.zody.entity.explore.Category
import com.zody.entity.explore.ExploreData
import com.zody.entity.explore.OnlineShopping
import com.zody.entity.explore.OnlineShoppingOrder
import com.zody.ui.explore.online.OrderCoinItem
import com.zody.ui.explore.online.OrderItem
import com.zody.ui.explore.online.OrderProductItem
import com.zody.utils.AppExecutors
import retrofit2.Call
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 04
 */
class ExploreRepository @Inject constructor(private val appExecutors: AppExecutors,
                                            private val apiService: ApiService,
                                            private val apiServiceOldVersion: ApiServiceOldVersion,
                                            private val configDb: ConfigDb) {


    fun loadExplorerData(city: String?, latitude: Double?, longitude: Double?): LiveData<Result<ExploreData>> {
        return object : NetworkResource<ExploreData, ExploreData>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<ExploreData>> {
                return apiService.loadExplorerData(city, latitude, longitude)
            }
        }.asLiveData()
    }

    fun loadCategories(): LiveData<List<Category>> {
        return configDb.configDao().loadCategories()
    }

    fun loadExploreQrCodeBusiness(city: String?, latitude: Double?, longitude: Double?): LiveData<Result<ApiPaging<BusinessList>>> {
        return object : NetworkResource<ApiPaging<BusinessList>, ApiPaging<BusinessList>>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<ApiPaging<BusinessList>>> {
                return apiService.loadExploreQrCodeBusiness(null, city, latitude, longitude)
            }

        }.asLiveData()
    }

    fun loadExploreNewBusiness(city: String?, latitude: Double?, longitude: Double?): LiveData<Result<ApiPaging<BusinessList>>> {
        return object : NetworkResource<ApiPaging<BusinessList>, ApiPaging<BusinessList>>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<ApiPaging<BusinessList>>> {
                return apiService.loadExploreNewBusiness(null, city, latitude, longitude)
            }

        }.asLiveData()
    }

    fun loadExploreTrending(city: String?, latitude: Double?, longitude: Double?): LiveData<Result<ApiPaging<BusinessList>>> {
        return object : NetworkResource<ApiPaging<BusinessList>, ApiPaging<BusinessList>>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<ApiPaging<BusinessList>>> {
                return apiService.loadExploreTrendingBusiness(null, city, latitude, longitude)
            }

        }.asLiveData()
    }

    fun loadExplorePromotion(city: String?, latitude: Double?, longitude: Double?): LiveData<Result<ApiPaging<BusinessPromotion>>> {
        return object : NetworkResource<ApiPaging<BusinessPromotion>, ApiPaging<BusinessPromotion>>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<ApiPaging<BusinessPromotion>>> {
                return apiService.loadExplorePromotionBusiness(null, city, latitude, longitude)
            }

        }.asLiveData()
    }

    fun loadBusinessOfCategory(id: String?, city: String?, latitude: Double?, longitude: Double?): Listing<BusinessList> {
        return loadExploreBusiness { after ->
            apiService.loadBusinessOfCategory(id, next = after, city = city, latitude = latitude, longitude = longitude)
        }
    }

    fun loadQrCodeBusiness(city: String?, latitude: Double?, longitude: Double?): Listing<BusinessList> {
        return loadExploreBusiness { after ->
            apiService.loadQrCodeBusiness(after, city, latitude, longitude)
        }
    }

    fun loadNewBusiness(city: String?, latitude: Double?, longitude: Double?): Listing<BusinessList> {
        return loadExploreBusiness { after ->
            apiService.loadNewBusiness(after, city, latitude, longitude)
        }
    }

    fun loadTrending(city: String?, latitude: Double?, longitude: Double?): Listing<BusinessList> {
        return loadExploreBusiness { after ->
            apiService.loadTrendingBusiness(next = after, city = city, latitude = latitude, longitude = longitude)
        }
    }

    fun loadPromotion(city: String?, latitude: Double?, longitude: Double?): Listing<BusinessPromotion> {
        return loadExploreBusiness { after ->
            apiService.loadPromotionBusiness(next = after, city = city, latitude = latitude, longitude = longitude)
        }
    }

    private fun <T> loadExploreBusiness(function: (String?) -> Call<ApiResponse<ApiPaging<T>>>): Listing<T> {
        val sourceFactory = object : DataSource.Factory<String, T>() {

            val sourceLiveData = MutableLiveData<PagingWithoutCacheDataSource<T, T>>()

            override fun create(): DataSource<String, T> {
                val dataSource = object : PagingWithoutCacheDataSource<T, T>(appExecutors) {
                    override fun transfer(data: List<T>): List<T> {
                        return data
                    }

                    override fun createApi(after: String?): Call<ApiResponse<ApiPaging<T>>> {
                        return function(after)
                    }
                }
                sourceLiveData.postValue(dataSource)
                return dataSource
            }
        }
        val pageList = LivePagedListBuilder<String, T>(sourceFactory, 15).build()
        val endData = Transformations.switchMap(sourceFactory.sourceLiveData) {
            it.endData
        }
        val refreshState = Transformations.switchMap(sourceFactory.sourceLiveData) {
            it.initialLoad
        }
        return Listing(
                data = ListingData.createLiveData(pageList, endData),
                networkState = Transformations.switchMap(sourceFactory.sourceLiveData) {
                    it.networkState
                },
                retry = {
                    sourceFactory.sourceLiveData.value?.retryAllFailed()
                },
                refresh = {
                    sourceFactory.sourceLiveData.value?.invalidate()
                },
                refreshState = refreshState
        )
    }

    fun loadOnlineShopping(id: String): LiveData<Result<OnlineShopping>> {
        return object : NetworkResource<OnlineShopping, OnlineShopping>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<OnlineShopping>> {
                return apiService.loadAffiliate(id)
            }
        }.asLiveData()
    }

    fun loadOnlineShoppingOrder(type: String, id: String): Listing<ListItem<OnlineShoppingOrder>> {
        val sourceFactory = object : DataSource.Factory<Int, ListItem<OnlineShoppingOrder>>() {

            val sourceLiveData = MutableLiveData<PagingWithPageDataSource<ListItem<OnlineShoppingOrder>, OnlineShoppingOrder>>()

            override fun create(): DataSource<Int, ListItem<OnlineShoppingOrder>> {
                val dataSource = object : PagingWithPageDataSource<ListItem<OnlineShoppingOrder>, OnlineShoppingOrder>(appExecutors) {
                    override fun transfer(data: List<OnlineShoppingOrder>): List<ListItem<OnlineShoppingOrder>> {
                        return data.flatMap { order ->
                            mutableListOf<ListItem<OnlineShoppingOrder>>().apply {
                                add(OrderItem(order))
                                if (order.products?.isNotEmpty() == true) {
                                    addAll(order.products.map { OrderProductItem(it) })
                                }
                                order.coin?.let { add(OrderCoinItem(it)) }
                            }
                        }
                    }

                    override fun createApi(page: Int): Call<ApiResponse<ApiPaging<OnlineShoppingOrder>>> {
                        return apiServiceOldVersion.loadOnlineShoppingOrder(type, id, page)
                    }
                }
                sourceLiveData.postValue(dataSource)
                return dataSource
            }
        }
        val pageList = LivePagedListBuilder<Int, ListItem<OnlineShoppingOrder>>(sourceFactory, 15).build()
        val endData = Transformations.switchMap(sourceFactory.sourceLiveData) {
            it.endData
        }
        val refreshState = Transformations.switchMap(sourceFactory.sourceLiveData) {
            it.initialLoad
        }
        return Listing(
                data = ListingData.createLiveData(pageList, endData),
                networkState = Transformations.switchMap(sourceFactory.sourceLiveData) {
                    it.networkState
                },
                retry = {
                    sourceFactory.sourceLiveData.value?.retryAllFailed()
                },
                refresh = {
                    sourceFactory.sourceLiveData.value?.invalidate()
                },
                refreshState = refreshState
        )
    }

    fun loadChain(id: String?): LiveData<Result<BusinessOfChain>> {
        return object : NetworkResource<BusinessOfChain, BusinessOfChain>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<BusinessOfChain>> {
                return apiService.loadBusinessOfChain(id)
            }
        }.asLiveData()
    }
}