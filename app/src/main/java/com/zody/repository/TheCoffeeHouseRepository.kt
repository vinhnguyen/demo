package com.zody.repository

import androidx.lifecycle.LiveData
import com.zody.api.ApiResponse
import com.zody.api.ApiService
import com.zody.entity.TheCoffeeHouseLinkedProgram
import com.zody.utils.AppExecutors
import com.zody.utils.bodyOf
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by vinhnguyen.it.vn on 2018, October 02
 */
@Singleton
class TheCoffeeHouseRepository @Inject constructor(private val appExecutors: AppExecutors,
                                                   private val apiService: ApiService) {

    fun loadTheCoffeeHouseLinkedProgram(): LiveData<Result<TheCoffeeHouseLinkedProgram>> {
        return object : NetworkResource<TheCoffeeHouseLinkedProgram, TheCoffeeHouseLinkedProgram>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<TheCoffeeHouseLinkedProgram>> {
                return apiService.loadTheCoffeeHouse()
            }
        }.asLiveData()
    }

    fun update(id: String): LiveData<Result<Unit>> {
        return object : NetworkResource<Unit, Unit>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<Unit>> {
                return apiService.updateTheCoffeeHouse(bodyOf("tchId" to id))
            }
        }.asLiveData()
    }

    fun delete(): LiveData<Result<Unit>> {
        return object : NetworkResource<Unit, Unit>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<Unit>> {
                return apiService.deleteTheCoffeeHouse()
            }

        }.asLiveData()
    }
}