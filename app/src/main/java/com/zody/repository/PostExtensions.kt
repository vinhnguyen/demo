package com.zody.repository

import com.zody.entity.*

/**
 * Created by vinhnguyen.it.vn on 2018, July 30
 */
fun Post.toItemTypes(): List<Int> {
    val types = mutableListOf<Int>()
    types.add(PostListItem.TYPE_AUTHOR_AND_TIME)
    types.add(PostListItem.TYPE_BUSINESS_AND_POINT)
    if (this.feedback?.isNotEmpty() == true || this.hashtags?.isNotEmpty() == true) {
        types.add(PostListItem.TYPE_FEEDBACK)
    }
    this.photos?.let {
        val type = when (it.size) {
            0 -> null
            1 -> PostListItem.TYPE_PHOTO_ONE
            else -> PostListItem.TYPE_PHOTO_MULTI
        }
        if (type != null) {
            types.add(type)
        }
    }
    if (this.statistic?.comments ?: 0 > 0 || this.statistic?.tipped ?: 0 > 0) {
        types.add(PostListItem.TYPE_STATISTIC)
    }
    types.add(PostListItem.TYPE_ACTION)
    return types
}

fun PostQueryItem.toListItems(): List<ListItem<Post>> {
    return this.post?.toItemTypes()?.map {
        ListItem(data = this.post,
                next = this.item?.next,
                order = this.item?.order ?: -1,
                type = it)
    } ?: emptyList<ListItem<Post>>()
}

fun List<PostQueryItem>.toListItems(): List<ListItem<Post>> {
    return this.flatMap { it.toListItems() }
}

fun List<PostQueryItem>.toListAuthors(): List<ListItem<Author>> {
    return this.mapNotNull {
        if (it.author != null) ListItem(it.author,
                next = it.item?.next,
                order = it.item?.order ?: -1,
                type = 0)
        else null
    }
}

fun PostPhotoItem.toListPhotoItem(): ListItem<Post> {
    val post = Post()
    post.id = this.postId
    return PostPhotoListItem(photo = this.photo, data = post, next = this.next, order = this.order, type = 0)
}