package com.zody.repository

import android.arch.paging.PagingRequestHelper
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import com.zody.api.ApiPaging
import com.zody.api.ApiResponse
import com.zody.entity.ListItem
import com.zody.utils.AppExecutors
import com.zody.utils.createStatusLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by vinhnguyen.it.vn on 2018, July 30
 */
abstract class PagingRequestBoundaryCallback<ResultType, RequestType>(private val appExecutors: AppExecutors) : PagedList.BoundaryCallback<ListItem<ResultType>>() {

    val helper = PagingRequestHelper(appExecutors.networkIO)
    val networkState: LiveData<NetworkState> = helper.createStatusLiveData()
    val endData = MutableLiveData<Boolean>()

    private var isEnd = false
        set(value) {
            field = value
            endData.postValue(value)
        }

    override fun onZeroItemsLoaded() {
        super.onZeroItemsLoaded()
        if (isEnd) {
            return
        }
        helper.runIfNotRunning(PagingRequestHelper.RequestType.INITIAL) {
            createApi(next = null).enqueue(createCallback(it, null))
        }
    }

    override fun onItemAtEndLoaded(itemAtEnd: ListItem<ResultType>) {
        if (isEnd) {
            return
        }
        helper.runIfNotRunning(PagingRequestHelper.RequestType.AFTER) {
            createApi(next = itemAtEnd.next).enqueue(createCallback(it, itemAtEnd.next))
        }
    }

    private fun createCallback(it: PagingRequestHelper.Request.Callback, current: String?): Callback<ApiResponse<ApiPaging<RequestType>>> {
        return object : Callback<ApiResponse<ApiPaging<RequestType>>> {
            override fun onFailure(call: Call<ApiResponse<ApiPaging<RequestType>>>?, t: Throwable) {
                it.recordFailure(t)
            }

            override fun onResponse(call: Call<ApiResponse<ApiPaging<RequestType>>>?, response: Response<ApiResponse<ApiPaging<RequestType>>>?) {
                appExecutors.diskIO.execute {
                    isEnd = response?.body()?.data?.endData != false
                    insertToDatabase(paging = response?.body()?.data, current = current)
                    it.recordSuccess()
                }
            }
        }
    }

    fun refresh(): LiveData<NetworkState> {
        val networkState = MutableLiveData<NetworkState>()
        networkState.value = NetworkState.loading()
        endData.postValue(false)
        createApi(next = null).enqueue(object : Callback<ApiResponse<ApiPaging<RequestType>>> {
            override fun onResponse(call: Call<ApiResponse<ApiPaging<RequestType>>>?, response: Response<ApiResponse<ApiPaging<RequestType>>>?) {
                appExecutors.diskIO.execute {
                    isEnd = response?.body()?.data?.endData != false
                    insertToDatabase(paging = response?.body()?.data, current = null)
                    networkState.postValue(NetworkState.success())
                }
            }

            override fun onFailure(call: Call<ApiResponse<ApiPaging<RequestType>>>?, t: Throwable?) {
                networkState.postValue(NetworkState.error(t?.message))
            }

        })
        return networkState
    }

    abstract fun createApi(next: String?): Call<ApiResponse<ApiPaging<RequestType>>>

    abstract fun insertToDatabase(paging: ApiPaging<RequestType>?, current: String?)
}

abstract class PagingBoundaryCallback<T>(appExecutors: AppExecutors) : PagingRequestBoundaryCallback<T, T>(appExecutors)