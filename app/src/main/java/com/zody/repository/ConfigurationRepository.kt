package com.zody.repository

import android.app.Application
import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.reflect.TypeToken
import com.zody.api.ApiResponse
import com.zody.api.ApiService
import com.zody.db.ConfigDao
import com.zody.db.ProfileDao
import com.zody.entity.City
import com.zody.entity.ZcoinCenter
import com.zody.entity.ZcoinCenterItem
import com.zody.entity.explore.Category
import com.zody.livedata.LiveDataProviders
import com.zody.utils.AppExecutors
import com.zody.utils.SPKey
import com.zody.utils.SPStorage
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.nio.charset.Charset
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by vinhnguyen.it.vn on 2018, July 12
 */
@Singleton
class ConfigurationRepository @Inject constructor(private val app: Application,
                                                  private val appExecutors: AppExecutors,
                                                  private val sp: SharedPreferences,
                                                  private val spStorage: SPStorage,
                                                  private val gson: Gson,
                                                  private val apiService: ApiService,
                                                  private val configDao: ConfigDao,
                                                  private val profileDao: ProfileDao) {

    fun getConfig() {
        apiService.loadConfiguration().enqueue(object : Callback<ApiResponse<JsonElement>> {
            override fun onFailure(call: Call<ApiResponse<JsonElement>>?, t: Throwable?) {
                initConfigurationFrommAssets()
            }

            override fun onResponse(call: Call<ApiResponse<JsonElement>>?, response: Response<ApiResponse<JsonElement>>?) {
                response?.body()?.data?.let { data ->
                    appExecutors.diskIO.execute {
                        try {
                            saveConfiguration(data.toString())
                            sp.edit {
                                putBoolean(SPKey.PREF_LOADED_CONFIGURATION, true)
                            }
                        } catch (e: JSONException) {
                            Timber.e(e)
                            initConfigurationFrommAssets()
                        }
                    }
                    return
                }
                initConfigurationFrommAssets()
            }
        })
    }

    fun loadCity(): LiveData<List<City>> {
        return configDao.loadCityAsLiveData()
    }

    fun getNearestCity(latitude: Double, longitude: Double): LiveData<City> {
        return MutableLiveData<City>().apply {
            appExecutors.diskIO.execute {
                val cities = configDao.loadCity()
                var min: Double = Double.MAX_VALUE
                var temp: City? = null
                cities.forEach {
                    val distance = it.location?.distanceTo(latitude, longitude) ?: Double.MAX_VALUE
                    if (distance < min) {
                        min = distance
                        temp = it
                    }
                }
                appExecutors.mainThread.execute {
                    value = temp
                }
            }
        }
    }

    fun getCity(id: String?): LiveData<City> {
        if (id == null) {
            return LiveDataProviders.createAbsent()
        }
        return configDao.loadCityAsLiveData(id)
    }

    fun loadZcoinCenter(): LiveData<List<ZcoinCenterItem>> {
        return configDao.loadZcoinCenters()
    }

    fun loadZcoinCenterItem(id: String): LiveData<ZcoinCenterItem> {
        return configDao.loadZcoinCenterItem(id)
    }

    @Throws(JSONException::class)
    private fun saveConfiguration(value: String) {
        val config = JSONObject(value)
        sp.edit {
            config.optJSONObject("photo")?.let { photo ->
                putLong(SPKey.PREF_PHOTO_MAX_SIZE, photo.optLong("maxSize"))
            }
            config.optJSONObject("review")?.let { review ->
                putLong(SPKey.PREF_REVIEW_TIME_TO_INCREASE_VIEW, review.optLong("timeToIncreaseView"))
                putLong(SPKey.PREF_REVIEW_MIN_VIEW_TO_DISPLAY, review.optLong("minViewToDisplayViewStat"))
            }
            config.optJSONObject("tip")?.let { tip ->
                putLong(SPKey.PREF_TIP_MIN_COIN, tip.optLong("minCoinToTip"))
                putLong(SPKey.PREF_TIP_DELAY_TIME, tip.optLong("delayTime"))
            }
        }
        config.opt("cities")?.toString()?.let {
            val cities = gson.fromJson<List<City>>(it, object : TypeToken<List<City>>() {}.type)
            configDao.deleteAllCity()
            configDao.insertCity(cities)
        }

        config.opt("filterCities")?.toString()?.let {
            val filterCities = gson.fromJson<List<String>>(it, object : TypeToken<List<String>>() {}.type)
            configDao.updateIsFilter(filterCities)
        }

        config.opt("zcoinCenter")?.toString()?.let { json ->
            gson.fromJson<ZcoinCenter>(json, ZcoinCenter::class.java)?.apply {
                items?.apply {
                    forEach {
                        it.link = link
                    }
                    configDao.deleteZcoinCenters()
                    configDao.insertZcoinCenters(this)
                }
            }
        }

        config.opt("categories")?.toString()?.let { json ->
            gson.fromJson<List<Category>>(json, object : TypeToken<List<Category>>() {}.type)?.apply {
                configDao.deleteCategories()
                configDao.insertCategories(this)
            }
        }

        val coinWhenInputSuccess = config.optJSONObject("referral")?.optInt("coinReceiveWhenInputSuccess")
        sp.edit {
            putInt(SPKey.PREF_REFERRAL_INPUT_SUCCESS, coinWhenInputSuccess ?: 0)
        }

        val coinWhenVerifyPhoneSuccess = config.optJSONObject("coinForActions")?.optInt("verifyPhone")
        sp.edit {
            putInt(SPKey.PREF_VERIFY_PHONE_SUCCESS, coinWhenVerifyPhoneSuccess ?: 0)
        }

        spStorage.newVersion = config.optJSONObject("version")?.optString("android")
        spStorage.qrCodeTimeOut = config.optJSONObject("scanPrinterQRCode")?.optLong("timeout")
    }

    private fun initConfigurationFrommAssets() {
        if (sp.getBoolean(SPKey.PREF_LOADED_CONFIGURATION, false)) {
            return
        }
        appExecutors.diskIO.execute {
            val stream = app.assets.open("configuration.json")
            val size = stream.available()
            val buffer = ByteArray(size)
            stream.read(buffer)
            stream.close()

            saveConfiguration(String(buffer, Charset.forName("UTF-8")))

            sp.edit {
                putBoolean(SPKey.PREF_LOADED_CONFIGURATION, true)
            }
        }
    }

    fun getMinCoinForTip(): LiveData<Long> {
        return MutableLiveData<Long>().apply {
            value = sp.getLong(SPKey.PREF_TIP_MIN_COIN, 0)
            sp.registerOnSharedPreferenceChangeListener { sharedPreferences, key ->
                if (key == SPKey.PREF_TIP_MIN_COIN) {
                    value = sharedPreferences.getLong(SPKey.PREF_TIP_MIN_COIN, 0)
                }
            }
        }
    }

    fun getReferralInputSuccess(): Int {
        return sp.getInt(SPKey.PREF_REFERRAL_INPUT_SUCCESS, 0)
    }

    fun getCoinWhenVerifyPhoneSuccess(): Int {
        return sp.getInt(SPKey.PREF_VERIFY_PHONE_SUCCESS, 0)
    }

    fun getNewVersion(): LiveData<String> {
        return spStorage.newVersionLiveData
    }

}