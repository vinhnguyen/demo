package com.zody.repository

/**
 * Created by vinhnguyen.it.vn on 2018, July 15
 */

enum class Status {
    RUNNING,
    SUCCESS,
    FAILED
}

@Suppress("DataClassPrivateConstructor")
data class NetworkState private constructor(
        private val status: Status,
        val msg: String? = null) {

    val loading: Boolean
        get() = status == Status.RUNNING
    val success: Boolean
        get() = status == Status.SUCCESS

    companion object {
        fun success() = NetworkState(Status.SUCCESS)
        fun loading() = NetworkState(Status.RUNNING)
        fun error(msg: String?) = NetworkState(Status.FAILED, msg)
    }
}