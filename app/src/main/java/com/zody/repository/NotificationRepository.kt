package com.zody.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import com.zody.api.ApiPaging
import com.zody.api.ApiResponse
import com.zody.api.ApiService
import com.zody.db.NotificationDb
import com.zody.entity.ListItem
import com.zody.entity.Notification
import com.zody.entity.NotificationItem
import com.zody.livedata.UnreadNotificationLiveData
import com.zody.utils.AppExecutors
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, August 08
 */
class NotificationRepository @Inject constructor(private val appExecutors: AppExecutors,
                                                 private val unreadNotification: UnreadNotificationLiveData,
                                                 private val apiService: ApiService,
                                                 private val notificationDb: NotificationDb) {

    companion object {
        const val LOAD_NOTIFICATION_SIZE_PER_PAGE = 20
    }

    fun loadNotification(): Listing<ListItem<Notification>> {
        val dataSourceFactory: DataSource.Factory<Int, ListItem<Notification>> =
                notificationDb.notificationDao().load().map {
                    ListItem(data = it.data, next = it.next, order = it.order, type = 0)
                }
        val boundaryCallback = object : PagingBoundaryCallback<Notification>(appExecutors) {
            override fun createApi(next: String?): Call<ApiResponse<ApiPaging<Notification>>> {
                return apiService.loadNotification(next)
            }

            override fun insertToDatabase(paging: ApiPaging<Notification>?, current: String?) {
                notificationDb.runInTransaction {
                    val notificationDao = notificationDb.notificationDao()
                    if (current == null) {
                        notificationDao.deleteAll()
                        notificationDao.deleteAllItems()
                    }
                    paging?.data?.let { data ->
                        var lastedOrder = notificationDao.getLastedOrder() ?: -1
                        val items = data.map {
                            NotificationItem(it.id, paging.next, ++lastedOrder)
                        }
                        notificationDao.insertItems(items)
                        notificationDao.insert(data)
                    }
                }
            }
        }
        val pageList = LivePagedListBuilder<Int, ListItem<Notification>>(dataSourceFactory, LOAD_NOTIFICATION_SIZE_PER_PAGE)
                .setBoundaryCallback(boundaryCallback)
                .build()
        val refreshTrigger = MutableLiveData<Unit>()
        val refreshState = Transformations.switchMap(refreshTrigger) {
            boundaryCallback.refresh()
        }

        return Listing(
                data = ListingData.createLiveData(pageList, boundaryCallback.endData),
                networkState = boundaryCallback.networkState,
                retry = {
                    boundaryCallback.helper.retryAllFailed()
                },
                refresh = {
                    refreshTrigger.value = null
                },
                refreshState = refreshState)
    }

    fun getUnreadNotification(): LiveData<Int> {
        return Transformations.map(unreadNotification) { it.size }
    }

    fun readNotification(id: String) {
        unreadNotification.remove(id)
        appExecutors.diskIO.execute {
            notificationDb.notificationDao().setRead(id, true)
        }
        apiService.readNotification(id).enqueue(object : Callback<ApiResponse<Unit>> {
            override fun onFailure(call: Call<ApiResponse<Unit>>?, t: Throwable?) {

            }

            override fun onResponse(call: Call<ApiResponse<Unit>>?, response: Response<ApiResponse<Unit>>?) {

            }
        })
    }

    fun forceRead() {
        unreadNotification.clear()
        apiService.forceReadNotification().enqueue(object : Callback<ApiResponse<Unit>> {
            override fun onFailure(call: Call<ApiResponse<Unit>>, t: Throwable) {

            }

            override fun onResponse(call: Call<ApiResponse<Unit>>, response: Response<ApiResponse<Unit>>) {

            }
        })
    }
}