package com.zody.repository

import androidx.lifecycle.LiveData
import com.zody.api.ApiResponse
import com.zody.api.ApiService
import com.zody.entity.SearchResult
import com.zody.utils.AppExecutors
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 22
 */
class SearchRepository @Inject constructor(private val appExecutors: AppExecutors,
                                           private val apiService: ApiService) {

    fun search(keyword: String, latitude: Double?, longitude: Double?, city: String?): LiveData<Result<SearchResult>> {
        return object : NetworkResource<SearchResult, SearchResult>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<SearchResult>> {
                return apiService.search(keyword, latitude, longitude, city)
            }
        }.asLiveData()
    }
}