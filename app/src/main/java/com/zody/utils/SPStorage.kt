package com.zody.utils

import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.lifecycle.MutableLiveData
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by vinhnguyen.it.vn on 2018, October 08
 */
@Singleton
class SPStorage @Inject constructor(private val sp: SharedPreferences) {

    val newVersionLiveData = MutableLiveData<String>()

    var newVersion: String? = null
        get() = sp.getString(SPKey.PREF_NEW_VERSION, null)
        set(value) {
            field = value
            sp.edit { putString(SPKey.PREF_NEW_VERSION, value) }
            newVersionLiveData.postValue(value)
        }

    var qrCodeTimeOut: Long? = null
        get() = sp.getLong(SPKey.PREF_QR_CODE_TIMEOUT, 15 * 60 * 1000)
        set(value) {
            field = value
            sp.edit { putLong(SPKey.PREF_QR_CODE_TIMEOUT, value ?: 0) }
        }

    var openPermissionSetting: Boolean = false
        get() = sp.getBoolean(SPKey.PREF_OPEN_PERMISSION_SETTING, false)
        set(value) {
            field = value
            sp.edit { putBoolean(SPKey.PREF_OPEN_PERMISSION_SETTING, value) }
        }

    var showLuckyDrawGuide: Boolean = true
        get() = sp.getBoolean(SPKey.PREF_SHOW_LUCKY_DRAW_GUIDE, true)
        set(value) {
            field = value
            sp.edit { putBoolean(SPKey.PREF_SHOW_LUCKY_DRAW_GUIDE, value) }
        }
}