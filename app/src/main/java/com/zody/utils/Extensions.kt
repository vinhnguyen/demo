package com.zody.utils

import android.app.Activity
import android.arch.paging.PagingRequestHelper
import android.content.*
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.*
import android.net.Uri
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import android.text.Editable
import android.text.Html
import android.text.Spanned
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.annotation.WorkerThread
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.ViewDataBinding
import androidx.exifinterface.media.ExifInterface
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.transition.Transition
import androidx.viewpager.widget.ViewPager
import com.google.android.material.snackbar.Snackbar
import com.zody.R
import com.zody.repository.NetworkState
import com.zody.repository.Result
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.joda.time.*
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.ISODateTimeFormat
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.FileNotFoundException
import java.io.IOException
import java.io.InputStream
import java.security.MessageDigest
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.collections.ArrayList


/**
 * Created by vinhnguyen.it.vn on 2018, July 14
 */
val Int.dp: Int
    get() = (this / Resources.getSystem().displayMetrics.density).toInt()
val Int.px: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()

val Float.dp: Float
    get() = this / Resources.getSystem().displayMetrics.density
val Float.px: Float
    get() = this * Resources.getSystem().displayMetrics.density


val PagingRequestHelper.networkState: LiveData<NetworkState>
    get() {
        val liveData = MutableLiveData<NetworkState>()
        addListener { report ->
            when {
                report.hasRunning() -> liveData.postValue(NetworkState.loading())
                report.hasError() -> liveData.postValue(NetworkState.error(PagingRequestHelper.RequestType.values().mapNotNull {
                    report.getErrorFor(it)?.message
                }.first()))
                else -> liveData.postValue(NetworkState.success())
            }
        }
        return liveData
    }

fun View.showMessage(message: String?) {
    message?.let {
        Snackbar.make(this, it, Snackbar.LENGTH_LONG)
                .setAction(android.R.string.ok) {}
                .show()
    }
}

fun Fragment.showMessage(message: String?) {
    this.view?.showMessage(message)
}

fun Activity.showMessage(message: String?) {
    this.findViewById<ViewGroup>(android.R.id.content)?.let {
        if (it.childCount > 0) {
            it.getChildAt(0).showMessage(message)
        }
    }
}

fun Fragment.showMessage(result: Result<*>?) {
    result?.let {
        if (!it.loading && it.success == false) {
            showMessage(it.message ?: getString(R.string.default_error_message))
        }
    }
}

fun Activity.showMessage(result: Result<*>?) {
    result?.let {
        if (!it.loading && it.success == false) {
            showMessage(it.message ?: getString(R.string.default_error_message))
        }
    }
}

fun EditText.afterTextChanged(l: (text: String) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) = l(editable?.toString() ?: "")

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }

    })
}

var <T> MutableLiveData<T>.valueIfDifferent: T?
    get() = null
    set(value) {
        if (this.value != value) {
            this.postValue(value)
        }
    }

fun Activity.checkPermissionAndRun(vararg permissions: String, actionIfGranted: (() -> Unit)? = null): Boolean {
    permissions.forEach {
        if (ContextCompat.checkSelfPermission(this, it) != PackageManager.PERMISSION_GRANTED) {
            return false
        }
    }
    actionIfGranted?.invoke()
    return true
}

fun Activity.checkAndRequestPermission(vararg permissions: String, requestCode: Int = 1, actionIfGranted: (() -> Unit)? = null): Boolean {
    permissions.forEach {
        if (ContextCompat.checkSelfPermission(this, it) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, permissions, requestCode)
            return false
        }
    }
    actionIfGranted?.invoke()
    return true
}

fun Activity.requestPermission(vararg permissions: String, requestCode: Int = 1) {
    ActivityCompat.requestPermissions(this, permissions, requestCode)
}

fun View.showKeyboard() {
    try {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
        requestFocus()
    } catch (e: Exception) {

    }
}

fun View.hideKeyboard() {
    try {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.hideSoftInputFromWindow(this.windowToken, 0)
        clearFocus()
    } catch (e: Exception) {

    }
}

val Number.decimalFormat: String
    get() = NumberFormat.getInstance(Locale.getDefault()).format(this)

val Number.roundFormat: String
    get() = String.format("%.0f", this)

val Number.stringFormat: String
    get() = this.toLong().stringFormat

val Long.stringFormat: String
    get() = when {
        this > 1000000000 -> "${this / 1000000000}b"
        this > 1000000 -> "${this / 1000000}m"
        this > 1000 -> "${this / 1000}k"
        else -> this.toString()
    }

val Double.distanceFormat: String
    get() = if (this > 1000) String.format("%1$,.0fkm", this / 1000) else String.format("%1$,.0fm", this)

fun Context.toActivity(): FragmentActivity? {
    if (this is FragmentActivity) {
        return this
    }
    if (this is ContextWrapper) {
        return this.baseContext.toActivity()
    }
    return null
}

val String.sha1: String
    get() {
        val bytes = this.toByteArray()
        val md = MessageDigest.getInstance("SHA-1")
        val digest = md.digest(bytes)
        val result = StringBuilder()
        digest.forEach {
            result.append("%02x".format(it))
        }
        return result.toString()
    }

fun <T> List<T>?.toArrayList(): ArrayList<T>? {
    if (this == null) return null
    if (this is ArrayList) {
        return this
    }
    return ArrayList(this)
}

fun String.toHtml(): Spanned {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(this, 0)
    } else {
        @Suppress("DEPRECATION")
        Html.fromHtml(this)
    }
}

fun <T> LiveData<T>.observerOne(owner: LifecycleOwner, function: (T) -> Unit) {
    observe(owner, object : Observer<T> {
        override fun onChanged(t: T) {
            removeObserver(this)
            function.invoke(t)
        }
    })
}

fun Date.isoFormat(): String {
    return ISODateTimeFormat.dateTime().print(LocalDate(this.time).toDateTimeAtStartOfDay(DateTimeZone.forOffsetHours(7)))
}

fun Date.periodFormat(context: Context): String {
    val date = DateTime(this.time)
    if (date.isAfterNow) {
        return DateTimeFormat.forPattern("dd MMMM, yyyy").print(date)
    }
    val now = DateTime.now()

    val days = Days.daysBetween(date, now).days
    if (days > 7) {
        if (date.year != now.year) {
            return DateTimeFormat.forPattern("dd MMMM, yyyy").print(date)
        }
        return DateTimeFormat.forPattern("dd MMMM").print(date)
    }
    if (days > 0) {
        return context.getString(R.string.x_days_ago, days)
    }

    val hours = Hours.hoursBetween(date, now).hours
    if (hours > 0) {
        return context.getString(R.string.x_hours_ago, hours)
    }

    val minutes = Minutes.minutesBetween(date, now).minutes
    if (minutes > 0) {
        return context.getString(R.string.x_minutes_ago, minutes)
    }

    val seconds = Seconds.secondsBetween(date, now).seconds
    return context.getString(R.string.x_seconds_ago, seconds)
}

fun Date.dateFormat(): String {
    return SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()).format(this)
}

fun Date.timeFormat(): String {
    return SimpleDateFormat("hh:mm aa", Locale.getDefault()).format(this)
}

@WorkerThread
fun Uri.toPart(context: Context): okhttp3.MultipartBody.Part? {
    return try {
        val inputData = resizeToByteArray(context) ?: return null
        val body = RequestBody.create(MediaType.parse("image/*"), inputData)
        MultipartBody.Part.createFormData("file", "image.jpeg", body)
    } catch (e: FileNotFoundException) {
        null
    }
}

@WorkerThread
@Throws(FileNotFoundException::class)
fun Uri.resizeToByteArray(context: Context): ByteArray? {
    val maxImageSize = context.sp.getLong(SPKey.PREF_PHOTO_MAX_SIZE, 10 * 1024 * 1024) // max final file size in kilobytes (10MB)

    // First decode with inJustDecodeBounds=true to check dimensions of image
    val rect = Rect()
    val options = BitmapFactory.Options()
    options.inJustDecodeBounds = true
    BitmapFactory.decodeStream(context.contentResolver.openInputStream(this), rect, options)

    // Calculate inSampleSize(First we are going to resize the image to 5120x5120 image, in order to not have a big but very low quality image.
    //resizing the image will already reduce the file size, but after resizing we will check the file size and start to compress image
    val reqHeight = 5120
    val reqWidth = 5210
    val height = options.outHeight
    val width = options.outWidth
    var inSampleSize = 1

    if (height > reqHeight || width > reqWidth) {

        val halfHeight = height / 2
        val halfWidth = width / 2

        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
        // height and width larger than the requested height and width.
        while (halfHeight / inSampleSize > reqHeight && halfWidth / inSampleSize > reqWidth) {
            inSampleSize *= 2
        }
    }
    options.inSampleSize = inSampleSize

    // Decode bitmap with inSampleSize set
    options.inJustDecodeBounds = false
    options.inPreferredConfig = Bitmap.Config.RGB_565
    options.inDither = true

    var bmpPic = BitmapFactory.decodeStream(context.contentResolver.openInputStream(this), rect, options)
            ?: return null
    var rotate = 0F
    try {
        var inputStream: InputStream? = null
        try {
            inputStream = context.contentResolver.openInputStream(this)
            val exifInterface = ExifInterface(inputStream)
            val orientation = exifInterface.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL)
            when (orientation) {
                ExifInterface.ORIENTATION_ROTATE_270 -> rotate = 270F
                ExifInterface.ORIENTATION_ROTATE_180 -> rotate = 180F
                ExifInterface.ORIENTATION_ROTATE_90 -> rotate = 90F
            }
        } catch (e: IOException) {
            // Handle any errors
        } finally {
            try {
                inputStream?.close()
            } catch (ignored: IOException) {
            }
        }
    } catch (e: IOException) {
        e.printStackTrace()
    }

    if (rotate > 0) {
        val matrix = Matrix()
        matrix.postRotate(rotate)
        bmpPic = Bitmap.createBitmap(bmpPic, 0, 0, bmpPic.width, bmpPic.height, matrix, true)
    }

    var compressQuality = 100 // quality decreasing by 5 every loop.
    var streamLength: Int
    var bmpPicByteArray: ByteArray
    do {
        val bmpStream = ByteArrayOutputStream()
        bmpPic.compress(Bitmap.CompressFormat.JPEG, compressQuality, bmpStream)
        bmpPicByteArray = bmpStream.toByteArray()
        streamLength = bmpPicByteArray.size
        compressQuality -= 5
    } while (streamLength > maxImageSize)

    return bmpPicByteArray
}

val Context.sp
    get() = this.getSharedPreferences(SPKey.PREF, Context.MODE_PRIVATE)

fun Context.startActivityIfResolved(intent: Intent) {
    if (packageManager.resolveActivity(intent, 0) != null) {
        startActivity(intent)
    }
}

fun ViewPager.back() {
    currentItem = if (currentItem > 0)
        currentItem - 1
    else (adapter?.count?.minus(1) ?: 0)
}

fun ViewPager.next() {
    currentItem = if (currentItem < adapter?.count?.minus(1) ?: 0)
        currentItem + 1
    else 0
}

fun AtomicBoolean.getAndReversed(): Boolean {
    val value = get()
    return getAndSet(!value)
}

fun Context.openUrl(url: String?) {
    url?.let {
        openUrl(Uri.parse(it))
    }
}

fun Context.openUrl(uri: Uri) {
    try {
        CustomTabsIntent.Builder().build().launchUrl(this, uri)
    } catch (e: Exception) {
        //Fix no activity found
    }
}

fun Context.copy(value: CharSequence) {
    (getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager).primaryClip = ClipData.newPlainText("", value)
}

fun bodyOf(vararg pairs: Pair<String, Any?>): RequestBody =
        RequestBody.create(MediaType.parse("application/json"), JSONObject(pairs.toMap()).toString())

val screenWith: Int = Resources.getSystem().displayMetrics.widthPixels

fun View.takeScreenShot(): Bitmap {
    val b = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
    val c = Canvas(b)
    this.draw(c)
    return b
}

fun Transition.doOnEnd(func: (Transition) -> Unit) {
    addListener(object : Transition.TransitionListener {
        override fun onTransitionResume(transition: Transition) {

        }

        override fun onTransitionPause(transition: Transition) {

        }

        override fun onTransitionCancel(transition: Transition) {

        }

        override fun onTransitionStart(transition: Transition) {

        }

        override fun onTransitionEnd(transition: Transition) {
            func.invoke(transition)
        }

    })
}

var View.gone: Boolean
    get() = visibility == View.GONE
    set(value) {
        visibility = if (value) View.GONE else View.VISIBLE
    }

var View.invisible: Boolean
    get() = visibility == View.INVISIBLE
    set(value) {
        visibility = if (value) View.INVISIBLE else View.VISIBLE
    }

var ViewDataBinding.gone: Boolean
    get() = root.visibility == View.GONE
    set(value) {
        root.visibility = if (value) View.GONE else View.VISIBLE
    }

var ViewDataBinding.invisible: Boolean
    get() = root.visibility == View.INVISIBLE
    set(value) {
        root.visibility = if (value) View.INVISIBLE else View.VISIBLE
    }

fun Context.vibrate(milliseconds: Long = 500) {
    val vibrator = getSystemService(Context.VIBRATOR_SERVICE) as? Vibrator
    if (Build.VERSION.SDK_INT >= 26) {
        vibrator?.vibrate(VibrationEffect.createOneShot(milliseconds, VibrationEffect.DEFAULT_AMPLITUDE))
    } else {
        vibrator?.vibrate(milliseconds)
    }
}

fun View.setOnDoubleClickListener(listener: ((View) -> Unit)?) {
    if (listener == null) {
        setOnClickListener(null)
    } else {
        var last = 0L
        setOnClickListener {
            val now = System.currentTimeMillis()
            last = if (now - last < 200) {
                listener.invoke(this)
                0L
            } else {
                now
            }
        }
    }
}
