package com.zody.utils

import org.joda.time.DateTime
import org.joda.time.format.ISODateTimeFormat
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by vinhnguyen.it.vn on 2019, February 17
 */
@Singleton
class TimeManager @Inject constructor() {

    private var delay: Long = 0L

    val now: Date
        get() = DateTime.now().plus(delay).toDate()

    fun setTimeFromServer(now: String) {
        delay = try {
            ISODateTimeFormat.dateTime().parseDateTime(now).millis - Date().time
        } catch (e: Exception) {
            0L
        }
    }
}