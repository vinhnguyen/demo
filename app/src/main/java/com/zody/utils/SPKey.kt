package com.zody.utils

/**
 * Created by vinhnguyen.it.vn on 2018, July 09
 */
object SPKey {
    const val PREF = "zody_pref"
    const val PREF_TOKEN = "pref_access_token"
    const val PREF_DEVICE_TOKEN = "device_token"
    const val PREF_OLD_HOST = "pref_old_host"
    const val PREF_HOST = "pref_new_host"
    const val PREF_TRACKING_HOST = "pref_tracking_host"
    const val PREF_PHOTO_MAX_SIZE = "pref_photo_max_size"
    const val PREF_REVIEW_MIN_VIEW_TO_DISPLAY = "pref_review_min_view_to_display"
    const val PREF_REVIEW_TIME_TO_INCREASE_VIEW = "pref_review_time_to_increase_view"
    const val PREF_TIP_MIN_COIN = "pref_min_coin_to_tip"
    const val PREF_TIP_DELAY_TIME = "pref_tip_delay_time"
    const val PREF_LOADED_CONFIGURATION = "pref_loaded_configuration_371"
    const val PREF_UNREAD_NOTIFICATION = "pref_unread_notification"
    const val PREF_TRENDING_HASH_TAG = "pref_trending_hash_tag"
    const val PREF_REFERRAL_INPUT_SUCCESS = "pref_referral_input_success"
    const val PREF_VERIFY_PHONE_SUCCESS = "pref_verify_phone_success"
    const val PREF_NEW_VERSION = "pref_new_version"
    const val PREF_QR_CODE_TIMEOUT = "pref_qr_code_timeout"
    const val PREF_OPEN_PERMISSION_SETTING = "pref_open_location_setting"
    const val PREF_SHOW_LUCKY_DRAW_GUIDE = "pref_show_lucky_draw_guide"
}