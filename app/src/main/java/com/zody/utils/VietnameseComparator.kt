package com.zody.utils

/**
 * Created by vinhnguyen.it.vn on 2018, October 12
 */
class VietnameseComparator : Comparator<String> {

    private val rule = "A Ă Â B C D Đ E Ê G H I J K L M N O Ô Ơ P Q R S T U Ư V W X Y Z"

    override fun compare(s1: String?, s2: String?): Int {
        return when {
            s1 == null -> if (s2 == null) 0 else -1
            s2 == null -> 1
            else -> {
                val length = Math.min(s1.length, s2.length)
                for (i in 0 until length) {
                    val c1 = s1[i]
                    val c2 = s2[i]
                    if (c1 == c2) {
                        continue
                    }
                    val index1 = rule.indexOf(c1, ignoreCase = true)
                    val index2 = rule.indexOf(c2, ignoreCase = true)

                    if (index1 == -1 || index2 == -1) return c1.compareTo(c2)
                    return index1 - index2
                }
                if (length < s1.length) 1 else if (length < s2.length) -1 else 0
            }
        }
    }
}