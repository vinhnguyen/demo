package com.zody.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.children
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.tabs.TabLayout
import com.zody.R
import com.zody.databinding.FragmentHomeBinding
import com.zody.ui.base.BaseFragmentWithChildFragment
import com.zody.ui.header.HeaderViewModel
import com.zody.ui.writepost.WritePostActivity
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 11
 */
class HomeFragment : BaseFragmentWithChildFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentHomeBinding

    private val headerViewModel by lazy { ViewModelProviders.of(this, viewModelFactory).get(HeaderViewModel::class.java) }

    override val screenName: String?
        get() = "Home"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.headerView.init(headerViewModel, this)
        binding.headerView.binding.btHeaderActionLeft.setImageResource(R.drawable.ic_write_review)
        binding.headerView.binding.btHeaderActionLeft.setOnClickListener {
            startActivity(Intent(context, WritePostActivity::class.java))
        }

        binding.tabLayout.apply {
            for (tabView in (getChildAt(0) as ViewGroup).children) {
                tabView.apply { alpha = if (isSelected) 1F else 0.5F }
            }
            addOnTabSelectedListener(object : TabLayout.ViewPagerOnTabSelectedListener(binding.viewPager) {
                private fun setAlphaForView(tab: TabLayout.Tab?, alpha: Float) {
                    tab?.let {
                        (it.parent.getChildAt(0) as ViewGroup).getChildAt(it.position).alpha = alpha
                    }
                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {
                    super.onTabUnselected(tab)
                    setAlphaForView(tab, 0.5F)
                }

                override fun onTabSelected(tab: TabLayout.Tab?) {
                    super.onTabSelected(tab)
                    setAlphaForView(tab, 1F)
                }
            })
        }
        binding.viewPager.apply {
            addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(binding.tabLayout))
            val adapter = HomePagerAdapter(this@HomeFragment)
            this.adapter = adapter
            offscreenPageLimit = adapter.count
        }
    }
}