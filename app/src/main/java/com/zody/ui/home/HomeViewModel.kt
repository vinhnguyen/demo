package com.zody.ui.home

import com.zody.repository.HashTagRepository
import com.zody.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 12
 */
class HomeViewModel @Inject constructor(hashTagRepository: HashTagRepository) : BaseViewModel() {

    val trendingHashTag = hashTagRepository.loadTrendingHashTag()
}