package com.zody.ui.home

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentStatePagerAdapter
import com.zody.ui.post.list.following.PostOfFollowingFragment
import com.zody.ui.post.list.nearby.PostNearByFragment
import com.zody.ui.post.list.newest.PostNewestFragment
import com.zody.ui.post.list.trending.PostTrendingFragment

/**
 * Created by vinhnguyen.it.vn on 2018, July 12
 */
class HomePagerAdapter(val parent: HomeFragment) : FragmentStatePagerAdapter(parent.childFragmentManager) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> PostTrendingFragment()
            1 -> PostNearByFragment()
            2 -> PostOfFollowingFragment()
            else -> PostNewestFragment()
        }
    }

    override fun getCount() = 4
}