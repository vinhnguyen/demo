package com.zody.ui.main

import android.text.SpannableStringBuilder
import android.widget.TextView
import androidx.databinding.BindingAdapter
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by vinhnguyen.it.vn on 2018, December 12
 */
object BindingInterface {

    @JvmStatic
    @BindingAdapter("start", "end")
    fun bindStartEndDate(view: TextView, start: Date?, end: Date?) {
        view.text = SpannableStringBuilder().apply {
            if (start != null) {
                append(SimpleDateFormat("dd.MM", Locale.getDefault()).format(start))
            }
            if (end != null) {
                if (isNotEmpty()) append(" - ")
                append(SimpleDateFormat("dd.MM", Locale.getDefault()).format(end))
            }
        }
    }
}