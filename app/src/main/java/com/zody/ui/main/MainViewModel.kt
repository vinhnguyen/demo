package com.zody.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Transformations
import com.zody.BuildConfig
import com.zody.entity.luckydraw.LuckyDrawData
import com.zody.livedata.CityAndLocationForApi
import com.zody.livedata.LocationEnableLiveData
import com.zody.livedata.SingleLiveEvent
import com.zody.repository.*
import com.zody.ui.base.BaseViewModel
import com.zody.ui.luckydraw.TurnLiveData
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 11
 */
class MainViewModel @Inject constructor(profileRepository: ProfileRepository,
                                        configurationRepository: ConfigurationRepository,
                                        notificationRepository: NotificationRepository,
                                        luckyDrawRepository: LuckyDrawRepository,
                                        locationEnable: LocationEnableLiveData,
                                        cityAndLocationForApi: CityAndLocationForApi,
                                        val turn: TurnLiveData) : BaseViewModel() {

    val needAskLocation = SingleLiveEvent<Unit>().apply {
        addSource(locationEnable) {
            if (it == false) {
                value = null
            }
        }
    }

    private val profile = profileRepository.loadProfile()

    val hasNotification: LiveData<Boolean> = Transformations.map(notificationRepository.getUnreadNotification()) {
        it > 0
    }

    val hasNewReward = MediatorLiveData<Boolean>().apply {
        addSource(profile) {
            it?.data?.statistic?.reward?.let { reward ->
                value = reward > 0
            }
        }
    }

    val needUpdateProfile = SingleLiveEvent<Boolean>().apply {
        addSource(profile) {
            it?.data?.let { p ->
                removeSource(profile)
                value = p.isNew == true || p.isForceUpdateData == true
            }
        }
    }

    val needUpdateVersion = SingleLiveEvent<Boolean>().apply {
        addSource(configurationRepository.getNewVersion()) { version ->
            value = version != null && BuildConfig.VERSION_NAME < version
        }
    }

    val luckyDraw: LiveData<Result<LuckyDrawData>> = Transformations.switchMap(cityAndLocationForApi) {
        luckyDrawRepository.loadCurrentActive(it.city, it.latitude, it.longitude)
    }

    init {
        turn.addSource(luckyDraw) { result ->
            result.data?.userTurnLeft?.let {
                turn.value = it
            }
        }
    }
}