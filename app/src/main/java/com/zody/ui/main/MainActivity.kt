package com.zody.ui.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zody.R
import com.zody.databinding.ActivityMainBinding
import com.zody.ui.base.AlertDialog
import com.zody.ui.base.BaseFragmentActivity
import com.zody.ui.base.Navigation
import com.zody.ui.business.openStore
import com.zody.ui.expert.AuthorViewModel
import com.zody.ui.explore.ExploreFragment
import com.zody.ui.home.HomeFragment
import com.zody.ui.luckydraw.LuckyDrawActivity
import com.zody.ui.notification.NotificationDialog
import com.zody.ui.permission.AskLocationActivity
import com.zody.ui.post.NotEnoughCoinObserver
import com.zody.ui.post.PostActionViewModel
import com.zody.ui.profile.ProfileFragment
import com.zody.ui.profile.edit.EditProfileActivity
import com.zody.ui.qrcode.ScanQrCodeActivity
import com.zody.ui.voucher.VoucherTabFragment
import com.zody.utils.gone
import com.zody.utils.openUrl
import com.zody.utils.showMessage
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 10
 */
class MainActivity : BaseFragmentActivity() {

    companion object {
        private const val EXTRA_GO_PROFILE = "MainActivity.GoProfile"

        fun goProfile(context: Context) {
            context.startActivity(Intent(context, MainActivity::class.java).apply {
                putExtra(EXTRA_GO_PROFILE, true)
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
            })
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityMainBinding

    private val mainViewModel: MainViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
    }
    private val authorViewModel: AuthorViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(AuthorViewModel::class.java)
    }
    private val postActionViewModel: PostActionViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(PostActionViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        binding.tabHost.apply {
            setup(supportFragmentManager)
            addTab(ExploreFragment::class.java, Tab.TAB_EXPLORE)
            addTab(HomeFragment::class.java, Tab.TAB_HOME)
            addTab(VoucherTabFragment::class.java, Tab.TAB_VOUCHER)
            addTab(ProfileFragment::class.java, Tab.TAB_PROFILE)
        }
        binding.layoutBottomNavigation.btNavigationScanCode.setOnClickListener { goScanQrCode() }

        checkOpenTab()
        checkOpenMessage()

        authorViewModel.message.observe(this, Observer {
            showMessage(it)
        })
        postActionViewModel.tipResult.observe(this, Observer {
            showMessage(it)
        })
        postActionViewModel.deleteResult.observe(this, Observer {
            showMessage(it)
        })
        postActionViewModel.showPopupNotEnoughCoin.observe(this, NotEnoughCoinObserver(this, supportFragmentManager))

        mainViewModel.hasNotification.observe(this, Observer {
            binding.layoutBottomNavigation.btNavigationTabHome.hasNew = it
            binding.layoutBottomNavigation.btNavigationTabExplore.hasNew = it
        })
        mainViewModel.hasNewReward.observe(this, Observer {
            binding.layoutBottomNavigation.btNavigationTabProfile.hasNew = it
            binding.layoutBottomNavigation.btNavigationTabVoucher.hasNew = it
        })
        mainViewModel.needUpdateProfile.observe(this, Observer {
            if (it) goEditProfile()
        })
        mainViewModel.needUpdateVersion.observe(this, Observer {
            if (it) showUpdateVersion()
        })
        mainViewModel.needAskLocation.observe(this, Observer {
            AskLocationActivity.start(this@MainActivity, false)
        })
        mainViewModel.luckyDraw.observe(this, Observer {
            binding.layoutLuckyDrawFloat.gone = true
            it.data?.luckyDraw?.let { luckyDraw ->
                binding.layoutLuckyDrawFloat.luckyDraw = luckyDraw
                binding.layoutLuckyDrawFloat.gone = !luckyDraw.hasSupport
                binding.layoutLuckyDrawFloat.root.setOnClickListener {
                    LuckyDrawActivity.start(this@MainActivity, luckyDraw.id, luckyDraw.name)
                }
            }
        })
        mainViewModel.turn.observe(this, Observer {
            binding.layoutLuckyDrawFloat.turn = it
        })
    }


    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        setIntent(intent)
        checkOpenTab()
        checkOpenMessage()
    }

    override fun onBackPressed() {
        if (binding.tabHost.currentTabTag != Tab.TAB_EXPLORE) {
            binding.tabHost.changeTab(Tab.TAB_EXPLORE)
        } else {
            super.onBackPressed()
        }
    }


    private fun checkOpenTab() {
        when {
            intent.action == "OPEN_VOUCHER" -> Tab.TAB_VOUCHER
            intent.action == "VOUCHER_LIST" -> Tab.TAB_VOUCHER
            intent.action == "OPEN_WEB" -> {
                intent.getStringExtra(Navigation.EXTRA_TARGET_VALUE)?.let { url ->
                    openUrl(url)
                }
                null
            }
            intent.getBooleanExtra(EXTRA_GO_PROFILE, false) -> Tab.TAB_PROFILE
            intent?.data?.host == "vouchers" -> Tab.TAB_VOUCHER
            else -> null
        }?.let {
            binding.tabHost.changeTab(it)
        }
    }

    private fun checkOpenMessage() {
        if (intent.action == "OPEN_SIMPLE_POPUP") {
            Navigation.getContent(intent)?.let { content ->
                NotificationDialog().apply {
                    this.content = content
                    show(supportFragmentManager, null)
                }
            }
        }
    }

    private fun goScanQrCode() {
        startActivity(Intent(this, ScanQrCodeActivity::class.java))
    }

    private fun goEditProfile() {
        startActivity(Intent(this, EditProfileActivity::class.java).apply {
            putExtra(EditProfileActivity.EXTRA_DISABLE_GO_BACK, true)
        })
    }

    private fun showUpdateVersion() {
        AlertDialog.Builder(this@MainActivity)
                .setMessage(R.string.require_update_version)
                .setPositiveButton(R.string.action_aggree) {
                    openStore()
                }
                .show(supportFragmentManager)
    }
}