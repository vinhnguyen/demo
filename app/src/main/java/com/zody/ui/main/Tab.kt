package com.zody.ui.main

/**
 * Created by vinhnguyen.it.vn on 2018, August 20
 */
object Tab {
    const val TAB_HOME = "Home"
    const val TAB_EXPLORE = "Explore"
    const val TAB_VOUCHER = "Voucher"
    const val TAB_PROFILE = "Profile"
}