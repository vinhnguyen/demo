package com.zody.ui.business.photo

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.zody.repository.BusinessRepository
import com.zody.ui.base.BaseViewModel
import com.zody.utils.valueIfDifferent
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 12
 */
class BusinessPhotoViewModel @Inject constructor(businessRepository: BusinessRepository) : BaseViewModel() {

    private val businessId = MutableLiveData<String>()

    val result = Transformations.map(businessId) {
        businessRepository.loadBusinessPhoto(it)
    }

    val photos = Transformations.switchMap(result) {
        it.data
    }

    fun init(businessId: String) {
        this.businessId.valueIfDifferent = businessId
    }

}