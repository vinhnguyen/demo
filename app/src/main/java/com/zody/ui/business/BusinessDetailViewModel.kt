package com.zody.ui.business

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.zody.entity.ListItem
import com.zody.entity.Photo
import com.zody.entity.Post
import com.zody.entity.business.Business
import com.zody.repository.*
import com.zody.ui.base.BaseViewModel
import com.zody.utils.valueIfDifferent
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, August 10
 */
class BusinessDetailViewModel @Inject constructor(businessRepository: BusinessRepository,
                                                  postRepository: PostRepository) : BaseViewModel() {

    private val businessId = MutableLiveData<String>()

    val business: LiveData<Result<Business>> = Transformations.switchMap(businessId) {
        businessRepository.loadBusinessDetail(it)
    }

    val businessPhotos = MediatorLiveData<List<Photo>>().apply {
        addSource(business) { businessResult ->
            if (businessResult.data != null) {
                removeSource(business)

                addSource(businessRepository.loadBusinessPhotoFirstPage(businessResult.data.id)) { photoResult ->
                    if (photoResult.data != null) {
                        value = photoResult.data
                    }
                }
            }
        }
    }

    private val postResult: LiveData<Listing<ListItem<Post>>> = Transformations.map(businessId) {
        postRepository.loadBusinessPost(it)
    }

    val posts: LiveData<ListingData<ListItem<Post>>> = Transformations.switchMap(postResult) { it.data }

    fun init(businessId: String) {
        this.businessId.valueIfDifferent = businessId
    }

}