package com.zody.ui.business.photo

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.zody.BR
import com.zody.R
import com.zody.databinding.ItemListBusinessDetailPhotoBinding
import com.zody.entity.Photo
import com.zody.ui.base.DataBindingViewHolder
import com.zody.ui.base.DatabindingPagedListAdapter
import com.zody.ui.photo.PhotoActivity

/**
 * Created by vinhnguyen.it.vn on 2018, September 12
 */
class BusinessPhotoAdapter : DatabindingPagedListAdapter<Photo>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingViewHolder {
        val holder = super.onCreateViewHolder(parent, viewType)
        when (holder.binding) {
            is ItemListBusinessDetailPhotoBinding ->
                holder.itemView.setOnClickListener { view ->
                    getItem(holder.adapterPosition)?.let {
                        PhotoActivity.start(view.context, currentList, it)
                    }

                }
        }
        return holder
    }

    override fun onBind(binding: ViewDataBinding, item: Photo?) {
        binding.setVariable(BR.url, item?.url)
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_list_business_detail_photo
    }


    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Photo>() {
            override fun areItemsTheSame(oldItem: Photo, newItem: Photo): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Photo, newItem: Photo): Boolean {
                return oldItem.url == newItem.url
            }
        }
    }
}