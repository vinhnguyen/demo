package com.zody.ui.business

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.telephony.TelephonyManager
import android.util.Patterns
import com.zody.utils.openUrl
import com.zody.utils.startActivityIfResolved

fun Context.openFacebook(url: String?) {
    try {
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("fb://facewebmodal/f?href=$url")))
    } catch (e: Exception) {
        openUrl(url)
    }
}

fun Context.openMessenger(id: String) {
    this.startActivityIfResolved(Intent(Intent.ACTION_VIEW, Uri.parse("http://m.me/$id")))
}

fun Context.openSupport() {
    this.openMessenger("zodydanang")
}

fun Context.openInstagram(url: String?) {
    try {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        intent.setPackage("com.instagram.android")
        this.startActivity(intent)
    } catch (e: Exception) {
        this.openUrl(url)
    }

}

fun Context.openStore() {
    openStore("com.zody")
}

fun Context.openStore(packageName: String) {
    try {
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$packageName")))
    } catch (e: android.content.ActivityNotFoundException) {
        openUrl("https://play.google.com/store/apps/details?id=$packageName")
    }
}

fun Context.directionOnMap(lat: Double?, lng: Double?) {
    val url = String.format("http://maps.google.com/maps?daddr=%s,%s", lat, lng)
    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
    if (this.packageManager.resolveActivity(intent, 0) != null) {
        this.startActivity(intent)
    }
}


fun Context.canMakeCall(): Boolean {
    return (this.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager).phoneType != TelephonyManager.PHONE_TYPE_NONE
}

fun Context.callPhone(phoneNumber: String?) {
    phoneNumber?.let {
        if (Patterns.PHONE.matcher(it).matches() && this.canMakeCall()) {
            val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phoneNumber, null))
            if (this.packageManager.resolveActivity(intent, 0) != null) {
                this.startActivity(intent)
            }
        }
    }
}

fun Context.shareLink(link: String?) {
    link?.let {
        val intent = Intent(Intent.ACTION_SEND).apply {
            type = "text/plain"
            putExtra(Intent.EXTRA_TEXT, it)
        }
        if (this.packageManager.resolveActivity(intent, 0) != null) {
            this.startActivity(intent)
        }
    }
}

fun Context.copy(link: String?) {
    if (link?.isNotEmpty() == true) {
        (getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager).primaryClip = ClipData.newPlainText(link, link)
    }
}