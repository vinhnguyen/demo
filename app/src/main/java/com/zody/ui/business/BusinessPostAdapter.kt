package com.zody.ui.business

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import com.zody.databinding.ItemListPostBusinessBinding
import com.zody.ui.expert.AuthorViewModel
import com.zody.ui.post.PostActionViewModel
import com.zody.ui.post.list.PostListAdapter

/**
 * Created by vinhnguyen.it.vn on 2018, August 10
 */
class BusinessPostAdapter(authorViewModel: AuthorViewModel, postActionViewModel: PostActionViewModel) : PostListAdapter(authorViewModel, postActionViewModel) {
    override fun initViewHolder(binding: ViewDataBinding, parent: ViewGroup) {
        super.initViewHolder(binding, parent)
        when (binding) {
            is ItemListPostBusinessBinding -> {
                binding.tvPostBusinessName.setOnClickListener(null)
            }
        }
    }
}