package com.zody.ui.business.photo

import android.content.Context
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.zody.R
import com.zody.databinding.ActivityBusinessPhotoBinding
import com.zody.ui.base.BaseActivity
import com.zody.ui.base.Navigation
import com.zody.utils.px
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 12
 */
class BusinessPhotoActivity : BaseActivity() {

    companion object {

        fun start(context: Context, businessId: String?, businessName: String?) {
            context.startActivity(Intent(context, BusinessPhotoActivity::class.java).apply {
                putExtra(Navigation.EXTRA_TARGET_VALUE, businessId)
                putExtra(Navigation.EXTRA_TARGET_TITLE, businessName)
            })
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityBusinessPhotoBinding

    private val businessPhotoViewModel: BusinessPhotoViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(BusinessPhotoViewModel::class.java)
    }

    override val screenName: String?
        get() = "Business photo"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val businessId = Navigation.getId(intent)
        if (businessId == null) {
            finish()
            return
        }

        binding = DataBindingUtil.setContentView(this, R.layout.activity_business_photo)
        binding.headerBar.btHeaderActionLeft.setOnClickListener { finish() }
        binding.headerBar.title = intent.getStringExtra(Navigation.EXTRA_TARGET_TITLE)

        val adapter = BusinessPhotoAdapter()
        binding.recyclerView.apply {
            this.adapter = adapter
            addItemDecoration(object : RecyclerView.ItemDecoration() {

                private val dividerSize = 8.px

                override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                    outRect.bottom = dividerSize
                    outRect.left = dividerSize
                    outRect.right = dividerSize
                    outRect.top = dividerSize
                }
            })
        }

        businessPhotoViewModel.init(businessId)
        businessPhotoViewModel.photos.observe(this, Observer {
            adapter.submitList(it.pagedList)
        })
    }
}