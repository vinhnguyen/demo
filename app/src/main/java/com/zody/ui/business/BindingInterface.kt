package com.zody.ui.business

import android.graphics.Color
import android.graphics.Typeface
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.text.color
import androidx.core.text.inSpans
import androidx.databinding.BindingAdapter
import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.zody.R
import com.zody.entity.BusinessList
import com.zody.entity.business.*
import com.zody.utils.decimalFormat
import com.zody.utils.gone
import com.zody.utils.px

/**
 * Created by vinhnguyen.it.vn on 2018, August 10
 */
object BindingInterface {

    @JvmStatic
    @BindingAdapter("openHours")
    fun bindOpenHour(view: TextView, hours: List<BusinessHour>?) {
        if (hours == null) {
            view.text = null
            return
        }
        val text = StringBuilder()
        hours.forEach {
            text.append(it.toString()).append(" ")
        }
        view.text = text.trim()
    }

    @JvmStatic
    @BindingAdapter("phone")
    fun bindPhone(view: TextView, phone: String?) {
        if (phone == null) {
            view.text = null
            return
        }
        try {
            val phoneNumber = PhoneNumberUtil.getInstance().parse(phone, "+84")
            view.text = SpannableStringBuilder()
                    .color(Color.parseColor("#9b9b9b")) {
                        append("+${phoneNumber.countryCode}")
                    }
                    .append(" ")
                    .append(phoneNumber.nationalNumber.toString())
        } catch (e: NumberParseException) {
            view.text = phone
        }
    }

    @JvmStatic
    @BindingAdapter("eventIcon")
    fun bindEventIcon(view: ImageView, event: BusinessEvent?) {
        if (event == null) {
            view.setImageBitmap(null)
            return
        }
        view.setImageResource(
                when (event) {
                    is BusinessEventFirstBill, is BusinessEventCoinEachBill -> R.drawable.ic_business_detail_event_bill
                    is BusinessEventBillStack -> R.drawable.ic_business_detail_event_milestone
                    is BusinessEventHappyHour -> R.drawable.ic_business_detail_event_happy_hour
                    is BusinessEventTextOnly -> R.drawable.ic_business_detail_event_text_only
                    else -> 0
                }
        )
    }

    @JvmStatic
    @BindingAdapter("eventTitle")
    fun bindEventTitle(view: TextView, event: BusinessEvent?) {
        val context = view.context
        view.text = when (event) {
            is BusinessEventFirstBill -> context.getString(R.string.business_detail_event_first_bill_title)
            is BusinessEventCoinEachBill -> context.getString(R.string.business_detail_event_coin_each_bill_title)
            is BusinessEventBillStack -> context.getString(R.string.business_detail_event_bill_stack_title)
            is BusinessEventHappyHour -> context.getString(R.string.business_detail_event_happy_hour_title)
            is BusinessEventTextOnly -> event.title
            else -> null
        }
    }

    @JvmStatic
    @BindingAdapter("eventDesc")
    fun bindEventDesc(view: TextView, event: BusinessEvent?) {
        view.text = when (event) {
            is BusinessEventFirstBill -> event.desc
            is BusinessEventCoinEachBill -> event.desc
            is BusinessEventBillStack -> event.desc
            is BusinessEventHappyHour -> event.desc
            is BusinessEventTextOnly -> event.desc
            else -> null
        }
    }

    @JvmStatic
    @BindingAdapter("eventShortDesc")
    fun bindEventShortDesc(view: TextView, event: BusinessEvent?) {
        view.text = when (event) {
            is BusinessEventHappyHour -> if (event.hours?.isNotEmpty() == true) event.hours.get(0).toString() else null
            else -> null
        }
        view.gone = event !is BusinessEventHappyHour
    }

    @JvmStatic
    @BindingAdapter("myTopUserStatistic")
    fun bindMyTopUserStatistic(view: TextView, topUser: BusinessTopUser?) {
        if (topUser?.statistic?.bill == null) {
            view.text = null
            return
        }
        view.text = SpannableStringBuilder()
                .inSpans(StyleSpan(Typeface.BOLD), ForegroundColorSpan(ContextCompat.getColor(view.context, R.color.orange))) {
                    append(topUser.name)
                }
                .append(" ")
                .append(view.context.getString(R.string.business_detail_top_user_my_statistic, topUser.statistic.bill.decimalFormat))
    }

    @JvmStatic
    @BindingAdapter("billStackTotalExpense")
    fun bindTotalExpense(view: TextView, business: Business?) {
        val expense = business?.statistic?.expense ?: 0
        view.text = String.format("đ %s", expense.decimalFormat)
        business?.events?.billStack?.mapNotNull { it.amount }?.let {
            if (it.isNotEmpty()) {
                val last = it.last()
                val layoutParams = view.layoutParams as ConstraintLayout.LayoutParams
                layoutParams.horizontalBias = Math.min(1F, expense.toFloat() / last)
            }
        }
    }

    @JvmStatic
    @BindingAdapter("billStackMilestone")
    fun bindBillStackMilestone(view: LinearLayout, business: Business?) {
        view.removeAllViews()
        view.weightSum = 100F
        business?.events?.billStack?.mapNotNull { it.amount }?.sorted()?.let { amounts ->
            val expense = business.statistic?.expense ?: 0
            val last = amounts.last()

            var subExpense = expense
            var previousAmount = 0L
            amounts.forEach {
                view.addView(ProgressBar(view.context, null, android.R.attr.progressBarStyleHorizontal).apply {
                    progressDrawable = ContextCompat.getDrawable(view.context, R.drawable.progress_business_detail_milestone)
                    max = 100
                    progress = if (it <= subExpense) 100 else (subExpense * 100 / it).toInt()
                    previousAmount += it
                    subExpense = Math.max(0, subExpense - it)
                }, LinearLayout.LayoutParams(0, 3.px, (100 * it / last).toFloat()).apply {
                    rightMargin = 2.px
                })
            }
        }
    }

    @JvmStatic
    @BindingAdapter("billStackMilestoneRemaining")
    fun bindBillStackMilestoneRemaining(view: TextView, business: Business?) {
        val expense = business?.statistic?.expense ?: 0
        var remaining = 0L
        business?.events?.billStack?.mapNotNull { it.amount }?.firstOrNull {
            remaining = it - expense
            remaining > 0
        }
        if (business?.events?.billStack?.isNotEmpty() != true) {
            view.text = null
            return
        }
        view.text = if (remaining > 0) view.resources.getString(R.string.business_detail_bill_stack_milestone_remaining, remaining.decimalFormat) else null
    }

    @JvmStatic
    @BindingAdapter("categories")
    fun bindCategories(view: TextView, business: BusinessList?) {
        view.text = business?.categories?.mapNotNull { it.name }?.joinToString()
    }
}