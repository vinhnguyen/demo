package com.zody.ui.business

import android.content.Context
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.zody.BR
import com.zody.R
import com.zody.databinding.*
import com.zody.entity.Photo
import com.zody.entity.business.Business
import com.zody.ui.base.BaseFragmentActivity
import com.zody.ui.base.Navigation
import com.zody.ui.business.photo.BusinessPhotoActivity
import com.zody.ui.expert.AuthorViewModel
import com.zody.ui.post.NotEnoughCoinObserver
import com.zody.ui.post.PostActionViewModel
import com.zody.ui.post.PostOptionsDialogFragment
import com.zody.ui.user.UserActivity
import com.zody.ui.writepost.WritePostActivity
import com.zody.utils.px
import com.zody.utils.showMessage
import java.util.concurrent.atomic.AtomicBoolean
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, August 10
 */
class BusinessDetailActivity : BaseFragmentActivity() {

    companion object {
        fun start(context: Context, businessId: String) {
            context.startActivity(Intent(context, BusinessDetailActivity::class.java).apply {
                putExtra(Navigation.EXTRA_TARGET_VALUE, businessId)
            })
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityBusinessDetailBinding
    private val businessDetailViewModel: BusinessDetailViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(BusinessDetailViewModel::class.java)
    }
    private val authorViewModel: AuthorViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(AuthorViewModel::class.java)
    }
    private val postActionViewModel: PostActionViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(PostActionViewModel::class.java)
    }

    override val screenName: String?
        get() = "business detail"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val id = Navigation.getId(intent)
        if (id == null) {
            finish()
            return
        }

        binding = DataBindingUtil.setContentView(this, R.layout.activity_business_detail)

        businessDetailViewModel.init(id)
        businessDetailViewModel.business.observe(this, Observer { result ->
            result.data?.let { initBusinessInfo(it) }
        })
        businessDetailViewModel.businessPhotos.observe(this, Observer {
            initBusinessPhoto(it)
        })

        val adapter = BusinessPostAdapter(authorViewModel, postActionViewModel)
        adapter.onButtonMoreClick = {
            PostOptionsDialogFragment.show(supportFragmentManager, it)
        }
        binding.recyclerView.adapter = adapter

        businessDetailViewModel.posts.observe(this, Observer {
            adapter.submitList(it.pagedList, it.endData)
        })

        authorViewModel.message.observe(this, Observer { showMessage(it) })
        postActionViewModel.deleteResult.observe(this, Observer { showMessage(it) })
        postActionViewModel.tipResult.observe(this, Observer { showMessage(it) })
        postActionViewModel.showPopupNotEnoughCoin.observe(this, NotEnoughCoinObserver(this, supportFragmentManager))
    }

    private fun initBusinessInfo(business: Business) {
        binding.layoutBusinessInfo.apply {
            removeAllViews()
            val inflater = layoutInflater
            val needAddLine = AtomicBoolean(false)

            addChildLayout<LayoutBusinessCoverBinding>(inflater, R.layout.layout_business_cover, this, business).apply {
                btHeaderActionLeft.setOnClickListener { finish() }
            }

            if (business.name != null || business.address != null || business.social != null) {
                addChildLayout<LayoutBusinessNameAddressSocialBinding>(inflater, R.layout.layout_business_name_address_social, this, business).apply {
                    btBusinessFacebook.setOnClickListener {
                        openFacebook(business.social?.facebook)
                    }
                    btBusinessInstagram.setOnClickListener {
                        openInstagram(business.social?.instagram)
                    }
                }
                needAddLine.set(true)
            }

            if (business.hours?.isNotEmpty() == true) {
                if (needAddLine.getAndSet(true)) {
                    addLine(inflater, this)
                }
                addChildLayout<LayoutBusinessOpenHoursBinding>(inflater, R.layout.layout_business_open_hours, this, business)
            }

            addChildLayout<LayoutBusinessActionsBinding>(inflater, R.layout.layout_business_actions, this, business).apply {
                btBusinessDirection.setOnClickListener { _ ->
                    business.location?.let {
                        this@BusinessDetailActivity.directionOnMap(it.latitude, it.longitude)
                    }
                }
                btBusinessCall.setOnClickListener { this@BusinessDetailActivity.callPhone(business.phone) }
                btBusinessMenu.setOnClickListener { }
                btBusinessShare.setOnClickListener { this@BusinessDetailActivity.shareLink(business.shareLink) }
            }

            if (business.hasScanPrinterQRCode == true) {
                addChildLayout<LayoutBusinessQrCodeGuideBinding>(inflater, R.layout.layout_business_qr_code_guide, this, business)
            }

            needAddLine.set(false)
            if (business.desc != null) {
                addChildLayout<LayoutBusinessDescriptionBinding>(inflater, R.layout.layout_business_description, this, business).apply {
                    tvBusinessDesc.setOnClickListener {
                        tvBusinessDesc.maxLines = Int.MAX_VALUE
                        tvBusinessDesc.ellipsize = null
                    }
                }
                needAddLine.set(true)
            }

            if (business.phone != null) {
                if (needAddLine.getAndSet(true)) {
                    addLine(inflater, this)
                }
                addChildLayout<LayoutBusinessPhoneBinding>(inflater, R.layout.layout_business_phone, this, business).apply {
                    tvBusinessPhone.setOnClickListener {
                        this@BusinessDetailActivity.callPhone(business.phone)
                    }
                }
            }

            if (business.categories?.isNotEmpty() == true) {
                if (needAddLine.getAndSet(true)) {
                    addLine(inflater, this)
                }
                addChildLayout<LayoutBusinessCategoriesBinding>(inflater, R.layout.layout_business_categories, this, business)
            }

            addChildLayout<LayoutBusinessAskForRateBinding>(inflater, R.layout.layout_business_ask_for_rate, this, business).apply {

                val onClickListener = View.OnClickListener { view ->
                    this.point = when (view.id) {
                        btRateOneStar.id -> 1
                        btRateTwoStar.id -> 2
                        btRateThreeStar.id -> 3
                        btRateFourStar.id -> 4
                        btRateFiveStar.id -> 5
                        else -> 0
                    }
                    WritePostActivity.start(view.context, business, this.point)
                }

                btRateOneStar.setOnClickListener(onClickListener)
                btRateTwoStar.setOnClickListener(onClickListener)
                btRateThreeStar.setOnClickListener(onClickListener)
                btRateFourStar.setOnClickListener(onClickListener)
                btRateFiveStar.setOnClickListener(onClickListener)

                business.statistic?.lastReview?.let {
                    this.point = it.point ?: 0
                }
            }

            val needAddTitle = AtomicBoolean(true)
            business.events?.fromZody?.forEach { event ->
                if (needAddTitle.getAndSet(false)) {
                    addChildLayout<LayoutBusinessEventTitleBinding>(inflater, R.layout.layout_business_event_title, this, business).apply {
                        this.tvTitle.setText(R.string.business_detail_event_from_zody)
                    }
                }
                addChildLayout<LayoutBusinessEventBinding>(inflater, R.layout.layout_business_event, this, business).apply {
                    this.event = event
                }
            }
            business.events?.billStack?.let { billStack ->
                if (billStack.isNotEmpty()) {
                    val sortedList = billStack.sortedBy { it.amount }
                    val event = sortedList.firstOrNull {
                        it.amount != null && it.amount > business.statistic?.expense ?: 0
                    } ?: sortedList.last()
                    addChildLayout<LayoutBusinessEventBinding>(inflater, R.layout.layout_business_event, this, business).apply {
                        this.event = event
                    }
                    addChildLayout<LayoutBusinessMilestonesBinding>(inflater, R.layout.layout_business_milestones, this, business).apply {

                    }
                }
            }

            needAddTitle.set(true)
            business.events?.fromMerchant?.forEach { event ->
                if (needAddTitle.getAndSet(false)) {
                    addChildLayout<LayoutBusinessEventTitleBinding>(inflater, R.layout.layout_business_event_title, this, business).apply {
                        this.tvTitle.setText(R.string.business_detail_event_from_merchant)
                    }
                }
                addChildLayout<LayoutBusinessEventBinding>(inflater, R.layout.layout_business_event, this, business).apply {
                    this.event = event
                }
            }

            business.topUsers?.let { topUsers ->
                if (topUsers.data?.isNotEmpty() == true) {
                    addChildLayout<LayoutBusinessTopUserTitleBinding>(inflater, R.layout.layout_business_top_user_title, this, business).apply {

                    }
                    if (topUsers.me?.statistic?.bill ?: 0 > 0) {
                        addChildLayout<LayoutBusinessTopUserMineBinding>(inflater, R.layout.layout_business_top_user_mine, this, business).apply {

                        }
                    }
                    needAddLine.set(false)
                    for (i in 0 until Math.min(3, topUsers.data.size)) {
                        if (needAddLine.getAndSet(true)) {
                            addLine(inflater, this)
                        }
                        addChildLayout<LayoutBusinessTopUserBinding>(inflater, R.layout.layout_business_top_user, this, business).apply {
                            this.topUser = topUsers.data[i]

                            root.setOnClickListener {
                                UserActivity.start(this@BusinessDetailActivity, topUser?.id)
                            }
                        }
                    }
                    if (needAddLine.getAndSet(false)) {
                        addLine(inflater, this)
                    }
                }
            }
            business.rating?.let { _ ->
                addChildLayout<LayoutBusinessRatingBinding>(inflater, R.layout.layout_business_rating, this, business).apply {

                }
            }
        }
    }

    private fun initBusinessPhoto(photos: List<Photo>) {
        if (photos.isEmpty()) return
        if (photos.isNotEmpty()) {
            DataBindingUtil.inflate<LayoutBusinessPhotoBinding>(layoutInflater, R.layout.layout_business_photo, binding.layoutBusinessInfo, false).apply {
                numberPhoto = photos.size
                recyclerView.adapter = BusinessDetailPhotoAdapter(photos.mapNotNull { it.url })
                recyclerView.addItemDecoration(object : RecyclerView.ItemDecoration() {

                    private val dividerSize = 8.px

                    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                        outRect.bottom = dividerSize
                        outRect.left = dividerSize
                        outRect.right = dividerSize
                        outRect.top = dividerSize
                    }
                })

                tvBusinessPhoto.setOnClickListener { view ->
                    businessDetailViewModel.business.value?.data?.let { business ->
                        BusinessPhotoActivity.start(view.context, business.id, business.name)
                    }
                }
                val index = binding.layoutBusinessInfo.childCount - (if (binding.layoutBusinessInfo.findViewById<View>(R.id.pbRatingAvg) != null) 1 else 0)
                binding.layoutBusinessInfo.addView(root, index)
            }
        }
    }

    private fun <T : ViewDataBinding> addChildLayout(inflater: LayoutInflater, layoutRes: Int, parent: ViewGroup, business: Business): T {
        val layout = DataBindingUtil.inflate<T>(inflater, layoutRes, parent, true)
        layout.setVariable(BR.business, business)
        return layout
    }

    private fun addLine(inflater: LayoutInflater, parent: ViewGroup) {
        inflater.inflate(R.layout.layout_business_line, parent, true)
    }
}