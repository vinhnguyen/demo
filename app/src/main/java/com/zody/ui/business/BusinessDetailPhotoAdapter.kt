package com.zody.ui.business

import androidx.databinding.ViewDataBinding
import com.zody.R
import com.zody.databinding.ItemListBusinessDetailPhotoBinding
import com.zody.ui.base.DatabindingAdapter
import com.zody.ui.photo.PhotoActivity

class BusinessDetailPhotoAdapter(val items: List<String>) : DatabindingAdapter() {
    companion object {
        const val DETAIL_PHOTO_MAX_ITEM = 6
    }

    override fun onBind(binding: ViewDataBinding, position: Int) {
        (binding as ItemListBusinessDetailPhotoBinding).apply {
            url = items[position]

            binding.root.setOnClickListener { view ->
                PhotoActivity.start(view.context, items, url)
            }
        }
    }

    override fun getItemCount(): Int {
        return if (items.size > DETAIL_PHOTO_MAX_ITEM) DETAIL_PHOTO_MAX_ITEM else items.size
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_list_business_detail_photo
    }
}