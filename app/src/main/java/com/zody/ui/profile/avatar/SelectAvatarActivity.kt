package com.zody.ui.profile.avatar

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import androidx.core.content.FileProvider
import com.zody.R
import com.zody.ui.base.BaseActivity
import java.io.File
import java.util.*


class SelectAvatarActivity : BaseActivity() {

    companion object {
        const val REQUEST_CODE_PICK_IMAGE = 1
        const val REQUEST_CODE_CROP_IMAGE = 2
    }

    private var tempUri: Uri? = null

    override val screenName: String?
        get() = "pick avatar"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pick()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when {
            requestCode == REQUEST_CODE_PICK_IMAGE && resultCode == Activity.RESULT_OK -> data?.data?.let { crop(it) }
            requestCode == REQUEST_CODE_CROP_IMAGE && resultCode == Activity.RESULT_OK -> returnResult(tempUri)
            else -> returnResult(null)
        }
    }

    private fun pick() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Intent(Intent.ACTION_OPEN_DOCUMENT)
        } else {
            Intent(Intent.ACTION_GET_CONTENT)
        }.apply {
            type = "image/*"
            addCategory(Intent.CATEGORY_OPENABLE)
            addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION)
            if (packageManager.resolveActivity(intent, 0) != null) {
                startActivityForResult(this, REQUEST_CODE_PICK_IMAGE)
            } else {
                returnResult(null)
            }
        }
    }

    private fun crop(uri: Uri) {
        tempUri = createTemp()

        val intent = Intent("com.android.camera.action.CROP").apply {
            setDataAndType(uri, "image/*")
            putExtra("aspectX", 1)
            putExtra("aspectY", 1)
            putExtra(MediaStore.EXTRA_OUTPUT, tempUri)
            addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
        }
        val list = packageManager.queryIntentActivities(intent, 0)
        if (list != null && !list.isEmpty()) {
            list.forEach {
                this.grantUriPermission(it.activityInfo.packageName, tempUri, Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
            }
            startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE)
        } else {
            returnResult(uri)
        }
    }

    private fun returnResult(uri: Uri?) {
        Intent().apply {
            data = uri
            setResult(Activity.RESULT_OK, this)
            finish()
        }
    }

    private fun createTemp(): Uri {
        val name = String.format("%s.png", UUID.randomUUID().toString().trim())
        val file = File(filesDir, name)
        return FileProvider.getUriForFile(this, getString(R.string.provider_authorities), file)
    }
}