package com.zody.ui.profile

import android.graphics.Bitmap
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.zody.entity.ListItem
import com.zody.entity.Post
import com.zody.entity.Profile
import com.zody.repository.Listing
import com.zody.repository.ListingData
import com.zody.repository.PostRepository
import com.zody.repository.ProfileRepository
import com.zody.ui.base.BaseViewModel
import com.zody.ui.writepost.WritePostData
import com.zody.utils.px
import com.zody.utils.valueIfDifferent
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 11
 */
class ProfileViewModel @Inject constructor(private val profileRepository: ProfileRepository,
                                           postRepository: PostRepository,
                                           writePostData: WritePostData) : BaseViewModel() {

    private val loadProfileTrigger = MutableLiveData<Unit>()

    private val loadProfileResult = Transformations.switchMap(loadProfileTrigger) {
        profileRepository.loadProfile()
    }

    val profile: LiveData<Profile> = MediatorLiveData<Profile>().apply {
        addSource(loadProfileResult) { result ->
            result.data?.let { postValue(it) }
        }
    }

    val writingPost: LiveData<Boolean> = Transformations.map(writePostData.result) {
        it.loading
    }

    private val code = MediatorLiveData<String>().apply {
        addSource(profile) {
            valueIfDifferent = it.referral?.code ?: it.phone ?: it.id
        }
    }

    val barCode: LiveData<LiveData<Bitmap>> = Transformations.map(code) {
        profileRepository.createBarCodeImage(it, width = 155.px, height = 28.px)
    }

    private val userPosts: Listing<ListItem<Post>> = postRepository.loadOwnerPost()

    private val userPhotos: Listing<ListItem<Post>> = postRepository.loadOwnerPhoto()

    val posts: LiveData<ListingData<ListItem<Post>>> = userPosts.data

    val photos: LiveData<ListingData<ListItem<Post>>> = userPhotos.data

    fun loadProfile() {
        loadProfileTrigger.value = null
    }

}