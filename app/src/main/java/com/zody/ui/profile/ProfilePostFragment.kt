package com.zody.ui.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zody.R
import com.zody.databinding.FragmentProfilePostBinding
import com.zody.ui.base.BaseFragment
import com.zody.ui.expert.AuthorViewModel
import com.zody.ui.post.PostActionViewModel
import com.zody.ui.post.PostOptionsDialogFragment
import com.zody.utils.showMessage
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 28
 */
class ProfilePostFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentProfilePostBinding
    private val profileViewModel: ProfileViewModel by lazy {
        ViewModelProviders.of(parentFragment!!, viewModelFactory).get(ProfileViewModel::class.java)
    }
    private val authorViewModel: AuthorViewModel by lazy {
        ViewModelProviders.of(activity!!, viewModelFactory).get(AuthorViewModel::class.java)
    }
    private val postActionViewModel: PostActionViewModel by lazy {
        ViewModelProviders.of(activity!!, viewModelFactory).get(PostActionViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile_post, container, false)
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = ProfilePostAdapter(authorViewModel, postActionViewModel)
        binding.recyclerView.adapter = adapter

        adapter.onButtonMoreClick = {
            PostOptionsDialogFragment.show(fragmentManager, it)
        }

        profileViewModel.posts.observe(this, Observer {
            adapter.submitList(it.pagedList, it.endData)
        })
        profileViewModel.writingPost.observe(this, Observer {
            adapter.writingPost = it
        })
        authorViewModel.message.observe(this, Observer {
            showMessage(it)
        })
        postActionViewModel.tipResult.observe(this, Observer {
            showMessage(it)
        })
        postActionViewModel.deleteResult.observe(this, Observer {
            showMessage(it)
        })
    }

}