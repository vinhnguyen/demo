package com.zody.ui.profile

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.paging.PagedList
import com.zody.R
import com.zody.databinding.ItemListPostAuthorBinding
import com.zody.entity.ListItem
import com.zody.entity.Post
import com.zody.ui.expert.AuthorViewModel
import com.zody.ui.post.PostActionViewModel
import com.zody.ui.post.list.PostListAdapter

/**
 * Created by vinhnguyen.it.vn on 2018, August 03
 */
class ProfilePostAdapter(authorViewModel: AuthorViewModel, postActionViewModel: PostActionViewModel) : PostListAdapter(authorViewModel, postActionViewModel) {

    var writingPost = false
        set(value) {
            val showingWritingItem = field

            field = value
            if (showingWritingItem && !value) {
                notifyItemRemoved(0)
            } else if (!showingWritingItem && value) {
                notifyItemInserted(0)
            }
        }
    private val startPositionOfRealList: Int
        get() = if (writingPost) 1 else 0

    private var empty = false

    override fun initViewHolder(binding: ViewDataBinding, parent: ViewGroup) {
        super.initViewHolder(binding, parent)
        when (binding) {
            is ItemListPostAuthorBinding -> {
                binding.tvUserName.setOnClickListener(null)
                binding.ivUserAvatar.setOnClickListener(null)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            initialing -> super.getItemViewType(position)
            position == 0 && writingPost -> R.layout.item_list_post_writing
            empty -> R.layout.item_list_post_in_profile_empty
            else -> super.getItemViewType(position)
        }
    }

    override fun getItem(position: Int): ListItem<Post>? {
        return when {
            initialing -> super.getItem(position)
            position == 0 && writingPost -> null
            empty -> null
            else -> super.getItem(position - startPositionOfRealList)
        }
    }

    override fun getItemCount(): Int {
        return (if (empty) 1 else super.getItemCount()) + startPositionOfRealList
    }

    override fun submitList(pagedList: PagedList<ListItem<Post>>?, endData: Boolean) {
        super.submitList(pagedList, endData)

        val oldEmpty = empty
        empty = pagedList?.isNotEmpty() != true && endData
        if (oldEmpty != empty) {
            notifyDataSetChanged()
        }
    }

    override fun onInserted(position: Int, count: Int) {
        super.onInserted(position + startPositionOfRealList, count)
    }

    override fun onRemoved(position: Int, count: Int) {
        super.onRemoved(position + startPositionOfRealList, count)
    }

    override fun onMoved(fromPosition: Int, toPosition: Int) {
        super.onMoved(fromPosition + startPositionOfRealList, toPosition + startPositionOfRealList)
    }

    override fun onChanged(position: Int, count: Int, payload: Any?) {
        super.onChanged(position + startPositionOfRealList, count, payload)
    }
}