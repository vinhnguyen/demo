package com.zody.ui.profile.edit

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.zody.R
import com.zody.entity.Profile
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by vinhnguyen.it.vn on 2018, August 09
 */
object BindingInterface {

    @JvmStatic
    @BindingAdapter("birthday")
    fun bindBirthday(view: TextView, birthday: Date?) {
        view.text = if (birthday == null) null else SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()).format(birthday)
    }

    @JvmStatic
    @BindingAdapter("gender")
    fun bindGender(view: TextView, gender: String?) {
        val context = view.context
        view.text = when (gender) {
            Profile.GENDER_MALE -> context.getString(R.string.edit_profile_gender_male)
            Profile.GENDER_FEMALE -> context.getString(R.string.edit_profile_gender_female)
            else -> null
        }
    }
}