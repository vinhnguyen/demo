package com.zody.ui.profile

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.children
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.tabs.TabLayout
import com.zody.R
import com.zody.databinding.FragmentProfileBinding
import com.zody.ui.base.BaseFragmentWithChildFragment
import com.zody.ui.business.openSupport
import com.zody.ui.evoucher.EVoucherActivity
import com.zody.ui.profile.edit.EditProfileActivity
import com.zody.ui.setting.SettingActivity
import com.zody.ui.user.UserActivity
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 11
 */
class ProfileFragment : BaseFragmentWithChildFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentProfileBinding
    private val profileViewModel: ProfileViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ProfileViewModel::class.java)
    }

    override val screenName: String?
        get() = "profile"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false)
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.vm = profileViewModel
        binding.layoutInfo.btProfileSettings.setOnClickListener {
            goSetting()
        }
        binding.layoutInfo.btProfileEdit.setOnClickListener {
            goEditProfile()
        }
        binding.layoutInfo.btProfileReward.setOnClickListener {
            goEVoucher()
        }
        binding.layoutInfo.btProfileSupport.setOnClickListener {
            context?.openSupport()
        }
        binding.tabLayout.apply {
            for (tabView in (getChildAt(0) as ViewGroup).children) {
                tabView.apply { alpha = if (isSelected) 1F else 0.5F }
            }
            addOnTabSelectedListener(object : TabLayout.ViewPagerOnTabSelectedListener(binding.viewPager) {
                private fun setAlphaForView(tab: TabLayout.Tab?, alpha: Float) {
                    tab?.let {
                        (it.parent.getChildAt(0) as ViewGroup).getChildAt(it.position).alpha = alpha
                    }
                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {
                    super.onTabUnselected(tab)
                    setAlphaForView(tab, 0.5F)
                }

                override fun onTabSelected(tab: TabLayout.Tab?) {
                    super.onTabSelected(tab)
                    setAlphaForView(tab, 1F)
                }
            })
        }
        binding.viewPager.apply {
            addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(binding.tabLayout))
            adapter = ProfilePageAdapter(this@ProfileFragment)
        }

        profileViewModel.profile.observe(this, Observer {
            if (activity is UserActivity) {
                activity?.findViewById<TextView>(R.id.tvHeaderTitle)?.text = it.nickname
            }
        })

        profileViewModel.writingPost.observe(this, Observer {
            if (it) {
                binding.appBarLayout.setExpanded(false, true)
                binding.tabLayout.getTabAt(0)?.select()
            }
        })
    }

    override fun onResume() {
        super.onResume()
        profileViewModel.loadProfile()
    }

    private fun goSetting() {
        startActivity(Intent(context, SettingActivity::class.java))
    }

    private fun goEditProfile() {
        startActivity(Intent(context, EditProfileActivity::class.java))
    }

    private fun goEVoucher() {
        startActivity(Intent(context, EVoucherActivity::class.java))
    }
}