package com.zody.ui.profile.edit

import android.app.Application
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.zody.R
import com.zody.db.ConfigDao
import com.zody.entity.City
import com.zody.entity.Profile
import com.zody.livedata.LiveDataProviders
import com.zody.repository.ConfigurationRepository
import com.zody.repository.ProfileRepository
import com.zody.repository.Result
import com.zody.ui.base.BaseViewModel
import com.zody.utils.valueIfDifferent
import java.util.*
import javax.inject.Inject

class EditProfileViewModel @Inject constructor(private val app: Application,
                                               private val profileRepository: ProfileRepository,
                                               configDao: ConfigDao,
                                               configurationRepository: ConfigurationRepository) : BaseViewModel() {

    private val saveTrigger = MutableLiveData<Unit?>()
    private val avatarForUpdate = MutableLiveData<Uri>()

    val updateResult: LiveData<Result<Unit>> = Transformations.switchMap(saveTrigger) {
        return@switchMap when {
            name.value?.isNotEmpty() != true -> LiveDataProviders.createSingleValue(Result.error(
                    message = app.getString(R.string.edit_profile_error_require_x, app.getString(R.string.edit_profile_info_name_label))))

            nickname.value?.length ?: 0 in 1..2 -> LiveDataProviders.createSingleValue(Result.error(
                    message = app.getString(R.string.edit_profile_error_nick_name_min_length, 3)))

            city.value == null -> LiveDataProviders.createSingleValue(Result.error(
                    message = app.getString(R.string.edit_profile_error_require_x, app.getString(R.string.edit_profile_info_city_label))))

            birthday.value == null -> LiveDataProviders.createSingleValue(Result.error(
                    message = app.getString(R.string.edit_profile_error_require_x, app.getString(R.string.edit_profile_info_birthday_label))))

            gender.value?.isNotEmpty() != true -> LiveDataProviders.createSingleValue(Result.error(
                    message = app.getString(R.string.edit_profile_error_require_x, app.getString(R.string.edit_profile_info_gender_label))))

            else -> profileRepository.updateProfile(name = name.value, nickname = nickname.value,
                    birthday = birthday.value, gender = gender.value,
                    city = city.value?.id, desc = desc.value)
        }
    }

    val updateAvatarResult: LiveData<Result<Unit>> = Transformations.switchMap(avatarForUpdate) {
        profileRepository.updateAvatar(it)
    }


    val profile: LiveData<Profile> = MediatorLiveData<Profile>().apply {
        addSource(profileRepository.loadProfileFromDB()) { result ->
            result?.let {
                postValue(it)
            }
        }
    }
    val cities = configurationRepository.loadCity()

    val avatar = MediatorLiveData<Uri>().apply {
        addSource(profile) { p ->
            removeSource(profile)
            if (needToFillInfo(p)) {
                p?.avatar?.let {
                    valueIfDifferent = Uri.parse(it)
                }
            }
        }
    }
    val name = MediatorLiveData<String>().apply {
        addSource(profile) { p ->
            removeSource(profile)
            if (needToFillInfo(p)) {
                valueIfDifferent = p.name
            }
        }
    }
    val nickname = MediatorLiveData<String>().apply {
        addSource(profile) { p ->
            removeSource(profile)
            if (needToFillInfo(p)) {
                valueIfDifferent = p.nickname
            }
        }
    }
    val birthday = MediatorLiveData<Date>().apply {
        addSource(profile) { p ->
            removeSource(profile)
            if (needToFillInfo(p)) {
                valueIfDifferent = p.birthday
            }
        }
    }
    val gender = MediatorLiveData<String>().apply {
        addSource(profile) { p ->
            removeSource(profile)
            if (needToFillInfo(p)) {
                valueIfDifferent = p.gender
            }
        }
    }
    val desc = MediatorLiveData<String>().apply {
        addSource(profile) { p ->
            removeSource(profile)
            if (needToFillInfo(p)) {
                valueIfDifferent = p.desc
            }
        }
    }
    val city = MediatorLiveData<City>().apply {

        var source: LiveData<City>? = null

        addSource(profile) { p ->
            removeSource(profile)
            if (needToFillInfo(p)) {
                p?.city?.let { cityId ->
                    source?.let { removeSource(it) }
                    source = configDao.loadCityAsLiveData(cityId)
                    addSource(source!!) { city ->
                        valueIfDifferent = city
                    }
                }
            }
        }
    }

    private fun needToFillInfo(p: Profile): Boolean {
        return p.isNew != true || p.facebook != null
    }

    fun updateBirthday(value: Date) {
        birthday.valueIfDifferent = value
    }

    fun updateGender(value: String) {
        gender.valueIfDifferent = value
    }

    fun updateCity(value: City) {
        city.valueIfDifferent = value
    }

    fun updateAvatar(value: Uri) {
        avatar.valueIfDifferent = value
        avatarForUpdate.valueIfDifferent = value
    }

    fun send(name: String, nickname: String, desc: String) {
        this.name.value = name
        this.nickname.value = nickname
        this.desc.value = desc
        saveTrigger.value = null
    }
}