package com.zody.ui.profile.edit

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zody.R
import com.zody.databinding.ActivityEditProfileBinding
import com.zody.entity.Profile
import com.zody.ui.base.BaseActivity
import com.zody.ui.profile.avatar.SelectAvatarActivity
import com.zody.ui.profile.referral.ReferralActivity
import com.zody.ui.views.DateChoiceDialogFragment
import com.zody.ui.views.SingleChoiceDialogFragment
import com.zody.utils.observerOne
import com.zody.utils.showMessage
import javax.inject.Inject


class EditProfileActivity : BaseActivity() {

    companion object {
        const val EXTRA_DISABLE_GO_BACK = "EditProfileActivity.DisableGoBack"
        const val REQUEST_CODE_SELECT_AVATAR = 2000
        const val REQUEST_CODE_REFERRAL = 1
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityEditProfileBinding

    private val disableGoBack: Boolean by lazy {
        intent.getBooleanExtra(EXTRA_DISABLE_GO_BACK, false)
    }

    private val editProfileViewModel: EditProfileViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(EditProfileViewModel::class.java)
    }

    override val screenName: String?
        get() = "edit profile"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_profile)
        binding.setLifecycleOwner(this)
        binding.vm = editProfileViewModel
        binding.hideNickname = disableGoBack

        if (disableGoBack) {
            binding.headerBar.btHeaderActionLeft.visibility = View.INVISIBLE
        }
        binding.headerBar.btHeaderActionLeft.setOnClickListener { finish() }
        binding.headerBar.btHeaderActionRight.setOnClickListener {
            editProfileViewModel.send(
                    name = binding.etEditName.text.toString().trim(),
                    nickname = binding.etEditNickname.text.toString().trim(),
                    desc = binding.etEditDesc.text.toString().trim()
            )
        }

        binding.etEditReferral.setOnClickListener {
            if (binding.etEditReferral.text.toString().isEmpty()) {
                startActivityForResult(Intent(this, ReferralActivity::class.java), REQUEST_CODE_REFERRAL)
            }
        }

        binding.etEditGender.setOnClickListener {
            val options = listOf(
                    SingleChoiceDialogFragment.Option(Profile.GENDER_MALE, getString(R.string.edit_profile_gender_male)),
                    SingleChoiceDialogFragment.Option(Profile.GENDER_FEMALE, getString(R.string.edit_profile_gender_female)))
            SingleChoiceDialogFragment.newInstance(options).apply {
                this.onItemSelected = { option, _ ->
                    editProfileViewModel.updateGender(option.key)
                }
                this.show(this@EditProfileActivity.supportFragmentManager, null)
            }
        }

        binding.etEditCity.setOnClickListener {
            editProfileViewModel.cities.observerOne(this) { cities ->
                val options = cities.map { city ->
                    SingleChoiceDialogFragment.Option(city, city.name!!)
                }
                SingleChoiceDialogFragment.newInstance(options).apply {
                    this.onItemSelected = { option, _ ->
                        editProfileViewModel.updateCity(option.key)
                    }
                    this.show(this@EditProfileActivity.supportFragmentManager, null)
                }
            }
        }

        binding.tvEditBirthday.setOnClickListener {
            DateChoiceDialogFragment.newInstance(editProfileViewModel.birthday.value).apply {
                this.onDateSelected = { date ->
                    editProfileViewModel.updateBirthday(date)
                }
                this.show(this@EditProfileActivity.supportFragmentManager, null)
            }
        }

        fun goSelectAvatar() {
            Intent(this@EditProfileActivity, SelectAvatarActivity::class.java).apply {
                startActivityForResult(this, REQUEST_CODE_SELECT_AVATAR)
            }
        }

        binding.tvAvatar.setOnClickListener {
            goSelectAvatar()
        }
        binding.ivUserAvatar.setOnClickListener {
            goSelectAvatar()
        }

        editProfileViewModel.updateResult.observe(this, Observer {
            showMessage(it)
            if (it.success == true) {
                finish()
            }
        })

        editProfileViewModel.updateAvatarResult.observe(this, Observer {
            binding.updatingAvatar = it.loading
            showMessage(it)
        })
    }

    override fun onBackPressed() {
        if (!disableGoBack) super.onBackPressed()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_SELECT_AVATAR) {
            data?.data?.let {
                editProfileViewModel.updateAvatar(it)
            }
        }
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_REFERRAL) {
            data?.getStringExtra(ReferralActivity.EXTRA_REFERRAL_CODE)?.let { code ->
                binding.referralCode = code
            }
        }
    }

}