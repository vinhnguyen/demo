package com.zody.ui.profile

import android.text.SpannableStringBuilder
import android.widget.TextView
import androidx.core.text.bold
import androidx.databinding.BindingAdapter
import com.zody.R
import com.zody.entity.Profile

/**
 * Created by vinhnguyen.it.vn on 2018, September 29
 */
object BindingInterface {

    @JvmStatic
    @BindingAdapter("phoneOrCodeForCashier")
    fun bindPhoneForCashier(view: TextView, profile: Profile?) {
        when {
            profile?.phone?.isNotEmpty() == true && profile.statuses?.verified == true ->
                view.text = SpannableStringBuilder()
                        .append(view.resources.getString(R.string.profile_give_your_phone_1))
                        .append(" ")
                        .bold {
                            append(profile.phone.replace("+84", "0"))
                        }
                        .append(" ")
                        .append(view.resources.getString(R.string.profile_give_your_phone_2))
            profile?.referral?.code?.isNotEmpty() == true ->
                view.text = SpannableStringBuilder()
                        .append(view.resources.getString(R.string.profile_give_your_code_1))
                        .append(" ")
                        .bold {
                            append(profile.referral.code)
                        }
                        .append(" ")
                        .append(view.resources.getString(R.string.profile_give_your_code_2))
            else ->
                view.text = null
        }
    }
}