package com.zody.ui.profile.referral

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.zody.R
import com.zody.livedata.LiveDataProviders
import com.zody.repository.ConfigurationRepository
import com.zody.repository.ProfileRepository
import com.zody.repository.Result
import com.zody.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 28
 */
class ReferralViewModel @Inject constructor(app: Application,
                                            configurationRepository: ConfigurationRepository,
                                            profileRepository: ProfileRepository) : BaseViewModel() {

    val code = MutableLiveData<String>()

    val coinWhenSuccess = configurationRepository.getReferralInputSuccess()

    val result: LiveData<Result<Int>> = Transformations.switchMap(code) {
        when {
            it?.isNotEmpty() != true ->
                LiveDataProviders.createSingleValue(Result.error(message = app.getString(R.string.refferal_error_empty)))
            it.length < 6 || it.length > 10 ->
                LiveDataProviders.createSingleValue(Result.error(message = app.getString(R.string.refferal_error_wrong_format, 6, 10)))
            else ->
                profileRepository.inputReferralCode(it)
        }
    }

    fun send(code: String) {
        this.code.value = code
    }
}