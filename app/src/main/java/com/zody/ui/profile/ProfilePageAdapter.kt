package com.zody.ui.profile

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentStatePagerAdapter

/**
 * Created by vinhnguyen.it.vn on 2018, July 28
 */
class ProfilePageAdapter(profileFragment: ProfileFragment) : FragmentStatePagerAdapter(profileFragment.childFragmentManager) {

    override fun getItem(position: Int): Fragment {
        return if (position == 0) ProfilePostFragment() else ProfilePhotoFragment()
    }

    override fun getCount(): Int = 2
}