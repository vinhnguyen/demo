package com.zody.ui.profile.referral

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zody.R
import com.zody.databinding.ActivityReferralBinding
import com.zody.ui.base.BaseActivity
import com.zody.utils.showMessage
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 28
 */
class ReferralActivity : BaseActivity() {

    companion object {
        const val EXTRA_REFERRAL_CODE = "ReferralActivity.ReferralCode"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityReferralBinding

    private val referralViewModel: ReferralViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ReferralViewModel::class.java)
    }

    override val screenName: String?
        get() = "enter referral code"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_referral)
        binding.headerBar.btHeaderActionLeft.setOnClickListener {
            finish()
        }
        binding.btReferralApply.setOnClickListener {
            referralViewModel.send(binding.etReferral.text.toString().trim())
        }

        if (referralViewModel.coinWhenSuccess > 0) {
            binding.textView2.text = getString(R.string.referral_introduction, referralViewModel.coinWhenSuccess)
        }
        referralViewModel.result.observe(this, Observer {
            showMessage(it)
            if (it.success == true) {
                setResult(Activity.RESULT_OK, Intent().apply {
                    putExtra(EXTRA_REFERRAL_CODE, referralViewModel.code.value)
                })
                finish()
            }
        })
    }
}