package com.zody.ui.header

import android.content.Context
import android.content.Intent
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.zody.R
import com.zody.databinding.LayoutHeaderViewBinding
import com.zody.ui.notification.NotificationActivity
import com.zody.ui.views.SingleChoiceDialogFragment
import com.zody.utils.observerOne
import com.zody.utils.toActivity

/**
 * Created by vinhnguyen.it.vn on 2018, November 14
 */
class HeaderView : FrameLayout {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    val binding: LayoutHeaderViewBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context), R.layout.layout_header_view, this, true)

    fun init(headerViewModel: HeaderViewModel, lifecycleOwner: LifecycleOwner) {
        headerViewModel.city.observe(lifecycleOwner, Observer {
            binding.city = it
        })
        headerViewModel.unreadNotification.observe(lifecycleOwner, Observer {
            binding.notification = it ?: 0
        })
        binding.btHomeNotification.setOnClickListener {
            context.startActivity(Intent(context, NotificationActivity::class.java))
        }
        binding.tvCurrentCity.setOnClickListener {
            headerViewModel.cities.observerOne(lifecycleOwner) { cities ->
                val options = cities.map {
                    SingleChoiceDialogFragment.Option(it, it.name!!)
                }
                SingleChoiceDialogFragment.newInstance(options).apply {
                    this.onItemSelected = { option, _ ->
                        headerViewModel.updateCity(option.key)
                    }
                    this.show(this@HeaderView.context.toActivity()?.supportFragmentManager, null)
                }
            }
        }
    }


}