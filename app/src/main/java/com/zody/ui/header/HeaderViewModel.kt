package com.zody.ui.header

import com.zody.entity.City
import com.zody.livedata.CityLiveData
import com.zody.livedata.ManualCityLiveData
import com.zody.repository.ConfigurationRepository
import com.zody.repository.NotificationRepository
import com.zody.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 12
 */
class HeaderViewModel @Inject constructor(val city: CityLiveData,
                                          private val manualCity: ManualCityLiveData,
                                          configurationRepository: ConfigurationRepository,
                                          notificationRepository: NotificationRepository) : BaseViewModel() {

    val cities = configurationRepository.loadCity()

    val unreadNotification = notificationRepository.getUnreadNotification()

    fun updateCity(city: City) {
        manualCity.manualUpdate(city)
    }
}