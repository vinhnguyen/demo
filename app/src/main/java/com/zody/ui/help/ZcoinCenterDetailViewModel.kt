package com.zody.ui.help

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.zody.entity.ZcoinCenterItem
import com.zody.repository.ConfigurationRepository
import com.zody.repository.ProfileRepository
import com.zody.ui.base.BaseViewModel
import com.zody.utils.valueIfDifferent
import javax.inject.Inject


/**
 * Created by vinhnguyen.it.vn on 2018, August 14
 */
class ZcoinCenterDetailViewModel @Inject constructor(configurationRepository: ConfigurationRepository,
                                                     profileRepository: ProfileRepository) : BaseViewModel() {

    private val id = MutableLiveData<String>()

    val zcoinCenterItem: LiveData<ZcoinCenterItem> = Transformations.switchMap(id) {
        configurationRepository.loadZcoinCenterItem(it)
    }

    val link: LiveData<String> = Transformations.switchMap(zcoinCenterItem) { item ->
        MediatorLiveData<String>().apply {
            val profile = profileRepository.loadProfileFromDB()
            addSource(profile) { p ->
                if (p != null) {
                    removeSource(profile)
                    val builder = Uri.parse(item.link).buildUpon()

                    item.type?.let { builder.appendQueryParameter("type", it) }
                    p.phone?.let { if (p.statuses?.verified == true) builder.appendQueryParameter("phone", it) }
                    p.referral?.code?.let { builder.appendQueryParameter("id", it) }

                    value = builder.build().toString()
                }
            }
        }
    }

    fun init(id: String) {
        this.id.valueIfDifferent = id
    }
}