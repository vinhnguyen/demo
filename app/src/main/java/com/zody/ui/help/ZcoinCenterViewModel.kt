package com.zody.ui.help

import com.zody.repository.ConfigurationRepository
import com.zody.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, August 14
 */
class ZcoinCenterViewModel @Inject constructor(configurationRepository: ConfigurationRepository) : BaseViewModel() {

    val data = configurationRepository.loadZcoinCenter()

}