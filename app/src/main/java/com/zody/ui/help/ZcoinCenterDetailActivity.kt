package com.zody.ui.help

import android.annotation.SuppressLint
import android.os.Bundle
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zody.R
import com.zody.databinding.ActivityZcoinCenterDetailBinding
import com.zody.ui.base.BaseActivity
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, August 14
 */
class ZcoinCenterDetailActivity : BaseActivity() {

    companion object {
        const val EXTRA_ZCOIN_CENTER_ITEM_ID = "ZcoinCenterDetailActivity.ZcoinCenterItemId"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityZcoinCenterDetailBinding

    override val screenName: String?
        get() = "zcoin center detail"

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_zcoin_center_detail)
        binding.headerBar.btHeaderActionLeft.setOnClickListener { finish() }

        val zcoinCenterDetailViewModel = ViewModelProviders.of(this, viewModelFactory).get(ZcoinCenterDetailViewModel::class.java)
        zcoinCenterDetailViewModel.init(intent.getStringExtra(EXTRA_ZCOIN_CENTER_ITEM_ID))
        zcoinCenterDetailViewModel.zcoinCenterItem.observe(this, Observer {
            binding.headerBar.title = it.title
        })
        zcoinCenterDetailViewModel.link.observe(this, Observer {
            binding.webView.apply {
                settings.javaScriptEnabled = true
                webViewClient = WebViewClient()
                loadUrl(it)
            }
        })
    }

    override fun onBackPressed() {
        if (binding.webView.canGoBack()) {
            binding.webView.goBack()
        } else {
            super.onBackPressed()
        }
    }
}