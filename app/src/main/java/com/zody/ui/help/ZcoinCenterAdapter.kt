package com.zody.ui.help

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.zody.BR
import com.zody.R
import com.zody.databinding.ItemListZcoinCenterBinding
import com.zody.entity.ZcoinCenterItem
import com.zody.ui.base.DataBindingViewHolder
import com.zody.ui.base.DatabindingListAdapter
import com.zody.utils.openUrl

/**
 * Created by vinhnguyen.it.vn on 2018, August 14
 */
class ZcoinCenterAdapter : DatabindingListAdapter<ZcoinCenterItem>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingViewHolder {
        val holder = super.onCreateViewHolder(parent, viewType)
        when (holder.binding) {
            is ItemListZcoinCenterBinding -> holder.itemView.setOnClickListener { view ->
                holder.binding.item?.link?.let { url ->
                    view.context.openUrl(url)
                }
            }
        }
        return holder
    }

    override fun onBind(binding: ViewDataBinding, item: ZcoinCenterItem?) {
        binding.setVariable(BR.item, item)
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) R.layout.item_list_zcoin_center_header else R.layout.item_list_zcoin_center
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + 1
    }

    override fun getItem(position: Int): ZcoinCenterItem? {
        return if (position == 0) null else super.getItem(position - 1)
    }

    override fun onInserted(position: Int, count: Int) {
        super.onInserted(position + 1, count)
    }

    override fun onRemoved(position: Int, count: Int) {
        super.onRemoved(position + 1, count)
    }

    override fun onMoved(fromPosition: Int, toPosition: Int) {
        super.onMoved(fromPosition + 1, toPosition + 1)
    }

    override fun onChanged(position: Int, count: Int, payload: Any?) {
        super.onChanged(position + 1, count, payload)
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ZcoinCenterItem>() {
            override fun areItemsTheSame(oldItem: ZcoinCenterItem, newItem: ZcoinCenterItem): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: ZcoinCenterItem, newItem: ZcoinCenterItem): Boolean {
                return oldItem == newItem
            }
        }
    }
}