package com.zody.ui.help

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import com.zody.R
import com.zody.databinding.ActivityZcoinCenterBinding
import com.zody.ui.base.BaseActivity
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, August 14
 */
class ZcoinCentreActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityZcoinCenterBinding

    override val screenName: String?
        get() = "zcoin center"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_zcoin_center)

        binding.headerBar.btHeaderActionLeft.setOnClickListener { finish() }

        val adapter = ZcoinCenterAdapter()
        binding.recyclerView.apply {
            addItemDecoration(DividerItemDecoration(this@ZcoinCentreActivity, DividerItemDecoration.VERTICAL))
            this.adapter = adapter
        }

        val zcoinCenterViewModel = ViewModelProviders.of(this, viewModelFactory).get(ZcoinCenterViewModel::class.java)
        zcoinCenterViewModel.data.observe(this, Observer {
            adapter.submitList(it)
        })
    }
}