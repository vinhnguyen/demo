package com.zody.ui.notification

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.zody.R
import com.zody.databinding.DialogNotificationBinding
import com.zody.ui.base.BaseDialogFragment

/**
 * Created by vinhnguyen.it.vn on 2018, November 02
 */
class NotificationDialog : BaseDialogFragment() {

    var content: String? = null

    private lateinit var binding: DialogNotificationBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_notification, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.content = content
        binding.btAction.setOnClickListener {
            dismiss()
        }
    }
}