package com.zody.ui.notification

import android.graphics.Rect
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.zody.R
import com.zody.databinding.ActivityNotificationBinding
import com.zody.ui.base.BaseActivity
import com.zody.utils.px
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, August 08
 */
class NotificationActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityNotificationBinding
    private val notificationViewModel: NotificationViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(NotificationViewModel::class.java)
    }

    override val screenName: String?
        get() = "notification list"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_notification)
        binding.setLifecycleOwner(this)
        binding.headerBar.btHeaderActionLeft.setOnClickListener {
            finish()
        }

        binding.swipeRefreshLayout.apply {
            setOnRefreshListener {
                notificationViewModel.refresh()
            }
        }

        val adapter = NotificationAdapter(notificationViewModel)
        binding.recyclerView.apply {
            this.adapter = adapter
            this.addItemDecoration(object : RecyclerView.ItemDecoration() {
                override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                    outRect.bottom = 1.px
                }
            })
        }

        notificationViewModel.forceRead()
        notificationViewModel.data.observe(this, Observer {
            adapter.submitList(it.pagedList, it.endData)
        })
        notificationViewModel.refreshState.observe(this, Observer {
            binding.swipeRefreshLayout.isRefreshing = it.loading
        })

    }
}