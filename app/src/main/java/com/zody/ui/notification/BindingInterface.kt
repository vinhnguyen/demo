package com.zody.ui.notification

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Build
import android.text.Layout
import android.text.SpannableStringBuilder
import android.text.StaticLayout
import android.widget.TextView
import androidx.core.text.color
import androidx.core.view.doOnPreDraw
import androidx.databinding.BindingAdapter
import com.zody.entity.Notification
import com.zody.ui.base.BindingInterface.setTextWithClickable
import com.zody.utils.periodFormat
import com.zody.utils.toHtml

/**
 * Created by vinhnguyen.it.vn on 2018, August 22
 */
object BindingInterface {

    @SuppressLint("ClickableViewAccessibility")
    @JvmStatic
    @BindingAdapter(value = ["notification", "maxLine"], requireAll = false)
    fun bindContentAndTime(view: TextView, notification: Notification?, maxLine: Int?) {
        if (notification?.content?.trim()?.isNotEmpty() != true) {
            view.text = null
            return
        }
        val timeAgo = notification.created?.periodFormat(view.context)
        val content = notification.content.trim().toHtml()
        if (maxLine == null || maxLine == 0) {
            val builder = SpannableStringBuilder().append(content)
            if (timeAgo?.isNotEmpty() == true) {
                builder.append(" ")
                        .color(Color.parseColor("#9b9b9b")) {
                            append(timeAgo)
                        }
            }
            setTextWithClickable(view, builder)
            return
        }
        view.doOnPreDraw { _ ->

            val width = view.width - view.paddingLeft - view.paddingRight
            fun createLayout(text: CharSequence): StaticLayout {
                return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    StaticLayout.Builder.obtain(text, 0, text.length, view.paint, width).build()
                } else {
                    @Suppress("DEPRECATION")
                    (StaticLayout(text, view.paint, width, Layout.Alignment.ALIGN_NORMAL, 0.0f, 0.0f, false))
                }
            }

            val layout = createLayout(content)
            if (layout.lineCount <= maxLine) {
                val builder = SpannableStringBuilder().append(content)
                if (timeAgo?.isNotEmpty() == true) {
                    builder.append(" ")
                            .color(Color.parseColor("#9b9b9b")) {
                                append(timeAgo)
                            }
                }
                setTextWithClickable(view, builder)
            } else {
                val viewMoreText = SpannableStringBuilder()
                        .append("... ").apply {
                            if (timeAgo?.isNotEmpty() == true) {
                                color(Color.parseColor("#9b9b9b")) {
                                    append(timeAgo)
                                }
                            }
                        }

                for (i in layout.getLineEnd(maxLine - 1) downTo layout.getLineStart(maxLine - 1)) {
                    val temp = SpannableStringBuilder(content.subSequence(0, i)).append(viewMoreText)
                    if (createLayout(temp).lineCount == maxLine) {
                        setTextWithClickable(view, temp)
                        break
                    }
                }
            }
        }
    }
}