package com.zody.ui.notification

import androidx.lifecycle.LiveData
import com.zody.entity.ListItem
import com.zody.entity.Notification
import com.zody.repository.Listing
import com.zody.repository.ListingData
import com.zody.repository.NetworkState
import com.zody.repository.NotificationRepository
import com.zody.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, August 08
 */
class NotificationViewModel @Inject constructor(private val notificationRepository: NotificationRepository) : BaseViewModel() {

    private val result: Listing<ListItem<Notification>> = notificationRepository.loadNotification()

    val data: LiveData<ListingData<ListItem<Notification>>> = result.data

    val networkState: LiveData<NetworkState> = result.networkState
    val refreshState: LiveData<NetworkState> = result.refreshState

    fun refresh() {
        result.refresh.invoke()
    }

    fun retry() {
        result.retry.invoke()
    }

    fun setRead(id: String) {
        notificationRepository.readNotification(id)
    }

    fun forceRead() {
        notificationRepository.forceRead()
    }

}