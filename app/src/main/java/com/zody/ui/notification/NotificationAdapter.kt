package com.zody.ui.notification

import androidx.databinding.ViewDataBinding
import androidx.paging.PagedList
import androidx.recyclerview.widget.DiffUtil
import com.zody.BR
import com.zody.R
import com.zody.databinding.ItemListNotificationBinding
import com.zody.entity.ListItem
import com.zody.entity.Notification
import com.zody.ui.base.ListItemAdapter
import com.zody.ui.base.Navigation
import com.zody.utils.toActivity

/**
 * Created by vinhnguyen.it.vn on 2018, August 08
 */
class NotificationAdapter(private val notificationViewModel: NotificationViewModel) : ListItemAdapter<Notification>(DIFF_CALLBACK) {

    private var empty = false

    override fun onBind(binding: ViewDataBinding, item: ListItem<Notification>?) {
        binding.setVariable(BR.notification, item?.data)
        if (binding is ItemListNotificationBinding) {
            binding.tvNotificationContent.maxLines = if (item?.data?.target?.action == "OPEN_SIMPLE_POPUP") 4 else Int.MAX_VALUE
        }
        binding.root.setOnClickListener { view ->
            item?.data?.let {
                if (it.read == false) {
                    notificationViewModel.setRead(it.id)
                }
                if (it.target?.action == "OPEN_SIMPLE_POPUP") {
                    if (it.content?.isNotEmpty() == true) {
                        NotificationDialog().apply {
                            content = it.content
                            show(view.context.toActivity()?.supportFragmentManager, null)
                        }
                    }
                } else {
                    Navigation.navigate(view.context, it)
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            initialing -> R.layout.item_list_notification_place_holder
            empty -> R.layout.item_list_notification_empty
            else -> R.layout.item_list_notification
        }
    }

    override fun getItem(position: Int): ListItem<Notification>? {
        return when {
            empty -> null
            else -> super.getItem(position)
        }
    }

    override fun getItemCount(): Int {
        return if (empty) 1 else super.getItemCount()
    }

    override fun submitList(pagedList: PagedList<ListItem<Notification>>?, endData: Boolean) {
        super.submitList(pagedList, endData)
        val oldEmpty = empty
        empty = pagedList?.isNotEmpty() != true && endData
        if (oldEmpty != empty) {
            notifyDataSetChanged()
        }
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ListItem<Notification>>() {
            override fun areItemsTheSame(oldItem: ListItem<Notification>, newItem: ListItem<Notification>): Boolean {
                return oldItem.type == newItem.type && oldItem.data?.id == newItem.data?.id
            }

            override fun areContentsTheSame(oldItem: ListItem<Notification>, newItem: ListItem<Notification>): Boolean {
                return oldItem.data == newItem.data
            }

        }
    }
}