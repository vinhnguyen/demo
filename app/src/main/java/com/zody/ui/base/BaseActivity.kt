package com.zody.ui.base

import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NavUtils
import androidx.core.app.TaskStackBuilder
import androidx.lifecycle.Observer
import com.google.android.gms.analytics.HitBuilders
import com.google.android.gms.analytics.Tracker
import com.google.android.material.snackbar.Snackbar
import com.zody.R
import com.zody.di.Injectable
import com.zody.livedata.LocationEnableLiveData
import com.zody.livedata.LuckyDrawTurnFromSocketLiveData
import com.zody.livedata.MessageFromSocketLiveData
import com.zody.repository.NotificationRepository
import com.zody.ui.luckydraw.dialog.NewTurnDialog
import com.zody.ui.writepost.WritePostActivity
import com.zody.ui.writepost.WritePostData
import com.zody.utils.hideKeyboard
import timber.log.Timber
import javax.inject.Inject


/**
 * Created by vinhnguyen.it.vn on 2018, June 27
 */
abstract class BaseActivity : AppCompatActivity(), Injectable {

    @Inject
    lateinit var messageFromSocket: MessageFromSocketLiveData

    @Inject
    lateinit var newTurnFromSocket: LuckyDrawTurnFromSocketLiveData

    @Inject
    lateinit var notificationRepository: NotificationRepository

    @Inject
    lateinit var writePostData: WritePostData

    @Inject
    lateinit var tracker: Tracker

    @Inject
    lateinit var locationEnable: LocationEnableLiveData

    open val screenName: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        screenName?.let {
            tracker.setScreenName(it)
            tracker.send(HitBuilders.ScreenViewBuilder().build())
        }

        try {
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        } catch (e: Exception) {
            Timber.e(e)
        }

        messageFromSocket.observe(this, Observer { message ->
            message?.let {
                MessageDialog.instantiate(title = getString(R.string.app_name),
                        message = it,
                        action = getString(android.R.string.ok)
                ).show(supportFragmentManager, null)
            }
        })

        newTurnFromSocket.observe(this, Observer {
            val dialog = NewTurnDialog()
            dialog.data = it
            dialog.show(supportFragmentManager, null)
        })

        Navigation.getNotification(intent)?.let { notification ->
            if (notification.read == false) {
                notificationRepository.readNotification(id = notification.id)
            }
        }

        writePostData.error.observe(this, Observer { message ->
            findViewById<ViewGroup>(android.R.id.content)?.let { root ->
                Snackbar.make(root, message, Snackbar.LENGTH_LONG)
                        .setAction("Thử lại") {
                            startActivity(Intent(this@BaseActivity, WritePostActivity::class.java).apply {
                                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                            })
                        }
                        .show()
            }
        })
    }

    override fun onPause() {
        super.onPause()
        currentFocus?.hideKeyboard()
    }

    override fun finish() {
        if (!backToParent()) {
            super.finish()
        }
    }

    override fun onResume() {
        super.onResume()
        locationEnable.update()
    }

    private fun backToParent(): Boolean {
        val upIntent = NavUtils.getParentActivityIntent(this) ?: return false

        if (NavUtils.shouldUpRecreateTask(this, upIntent) || isTaskRoot) {
            TaskStackBuilder.create(this)
                    .addNextIntentWithParentStack(upIntent)
                    .startActivities()
            return true
        }
        return false
    }
}