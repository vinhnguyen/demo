package com.zody.ui.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by vinhnguyen.it.vn on 2018, July 13
 */
open class DataBindingViewHolder(val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {

    constructor(parent: ViewGroup, @LayoutRes layout: Int)
            : this(DataBindingUtil.inflate(LayoutInflater.from(parent.context), layout, parent, false))
}