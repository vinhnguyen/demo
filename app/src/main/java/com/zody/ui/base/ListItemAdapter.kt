package com.zody.ui.base

import androidx.paging.PagedList
import androidx.recyclerview.widget.DiffUtil
import com.zody.entity.ListItem

/**
 * Created by vinhnguyen.it.vn on 2018, July 30
 */
abstract class ListItemAdapter<T>(diffCallback: DiffUtil.ItemCallback<ListItem<T>>) : DatabindingPagedListAdapter<ListItem<T>>(diffCallback) {

    var initialing = true

    open val numberOfPlaceHolder: Int = 10

    override fun getItem(position: Int): ListItem<T>? {
        return when {
            initialing -> null
            position >= super.getItemCount() -> null
            else -> super.getItem(position)
        }
    }

    override fun getItemCount(): Int {
        return when {
            initialing -> numberOfPlaceHolder
            else -> super.getItemCount()
        }
    }

    open fun submitList(pagedList: PagedList<ListItem<T>>?, endData: Boolean) {
        val oldInitialing = initialing
        initialing = pagedList?.isNotEmpty() != true && !endData
        if (oldInitialing && !initialing) {
            notifyDataSetChanged()
        }
        submitList(pagedList)
    }
}