package com.zody.ui.base

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager

/**
 * Created by vinhnguyen.it.vn on 2018, October 30
 */
abstract class BaseDialogFragment : DialogFragment() {

    var onShowListener: DialogInterface.OnShowListener? = null
    var onDismissListener: DialogInterface.OnDismissListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setOnShowListener {
            onShowListener?.onShow(it)
        }
        return dialog
    }

    override fun onStart() {
        super.onStart()
        dialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    override fun show(manager: FragmentManager?, tag: String?) {
        if (manager?.isStateSaved == false) {
            super.show(manager, tag)
        }
    }

    override fun onDismiss(dialog: DialogInterface?) {
        onDismissListener?.onDismiss(dialog)
        super.onDismiss(dialog)
    }
}