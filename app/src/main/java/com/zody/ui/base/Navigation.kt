package com.zody.ui.base

import android.content.Context
import android.content.Intent
import android.text.TextUtils
import com.google.gson.Gson
import com.zody.entity.Notification
import com.zody.entity.explore.Category
import com.zody.ui.business.openFacebook
import com.zody.ui.business.openInstagram
import com.zody.utils.openUrl
import com.zody.utils.startActivityIfResolved
import com.zody.utils.toArrayList
import org.json.JSONException


/**
 * Created by vinhnguyen.it.vn on 2018, August 27
 */
object Navigation {

    const val EXTRA_TARGET_VALUE = "targetValue"
    const val EXTRA_TARGET_TITLE = "targetTitle"
    const val EXTRA_CATEGORIES = "categories"

    private const val TYPE_URL = "Url"
    private const val TYPE_ZCOIN_CENTRE = "ZcoinCentre"
    private const val TYPE_BUSINESS = "Business"
    private const val TYPE_VOUCHER = "Voucher"
    private const val TYPE_LUCKY_DRAW = "LuckyDraw"
    private const val TYPE_LUCKY_MONEY = "LuckyMoney"
    private const val TYPE_PROMOTION = "PromotionList"
    private const val TYPE_FACEBOOK = "Facebook"
    private const val TYPE_INSTAGRAM = "Instagram"
    private const val TYPE_CHAIN = "Chain"
    private const val TYPE_INVITE_FRIEND = "InviteFriend"
    private const val TYPE_REWARD = "Reward"
    private const val TYPE_TCH = "TheCoffeeHouse"
    private const val TYPE_TRENDING = "Trending"
    private const val TYPE_NEW_BUSINESS = "NewBusinessList"
    private const val TYPE_NEWS = "News"
    private const val TYPE_AFFILIATE_STATUS = "AffiliateStatus"

    fun start(context: Context?, type: String?, value: String?, url: String?, title: String?, categories: List<Category>?) {
        if (context == null || TextUtils.isEmpty(type)) return
        val action = when (type) {
            TYPE_URL -> {
                context.openUrl(url ?: value)
                null
            }
            TYPE_BUSINESS -> if (TextUtils.isEmpty(value)) {
                "BUSINESS_LIST"
            } else {
                "OPEN_BUSINESS_DETAIL"
            }
            TYPE_ZCOIN_CENTRE -> if (TextUtils.isEmpty(value)) {
                "OPEN_ZCOIN_CENTRE"
            } else {
                "OPEN_ZCOIN_CENTRE_DETAIL"
            }
            TYPE_VOUCHER -> if (TextUtils.isEmpty(value)) {
                "OPEN_VOUCHER"
            } else {
                "OPEN_VOUCHER_DETAIL"
            }
            TYPE_FACEBOOK -> {
                context.openFacebook(url = url ?: value)
                null
            }
            TYPE_INSTAGRAM -> {
                context.openInstagram(url = url ?: value)
                null
            }
            TYPE_LUCKY_DRAW -> "OPEN_LUCKY_DRAW"
            TYPE_LUCKY_MONEY -> "LUCKY_MONEY_DETAIL"
            TYPE_CHAIN -> "OPEN_CHAIN_DETAIL"
            TYPE_INVITE_FRIEND -> "OPEN_INVITE_FRIEND"
            TYPE_REWARD -> if (TextUtils.isEmpty(value)) {
                "OPEN_REWARD"
            } else {
                "OPEN_REWARD_DETAIL"
            }
            TYPE_PROMOTION -> "PROMOTION_LIST"
            TYPE_TRENDING -> "TRENDING_LIST"
            TYPE_NEW_BUSINESS -> "NEW_BUSINESSES_LIST"
            TYPE_TCH -> "OPEN_THE_COFFEE_HOUSE_LINKED"
            TYPE_NEWS -> if (TextUtils.isEmpty(value)) {
                "NEWS_LIST"
            } else {
                "OPEN_NEWS_DETAIL"
            }
            TYPE_AFFILIATE_STATUS -> "OPEN_AFFILIATE_STATUS"
            else -> null
        }
        if (action != null) {
            context.startActivityIfResolved(Intent(action).apply {
                flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
                setPackage(context.packageName)
                putExtra(EXTRA_TARGET_VALUE, value)
                putExtra(EXTRA_TARGET_TITLE, title)
                putStringArrayListExtra(EXTRA_CATEGORIES, categories?.map { it.id }?.toArrayList())
            })
        }
    }

    fun navigate(context: Context, notification: Notification) {
        notification.target?.action?.let {
            if (it == "OPEN_LIST_NOTIFICATION") return
            context.startActivityIfResolved(Intent(it).apply {
                setPackage(context.packageName)
                putExtra("data", Gson().toJson(notification))
            })
        }
    }

    fun getNotification(intent: Intent): Notification? {
        return try {
            Gson().fromJson(intent.getStringExtra("data"), Notification::class.java)
        } catch (e: JSONException) {
            null
        }
    }

    fun getId(intent: Intent): String? {
        return getNotification(intent)?.target?.id
                ?: intent.getStringExtra(EXTRA_TARGET_VALUE)
                ?: intent.data?.lastPathSegment
    }

    fun getContent(intent: Intent): String? {
        return getNotification(intent)?.content
    }
}