package com.zody.ui.base

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil

/**
 * Created by vinhnguyen.it.vn on 2018, July 19
 */
abstract class DatabindingListAdapter<T>(diffCallback: DiffUtil.ItemCallback<T>) : ListAdapter<T, DataBindingViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingViewHolder {
        return DataBindingViewHolder(parent, viewType)
    }

    override fun onBindViewHolder(holder: DataBindingViewHolder, position: Int) {
        holder.binding.apply {
            onBind(this, getItem(position))
            executePendingBindings()
        }
    }

    abstract fun onBind(binding: ViewDataBinding, item: T?)
}