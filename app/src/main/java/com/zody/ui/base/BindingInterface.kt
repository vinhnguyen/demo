package com.zody.ui.base

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.text.*
import android.text.style.ClickableSpan
import android.text.util.Linkify
import android.view.MotionEvent
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.Guideline
import androidx.core.content.ContextCompat
import androidx.core.text.color
import androidx.core.text.toSpannable
import androidx.core.text.util.LinkifyCompat
import androidx.core.view.doOnPreDraw
import androidx.core.widget.TextViewCompat
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.zody.R
import com.zody.ui.views.CircleCropWithBorderTransformation
import com.zody.ui.views.RoundedCornersWithBorderTransformation
import com.zody.utils.*
import java.util.*
import kotlin.math.roundToInt


/**
 * Created by vinhnguyen.it.vn on 2018, July 04
 */
object BindingInterface {
    @JvmStatic
    @BindingAdapter("invisible")
    fun setInvisible(view: View, invisible: Boolean) {
        view.visibility = if (invisible) View.INVISIBLE else View.VISIBLE
    }

    @JvmStatic
    @BindingAdapter("gone")
    fun setGone(view: View, gone: Boolean) {
        view.visibility = if (gone) View.GONE else View.VISIBLE
    }

    @JvmStatic
    @BindingAdapter("selected")
    fun setSelected(view: View, selected: Boolean) {
        view.isSelected = selected
    }

    @JvmStatic
    @BindingAdapter("bitmap")
    fun setImageBitmap(imageView: ImageView, bitmap: Bitmap?) {
        imageView.setImageBitmap(bitmap)
    }

    @JvmStatic
    @BindingAdapter(value = [
        "actualImageUri",
        "placeholderImage",
        "roundAsCircle",
        "roundedCornerRadius",
        "roundingBorderColor",
        "roundingBorderWidth"
    ], requireAll = false)
    fun setImageUri(imageView: ImageView,
                    uri: String?,
                    placeholder: Drawable?,
                    roundAsCircle: Boolean?,
                    roundedCornerRadius: Float?,
                    roundingBorderColor: Int?,
                    roundingBorderWidth: Float?) {
        var builder = Glide.with(imageView)
                .load(uri)
                .placeholder(placeholder)
        if (roundAsCircle == true) {
            builder = builder.transform(CircleCropWithBorderTransformation(roundingBorderColor, roundingBorderWidth?.px))
        } else if (roundedCornerRadius != null) {
            builder = builder.transform(RoundedCornersWithBorderTransformation(roundedCornerRadius.px.roundToInt(), roundingBorderColor, roundingBorderWidth?.px))
        }
        builder.into(imageView)
    }

    @JvmStatic
    @BindingAdapter(value = [
        "actualImageResource",
        "placeholderImage",
        "roundAsCircle",
        "roundedCornerRadius",
        "roundingBorderColor",
        "roundingBorderWidth"
    ], requireAll = false)
    fun setImageDrawable(imageView: ImageView,
                         src: Drawable?,
                         placeholder: Drawable?,
                         roundAsCircle: Boolean?,
                         roundedCornerRadius: Float?,
                         roundingBorderColor: Int?,
                         roundingBorderWidth: Float?) {
        var builder = Glide.with(imageView)
                .load(src)
                .placeholder(placeholder)
        if (roundAsCircle == true) {
            builder = builder.transform(CircleCropWithBorderTransformation(roundingBorderColor, roundingBorderWidth?.px))
        } else if (roundedCornerRadius != null) {
            builder = builder.transform(RoundedCornersWithBorderTransformation(roundedCornerRadius.px.roundToInt(), roundingBorderColor, roundingBorderWidth?.px))
        }
        builder.into(imageView)
    }

    @JvmStatic
    @BindingAdapter(value = ["actualImageUri", "imageWidth", "imageHeight"], requireAll = false)
    fun bindImageWitSize(imageView: ImageView, uri: Uri?, width: Int?, height: Int?) {
        var builder = Glide.with(imageView).load(uri)
        if (width != null && height != null) {
            builder = builder.apply(RequestOptions().override(width, height))
        }
        builder.into(imageView)
    }

    @JvmStatic
    @BindingAdapter("bold")
    fun setTextStyle(view: TextView, bold: Boolean) {
        view.setTypeface(Typeface.create(view.typeface, Typeface.NORMAL), if (bold) Typeface.BOLD else Typeface.NORMAL)
    }

    @JvmStatic
    @BindingAdapter("layout_constraintDimensionRatio")
    fun setRatio(view: View, ratio: String?) {
        if (view.parent is ConstraintLayout) {
            (view.layoutParams as ConstraintLayout.LayoutParams).dimensionRatio = ratio
        }
    }

    @JvmStatic
    @BindingAdapter("layout_constraintDimensionRatio")
    fun setRatio(view: View, ratio: Float?) {
        val lp = view.layoutParams
        if (view.parent is ConstraintLayout) {
            (view.layoutParams as ConstraintLayout.LayoutParams).dimensionRatio = ratio?.toString()
        }
    }

    @JvmStatic
    @BindingAdapter("created")
    fun bindCreated(view: TextView, created: Date?) {
        view.text = created?.periodFormat(view.context)
    }

    @JvmStatic
    @BindingAdapter("layout_constraintGuide_percent")
    fun setGuideLinePercent(view: Guideline, ratio: Float?) {
        ratio?.let {
            view.setGuidelinePercent(it)
        }
    }

    @JvmStatic
    @BindingAdapter("layout_constraintVertical_bias")
    fun setVerticalBias(view: View, bias: Float?) {
        bias?.let {
            val params = view.layoutParams
            if (params is ConstraintLayout.LayoutParams) {
                params.verticalBias = bias
            }
        }
    }

    @JvmStatic
    @BindingAdapter("layout_constraintHorizontal_bias")
    fun setHorizontalBias(view: View, bias: Float?) {
        bias?.let {
            val params = view.layoutParams
            if (params is ConstraintLayout.LayoutParams) {
                params.horizontalBias = bias
            }
        }
    }

    @JvmStatic
    @BindingAdapter("textHtml")
    fun setTextHtml(view: TextView, text: String?) {
        if (text == null) {
            view.text = null
            return
        }
        val spannable = text.replace("\n", "<br/>").toHtml().toSpannable()
        LinkifyCompat.addLinks(spannable, Linkify.ALL)
        setTextWithClickable(view, spannable)
    }

    @JvmStatic
    @BindingAdapter("textWithLinkify")
    fun setTextWithLinkify(view: TextView, text: String?) {
        if (text == null) {
            view.text = null
            return
        }
        val spannable = SpannableString(text)
        LinkifyCompat.addLinks(spannable, Linkify.ALL)
        setTextWithClickable(view, spannable)
    }

    @SuppressLint("ClickableViewAccessibility")
    @JvmStatic
    @BindingAdapter("texWithClickable")
    fun setTextWithClickable(view: TextView, spannable: Spannable?) {
        view.text = spannable
        spannable?.let {
            view.setOnTouchListener { _, event ->
                var ret = false
                val action = event.action

                if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_DOWN) {
                    var x = event.x.toInt()
                    var y = event.y.toInt()

                    x -= view.totalPaddingLeft
                    y -= view.totalPaddingTop

                    x += view.scrollX
                    y += view.scrollY

                    val layout = view.layout
                    val line = layout.getLineForVertical(y)
                    val off = layout.getOffsetForHorizontal(line, x.toFloat())

                    val link = it.getSpans(off, off, ClickableSpan::class.java)
                    if (link.isNotEmpty()) {
                        if (action == MotionEvent.ACTION_UP) {
                            link[0].onClick(view)
                        }
                        ret = true
                    }
                }
                ret
            }
        }
    }

    @JvmStatic
    @BindingAdapter("android:text")
    fun setText(view: TextView, spannable: Spannable?) {
        view.text = spannable
    }

    @JvmStatic
    @BindingAdapter("android:drawableLeft")
    fun setDrawableLeft(view: TextView, icon: Drawable?) {
        val drawables = TextViewCompat.getCompoundDrawablesRelative(view)
        TextViewCompat.setCompoundDrawablesRelativeWithIntrinsicBounds(view, icon, drawables[1], drawables[2], drawables[3])
    }

    @JvmStatic
    @BindingAdapter("android:drawableRight")
    fun setDrawableRight(view: TextView, icon: Drawable?) {
        val drawables = TextViewCompat.getCompoundDrawablesRelative(view)
        TextViewCompat.setCompoundDrawablesRelativeWithIntrinsicBounds(view, drawables[0], drawables[1], icon, drawables[3])
    }

    @JvmStatic
    @BindingAdapter("android:text")
    fun setText(view: EditText, text: CharSequence?) {
        view.setText(text)
        view.setSelection(view.text.length)
    }

    @SuppressLint("ClickableViewAccessibility")
    @JvmStatic
    @BindingAdapter(value = ["textWithViewMore", "maxLine", "expandWhenClicked"], requireAll = false)
    fun bindTextWithViewMore(view: TextView, text: CharSequence?, maxLine: Int?, expandWhenClicked: Boolean = false) {
        if (text?.isNotEmpty() != true) {
            view.text = null
            return
        }
        val spannable = SpannableString(text)
        LinkifyCompat.addLinks(spannable, Linkify.ALL)
        if (maxLine == null || maxLine == 0) {
            setTextWithClickable(view, spannable)
            return
        }
        view.doOnPreDraw { _ ->

            val width = view.width - view.paddingLeft - view.paddingRight
            fun createLayout(text: CharSequence): StaticLayout {
                return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    StaticLayout.Builder.obtain(text, 0, text.length, view.paint, width).build()
                } else {
                    @Suppress("DEPRECATION")
                    StaticLayout(text, view.paint, width, Layout.Alignment.ALIGN_NORMAL, 0.0f, 0.0f, false)
                }
            }

            val layout = createLayout(spannable)
            if (layout.lineCount <= maxLine) {
                setTextWithClickable(view, spannable)
                if (expandWhenClicked) {
                    view.tag = null
                    view.setOnClickListener(null)
                }
            } else {
                val viewMoreText = SpannableStringBuilder()
                        .append("... ")
                        .color(ContextCompat.getColor(view.context, R.color.gray)) {
                            append(view.resources.getString(R.string.view_more))
                        }

                for (i in layout.getLineEnd(maxLine - 1) downTo layout.getLineStart(maxLine - 1)) {
                    val temp = SpannableStringBuilder(spannable.subSequence(0, i)).append(viewMoreText)
                    if (createLayout(temp).lineCount == maxLine) {
                        setTextWithClickable(view, temp)
                        break
                    }
                }

                if (expandWhenClicked) {
                    view.tag = text
                    view.setOnClickListener { _ ->
                        view.tag = view.text.also { view.text = view.tag as CharSequence }
                    }
                }
            }
        }
    }

    @JvmStatic
    @BindingAdapter(value = ["format", "decimalNumber"], requireAll = true)
    fun formatWithDecimal(view: TextView, format: String?, number: Number?) {
        view.text = if (format == null) number?.decimalFormat else String.format(format, number?.decimalFormat)
    }

    @JvmStatic
    @BindingAdapter(value = ["decimalNumber"])
    fun formatWithDecimal(view: TextView, number: Number?) {
        view.text = number?.decimalFormat
    }

    @JvmStatic
    @BindingAdapter(value = ["format", "stringNumber"], requireAll = true)
    fun formatWithString(view: TextView, format: String?, number: Number?) {
        view.text = if (format == null) number?.stringFormat else String.format(format, number?.stringFormat)
    }

    @JvmStatic
    @BindingAdapter(value = ["format", "roundNumber"], requireAll = true)
    fun formatWithRound(view: TextView, format: String?, number: Number?) {
        view.text = if (format == null) number?.roundFormat else String.format(format, number?.roundFormat)
    }

    @JvmStatic
    @BindingAdapter(value = ["stringNumber"])
    fun formatWithString(view: TextView, number: Number?) {
        view.text = number?.stringFormat
    }

    @JvmStatic
    @BindingAdapter(value = ["distance"])
    fun formatWithDistance(view: TextView, number: Double?) {
        view.text = number?.distanceFormat
    }

    @JvmStatic
    @BindingAdapter("textForCopy")
    fun setAllowCopyText(view: View, textForCopy: String?) {
        if (textForCopy?.isNotEmpty() != true) {
            view.setOnLongClickListener(null)
            return
        }
        view.setOnLongClickListener { _ ->
            val popupMenu = PopupMenu(view.context, view)
            popupMenu.inflate(R.menu.menu_copy_text_value)
            popupMenu.setOnMenuItemClickListener {
                view.context.copy(value = textForCopy)
                return@setOnMenuItemClickListener true
            }
            popupMenu.show()
            return@setOnLongClickListener true
        }
    }
}