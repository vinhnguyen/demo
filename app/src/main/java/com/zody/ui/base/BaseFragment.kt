package com.zody.ui.base

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.google.android.gms.analytics.HitBuilders
import com.google.android.gms.analytics.Tracker
import com.zody.di.Injectable
import javax.inject.Inject


/**
 * Created by vinhnguyen.it.vn on 2018, June 29
 */
abstract class BaseFragment : Fragment(), Injectable {

    @Inject
    lateinit var tracker: Tracker

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        screenName?.let {
            tracker.setScreenName(it)
            tracker.send(HitBuilders.ScreenViewBuilder().build())
        }
    }

    open val screenName: String? = null
}