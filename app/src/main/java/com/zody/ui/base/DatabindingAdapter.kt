package com.zody.ui.base

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by vinhnguyen.it.vn on 2018, August 09
 */
abstract class DatabindingAdapter : RecyclerView.Adapter<DataBindingViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingViewHolder {
        val holder = DataBindingViewHolder(parent, viewType)
        initViewHolder(holder, parent)
        return holder
    }

    override fun onBindViewHolder(holder: DataBindingViewHolder, position: Int) {
        onBind(holder.binding, position)
        holder.binding.executePendingBindings()
    }

    open fun initViewHolder(holder: DataBindingViewHolder, parent: ViewGroup) {

    }

    abstract fun onBind(binding: ViewDataBinding, position: Int)
}