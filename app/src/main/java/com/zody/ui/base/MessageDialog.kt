package com.zody.ui.base

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.zody.R
import com.zody.databinding.DialogMessageBinding

/**
 * Created by vinhnguyen.it.vn on 2018, August 14
 */
class MessageDialog : BaseBottomSheetDialogFragment() {

    private lateinit var binding: DialogMessageBinding

    var icon: Drawable? = null
    var title: CharSequence? = null
    var message: CharSequence? = null
    var action: CharSequence? = null
    var onActionClick: (() -> Unit)? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_message, container, false)
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btDialogClose.setOnClickListener { dismissAllowingStateLoss() }
        binding.icon = icon
        binding.title = title
        binding.message = message
        binding.action = action

        binding.btAction.setOnClickListener {
            onActionClick?.invoke()
            dismissAllowingStateLoss()
        }
    }

    companion object {

        @JvmStatic
        fun instantiate(context: Context,
                        @DrawableRes icon: Int? = null,
                        @StringRes title: Int? = null,
                        @StringRes message: Int? = null,
                        @StringRes action: Int? = null,
                        onActionClick: (() -> Unit)? = null): MessageDialog {
            return instantiate(
                    icon = if (icon == null) null else ContextCompat.getDrawable(context, icon),
                    title = if (title == null) null else context.getString(title),
                    message = if (message == null) null else context.getString(message),
                    action = if (action == null) null else context.getString(action),
                    onActionClick = onActionClick)
        }

        @JvmStatic
        fun instantiate(@DrawableRes icon: Drawable? = null,
                        @StringRes title: CharSequence? = null,
                        @StringRes message: CharSequence? = null,
                        @StringRes action: CharSequence? = null,
                        onActionClick: (() -> Unit)? = null): MessageDialog {
            return MessageDialog().apply {
                this.icon = icon
                this.title = title
                this.message = message
                this.action = action
                this.onActionClick = onActionClick
            }
        }
    }
}