package com.zody.ui.base

import android.content.Context
import android.content.DialogInterface
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.core.text.underline
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import com.zody.R
import com.zody.databinding.DialogAlertBinding
import com.zody.ui.business.openSupport

/**
 * Created by vinhnguyen.it.vn on 2018, October 30
 */
class AlertDialog : BaseDialogFragment() {

    private lateinit var binding: DialogAlertBinding

    private var params: AlertParams? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_alert, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.params = params

        binding.btPositive.setOnClickListener {
            params?.positiveButton?.onClickListener?.invoke()
            dismissAllowingStateLoss()
        }
        binding.btNegative.setOnClickListener {
            params?.negativeButton?.onClickListener?.invoke()
            dismissAllowingStateLoss()
        }
        binding.btNeutral.setOnClickListener {
            params?.neutralButton?.onClickListener?.invoke()
            dismissAllowingStateLoss()
        }
    }

    override fun onDismiss(dialog: DialogInterface?) {
        params?.onDismissListener?.onDismiss(dialog)
        super.onDismiss(dialog)
    }

    data class AlertParams(var icon: Drawable? = null,
                           var title: CharSequence? = null,
                           var message: CharSequence? = null,
                           var positiveButton: ButtonParams? = null,
                           var negativeButton: ButtonParams? = null,
                           var neutralButton: ButtonParams? = null,
                           var onDismissListener: DialogInterface.OnDismissListener? = null,
                           var cancelable: Boolean = true)

    data class ButtonParams(val text: CharSequence,
                            val onClickListener: (() -> Unit)?)

    class Builder(val context: Context) {

        private val params = AlertParams()

        fun setIcon(@DrawableRes iconId: Int): Builder {
            params.icon = ContextCompat.getDrawable(context, iconId)
            return this
        }

        fun setIcon(icon: Drawable): Builder {
            params.icon = icon
            return this
        }

        fun setTitle(@StringRes titleId: Int): Builder {
            params.title = context.getString(titleId)
            return this
        }

        fun setTitle(title: CharSequence): Builder {
            params.title = title
            return this
        }

        fun setMessage(@StringRes messageId: Int): Builder {
            params.message = context.getString(messageId)
            return this
        }

        fun setMessage(message: CharSequence): Builder {
            params.message = message
            return this
        }

        fun setPositiveButton(@StringRes textId: Int, onClick: (() -> Unit)?): Builder {
            params.positiveButton = ButtonParams(text = context.getString(textId), onClickListener = onClick)
            return this
        }

        fun setPositiveButton(text: CharSequence, onClick: (() -> Unit)?): Builder {
            params.positiveButton = ButtonParams(text = text, onClickListener = onClick)
            return this
        }

        fun setNegativeButton(@StringRes textId: Int, onClick: (() -> Unit)?): Builder {
            params.negativeButton = ButtonParams(text = context.getString(textId), onClickListener = onClick)
            return this
        }

        fun setNegativeButton(text: CharSequence, onClick: (() -> Unit)?): Builder {
            params.negativeButton = ButtonParams(text = text, onClickListener = onClick)
            return this
        }

        fun setNeutralButton(@StringRes textId: Int, onClick: (() -> Unit)?): Builder {
            params.neutralButton = ButtonParams(text = context.getString(textId), onClickListener = onClick)
            return this
        }

        fun setNeutralButton(text: CharSequence, onClick: (() -> Unit)?): Builder {
            params.neutralButton = ButtonParams(text = text, onClickListener = onClick)
            return this
        }

        fun showSupportAsNeutral(): Builder {
            setNeutralButton(text = SpannableStringBuilder().underline { append(context.getString(R.string.support)) }) {
                context.openSupport()
            }
            return this
        }

        fun setCancelable(cancelable: Boolean): Builder {
            params.cancelable = cancelable
            return this
        }

        fun setOnDismissListener(onDismiss: DialogInterface.OnDismissListener): Builder {
            params.onDismissListener = onDismiss
            return this
        }

        fun show(manager: FragmentManager?, tag: String? = null) {
            val dialog = AlertDialog()
            dialog.params = params
            dialog.show(manager, tag)
        }
    }
}