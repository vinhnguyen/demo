package com.zody.ui.base

import android.app.Dialog
import android.os.Bundle
import android.view.View
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.zody.R


/**
 * Created by vinhnguyen.it.vn on 2018, July 26
 */
abstract class BaseBottomSheetDialogFragment : BottomSheetDialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.setOnShowListener {
            dialog.findViewById<View>(R.id.design_bottom_sheet)?.background = null
        }
        return dialog
    }
}