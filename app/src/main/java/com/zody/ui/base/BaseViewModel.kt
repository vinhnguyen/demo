package com.zody.ui.base

import androidx.lifecycle.ViewModel

/**
 * Created by vinhnguyen.it.vn on 2018, June 27
 */
abstract class BaseViewModel : ViewModel()