package com.zody.ui.base

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil

/**
 * Created by vinhnguyen.it.vn on 2018, July 15
 */
abstract class DatabindingPagedListAdapter<T>(diffCallback: DiffUtil.ItemCallback<T>) : PagedListAdapter<T, DataBindingViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingViewHolder {
        val holder = DataBindingViewHolder(parent, viewType)
        initViewHolder(holder.binding, parent)
        return holder
    }

    override fun onBindViewHolder(holder: DataBindingViewHolder, position: Int) {
        holder.binding.apply {
            onBind(this, getItem(position))
            executePendingBindings()
        }
    }

    open fun initViewHolder(binding: ViewDataBinding, parent: ViewGroup) {

    }

    abstract fun onBind(binding: ViewDataBinding, item: T?)

}