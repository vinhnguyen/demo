package com.zody.ui.expert

import android.view.View
import androidx.fragment.app.FragmentActivity
import com.zody.R
import com.zody.ui.base.MessageDialog
import com.zody.utils.toActivity

/**
 * Created by vinhnguyen.it.vn on 2018, August 15
 */
class OpenLocalExpertOnClickListener : View.OnClickListener {

    override fun onClick(view: View) {
        val context = view.context

        MessageDialog.instantiate(
                context = context,
                icon = R.drawable.ic_local_expert_big,
                title = R.string.local_expert_explain_title,
                message = R.string.local_expert_explain_description
        ).show((view.context.toActivity() as FragmentActivity).supportFragmentManager, null)
    }

    companion object {

        @JvmStatic
        fun create() = OpenLocalExpertOnClickListener()
    }
}