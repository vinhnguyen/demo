package com.zody.ui.expert

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zody.R
import com.zody.databinding.ActivityLocalExpertBinding
import com.zody.ui.base.BaseActivity
import com.zody.ui.post.NotEnoughCoinObserver
import com.zody.ui.post.PostActionViewModel
import com.zody.ui.post.list.PostListAdapter
import com.zody.utils.showMessage
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 19
 */
class LocalExpertActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityLocalExpertBinding

    private val localExpertViewModel: LocalExpertViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(LocalExpertViewModel::class.java)
    }
    private val authorViewModel: AuthorViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(AuthorViewModel::class.java)
    }
    private val postActionViewModel: PostActionViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(PostActionViewModel::class.java)
    }

    override val screenName: String?
        get() = "suggestion local expert"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_local_expert)
        binding.headerBar.btHeaderActionLeft.setOnClickListener { finish() }

        val adapter = PostListAdapter(authorViewModel, postActionViewModel)
        binding.recyclerView.adapter = adapter

        localExpertViewModel.data.observe(this, Observer {
            adapter.submitList(it.pagedList, it.endData)
        })
        authorViewModel.message.observe(this, Observer {
            showMessage(it)
        })
        postActionViewModel.tipResult.observe(this, Observer {
            showMessage(it)
        })
        postActionViewModel.deleteResult.observe(this, Observer {
            showMessage(it)
        })
        postActionViewModel.showPopupNotEnoughCoin.observe(this, NotEnoughCoinObserver(this, supportFragmentManager))

    }
}