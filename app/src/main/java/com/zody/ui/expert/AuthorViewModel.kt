package com.zody.ui.expert

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Transformations
import com.zody.repository.AuthorRepository
import com.zody.repository.Result
import com.zody.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 22
 */
class AuthorViewModel @Inject constructor(private val authorRepository: AuthorRepository) : BaseViewModel() {

    private val result = MediatorLiveData<Result<Unit>>()

    val message: LiveData<String?> = Transformations.map(result) {
        if (it.loading) null else it.message
    }

    fun follow(authorId: String) {
        val followResult = authorRepository.followAuthor(authorId)
        result.addSource(followResult) {
            if (!it.loading) result.removeSource(followResult)
            result.value = it
        }
    }

    fun unFollow(authorId: String) {
        val unFollowResult = authorRepository.unFollowAuthor(authorId)
        result.addSource(unFollowResult) {
            if (!it.loading) result.removeSource(unFollowResult)
            result.value = it
        }
    }
}