package com.zody.ui.expert

import androidx.lifecycle.LiveData
import com.zody.entity.ListItem
import com.zody.entity.Post
import com.zody.repository.Listing
import com.zody.repository.ListingData
import com.zody.repository.PostRepository
import com.zody.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, August 16
 */
class LocalExpertViewModel @Inject constructor(postRepository: PostRepository) : BaseViewModel() {

    val listing: Listing<ListItem<Post>> = postRepository.loadLocalExpert()

    val data: LiveData<ListingData<ListItem<Post>>> = listing.data
}