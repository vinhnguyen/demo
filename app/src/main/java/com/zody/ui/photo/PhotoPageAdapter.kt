package com.zody.ui.photo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.PagerAdapter
import com.zody.R
import com.zody.databinding.ItemPagePhotoBinding
import com.zody.entity.Photo


/**
 * Created by vinhnguyen.it.vn on 2018, August 07
 */
class PhotoPageAdapter(private val photos: List<Photo>?) : PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val binding: ItemPagePhotoBinding = DataBindingUtil.inflate(LayoutInflater.from(container.context), R.layout.item_page_photo, container, false)
        binding.url = photos?.get(position)?.url
        val view = binding.root
        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        container.removeView(obj as View)
    }

    override fun isViewFromObject(view: View, obj: Any): Boolean = view == obj

    override fun getCount(): Int = photos?.size ?: 0
}