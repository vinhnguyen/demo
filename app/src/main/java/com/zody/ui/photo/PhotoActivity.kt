package com.zody.ui.photo

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zody.R
import com.zody.databinding.ActivityPhotoBinding
import com.zody.entity.Photo
import com.zody.entity.Post
import com.zody.ui.base.BaseActivity
import com.zody.ui.post.PostActionViewModel
import com.zody.utils.toArrayList
import java.util.*
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, August 07
 */
class PhotoActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityPhotoBinding

    override val screenName: String?
        get() = "photos"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_photo)
        binding.btHeaderActionLeft.setOnClickListener { finish() }

        val photos = intent.getParcelableArrayListExtra<Photo>(EXTRA_PHOTOS)
        val selectedPosition = intent.getIntExtra(EXTRA_SELECTED, 0)
        binding.viewPager.adapter = PhotoPageAdapter(photos)
        binding.viewPager.currentItem = selectedPosition

        intent.getStringExtra(EXTRA_POST_ID)?.let {
            val postActionViewModel = ViewModelProviders.of(this, viewModelFactory).get(PostActionViewModel::class.java)
            postActionViewModel.view(it)
        }
    }

    companion object {
        private const val EXTRA_PHOTOS = "PhotoActivity.Photos"
        private const val EXTRA_SELECTED = "PhotoActivity.Selected"
        private const val EXTRA_POST_ID = "PhotoActivity.PostId"

        @JvmStatic
        fun viewPostPhoto(context: Context, post: Post, selected: Photo) {
            context.startActivity(Intent(context, PhotoActivity::class.java).apply {
                putParcelableArrayListExtra(EXTRA_PHOTOS, post.photos.toArrayList())
                putExtra(EXTRA_SELECTED, post.photos?.indexOf(selected))
                putExtra(EXTRA_POST_ID, post.id)
            })
        }

        fun start(context: Context, photos: List<Photo>?, selected: Photo) {
            start(context, photos.toArrayList(), selected)
        }

        private fun start(context: Context, photos: ArrayList<Photo>?, selected: Photo) {
            context.startActivity(Intent(context, PhotoActivity::class.java).apply {
                putParcelableArrayListExtra(EXTRA_PHOTOS, photos)
                putExtra(EXTRA_SELECTED, photos?.indexOf(selected))
            })
        }

        fun start(context: Context, photos: List<String>?, selected: String?) {
            context.startActivity(Intent(context, PhotoActivity::class.java).apply {
                putParcelableArrayListExtra(EXTRA_PHOTOS, photos?.map {
                    Photo().apply {
                        id = UUID.randomUUID().toString()
                        url = it
                    }
                }.toArrayList())
                putExtra(EXTRA_SELECTED, photos?.indexOf(selected))
            })
        }
    }
}