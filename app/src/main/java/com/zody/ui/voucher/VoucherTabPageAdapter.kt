package com.zody.ui.voucher

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.zody.ui.evoucher.EVoucherFragment

/**
 * Created by vinhnguyen.it.vn on 2019, January 30
 */
class VoucherTabPageAdapter(fm: FragmentManager?) : FragmentStatePagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        return if (position == 0) VoucherFragment() else EVoucherFragment()
    }

    override fun getCount(): Int {
        return 2
    }


}