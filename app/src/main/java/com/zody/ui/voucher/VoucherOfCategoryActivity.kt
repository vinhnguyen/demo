package com.zody.ui.voucher

import android.content.Context
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.zody.R
import com.zody.databinding.ActivityVoucherOfCategoryBinding
import com.zody.entity.voucher.VoucherCategory
import com.zody.ui.base.BaseActivity
import com.zody.ui.base.Navigation
import com.zody.ui.views.SingleChoiceDialogFragment
import com.zody.utils.px
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2019, January 31
 */
class VoucherOfCategoryActivity : BaseActivity() {

    companion object {

        fun start(context: Context, id: String) {
            context.startActivity(Intent(context, VoucherOfCategoryActivity::class.java).apply {
                putExtra(Navigation.EXTRA_TARGET_VALUE, id)
            })
        }

        val SORT = listOf(
                VoucherOfCategoryViewModel.SORT_COIN_ASC to R.string.voucher_sort_coin_asc,
                VoucherOfCategoryViewModel.SORT_COIN_DESC to R.string.voucher_sort_coin_desc,
                VoucherOfCategoryViewModel.SORT_MOST_EXCHANGE to R.string.voucher_sort_most_exchange,
                VoucherOfCategoryViewModel.SORT_RECENT_UPDATE to R.string.voucher_sort_recent_update
        )
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityVoucherOfCategoryBinding

    private val voucherOfCategoryViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(VoucherOfCategoryViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_voucher_of_category)
        binding.headerBar.btHeaderActionLeft.setOnClickListener { finish() }

        binding.headerBar.title = intent.getStringExtra(Navigation.EXTRA_TARGET_TITLE)

        val adapter = VoucherOfCategoryAdapter()
        binding.recyclerView.adapter = adapter
        binding.recyclerView.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                val position = parent.getChildAdapterPosition(view)
                outRect.left = if (position.rem(2) == 0) 0 else 6.px
                outRect.right = if (position.rem(2) == 0) 6.px else 0
                outRect.top = 17.px
                outRect.bottom = 12.px
            }
        })

        binding.ddSort.root.setOnClickListener {
            SingleChoiceDialogFragment<String>().apply {
                options = SORT.map {
                    SingleChoiceDialogFragment.Option(key = it.first, text = this@VoucherOfCategoryActivity.getString(it.second))
                }
                onItemSelected = { option, _ ->
                    voucherOfCategoryViewModel.sort(option.key)
                }
            }.show(supportFragmentManager, null)
        }
        voucherOfCategoryViewModel.categories.observe(this, Observer { categories ->
            binding.ddCategory.root.setOnClickListener {
                SingleChoiceDialogFragment<VoucherCategory>().apply {
                    options = categories.map {
                        SingleChoiceDialogFragment.Option(key = it, text = it.name
                                ?: "", icon = null)
                    }
                    onItemSelected = { option, _ ->
                        voucherOfCategoryViewModel.selectCategory(option.key)
                    }
                }.show(supportFragmentManager, null)
            }
        })
        voucherOfCategoryViewModel.data.observe(this, Observer {
            adapter.submitList(it.pagedList)
        })
        voucherOfCategoryViewModel.category.observe(this, Observer {
            binding.ddCategory.value = it.name
            binding.headerBar.title = it.name
        })
        voucherOfCategoryViewModel.sort.observe(this, Observer { sort ->
            SORT.find { it.first == sort }?.second?.let {
                binding.ddSort.value = getString(it)
            }
        })
        voucherOfCategoryViewModel.init(id = Navigation.getId(intent))
    }
}