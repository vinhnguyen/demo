package com.zody.ui.voucher

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.zody.R
import com.zody.databinding.FragmentVoucherBinding
import com.zody.databinding.LayoutVoucherBannerBinding
import com.zody.databinding.LayoutVoucherCategoryBinding
import com.zody.entity.voucher.VoucherList
import com.zody.ui.base.BaseFragment
import com.zody.utils.px
import com.zody.utils.showMessage
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2019, January 30
 */
class VoucherFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val voucherViewModel: VoucherViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(VoucherViewModel::class.java)
    }

    private lateinit var binding: FragmentVoucherBinding


    override val screenName: String?
        get() = "voucher"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_voucher, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.swipeRefreshLayout.setOnRefreshListener {
            voucherViewModel.refresh()
        }

        voucherViewModel.voucherData.observe(this, Observer { result ->

            showMessage(result)
            if (!result.loading) {
                binding.swipeRefreshLayout.isRefreshing = false
            }

            binding.container.removeAllViews()

            val inflater = LayoutInflater.from(context)

            result.data?.banners?.let { banners ->
                if (banners.isNotEmpty()) {
                    DataBindingUtil.inflate<LayoutVoucherBannerBinding>(inflater, R.layout.layout_voucher_banner, binding.container, true).apply {
                        recyclerView.addItemDecoration(object : RecyclerView.ItemDecoration() {
                            override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                                outRect.right = 13.px
                            }
                        })
                        recyclerView.adapter = VoucherBannerAdapter().apply {
                            submitList(banners)
                        }
                    }
                }
            }

            result.data?.list?.forEach { category ->
                if (category.vouchers?.isNotEmpty() != true) return@forEach
                DataBindingUtil.inflate<LayoutVoucherCategoryBinding>(inflater, R.layout.layout_voucher_category, binding.container, true).apply {
                    title = category.name
                    showViewMore = category.hasLoadMore == true && category.hasSupport
                    recyclerView.addItemDecoration(object : RecyclerView.ItemDecoration() {
                        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                            outRect.right = 11.px
                        }
                    })
                    recyclerView.adapter = VoucherAdapter().apply {
                        submitList(category.vouchers)
                    }
                    tvViewMore.setOnClickListener {
                        if (category.type == VoucherList.TYPE_CATEGORY) {
                            VoucherOfCategoryActivity.start(requireContext(), category.id)
                        } else {
                            VoucherOfOtherCategoryActivity.start(requireContext(), category.type, category.name)
                        }
                    }
                }
            }

        })
    }
}