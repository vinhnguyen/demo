package com.zody.ui.voucher.detail

import android.os.CountDownTimer
import android.view.View
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.zody.R
import com.zody.entity.voucher.Voucher
import com.zody.utils.decimalFormat
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import java.util.*

/**
 * Created by vinhnguyen.it.vn on 2019, February 17
 */
object BindingInterface {

    @JvmStatic
    @BindingAdapter(value = ["coin", "now", "voucher"], requireAll = true)
    fun bindVoucherAction(view: TextView, coin: Int?, now: Date?, voucher: Voucher?) {
        (view.tag as? CountDownTimer)?.cancel()
        if (coin == null || voucher?.coin == null || now == null) return
        when {
            coin < voucher.coin -> {
                view.isEnabled = false
                view.text = view.context.getString(R.string.voucher_detail_need_more_zcoin_to_exchange_voucher, (voucher.coin - coin).decimalFormat)
            }
            voucher.canExchangeAfterDate == null || voucher.canExchangeAfterDate <= now -> {
                view.isEnabled = true
                view.text = view.context.getString(R.string.voucher_exchange)
            }
            else -> {
                view.isEnabled = false

                val period = voucher.canExchangeAfterDate.time - now.time

                if (period > 24 * 60 * 60 * 1000) {
                    val text = DateTimeFormat.forPattern("HH:mm, dd/MM").print(DateTime(voucher.canExchangeAfterDate))
                    view.text = view.context.getString(R.string.voucher_detail_need_to_wait_to_exchange_voucher, text)
                } else {
                    val timber = object : CountDownTimer(period, 1000) {
                        override fun onFinish() {
                            view.isEnabled = true
                            view.text = view.context.getString(R.string.voucher_exchange)
                        }

                        override fun onTick(millisUntilFinished: Long) {
                            val seconds = millisUntilFinished / 1000
                            val minutes = seconds / 60
                            val text = String.format("%02d:%02d:%02d", minutes / 60, minutes % 60, seconds % 60)
                            view.text = view.context.getString(R.string.voucher_detail_count_down_to_exchange_voucher, text)
                        }
                    }
                    view.tag = timber
                    timber.start()
                    view.addOnAttachStateChangeListener(object : View.OnAttachStateChangeListener {
                        override fun onViewDetachedFromWindow(v: View?) {
                            timber.cancel()
                            view.removeOnAttachStateChangeListener(this)
                        }

                        override fun onViewAttachedToWindow(v: View?) {

                        }
                    })
                }
            }
        }
    }
}