package com.zody.ui.voucher

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Transformations
import com.zody.entity.voucher.VoucherData
import com.zody.livedata.CityAndLocationForApi
import com.zody.repository.Result
import com.zody.repository.VoucherRepository
import com.zody.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, August 09
 */
class VoucherViewModel @Inject constructor(cityAndLocationForApi: CityAndLocationForApi,
                                           voucherRepository: VoucherRepository) : BaseViewModel() {

    private val trigger = MediatorLiveData<CityAndLocationForApi.Data>().apply {
        addSource(cityAndLocationForApi) { value = it }
    }

    val voucherData: LiveData<Result<VoucherData>> = Transformations.switchMap(trigger) {
        voucherRepository.loadVoucherData(it.city, it.latitude, it.longitude)
    }

    fun refresh() {
        trigger.value = trigger.value
    }
}