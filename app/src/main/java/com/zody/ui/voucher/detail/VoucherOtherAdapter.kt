package com.zody.ui.voucher.detail

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.zody.BR
import com.zody.R
import com.zody.entity.voucher.Voucher
import com.zody.ui.base.DatabindingListAdapter

/**
 * Created by vinhnguyen.it.vn on 2019, February 15
 */
class VoucherOtherAdapter : DatabindingListAdapter<Voucher>(DIFF_CALLBACK) {

    companion object {

        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Voucher>() {
            override fun areItemsTheSame(oldItem: Voucher, newItem: Voucher): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Voucher, newItem: Voucher): Boolean {
                return oldItem.name == newItem.name
            }
        }
    }

    var selectedVoucher: Voucher? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    var onItemSelected: ((Voucher) -> Unit)? = null

    override fun onBind(binding: ViewDataBinding, item: Voucher?) {
        binding.setVariable(BR.voucher, item)
        binding.root.isSelected = item?.id == selectedVoucher?.id
        binding.root.setOnClickListener { view ->
            item?.let {
                onItemSelected?.invoke(item)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_list_voucher_other
    }
}