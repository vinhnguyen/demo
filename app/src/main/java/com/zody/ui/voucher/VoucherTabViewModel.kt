package com.zody.ui.voucher

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.zody.entity.Profile
import com.zody.repository.ProfileRepository
import com.zody.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, August 09
 */
class VoucherTabViewModel @Inject constructor(profileRepository: ProfileRepository) : BaseViewModel() {

    private val profile: LiveData<Profile> = profileRepository.loadProfileFromDB()

    val coin: LiveData<Int> = Transformations.map(profile) {
        it?.statistic?.coin ?: 0
    }

    val reward: LiveData<Int> = Transformations.map(profile) {
        it?.statistic?.reward ?: 0
    }
}