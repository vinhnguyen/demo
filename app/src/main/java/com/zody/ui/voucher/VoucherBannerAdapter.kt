package com.zody.ui.voucher

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.zody.BR
import com.zody.R
import com.zody.databinding.ItemListVoucherBannerBinding
import com.zody.entity.voucher.VoucherBanner
import com.zody.ui.base.DataBindingViewHolder
import com.zody.ui.base.DatabindingListAdapter
import com.zody.ui.voucher.detail.VoucherDetailActivity

/**
 * Created by vinhnguyen.it.vn on 2019, January 30
 */
class VoucherBannerAdapter : DatabindingListAdapter<VoucherBanner>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingViewHolder {
        val holder = super.onCreateViewHolder(parent, viewType)
        if (holder.binding is ItemListVoucherBannerBinding) {
            holder.itemView.setOnClickListener { view ->
                holder.binding.banner?.let { banner ->
                    if (banner.linkValue != null) {
                        when (banner.linkTo) {
                            VoucherBanner.LINK_TO_VOUCHER -> VoucherDetailActivity.start(view.context, banner.linkValue)
                            VoucherBanner.LINK_TO_CATEGORY -> {
                                VoucherOfCategoryActivity.start(view.context, banner.linkValue)
                            }
                        }
                    }
                }
            }
        }
        return holder
    }

    override fun onBind(binding: ViewDataBinding, item: VoucherBanner?) {
        binding.setVariable(BR.banner, item)
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_list_voucher_banner
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<VoucherBanner>() {
            override fun areItemsTheSame(oldItem: VoucherBanner, newItem: VoucherBanner): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: VoucherBanner, newItem: VoucherBanner): Boolean {
                return oldItem.cover == newItem.cover
            }

        }
    }

}