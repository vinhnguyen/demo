package com.zody.ui.voucher

import android.content.Context
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.zody.R
import com.zody.databinding.ActivityVoucherOfOtherCategoryBinding
import com.zody.ui.base.BaseActivity
import com.zody.ui.base.Navigation
import com.zody.utils.px
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2019, January 31
 */
class VoucherOfOtherCategoryActivity : BaseActivity() {

    companion object {

        fun start(context: Context, type: String?, name: String?) {
            context.startActivity(Intent(context, VoucherOfOtherCategoryActivity::class.java).apply {
                putExtra(Navigation.EXTRA_TARGET_VALUE, type)
                putExtra(Navigation.EXTRA_TARGET_TITLE, name)
            })
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityVoucherOfOtherCategoryBinding

    private val voucherOfCategoryViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(VoucherOfOtherCategoryViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_voucher_of_other_category)
        binding.headerBar.btHeaderActionLeft.setOnClickListener { finish() }

        binding.headerBar.title = intent.getStringExtra(Navigation.EXTRA_TARGET_TITLE)

        val adapter = VoucherOfCategoryAdapter()
        binding.recyclerView.adapter = adapter
        binding.recyclerView.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                val position = parent.getChildAdapterPosition(view)
                outRect.left = if (position.rem(2) == 0) 0 else 6.px
                outRect.right = if (position.rem(2) == 0) 6.px else 0
                outRect.top = 17.px
                outRect.bottom = 12.px
            }
        })
        voucherOfCategoryViewModel.data.observe(this, Observer {
            adapter.submitList(it.pagedList)
        })

        voucherOfCategoryViewModel.init(Navigation.getId(intent))
    }
}