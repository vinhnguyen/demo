package com.zody.ui.voucher

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.zody.BR
import com.zody.R
import com.zody.entity.voucher.Voucher
import com.zody.ui.base.DatabindingPagedListAdapter
import com.zody.ui.voucher.detail.VoucherDetailActivity

/**
 * Created by vinhnguyen.it.vn on 2018, August 09
 */
class VoucherOfCategoryAdapter : DatabindingPagedListAdapter<Voucher>(DIFF_CALLBACK) {

    override fun onBind(binding: ViewDataBinding, item: Voucher?) {
        item?.let {
            binding.setVariable(BR.voucher, it)
            binding.root.setOnClickListener { view ->
                VoucherDetailActivity.start(view.context, item.id)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_list_voucher_of_category
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Voucher>() {
            override fun areItemsTheSame(oldItem: Voucher, newItem: Voucher): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Voucher, newItem: Voucher): Boolean {
                return oldItem == newItem
            }

        }
    }
}