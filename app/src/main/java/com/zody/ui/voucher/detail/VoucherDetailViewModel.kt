package com.zody.ui.voucher.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.zody.entity.voucher.Voucher
import com.zody.entity.voucher.VoucherDetailData
import com.zody.livedata.SingleLiveEvent
import com.zody.repository.ProfileRepository
import com.zody.repository.Result
import com.zody.repository.VoucherRepository
import com.zody.ui.base.BaseViewModel
import com.zody.utils.TimeManager
import com.zody.utils.valueIfDifferent
import java.util.*
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, August 12
 */
class VoucherDetailViewModel @Inject constructor(private val voucherRepository: VoucherRepository,
                                                 profileRepository: ProfileRepository,
                                                 private val timeManager: TimeManager) : BaseViewModel() {

    val profile = profileRepository.loadProfileFromDB()

    val now = MutableLiveData<Date>().apply {
        value = timeManager.now
    }

    private val voucherId = MutableLiveData<String>()

    private val voucherDetailResult: LiveData<Result<VoucherDetailData>> = Transformations.switchMap(voucherId) {
        voucherRepository.loadVoucherDetail(it)
    }

    val voucherDetailData = MediatorLiveData<VoucherDetailData>().apply {
        addSource(voucherDetailResult) {
            it?.data?.let { data ->
                value = data
            }
        }
    }

    val voucher = MediatorLiveData<Voucher>().apply {
        addSource(voucherDetailData) { data ->
            value = data.vouchers?.find { voucher -> voucher.id == voucherId.value }
        }
    }

    private val exchangeVoucher = SingleLiveEvent<Voucher>()

    val exchangeResult: LiveData<Result<Unit>> = Transformations.switchMap(exchangeVoucher) { voucher ->
        voucherRepository.exchangeVoucher(voucher)
    }

    fun init(voucherId: String) {
        this.voucherId.valueIfDifferent = voucherId
    }

    fun select(voucher: Voucher) {
        if (voucher != this.voucher.value) {
            this.voucher.value = voucher
            now.value = timeManager.now
        }
    }

    fun exchange() {
        if (voucher.value != null) {
            exchangeVoucher.value = voucher.value
        }
    }
}