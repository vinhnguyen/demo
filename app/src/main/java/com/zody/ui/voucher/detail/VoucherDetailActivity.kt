package com.zody.ui.voucher.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.core.view.isGone
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zody.R
import com.zody.databinding.ActivityVoucherDetailBinding
import com.zody.ui.base.BaseActivity
import com.zody.ui.base.MessageDialog
import com.zody.ui.base.Navigation
import com.zody.ui.evoucher.EVoucherActivity
import com.zody.utils.decimalFormat
import com.zody.utils.showMessage
import javax.inject.Inject

class VoucherDetailActivity : BaseActivity() {

    companion object {
        fun start(context: Context, id: String?) {
            context.startActivity(Intent(context, VoucherDetailActivity::class.java).apply {
                putExtra(Navigation.EXTRA_TARGET_VALUE, id)
            })
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityVoucherDetailBinding

    override val screenName: String?
        get() = "voucher detail"

    private val voucherDetailViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(VoucherDetailViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val voucherId = Navigation.getId(intent)
        if (voucherId?.isNotEmpty() != true) {
            finish()
            return
        }

        binding = DataBindingUtil.setContentView(this, R.layout.activity_voucher_detail)
        binding.headerBar.btHeaderActionLeft.setOnClickListener { finish() }
        binding.btVoucherExchange.setOnClickListener {
            val voucher = voucherDetailViewModel.voucher.value ?: return@setOnClickListener
            MessageDialog.instantiate(
                    title = voucher.name,
                    message = getString(R.string.voucher_detail_exchange_confirm, voucher.coin?.decimalFormat),
                    action = getString(R.string.voucher_detail_exchange_confirm_action),
                    onActionClick = {
                        voucherDetailViewModel.exchange()
                    })
                    .show(supportFragmentManager, null)
        }

        val adapter = VoucherOtherAdapter()
        adapter.onItemSelected = { voucher ->
            voucherDetailViewModel.select(voucher)
        }
        binding.rvVoucherOther.adapter = adapter
        voucherDetailViewModel.profile.observe(this, Observer { profile ->
            binding.coin = profile?.statistic?.coin ?: 0
        })
        voucherDetailViewModel.now.observe(this, Observer {
            binding.now = it
        })
        voucherDetailViewModel.voucherDetailData.observe(this, Observer {
            binding.name = it.name
            adapter.submitList(it.vouchers)
            binding.groupOther.isGone = it.showListVoucherGroup != true
        })
        voucherDetailViewModel.voucher.observe(this, Observer {
            binding.voucher = it
            adapter.selectedVoucher = it
        })
        voucherDetailViewModel.exchangeResult.observe(this, Observer {
            showMessage(it)
            if (it.success == true) {
                MessageDialog.instantiate(
                        message = getString(R.string.voucher_detail_exchange_success_message),
                        action = getString(R.string.action_aggree),
                        onActionClick = {
                            goEvoucher()
                        })
                        .show(supportFragmentManager, null)
            }
        })

        voucherDetailViewModel.init(voucherId)
    }

    private fun goEvoucher() {
        startActivity(Intent(this, EVoucherActivity::class.java))
        finish()
    }
}