package com.zody.ui.voucher

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.zody.entity.voucher.Voucher
import com.zody.entity.voucher.VoucherCategory
import com.zody.livedata.CityAndLocationForApi
import com.zody.repository.Listing
import com.zody.repository.ListingData
import com.zody.repository.VoucherRepository
import com.zody.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2019, January 31
 */
class VoucherOfCategoryViewModel @Inject constructor(val cityAndLocationForApi: CityAndLocationForApi,
                                                     voucherRepository: VoucherRepository) : BaseViewModel() {

    val categories = voucherRepository.loadVoucherCategory()

    val category = MediatorLiveData<VoucherCategory>()

    val sort = MutableLiveData<String>().apply {
        value = SORT_RECENT_UPDATE
    }

    private val params = MediatorLiveData<ApiParams>().apply {
        addSource(category) { c ->
            removeSource(sort)
            addSource(sort) { s ->
                removeSource(cityAndLocationForApi)
                addSource(cityAndLocationForApi) {
                    value = ApiParams(c.id, s, it.city, it.latitude, it.longitude)
                }
            }

        }
    }

    private val result: LiveData<Listing<Voucher>> = Transformations.map(params) { param ->
        voucherRepository.loadVoucherOfCategory(id = param.id, sort = param.sort,
                city = param.city, latitude = param.latitude, longitude = param.longitude)
    }

    val data: LiveData<ListingData<Voucher>> = Transformations.switchMap(result) { it.data }

    fun init(id: String?) {
        if (id != null) {
            category.addSource(categories) { list ->
                category.removeSource(categories)
                category.value = list.firstOrNull { it.id == id }
            }
        }
    }

    fun selectCategory(category: VoucherCategory) {
        if (category != this.category.value) {
            this.category.value = category
        }
    }

    fun sort(sort: String) {
        if (this.sort.value != sort) {
            this.sort.value = sort
        }
    }

    private class ApiParams(val id: String?,
                            val sort: String?,
                            val city: String?,
                            val latitude: Double?,
                            val longitude: Double?)

    companion object {
        const val SORT_COIN_ASC = "coinAsc"
        const val SORT_COIN_DESC = "coinDesc"
        const val SORT_MOST_EXCHANGE = "mostExchanged"
        const val SORT_RECENT_UPDATE = "recentUpdate"
    }
}