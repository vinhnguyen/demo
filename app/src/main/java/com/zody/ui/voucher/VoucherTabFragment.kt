package com.zody.ui.voucher

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.children
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.tabs.TabLayout
import com.zody.R
import com.zody.databinding.FragmentVoucherTabBinding
import com.zody.ui.base.BaseFragmentWithChildFragment
import com.zody.ui.evoucher.EVoucherViewModel
import com.zody.ui.help.ZcoinCentreActivity
import com.zody.ui.search.SearchActivity
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 11
 */
class VoucherTabFragment : BaseFragmentWithChildFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentVoucherTabBinding
    private val voucherTabViewModel: VoucherTabViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(VoucherTabViewModel::class.java)
    }
    private val evoucherViewModel: EVoucherViewModel by lazy {
        ViewModelProviders.of(requireActivity(), viewModelFactory).get(EVoucherViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_voucher_tab, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btHeaderActionLeft.setOnClickListener {
            SearchActivity.start(requireContext(), SearchActivity.TAB_VOUCHER)
        }
        binding.tvUserCoin.setOnClickListener {
            startActivity(Intent(requireContext(), ZcoinCentreActivity::class.java))
        }

        binding.tabLayout.apply {
            for (tabView in (getChildAt(0) as ViewGroup).children) {
                tabView.apply { alpha = if (isSelected) 1F else 0.5F }
            }
            addOnTabSelectedListener(object : TabLayout.ViewPagerOnTabSelectedListener(binding.viewPager) {
                private fun setAlphaForView(tab: TabLayout.Tab?, alpha: Float) {
                    tab?.let {
                        (it.parent.getChildAt(0) as ViewGroup).getChildAt(it.position).alpha = alpha
                    }
                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {
                    super.onTabUnselected(tab)
                    setAlphaForView(tab, 0.5F)
                }

                override fun onTabSelected(tab: TabLayout.Tab?) {
                    super.onTabSelected(tab)
                    setAlphaForView(tab, 1F)
                }
            })
        }
        binding.viewPager.apply {
            addOnPageChangeListener(object : TabLayout.TabLayoutOnPageChangeListener(binding.tabLayout) {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    if (position == 1) {
                        evoucherViewModel.refresh()
                    }
                }
            })
            adapter = VoucherTabPageAdapter(childFragmentManager)
        }

        voucherTabViewModel.coin.observe(this, Observer {
            binding.coin = it
        })
        voucherTabViewModel.reward.observe(this, Observer {
            binding.tabLayout.getTabAt(1)?.text = if (it > 0)
                getString(R.string.voucher_tab_evoucher_has_badge, it)
            else
                getString(R.string.voucher_tab_evoucher)
        })
    }

    override fun onResume() {
        super.onResume()
        if (binding.viewPager.currentItem == 1) {
            evoucherViewModel.refresh()
        }
    }
}