package com.zody.ui.voucher

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Transformations
import com.zody.entity.voucher.Voucher
import com.zody.livedata.CityAndLocationForApi
import com.zody.repository.ListingData
import com.zody.repository.VoucherRepository
import com.zody.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2019, January 31
 */
class VoucherOfOtherCategoryViewModel @Inject constructor(val cityAndLocationForApi: CityAndLocationForApi,
                                                          voucherRepository: VoucherRepository) : BaseViewModel() {

    private val params = MediatorLiveData<ApiParams>()

    private val result = Transformations.map(params) { param ->
        voucherRepository.loadVoucherOfOtherCategory(type = param.type, city = param.city, latitude = param.latitude, longitude = param.longitude)
    }

    val data: LiveData<ListingData<Voucher>> = Transformations.switchMap(result) { it.data }

    fun init(type: String?) {
        if (type != null) {
            params.removeSource(cityAndLocationForApi)
            params.addSource(cityAndLocationForApi) {
                params.value = ApiParams(type, it.city, it.latitude, it.longitude)
            }
        }
    }

    private class ApiParams(val type: String,
                            val city: String?,
                            val latitude: Double?,
                            val longitude: Double?)
}