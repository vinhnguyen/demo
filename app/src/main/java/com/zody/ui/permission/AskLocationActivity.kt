package com.zody.ui.permission

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.SettingsClient
import com.zody.ui.base.BaseActivity
import com.zody.utils.SPStorage
import javax.inject.Inject


/**
 * Created by vinhnguyen.it.vn on 2018, November 13
 */
class AskLocationActivity : BaseActivity() {

    companion object {
        private const val permission = Manifest.permission.ACCESS_FINE_LOCATION

        private const val REQUEST_CODE_PERMISSION_LOCATION = 1
        private const val REQUEST_CODE_SETTING_LOCATION = 2
        private const val REQUEST_CODE_SETTING_PERMISSION = 3

        private const val EXTRA_NEED_OPEN_PERMISSION_SETTING = "OpenPermissionSettings"

        fun start(context: Context, openSetting: Boolean) {
            context.startActivity(Intent(context, AskLocationActivity::class.java).apply {
                putExtra(EXTRA_NEED_OPEN_PERMISSION_SETTING, openSetting)
            })
        }
    }

    @Inject
    lateinit var spStorage: SPStorage

    override val screenName: String?
        get() = "ask location"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            if (spStorage.openPermissionSetting) {
                if (intent.getBooleanExtra(EXTRA_NEED_OPEN_PERMISSION_SETTING, false)) {
                    openPermissionSetting()
                } else {
                    finish()
                }
            } else {
                ActivityCompat.requestPermissions(this, arrayOf(permission), REQUEST_CODE_PERMISSION_LOCATION)
            }
        } else {
            checkLocationSetting()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_PERMISSION_LOCATION) {
            spStorage.openPermissionSetting = !ActivityCompat.shouldShowRequestPermissionRationale(this, permission)
            if (grantResults.firstOrNull() == PackageManager.PERMISSION_GRANTED) {
                checkLocationSetting()
            } else {
                finish()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_SETTING_PERMISSION) {
            if (ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED) {
                checkLocationSetting()
            } else {
                finish()
            }
        } else if (requestCode == REQUEST_CODE_SETTING_LOCATION) {
            setResult(resultCode)
            finish()
        }
    }

    private fun checkLocationSetting() {
        val locationRequest = LocationRequest().apply {
            interval = 0
            fastestInterval = 0
            smallestDisplacement = 1000F
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
        val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest)
        val client: SettingsClient = LocationServices.getSettingsClient(this)
        client.checkLocationSettings(builder.build())
                .addOnSuccessListener {
                    setResult(Activity.RESULT_OK)
                    finish()
                }
                .addOnFailureListener { exception ->
                    if (exception is ResolvableApiException) {
                        try {
                            exception.startResolutionForResult(this, REQUEST_CODE_SETTING_LOCATION)
                        } catch (e: IntentSender.SendIntentException) {
                            finish()
                        }
                    } else {
                        finish()
                    }
                }
    }

    private fun openPermissionSetting(): Boolean {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", packageName, null)
        intent.data = uri
        if (packageManager.resolveActivity(intent, 0) != null) {
            startActivityForResult(intent, REQUEST_CODE_SETTING_PERMISSION)
            return true
        }
        return false
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(0, 0)
    }
}