package com.zody.ui.permission

import com.zody.livedata.LocationLiveData
import com.zody.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, November 09
 */
class AskPermissionViewModel @Inject constructor(val location: LocationLiveData) : BaseViewModel()