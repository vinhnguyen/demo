package com.zody.ui.login

import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan
import android.text.style.UnderlineSpan
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.text.inSpans
import androidx.core.text.toSpanned
import androidx.core.view.forEachIndexed
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.accountkit.AccountKit
import com.facebook.accountkit.AccountKitLoginResult
import com.facebook.accountkit.ui.AccountKitActivity
import com.facebook.accountkit.ui.AccountKitConfiguration
import com.facebook.accountkit.ui.LoginType
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.zody.BuildConfig
import com.zody.R
import com.zody.databinding.ActivityLoginBinding
import com.zody.ui.base.BaseActivity
import com.zody.ui.main.MainActivity
import com.zody.ui.setting.ChangeHostActivity
import com.zody.utils.px
import com.zody.utils.showMessage
import javax.inject.Inject


/**
 * Created by vinhnguyen.it.vn on 2018, July 10
 */
class LoginActivity : BaseActivity() {

    companion object {
        private const val REQUEST_CODE_ACCOUNT_KIT = 1
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val callbackManager = CallbackManager.Factory.create()

    private lateinit var binding: ActivityLoginBinding
    private lateinit var viewModel: LoginViewModel

    override val screenName: String?
        get() = "login"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        binding.tvLoginTosAndPrivacy.apply {
            text = SpannableStringBuilder(getString(R.string.login_promise))
                    .append(' ')
                    .inSpans(StyleSpan(Typeface.BOLD), UnderlineSpan()) {
                        append(getString(R.string.login_terms_of_use))
                    }.toSpanned()
            if (BuildConfig.DEBUG) {
                setOnLongClickListener {
                    goChangeHost()
                    true
                }
            }
            setOnClickListener {
                CustomTabsIntent.Builder().build().launchUrl(this@LoginActivity, Uri.parse(getString(R.string.term_of_use_url)))
            }
        }
        binding.btLoginFacebook.setOnClickListener { loginWithFacebook() }
        binding.btLoginPhone.setOnClickListener { loginWithPhone() }

        binding.viewPager.apply {
            adapter = OnBoardingPageAdapter()
            pageMargin = 25.px

            addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {

                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    forEachIndexed { _, view ->
                        view.isSelected = view.tag == position.toString()
                    }
                }
            })

            post {
                val position = binding.viewPager.currentItem
                if (position in 0..(childCount - 1)) {
                    getChildAt(position).isSelected = true
                }
            }
        }

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(LoginViewModel::class.java)
        viewModel.loginLiveData.observe(this, Observer {
            binding.loading = it.loading
            showMessage(it)
        })
        viewModel.nextAction.observe(this, Observer {
            when (it) {
                LoginViewModel.ACTION_GO_MAIN -> goMain()
            }
        })

        LoginManager.getInstance().registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {
                val token = result?.accessToken?.token ?: return
                viewModel.loginWithFacebook(token)
            }

            override fun onCancel() {

            }

            override fun onError(error: FacebookException?) {
            }

        })

        LoginManager.getInstance().logOut()
        AccountKit.logOut()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_ACCOUNT_KIT && resultCode == Activity.RESULT_OK) {
            data?.getParcelableExtra<AccountKitLoginResult>(AccountKitLoginResult.RESULT_KEY)
                    ?.accessToken
                    ?.let { viewModel.loginWithAccountKit(it.token) }
        }
    }

    private fun loginWithFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(this, arrayListOf("email", "user_gender", "user_birthday", "user_friends"))
    }

    private fun loginWithPhone() {
        startActivityForResult(Intent(this, AccountKitActivity::class.java).apply {
            putExtra(AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                    AccountKitConfiguration.AccountKitConfigurationBuilder(LoginType.PHONE, AccountKitActivity.ResponseType.TOKEN)
                            .setDefaultCountryCode("VN")
                            .build())
        }, REQUEST_CODE_ACCOUNT_KIT)
    }

    private fun goMain() {
        startActivity(Intent(this, MainActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        })
    }

    private fun goChangeHost() {
        startActivity(Intent(this, ChangeHostActivity::class.java))
    }
}