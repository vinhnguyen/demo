package com.zody.ui.login

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.PagerAdapter
import com.zody.R
import com.zody.databinding.ItemPageOnBoardingBinding

/**
 * Created by vinhnguyen.it.vn on 2018, September 27
 */
class OnBoardingPageAdapter : PagerAdapter() {

    private val data = arrayOf(
            Triple(R.drawable.ic_on_boarding_voucher, R.string.on_boarding_voucher_title, R.string.on_boarding_voucher_desc),
            Triple(R.drawable.ic_on_boarding_coin, R.string.on_boarding_coin_title, R.string.on_boarding_coin_desc),
            Triple(R.drawable.ic_on_boarding_share, R.string.on_boarding_share_title, R.string.on_boarding_share_desc),
            Triple(R.drawable.ic_on_boarding_explore, R.string.on_boarding_explore_title, R.string.on_boarding_explore_desc)
    )


    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val binding: ItemPageOnBoardingBinding = DataBindingUtil.inflate(LayoutInflater.from(container.context), R.layout.item_page_on_boarding, container, false)
        data[position].apply {
            binding.imageView.setImageResource(first)
            binding.textView1.setText(second)
            binding.textView2.setText(third)
        }
        val view = binding.root
        container.addView(view)
        view.tag = position.toString()
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        container.removeView(obj as View)
    }

    override fun isViewFromObject(view: View, obj: Any): Boolean = view == obj

    override fun getCount(): Int = data.size

}