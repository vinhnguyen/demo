package com.zody.ui.login

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.zody.repository.ConfigurationRepository
import com.zody.repository.ProfileRepository
import com.zody.repository.Result
import com.zody.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 10
 */
class LoginViewModel @Inject constructor(profileRepository: ProfileRepository,
                                         configurationRepository: ConfigurationRepository) : BaseViewModel() {

    companion object {
        const val ACTION_GO_MAIN = 2
    }

    private val facebookTokenLiveData = MutableLiveData<String>()
    private val accountKitTokenLiveData = MutableLiveData<String>()

    private val loginWithFacebookLiveData = Transformations.switchMap(facebookTokenLiveData) { profileRepository.loginWithFacebook(it) }
    private val loginWithAccountKitLiveData = Transformations.switchMap(accountKitTokenLiveData) { profileRepository.loginWithAccountKit(it) }

    val loginLiveData = MediatorLiveData<Result<Unit>>().apply {
        addSource(loginWithAccountKitLiveData) {
            value = it
        }
        addSource(loginWithFacebookLiveData) {
            value = it
        }
        observeForever {
            if (it.success == true) {
                configurationRepository.getConfig()
            }
        }
    }

    val nextAction = MediatorLiveData<Int>().apply {
        addSource(loginLiveData) { result ->
            if (result.success == true) {
                value = ACTION_GO_MAIN
            }
        }
    }

    fun loginWithFacebook(token: String) {
        facebookTokenLiveData.value = token
    }

    fun loginWithAccountKit(token: String) {
        accountKitTokenLiveData.value = token
    }
}