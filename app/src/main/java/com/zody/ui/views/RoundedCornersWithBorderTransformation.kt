package com.zody.ui.views

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import com.bumptech.glide.load.Key
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation
import com.bumptech.glide.load.resource.bitmap.TransformationUtils
import com.bumptech.glide.util.Preconditions
import com.bumptech.glide.util.Util
import java.nio.ByteBuffer
import java.security.MessageDigest

class RoundedCornersWithBorderTransformation(private val roundingRadius: Int,
                                             private val borderColor: Int?,
                                             private val borderWidth: Float?) : BitmapTransformation() {

    init {
        Preconditions.checkArgument(roundingRadius > 0, "roundingRadius must be greater than 0.")
    }

    override fun transform(pool: BitmapPool, toTransform: Bitmap, outWidth: Int, outHeight: Int): Bitmap {
        val bitmap = TransformationUtils.roundedCorners(pool, toTransform, roundingRadius)
        if (borderColor == null || borderWidth == null) return bitmap
        val width = bitmap.width + (borderWidth * 2).toInt()
        val height = bitmap.height + (borderWidth * 2).toInt()
        val bitmapWithBorder = Bitmap.createBitmap(width, height, bitmap.config)
        val canvas = Canvas(bitmapWithBorder)
        canvas.drawBitmap(bitmap, borderWidth, borderWidth, null)
        val paint = Paint()
        paint.color = borderColor
        paint.style = Paint.Style.STROKE
        paint.strokeWidth = borderWidth
        paint.isAntiAlias = true

        val rectF = RectF(0F, 0F, canvas.width.toFloat(), canvas.height.toFloat())
        canvas.drawRoundRect(rectF, roundingRadius.toFloat(), roundingRadius.toFloat(), paint)

        // Free the native object associated with this bitmap.
        bitmap.recycle()
        return bitmapWithBorder
    }


    override fun hashCode(): Int {
        return Util.hashCode(ID.hashCode(),
                Util.hashCode(roundingRadius,
                        Util.hashCode(borderColor ?: 0,
                                Util.hashCode(borderWidth ?: 0F))))
    }

    override fun updateDiskCacheKey(messageDigest: MessageDigest) {
        messageDigest.update(ID_BYTES)
        messageDigest.update(ByteBuffer.allocate(4).putInt(roundingRadius).array())
        messageDigest.update(ByteBuffer.allocate(4).putInt(borderColor ?: 0))
        messageDigest.update(ByteBuffer.allocate(4).putFloat(borderWidth ?: 0F))
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RoundedCornersWithBorderTransformation

        if (roundingRadius != other.roundingRadius) return false
        if (borderColor != other.borderColor) return false
        if (borderWidth != other.borderWidth) return false

        return true
    }


    companion object {
        private const val VERSION = 1
        private const val ID = "com.zody.ui.views.RoundedCornersWithBorderTransformation.$VERSION"
        private val ID_BYTES = ID.toByteArray(Key.CHARSET)

    }
}