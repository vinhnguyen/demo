package com.zody.ui.views

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import com.bumptech.glide.load.Key
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.util.Util
import java.nio.ByteBuffer
import java.security.MessageDigest


class CircleCropWithBorderTransformation(private val borderColor: Int?, private val borderWidth: Float?) : CircleCrop() {

    override fun transform(pool: BitmapPool, toTransform: Bitmap, outWidth: Int, outHeight: Int): Bitmap {
        val bitmap = super.transform(pool, toTransform, outWidth, outHeight)
        if (borderColor == null || borderWidth == null) return bitmap
        val width = bitmap.width + (borderWidth * 2).toInt()
        val bitmapWithBorder = Bitmap.createBitmap(width, width, bitmap.config)
        val canvas = Canvas(bitmapWithBorder)
        canvas.drawBitmap(bitmap, borderWidth, borderWidth, null)
        val paint = Paint()
        paint.color = borderColor
        paint.style = Paint.Style.STROKE
        paint.strokeWidth = borderWidth
        paint.isAntiAlias = true

        // Draw the circular border around circular bitmap
        canvas.drawCircle(
                canvas.width.toFloat() / 2, // cx
                canvas.width.toFloat() / 2, // cy
                canvas.width / 2 - borderWidth / 2, // Radius
                paint // Paint
        )

        // Free the native object associated with this bitmap.
        bitmap.recycle()
        return bitmapWithBorder
    }


    override fun hashCode(): Int {
        return Util.hashCode(ID.hashCode(),
                Util.hashCode(borderColor ?: 0,
                        Util.hashCode(borderWidth ?: 0F)))
    }

    override fun updateDiskCacheKey(messageDigest: MessageDigest) {
        messageDigest.update(ID_BYTES)
        messageDigest.update(ByteBuffer.allocate(4).putInt(borderColor ?: 0))
        messageDigest.update(ByteBuffer.allocate(4).putFloat(borderWidth ?: 0F))
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        if (!super.equals(other)) return false

        other as CircleCropWithBorderTransformation

        if (borderColor != other.borderColor) return false
        if (borderWidth != other.borderWidth) return false

        return true
    }

    companion object {
        private const val VERSION = 1
        private const val ID = "com.zody.ui.views.CircleCropWithBorderTransformation.$VERSION"
        private val ID_BYTES = ID.toByteArray(Key.CHARSET)

    }
}