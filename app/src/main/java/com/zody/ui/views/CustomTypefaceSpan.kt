package com.zody.ui.views

import android.graphics.Typeface
import android.text.TextPaint
import android.text.style.MetricAffectingSpan

/**
 * Created by vinhnguyen.it.vn on 2018, August 13
 */
class CustomTypefaceSpan(private val typeface: Typeface?) : MetricAffectingSpan() {
    override fun updateMeasureState(textPaint: TextPaint?) {
        update(textPaint)
    }

    override fun updateDrawState(textPaint: TextPaint?) {
        update(textPaint)
    }

    private fun update(textPaint: TextPaint?) {
        textPaint?.let {
            val old = it.typeface
            val oldStyle = old?.style ?: 0
            it.typeface = Typeface.create(typeface, oldStyle)
        }
    }
}