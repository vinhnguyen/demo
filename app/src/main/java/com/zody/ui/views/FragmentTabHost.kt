package com.zody.ui.views

import android.content.Context
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.util.AttributeSet
import android.widget.FrameLayout
import androidx.databinding.ObservableField
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.transaction

/**
 * Created by vinhnguyen.it.vn on 2018, July 11
 */
class FragmentTabHost : FrameLayout {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private val tabs = mutableListOf<TabInfo>()

    private lateinit var fragmentManager: FragmentManager
    private var lastTab: TabInfo? = null
    private var attached: Boolean = false

    var onTabChangeListener: ((String) -> Unit)? = null
    val currentTabTagObserver = ObservableField<String>()
    var currentTabTag: String? = null
        set(value) {
            currentTabTagObserver.set(value)
            field = value
        }

    internal data class TabInfo(val tag: String, val clazz: Class<*>, val args: Bundle?) {
        var fragment: Fragment? = null
    }

    internal class SavedState : BaseSavedState {
        var curTab: String? = null

        constructor(superState: Parcelable?) : super(superState)

        constructor(source: Parcel?) : super(source) {
            curTab = source?.readString()
        }

        override fun writeToParcel(out: Parcel?, flags: Int) {
            super.writeToParcel(out, flags)
            out?.writeString(curTab);
        }

        override fun toString(): String {
            return "FragmentTabHost.SavedState{${Integer.toHexString(System.identityHashCode(this))}  curTab=$curTab}"
        }

        companion object CREATOR : Parcelable.Creator<SavedState> {
            override fun createFromParcel(parcel: Parcel): SavedState {
                return SavedState(parcel)
            }

            override fun newArray(size: Int): Array<SavedState?> {
                return arrayOfNulls(size)
            }
        }
    }

    fun setup(fragmentManager: FragmentManager) {
        this.fragmentManager = fragmentManager
    }

    fun addTab(clazz: Class<out Fragment>, tag: String = clazz.name, args: Bundle? = null) {
        val tabInfo = TabInfo(tag, clazz, args)

        if (attached) {
            // If we are already attached to the window, then check to make
            // sure this tab's fragment is inactive if it exists.  This shouldn't
            // normally happen.
            tabInfo.fragment = fragmentManager.findFragmentByTag(tag)
            tabInfo.fragment?.let {
                if (!it.isDetached) {
                    fragmentManager.transaction {
                        this.detach(it)
                    }
                }
            }
        }
        tabs.add(tabs.size, tabInfo)
    }

    fun changeTab(tag: String) {
        if (tag != currentTabTag) {
            if (attached) {
                doTabChange(tag, null)?.commitAllowingStateLoss()
            }
            onTabChangeListener?.invoke(tag)
            currentTabTag = tag
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        if (tabs.size == 0) return
        currentTabTag = currentTabTag ?: tabs[0].tag

        var ft: FragmentTransaction? = null
        // Go through all tabs and make sure their fragments match
        // the correct state.
        for (tab in tabs) {
            tab.fragment = fragmentManager.findFragmentByTag(tab.tag)
            tab.fragment?.let {
                if (!it.isDetached) {
                    if (tab.tag == currentTabTag) {
                        // The fragment for this tab is already there and
                        // active, and it is what we really want to have
                        // as the current tab.  Nothing to do.
                        lastTab = tab
                    } else {
                        // This fragment was restored in the active state,
                        // but is not the current tab.  Deactivate it.
                        if (ft == null) ft = fragmentManager.beginTransaction()
                        ft?.detach(it)
                    }
                }
            }
        }

        // We are now ready to go.  Make sure we are switched to the
        // correct tab.
        attached = true
        ft = doTabChange(currentTabTag, ft)
        ft?.commitAllowingStateLoss()
        fragmentManager.executePendingTransactions()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        attached = false
    }

    override fun onSaveInstanceState(): Parcelable {
        val superState = super.onSaveInstanceState()
        val ss = SavedState(superState)
        ss.curTab = currentTabTag
        return ss
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        if (state !is SavedState) {
            super.onRestoreInstanceState(state)
            return
        }
        super.onRestoreInstanceState(state.superState)
        currentTabTag = state.curTab
    }

    private fun doTabChange(tag: String?, ft: FragmentTransaction?): FragmentTransaction? {
        val tab = getTagInfoByTag(tag)
        if (lastTab != tab) {
            val fragmentTransaction = ft ?: fragmentManager.beginTransaction()

            lastTab?.let { t ->
                t.fragment?.let { fragmentTransaction.detach(it) }
            }
            tab?.let {
                if (it.fragment != null) {
                    fragmentTransaction.attach(it.fragment!!)
                } else {
                    val fragment = Fragment.instantiate(context, it.clazz.name, it.args)
                    it.fragment = fragment
                    fragmentTransaction.add(id, fragment, it.tag)
                }
            }

            lastTab = tab
            return fragmentTransaction
        }
        return ft
    }

    private fun getTagInfoByTag(tag: String?): TabInfo? {
        return tabs.find { it.tag == tag }
    }
}