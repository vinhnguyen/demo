package com.zody.ui.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.zody.R
import com.zody.databinding.DialogBottomChoiceDateBinding
import com.zody.ui.base.BaseBottomSheetDialogFragment
import org.joda.time.DateTime
import org.joda.time.LocalDate
import java.util.*

open class DateChoiceDialogFragment : BaseBottomSheetDialogFragment() {
    private lateinit var binding: DialogBottomChoiceDateBinding

    open var onDateSelected: ((Date) -> Unit)? = null
    open lateinit var localDate: LocalDate

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_bottom_choice_date, null, false)
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btAction.setOnClickListener {
            binding.datePicker.apply {
                val date = LocalDate(this.year, this.month + 1, this.dayOfMonth)
                onDateSelected?.invoke(date.toDate())
            }
            dismissAllowingStateLoss()
        }
        binding.btDialogClose.setOnClickListener {
            dismissAllowingStateLoss()
        }
        binding.datePicker.updateDate(localDate.year, localDate.monthOfYear - 1, localDate.dayOfMonth)
        binding.datePicker.maxDate = DateTime.now().millis
    }

    companion object {
        fun newInstance(date: Date?): DateChoiceDialogFragment {
            return DateChoiceDialogFragment().apply {
                this.localDate = if (date != null) LocalDate(date) else LocalDate(1991, 1, 1)
            }
        }
    }
}