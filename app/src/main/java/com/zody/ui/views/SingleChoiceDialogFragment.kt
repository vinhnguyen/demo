package com.zody.ui.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.widget.TextViewCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.zody.R
import com.zody.databinding.DialogBottomSelectListBinding
import com.zody.ui.base.BaseBottomSheetDialogFragment


/**
 * Created by vinhnguyen.it.vn on 2018, July 26
 */
open class SingleChoiceDialogFragment<T> : BaseBottomSheetDialogFragment() {

    private lateinit var binding: DialogBottomSelectListBinding

    open var onItemSelected: ((Option<T>, Int) -> Unit)? = null
    open lateinit var options: List<Option<T>>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_bottom_select_list, null, false)
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerView.apply {
            this.adapter = Adapter()
            this.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL).apply {
                ContextCompat.getDrawable(context, R.drawable.divider_dialog_single_choice)?.let { setDrawable(it) }
            })
        }
        binding.btDialogClose.setOnClickListener {
            dismissAllowingStateLoss()
        }
    }

    inner class Adapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_list_bottom_sheet_select, parent, false)
            val holder = object : RecyclerView.ViewHolder(view) {}
            view.setOnClickListener {
                val position = holder.adapterPosition
                onItemSelected?.invoke(options[position], position)
                dismissAllowingStateLoss()
            }
            return holder
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            (holder.itemView as TextView).apply {
                text = options[position].text
                TextViewCompat.setCompoundDrawablesRelativeWithIntrinsicBounds(this,
                        options[position].icon ?: 0, 0, 0, 0)
            }
        }

        override fun getItemCount(): Int {
            return options.size
        }

    }

    data class Option<T>(val key: T, val text: String, val icon: Int? = null)

    companion object {

        fun <T> newInstance(options: List<Option<T>>): SingleChoiceDialogFragment<T> {
            return SingleChoiceDialogFragment<T>().apply {
                this.options = options
            }
        }
    }
}