package com.zody.ui.views

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.view.ViewCompat
import androidx.core.view.forEach
import androidx.core.view.forEachIndexed
import androidx.viewpager.widget.ViewPager
import com.zody.utils.px

/**
 * Created by vinhnguyen.it.vn on 2018, November 26
 */
class PageIndicator : LinearLayoutCompat {

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        val a = context.obtainStyledAttributes(attrs, intArrayOf(android.R.attr.drawable, android.R.attr.spacing))
        drawable = a.getDrawable(0) ?: GradientDrawable().apply {
            shape = GradientDrawable.OVAL
            setColor(Color.WHITE)
            setSize(6.px, 6.px)
        }
        spacing = a.getInt(1, 3.px)
        a.recycle()
    }

    private val drawable: Drawable
    private val spacing: Int

    private fun setupWithViewPager(viewPager: ViewPager) {
        initView(viewPager)
        viewPager.addOnAdapterChangeListener { _, _, _ ->
            initView(viewPager)
        }
        viewPager.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                initSelectedPage(position)
            }
        })
    }

    private fun initView(viewPager: ViewPager) {
        removeAllViews()

        val pageCount = viewPager.adapter?.count ?: 0
        for (i in 0 until pageCount) {
            val view = View(context)

            ViewCompat.setBackground(view, drawable.constantState?.newDrawable())
            val layoutParams = LayoutParams(drawable.intrinsicWidth, drawable.intrinsicHeight)
            layoutParams.setMargins(0, 0, spacing, 0)
            addView(view, layoutParams)
        }

        initSelectedPage(viewPager.currentItem)
    }

    private fun initSelectedPage(position: Int) {
        forEachIndexed { index, view ->
            view.isSelected = (index == position)
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        (parent as? ViewGroup)?.forEach {
            if (it is ViewPager) {
                setupWithViewPager(it)
            }
        }
    }

}