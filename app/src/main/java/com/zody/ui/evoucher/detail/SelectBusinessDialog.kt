package com.zody.ui.evoucher.detail

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.bold
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DividerItemDecoration
import com.zody.R
import com.zody.databinding.DialogSelectBusinessBinding
import com.zody.entity.BusinessCompat
import com.zody.entity.EVoucher
import com.zody.ui.base.BaseDialogFragment
import com.zody.ui.base.MessageDialog

/**
 * Created by vinhnguyen.it.vn on 2019, February 26
 */
class SelectBusinessDialog : BaseDialogFragment() {

    private lateinit var binding: DialogSelectBusinessBinding

    lateinit var evoucher: EVoucher
    var onItemSelected: ((BusinessCompat) -> Unit)? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_select_business, container, false)
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.headerBar.btHeaderActionLeft.setOnClickListener {
            dismissAllowingStateLoss()
        }

        val adapter = SelectBusinessAdapter().apply {
            submitList(evoucher.applyFor)
        }
        binding.recyclerView.addItemDecoration(DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL))
        binding.recyclerView.adapter = adapter

        binding.btAction.setOnClickListener {
            adapter.selected?.let { business ->
                confirmUse(business)
            }
        }
    }

    private fun confirmUse(business: BusinessCompat) {
        MessageDialog.instantiate(
                title = evoucher.title,
                message = SpannableStringBuilder()
                        .append(getString(R.string.evoucher_confirm_message_with_business_1))
                        .append(" ")
                        .bold {
                            append(business.name)
                        }
                        .append(" ")
                        .append(getString(R.string.evoucher_confirm_message_with_business_2)),
                action = getString(R.string.evoucher_confirm_negative),
                onActionClick = {
                    onItemSelected?.invoke(business)
                }).show(fragmentManager, null)
    }
}