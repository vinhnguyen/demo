package com.zody.ui.evoucher.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.zody.entity.EVoucher
import com.zody.livedata.SingleLiveEvent
import com.zody.repository.ProfileRepository
import com.zody.repository.Result
import com.zody.ui.base.BaseViewModel
import com.zody.utils.valueIfDifferent
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, August 15
 */
class EVoucherDetailViewModel @Inject constructor(profileRepository: ProfileRepository) : BaseViewModel() {

    private val evoucherId = MutableLiveData<String>()

    private val useEvoucherAtBusiness = SingleLiveEvent<String>()

    val evoucher: LiveData<Result<EVoucher>> = Transformations.switchMap(evoucherId) {
        profileRepository.loadEvoucherDetail(it)
    }

    val useEvoucherResult: LiveData<Result<Unit>> = Transformations.switchMap(useEvoucherAtBusiness) {
        profileRepository.useEvoucher(evoucherId.value!!, it)
    }

    fun init(evoucherId: String) {
        this.evoucherId.valueIfDifferent = evoucherId
    }

    fun use(businessId: String? = null) {
        useEvoucherAtBusiness.value = businessId
    }
}