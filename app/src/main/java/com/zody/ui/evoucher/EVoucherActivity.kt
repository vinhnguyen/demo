package com.zody.ui.evoucher

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.zody.R
import com.zody.databinding.ActivityEvoucherBinding
import com.zody.ui.base.BaseFragmentActivity
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, August 15
 */
class EVoucherActivity : BaseFragmentActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityEvoucherBinding

    override val screenName: String?
        get() = "evoucher"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_evoucher)
        binding.headerBar.btHeaderActionLeft.setOnClickListener { finish() }
    }
}