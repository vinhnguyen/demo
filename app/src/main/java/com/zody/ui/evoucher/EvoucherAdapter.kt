package com.zody.ui.evoucher

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.paging.PagedList
import androidx.recyclerview.widget.DiffUtil
import com.zody.BR
import com.zody.R
import com.zody.databinding.ItemListEvoucherBinding
import com.zody.entity.EVoucher
import com.zody.ui.base.DataBindingViewHolder
import com.zody.ui.base.DatabindingPagedListAdapter
import java.util.*

/**
 * Created by vinhnguyen.it.vn on 2018, August 15
 */
class EvoucherAdapter : DatabindingPagedListAdapter<EVoucher>(DIFF_CALLBACK) {

    private var empty = false

    var now: Date? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    var onItemSelected: ((EVoucher) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingViewHolder {
        val holder = super.onCreateViewHolder(parent, viewType)
        if (holder.binding is ItemListEvoucherBinding) {
            holder.binding.root.setOnClickListener {
                holder.binding.evoucher?.let { evoucher ->
                    onItemSelected?.invoke(evoucher)
                }
            }
        }
        return holder
    }

    override fun onBind(binding: ViewDataBinding, item: EVoucher?) {
        binding.setVariable(BR.now, now)
        binding.setVariable(BR.evoucher, item)
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            empty -> R.layout.item_list_evoucher_empty
            getItem(position) != null -> R.layout.item_list_evoucher
            else -> R.layout.item_list_evoucher_place_holder
        }
    }

    override fun getItem(position: Int): EVoucher? {
        return when {
            empty -> null
            else -> super.getItem(position)
        }
    }

    override fun getItemCount(): Int {
        return when {
            empty -> 1
            else -> super.getItemCount()
        }
    }

    override fun submitList(pagedList: PagedList<EVoucher>?) {
        super.submitList(pagedList)
        val oldEmpty = empty
        empty = pagedList?.isEmpty() == true
        if (oldEmpty != empty) {
            notifyDataSetChanged()
        }
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<EVoucher>() {
            override fun areItemsTheSame(oldItem: EVoucher, newItem: EVoucher): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: EVoucher, newItem: EVoucher): Boolean {
                return oldItem == newItem
            }

        }
    }
}