package com.zody.ui.evoucher.detail

import android.app.Activity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zody.R
import com.zody.databinding.ActivityEvoucherDetailBinding
import com.zody.entity.EVoucher
import com.zody.ui.base.BaseActivity
import com.zody.ui.base.MessageDialog
import com.zody.ui.base.Navigation
import com.zody.utils.showMessage
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, August 15
 */
class EVoucherDetailActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityEvoucherDetailBinding

    private val evoucherDetailViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(EVoucherDetailViewModel::class.java)
    }

    override val screenName: String?
        get() = "evoucher detail"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val id = Navigation.getId(intent)
        if (id?.isNotEmpty() != true) {
            finish()
            return
        }

        binding = DataBindingUtil.setContentView(this, R.layout.activity_evoucher_detail)
        binding.headerBar.btHeaderActionLeft.setOnClickListener { finish() }

        evoucherDetailViewModel.evoucher.observe(this, Observer { result ->
            showMessage(result)
            result.data?.let { evoucher ->
                binding.evoucher = evoucher
                binding.btEvoucherAction.setOnClickListener {
                    if (evoucher.wayToRedeemReward == EVoucher.WAY_TO_REDEEM_SELF_REMOVE) {
                        confirmRemove(evoucher)
                    } else if (evoucher.wayToRedeemReward == EVoucher.WAY_TO_REDEEM_STAFF_CONFIRM) {
                        if (evoucher.applyFor?.isNotEmpty() == true) {
                            selectBusiness(evoucher)
                        } else {
                            confirmUse(evoucher)
                        }
                    }
                }
            }
        })
        evoucherDetailViewModel.useEvoucherResult.observe(this, Observer {
            showMessage(it)
            if (it.success == true) {
                setResult(Activity.RESULT_OK)
                finish()
            }
        })
        evoucherDetailViewModel.init(id)
    }

    private fun confirmRemove(evoucher: EVoucher) {
        MessageDialog.instantiate(
                title = evoucher.title,
                message = getString(R.string.evoucher_remove_mesage),
                action = getString(R.string.evoucher_remove_negative),
                onActionClick = {
                    evoucherDetailViewModel.use()
                }).show(supportFragmentManager, null)
    }

    private fun confirmUse(evoucher: EVoucher) {
        MessageDialog.instantiate(
                title = evoucher.title,
                message = getString(R.string.evoucher_confirm_message),
                action = getString(R.string.evoucher_confirm_negative),
                onActionClick = {
                    evoucherDetailViewModel.use()
                }).show(supportFragmentManager, null)
    }

    private fun selectBusiness(evoucher: EVoucher) {
        SelectBusinessDialog().apply {
            this.evoucher = evoucher
            onItemSelected = { business ->
                evoucherDetailViewModel.use(business.id)
            }
        }.show(supportFragmentManager, null)
    }
}