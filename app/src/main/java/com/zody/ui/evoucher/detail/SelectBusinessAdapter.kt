package com.zody.ui.evoucher.detail

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.zody.BR
import com.zody.R
import com.zody.entity.BusinessCompat
import com.zody.ui.base.DatabindingListAdapter

/**
 * Created by vinhnguyen.it.vn on 2019, February 26
 */
class SelectBusinessAdapter : DatabindingListAdapter<BusinessCompat>(DIFF_CALLBACK) {

    var selected: BusinessCompat? = null

    override fun onBind(binding: ViewDataBinding, item: BusinessCompat?) {
        binding.setVariable(BR.business, item)
        binding.setVariable(BR.selected, item?.id == selected?.id)

        binding.root.setOnClickListener {
            selected = item
            notifyDataSetChanged()
        }
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_list_select_business
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<BusinessCompat>() {
            override fun areItemsTheSame(oldItem: BusinessCompat, newItem: BusinessCompat): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: BusinessCompat, newItem: BusinessCompat): Boolean {
                return oldItem == newItem
            }

        }
    }
}