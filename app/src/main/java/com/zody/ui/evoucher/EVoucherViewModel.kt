package com.zody.ui.evoucher

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.zody.entity.EVoucher
import com.zody.repository.Listing
import com.zody.repository.ListingData
import com.zody.repository.NetworkState
import com.zody.repository.ProfileRepository
import com.zody.ui.base.BaseViewModel
import com.zody.utils.TimeManager
import java.util.*
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, August 15
 */
class EVoucherViewModel @Inject constructor(profileRepository: ProfileRepository,
                                            timeManager: TimeManager) : BaseViewModel() {

    val now = MutableLiveData<Date>().apply {
        value = timeManager.now
    }

    val status = MutableLiveData<String>().apply {
        value = EVoucher.STATUS_UNUSED
    }

    val sort = MediatorLiveData<String>().apply {
        value = SORT_NEWEST
        addSource(status) {
            if (it != EVoucher.STATUS_UNUSED && value != SORT_NEWEST) {
                value = SORT_NEWEST
            }
        }
    }

    private val filter = MediatorLiveData<Filter>().apply {
        addSource(sort) { so ->
            removeSource(status)
            addSource(status) { st ->
                value = Filter(st, so)
            }
        }
    }

    private val result: LiveData<Listing<EVoucher>> = Transformations.map(filter) {
        profileRepository.loadEvoucher(it.status, it.sort)
    }

    val data: LiveData<ListingData<EVoucher>> = Transformations.switchMap(result) { it.data }
    val refreshState: LiveData<NetworkState> = Transformations.switchMap(result) { it.refreshState }

    fun refresh() {
        result.value?.refresh?.invoke()
    }

    fun filter(status: String?) {
        if (this.status.value != status) {
            this.status.value = status
        }
    }

    fun sort(sort: String?) {
        if (this.sort.value != sort) {
            this.sort.value = sort
        }
    }

    data class Filter(val status: String?,
                      val sort: String?)

    companion object {
        const val SORT_NEWEST = "-createdAt"
        const val SORT_EXPIRE = "expireAt"
    }

}