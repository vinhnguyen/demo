package com.zody.ui.evoucher

import android.graphics.Color
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.zody.R
import com.zody.entity.EVoucher
import org.joda.time.DateTimeConstants
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by vinhnguyen.it.vn on 2018, August 15
 */
object BindingInterface {

    @JvmStatic
    @BindingAdapter(value = ["evoucher", "now"], requireAll = true)
    fun bindEvoucherExpire(view: TextView, evoucher: EVoucher?, now: Date?) {
        val context = view.context
        view.setTextColor(Color.parseColor("#D0021B"))
        view.text = when {
            evoucher == null -> null
            evoucher.status == EVoucher.STATUS_USED && evoucher.used == null -> context.getString(R.string.e_voucher_status_used)
            evoucher.status == EVoucher.STATUS_USED && evoucher.used != null -> context.getString(R.string.e_voucher_status_used_at,
                    SimpleDateFormat("HH:mm, dd/MM/yyyy", Locale.getDefault()).format(evoucher.used))
            evoucher.status == EVoucher.STATUS_EXPIRED && evoucher.expire == null -> context.getString(R.string.evoucher_expired)
            evoucher.status == EVoucher.STATUS_EXPIRED && evoucher.expire != null -> context.getString(R.string.evoucher_expired_at,
                    SimpleDateFormat("HH:mm, dd/MM/yyyy", Locale.getDefault()).format(evoucher.expire))
            evoucher.expire == null || now == null -> null
            now >= evoucher.expire -> context.getString(R.string.evoucher_expired_at,
                    SimpleDateFormat("HH:mm, dd/MM/yyyy", Locale.getDefault()).format(evoucher.expire))
            else -> {
                view.setTextColor(Color.parseColor("#57A600"))
                val period = evoucher.expire.time - now.time
                when {
                    period > DateTimeConstants.MILLIS_PER_DAY -> {
                        context.getString(R.string.evoucher_expire_remaining_days, period / DateTimeConstants.MILLIS_PER_DAY)
                    }
                    period > DateTimeConstants.MILLIS_PER_HOUR -> {
                        context.getString(R.string.evoucher_expire_remaining_hours, period / DateTimeConstants.MILLIS_PER_HOUR)
                    }
                    period > DateTimeConstants.MILLIS_PER_MINUTE -> {
                        context.getString(R.string.evoucher_expire_remaining_minutes, period / DateTimeConstants.MILLIS_PER_MINUTE)
                    }
                    else -> {
                        context.getString(R.string.evoucher_expire_remaining_seconds, period / DateTimeConstants.MILLIS_PER_SECOND)
                    }
                }
            }
        }
    }
}