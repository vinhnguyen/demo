package com.zody.ui.evoucher

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.zody.R
import com.zody.databinding.FragmentEvoucherBinding
import com.zody.entity.EVoucher
import com.zody.ui.base.BaseFragment
import com.zody.ui.base.Navigation
import com.zody.ui.evoucher.detail.EVoucherDetailActivity
import com.zody.ui.views.SingleChoiceDialogFragment
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2019, February 12
 */
class EVoucherFragment : BaseFragment() {

    companion object {
        const val REQUEST_CODE = 1

        val STATUSES = listOf(
                EVoucher.STATUS_UNUSED to R.string.e_voucher_status_unused,
                EVoucher.STATUS_USED to R.string.e_voucher_status_used,
                EVoucher.STATUS_EXPIRED to R.string.e_voucher_status_expired
        )

        val SORT = listOf(
                EVoucherViewModel.SORT_NEWEST to R.string.e_voucher_sort_newest,
                EVoucherViewModel.SORT_EXPIRE to R.string.e_voucher_sort_expire
        )
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val eVoucherViewModel by lazy {
        ViewModelProviders.of(requireActivity(), viewModelFactory).get(EVoucherViewModel::class.java)
    }

    private lateinit var binding: FragmentEvoucherBinding

    override val screenName: String?
        get() = "evoucher"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_evoucher, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = EvoucherAdapter()
        adapter.onItemSelected = { evoucher ->
            startActivityForResult(Intent(requireContext(), EVoucherDetailActivity::class.java).apply {
                putExtra(Navigation.EXTRA_TARGET_VALUE, evoucher.id)
            }, REQUEST_CODE)
        }

        binding.recyclerView.apply {
            addItemDecoration(DividerItemDecoration(requireContext(), LinearLayoutManager.VERTICAL))
            this.adapter = adapter
        }

        binding.ddStatus.root.setOnClickListener {
            SingleChoiceDialogFragment<String>().apply {
                options = STATUSES.map {
                    SingleChoiceDialogFragment.Option(key = it.first, text = this@EVoucherFragment.getString(it.second))
                }
                onItemSelected = { option, _ ->
                    eVoucherViewModel.filter(option.key)
                }
            }.show(fragmentManager, null)
        }

        binding.ddSort.root.setOnClickListener {
            SingleChoiceDialogFragment<String>().apply {
                options = SORT.map {
                    SingleChoiceDialogFragment.Option(key = it.first, text = this@EVoucherFragment.getString(it.second))
                }
                onItemSelected = { option, _ ->
                    eVoucherViewModel.sort(option.key)
                }
            }.show(fragmentManager, null)
        }

        eVoucherViewModel.now.observe(this, Observer {
            adapter.now = it
        })
        eVoucherViewModel.data.observe(this, Observer {
            adapter.submitList(it.pagedList)
        })
        eVoucherViewModel.status.observe(this, Observer { status ->
            val text = STATUSES.firstOrNull { it.first == status }?.second
            binding.ddStatus.value = if (text != null) getString(text) else null
            binding.ddSort.root.isEnabled = status == EVoucher.STATUS_UNUSED
        })
        eVoucherViewModel.sort.observe(this, Observer { sort ->
            val text = SORT.firstOrNull { it.first == sort }?.second
            binding.ddSort.value = if (text != null) getString(text) else null
        })

        eVoucherViewModel.refreshState.observe(this, Observer {
            binding.swipeRefreshLayout.isRefreshing = it.loading
        })
        binding.swipeRefreshLayout.setOnRefreshListener { eVoucherViewModel.refresh() }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            eVoucherViewModel.refresh()
        }
    }
}