package com.zody.ui.leaderboard

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.zody.R
import com.zody.databinding.FragmentLeaderBoardFilterBinding
import com.zody.di.Injectable
import com.zody.utils.px
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, August 18
 */
class LeaderBoardFilterFragment : BottomSheetDialogFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentLeaderBoardFilterBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_leader_board_filter, container, false)
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val leaderBoardViewModel = ViewModelProviders.of(activity!!, viewModelFactory).get(LeaderBoardViewModel::class.java)

        val timeAdapter = LeaderBoardFilterAdapter()
        binding.rvLeaderBoardFilterTime.apply {
            layoutManager = object : FlexboxLayoutManager(context) {
                override fun canScrollHorizontally(): Boolean {
                    return false
                }

                override fun canScrollVertically(): Boolean {
                    return false
                }
            }
            isNestedScrollingEnabled = false
            addItemDecoration(createItemDecoration())
            this.adapter = timeAdapter
        }
        leaderBoardViewModel.timeFilters.observe(this, Observer {
            timeAdapter.items = it
        })
        leaderBoardViewModel.selectedTime.observe(this, Observer {
            timeAdapter.selectedData = it
        })

        val cityAdapter = LeaderBoardFilterAdapter()
        binding.rvLeaderBoardFilterCity.apply {
            addItemDecoration(createItemDecoration())
            this.adapter = cityAdapter
        }
        leaderBoardViewModel.cityFilters.observe(this, Observer {
            cityAdapter.items = it
        })
        leaderBoardViewModel.selectedCity.observe(this, Observer {
            cityAdapter.selectedData = it
        })


        binding.btLeaderBoardFilterCancel.setOnClickListener { dismissAllowingStateLoss() }
        binding.btLeaderBoardFilterDone.setOnClickListener {
            leaderBoardViewModel.filter(timeAdapter.selectedData, cityAdapter.selectedData)
            dismissAllowingStateLoss()
        }
    }

    private fun createItemDecoration(): RecyclerView.ItemDecoration {
        return object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                outRect.left = 12.px
                outRect.bottom = 14.px
            }
        }
    }

}