package com.zody.ui.leaderboard

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.zody.R
import com.zody.entity.City
import com.zody.entity.LeaderBoardItem
import com.zody.repository.ConfigurationRepository
import com.zody.repository.LeaderBoardRepository
import com.zody.repository.Result
import com.zody.ui.base.BaseViewModel
import com.zody.utils.valueIfDifferent
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 18
 */
class LeaderBoardViewModel @Inject constructor(app: Application,
                                               leaderBoardRepository: LeaderBoardRepository,
                                               configurationRepository: ConfigurationRepository) : BaseViewModel() {

    private val timeFiltersData: List<Pair<String, String>> = listOf(
            "all" to app.getString(R.string.leader_board_filter_time_all),
            "week" to app.getString(R.string.leader_board_filter_time_week),
            "month" to app.getString(R.string.leader_board_filter_time_month)
    )
    private val city: LiveData<List<City>> = configurationRepository.loadCity()

    val timeFilters = MutableLiveData<List<Pair<String, String>>>().apply {
        value = timeFiltersData
    }
    val cityFilters: LiveData<List<Pair<String, String>>> = Transformations.map(city) { cities ->
        mutableListOf(
                "all" to app.getString(R.string.leader_board_filter_city_all)
        ).apply {
            addAll(cities.map { it.id to it.name })
        }
    }

    private val filter: MediatorLiveData<Filter> = Transformations.map(cityFilters) {
        Filter("all", "all")
    } as MediatorLiveData<Filter>

    val selectedTime: LiveData<String> = Transformations.map(filter) { it.timeFilter }

    val selectedCity: LiveData<String> = Transformations.map(filter) { it.cityFilter }

    private val data: Map<String, LiveData<Result<List<LeaderBoardItem>>>> = LeaderBoardItem.types.map { type ->
        type to Transformations.switchMap(filter) {
            leaderBoardRepository.loadLeaderBoard(type, time = it.timeFilter, city = it.cityFilter)
        }
    }.toMap()

    fun dataOfType(type: String): LiveData<Result<List<LeaderBoardItem>>> {
        return data[type] ?: throw Exception("Type must be in ${LeaderBoardItem.types}]")
    }

    fun filter(timeFilter: String?, cityFilter: String?) {
        if (timeFilter != null && cityFilter != null) {
            filter.valueIfDifferent = Filter(timeFilter, cityFilter)
        }
    }

    class Filter(val timeFilter: String,
                 val cityFilter: String) {

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is Filter) return false

            if (timeFilter != other.timeFilter) return false
            if (cityFilter != other.cityFilter) return false

            return true
        }

        override fun hashCode(): Int {
            var result = timeFilter.hashCode()
            result = 31 * result + cityFilter.hashCode()
            return result
        }
    }
}