package com.zody.ui.leaderboard

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.zody.R
import com.zody.databinding.FragmentLeaderBoardBinding
import com.zody.entity.LeaderBoardItem
import com.zody.ui.base.BaseFragment
import com.zody.utils.px
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 17
 */
class LeaderBoardFragment : BaseFragment() {

    companion object {
        const val ARG_TYPE = "LeaderBoardFragment.Type"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentLeaderBoardBinding
    private val type: String by lazy {
        arguments?.getString(ARG_TYPE) ?: LeaderBoardItem.TYPE_VIEW
    }
    private val adapter: LeaderBoardAdapter by lazy {
        LeaderBoardAdapter(type)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_leader_board, container, false)
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val leaderBoardViewModel = ViewModelProviders.of(activity!!, viewModelFactory).get(LeaderBoardViewModel::class.java)
        leaderBoardViewModel.dataOfType(type).observe(this, Observer {
            if (it.success == true) {
                adapter.submitList(it.data)
            }
        })

        binding.recyclerView.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                super.getItemOffsets(outRect, view, parent, state)
                outRect.bottom = 1.px
            }
        })
        binding.recyclerView.adapter = adapter
    }

}