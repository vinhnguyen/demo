package com.zody.ui.leaderboard

import android.graphics.Typeface
import android.os.Bundle
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.doOnPreDraw
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.tabs.TabLayout
import com.zody.R
import com.zody.databinding.ActivityLeaderBoardBinding
import com.zody.ui.base.BaseFragmentActivity
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 17
 */
class LeaderBoardActivity : BaseFragmentActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityLeaderBoardBinding

    private val bold: Typeface? by lazy {
        ResourcesCompat.getFont(this@LeaderBoardActivity, R.font.bold)
    }
    private val semiBold: Typeface? by lazy {
        ResourcesCompat.getFont(this@LeaderBoardActivity, R.font.semi_bold)
    }

    override val screenName: String?
        get() = "leader board"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_leader_board)
        binding.headerBar.btHeaderActionLeft.setOnClickListener { finish() }
        binding.headerBar.btHeaderActionRight.setOnClickListener {
            LeaderBoardFilterFragment().show(supportFragmentManager, null)
        }

        binding.viewPager.apply {
            adapter = LeaderBoardPageAdapter(this@LeaderBoardActivity)
            offscreenPageLimit = 3

        }
        binding.tabLayout.apply {
            setupWithViewPager(binding.viewPager)
            addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabReselected(tab: TabLayout.Tab?) {

                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {
                    setupTabStyle(tab)
                }

                override fun onTabSelected(tab: TabLayout.Tab?) {
                    setupTabStyle(tab)
                }
            })
            doOnPreDraw {
                for (i in 0 until tabCount) {
                    setupTabStyle(getTabAt(i))
                }
            }
        }
    }

    private fun setupTabStyle(tab: TabLayout.Tab?) {
        tab?.let { t ->
            (((t.parent.getChildAt(0) as ViewGroup)
                    .getChildAt(t.position) as ViewGroup)
                    .getChildAt(1) as TextView)
                    .typeface = if (t.isSelected) bold else semiBold
        }
    }
}