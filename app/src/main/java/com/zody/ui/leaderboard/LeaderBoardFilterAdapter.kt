package com.zody.ui.leaderboard

import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.ViewDataBinding
import com.zody.R
import com.zody.databinding.ItemListLeaderBoardFilterBinding
import com.zody.ui.base.DataBindingViewHolder
import com.zody.ui.base.DatabindingAdapter

/**
 * Created by vinhnguyen.it.vn on 2018, August 18
 */
class LeaderBoardFilterAdapter : DatabindingAdapter() {
    var items: List<Pair<String, String>>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var selectedData: String? = null
        set(value) {
            if (field != value) {
                val backup = selectedPosition
                field = value
                backup?.let { notifyItemChanged(it) }
                selectedPosition?.let { notifyItemChanged(it) }
            }
        }

    private val selectedPosition: Int?
        get() = items?.indexOfFirst { it.first == selectedData }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingViewHolder {
        val holder = super.onCreateViewHolder(parent, viewType)
        when (holder.binding) {
            is ItemListLeaderBoardFilterBinding ->
                holder.itemView.setOnClickListener {
                    selectedData = items?.get(holder.adapterPosition)?.first
                }
        }
        return holder
    }

    override fun onBind(binding: ViewDataBinding, position: Int) {
        (binding.root as TextView).apply {
            text = items?.get(position)?.second
            isSelected = items?.get(position)?.first == selectedData
        }
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_list_leader_board_filter
    }

    override fun getItemCount(): Int {
        return items?.size ?: 0
    }
}