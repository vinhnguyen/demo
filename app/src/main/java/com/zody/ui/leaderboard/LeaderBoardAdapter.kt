package com.zody.ui.leaderboard

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.zody.R
import com.zody.databinding.ItemListLeaderBoardBinding
import com.zody.entity.LeaderBoardItem
import com.zody.ui.base.DataBindingViewHolder
import com.zody.ui.base.DatabindingListAdapter
import com.zody.ui.user.UserActivity

/**
 * Created by vinhnguyen.it.vn on 2018, July 18
 */
class LeaderBoardAdapter(val type: String) : DatabindingListAdapter<LeaderBoardItem>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingViewHolder {
        val holder = super.onCreateViewHolder(parent, viewType)
        when (holder.binding) {
            is ItemListLeaderBoardBinding -> holder.itemView.setOnClickListener { view ->
                UserActivity.start(view.context, holder.binding.item?.user?.id)
            }
        }
        return holder
    }

    override fun onBind(binding: ViewDataBinding, item: LeaderBoardItem?) {
        (binding as ItemListLeaderBoardBinding).apply {
            this.type = this@LeaderBoardAdapter.type
            this.item = item
        }
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_list_leader_board
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<LeaderBoardItem>() {
            override fun areItemsTheSame(oldItem: LeaderBoardItem, newItem: LeaderBoardItem): Boolean {
                return oldItem.rank == newItem.rank
            }

            override fun areContentsTheSame(oldItem: LeaderBoardItem, newItem: LeaderBoardItem): Boolean {
                return oldItem == newItem
            }
        }
    }

}