package com.zody.ui.leaderboard

/**
 * Created by vinhnguyen.it.vn on 2018, August 18
 */
class LeaderBoardFilterItem(val data: Pair<String, String>, var selected: Boolean)