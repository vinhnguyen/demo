package com.zody.ui.leaderboard

import android.os.Bundle
import androidx.fragment.app.FragmentStatePagerAdapter
import com.zody.R
import com.zody.entity.LeaderBoardItem

/**
 * Created by vinhnguyen.it.vn on 2018, July 17
 */
class LeaderBoardPageAdapter(private val activity: LeaderBoardActivity) : FragmentStatePagerAdapter(activity.supportFragmentManager) {

    companion object {
        private val titles = arrayOf(R.string.leader_board_type_view, R.string.leader_board_type_tips, R.string.leader_board_type_coin)
        private val types = LeaderBoardItem.types
    }

    override fun getItem(position: Int) = LeaderBoardFragment().apply { arguments = Bundle().apply { putString(LeaderBoardFragment.ARG_TYPE, types[position]) } }

    override fun getCount() = titles.size

    override fun getPageTitle(position: Int): CharSequence? {
        return activity.getString(titles[position])
    }
}