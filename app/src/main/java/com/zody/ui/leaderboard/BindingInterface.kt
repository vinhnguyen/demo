package com.zody.ui.leaderboard

import android.text.SpannableStringBuilder
import android.text.style.ImageSpan
import android.widget.TextView
import androidx.core.text.inSpans
import androidx.core.text.toSpanned
import androidx.core.widget.TextViewCompat
import androidx.databinding.BindingAdapter
import com.zody.R
import com.zody.entity.LeaderBoardItem
import com.zody.utils.decimalFormat
import com.zody.utils.gone
import com.zody.utils.stringFormat

/**
 * Created by vinhnguyen.it.vn on 2018, August 06
 */
object BindingInterface {

    @JvmStatic
    @BindingAdapter("type", "value")
    fun bindValue(view: TextView, type: String?, value: LeaderBoardItem?) {
        view.gone = type == LeaderBoardItem.TYPE_TIPS

        val icon = when (type) {
            LeaderBoardItem.TYPE_COIN -> R.drawable.ic_coin
            else -> 0
        }
        TextViewCompat.setCompoundDrawablesRelativeWithIntrinsicBounds(view, 0, 0, icon, 0)

        view.text = when (type) {
            LeaderBoardItem.TYPE_VIEW -> view.resources.getString(R.string.leader_board_number_of_views, value?.statistic?.views?.stringFormat
                    ?: 0)
            LeaderBoardItem.TYPE_COIN -> value?.statistic?.coin?.stringFormat
            else -> null
        }
    }

    @JvmStatic
    @BindingAdapter("type", "statistic")
    fun bindStatistic(view: TextView, type: String?, statistic: LeaderBoardItem?) {
        val context = view.context
        view.text = when (type) {
            LeaderBoardItem.TYPE_VIEW -> {
                context.getString(R.string.leader_board_number_of_reviews,
                        statistic?.statistic?.reviews?.stringFormat ?: "0")
            }
            LeaderBoardItem.TYPE_TIPS -> {
                SpannableStringBuilder()
                        .append(context.getString(R.string.leader_board_had_tips))
                        .append(" ")
                        .append("${statistic?.statistic?.coin?.decimalFormat ?: "0"} ")
                        .inSpans(ImageSpan(context, R.drawable.ic_coin)) {
                            append(" ")
                        }
                        .toSpanned()
            }
            LeaderBoardItem.TYPE_COIN -> {
                context.getString(R.string.leader_board_number_of_bills,
                        statistic?.statistic?.bills?.stringFormat ?: "0")
            }
            else -> null
        }
    }
}