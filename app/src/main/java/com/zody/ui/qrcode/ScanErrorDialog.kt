package com.zody.ui.qrcode

import android.app.AlertDialog
import android.content.DialogInterface
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.zody.R
import com.zody.databinding.DialogScanQrCodeErrorBinding
import com.zody.ui.base.BaseDialogFragment

/**
 * Created by vinhnguyen.it.vn on 2018, October 30
 */
class ScanErrorDialog : BaseDialogFragment() {

    private lateinit var binding: DialogScanQrCodeErrorBinding

    var icon: Drawable? = null
    var title: String? = null
    var message: String? = null

    var onClickListener: DialogInterface.OnClickListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_scan_qr_code_error, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.ivIcon.setImageDrawable(icon)
        binding.tvTitle.text = title
        binding.tvMessage.text = message

        binding.btPositive.setOnClickListener {
            onClickListener?.onClick(dialog, AlertDialog.BUTTON_POSITIVE)
            dismissAllowingStateLoss()
        }
        binding.btNegative.setOnClickListener {
            onClickListener?.onClick(dialog, AlertDialog.BUTTON_NEGATIVE)
            dismissAllowingStateLoss()
        }
        binding.btNeutral.setOnClickListener {
            onClickListener?.onClick(dialog, AlertDialog.BUTTON_NEUTRAL)
            dismissAllowingStateLoss()
        }
    }
}