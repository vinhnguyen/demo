package com.zody.ui.qrcode

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.zody.entity.ScanQrCodeResult
import com.zody.repository.Result
import com.zody.repository.ScanQrCodeRepository
import com.zody.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, October 29
 */
class ScanQrCodeViewModel @Inject constructor(private val scanQrCodeRepository: ScanQrCodeRepository) : BaseViewModel() {

    private val code = MutableLiveData<String>()

    val result: LiveData<Result<ScanQrCodeResult>> = Transformations.switchMap(code) {
        scanQrCodeRepository.checkQrCode(it)
    }

    fun sendCode(code: String) {
        this.code.value = code
    }
}