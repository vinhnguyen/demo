package com.zody.ui.qrcode

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.zody.R
import com.zody.databinding.DialogScanQrCodeErrorOtherBinding
import com.zody.ui.base.BaseDialogFragment
import com.zody.utils.toHtml

/**
 * Created by vinhnguyen.it.vn on 2018, October 30
 */
class ScanErrorOtherDialog : BaseDialogFragment() {

    private lateinit var binding: DialogScanQrCodeErrorOtherBinding

    var message: String? = null
    var onClickListener: DialogInterface.OnClickListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_scan_qr_code_error_other, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.tvMessage.text = message?.toHtml()

        binding.btPositive.setOnClickListener {
            onClickListener?.onClick(dialog, AlertDialog.BUTTON_POSITIVE)
            dismissAllowingStateLoss()
        }
        binding.btNegative.setOnClickListener {
            onClickListener?.onClick(dialog, AlertDialog.BUTTON_NEGATIVE)
            dismissAllowingStateLoss()
        }
        binding.btNeutral.setOnClickListener {
            onClickListener?.onClick(dialog, AlertDialog.BUTTON_NEUTRAL)
            dismissAllowingStateLoss()
        }
    }
}