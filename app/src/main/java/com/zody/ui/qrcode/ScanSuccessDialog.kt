package com.zody.ui.qrcode

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.zody.R
import com.zody.databinding.DialogScanSuccessBinding
import com.zody.databinding.LayoutDialogScanSuccessCoinBinding
import com.zody.entity.ScanQrCodeResult
import com.zody.ui.base.BaseDialogFragment

/**
 * Created by vinhnguyen.it.vn on 2018, October 30
 */
class ScanSuccessDialog : BaseDialogFragment() {

    private lateinit var binding: DialogScanSuccessBinding

    var result: ScanQrCodeResult? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_scan_success, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.result = result

        val inflater = LayoutInflater.from(requireContext())
        val other = result?.summary?.other ?: 0
        if (other > 0) {
            DataBindingUtil.inflate<LayoutDialogScanSuccessCoinBinding>(inflater, R.layout.layout_dialog_scan_success_coin, binding.group2, true).apply {
                icon = ContextCompat.getDrawable(requireContext(), R.drawable.dialog_scan_success_temp_3)
                title = getString(R.string.dialog_scan_sucess_text_4)
                coin = other
            }
        }

        result?.budweiserItems?.forEach {
            DataBindingUtil.inflate<LayoutDialogScanSuccessCoinBinding>(inflater, R.layout.layout_dialog_scan_success_coin, binding.group2, true).apply {
                icon = ContextCompat.getDrawable(requireContext(), R.drawable.dialog_scan_success_temp_6)
                title = "${it.name} x ${it.quantity ?: 0}"
                coin = it.coin ?: 0
            }
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            window?.decorView?.background?.alpha = 0
        }
    }
}