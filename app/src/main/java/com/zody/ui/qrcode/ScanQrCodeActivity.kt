package com.zody.ui.qrcode

import android.Manifest
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.zxing.Result
import com.zody.R
import com.zody.databinding.ActivityScanQrCodeBinding
import com.zody.ui.base.BaseActivity
import com.zody.ui.business.openSupport
import com.zody.utils.checkAndRequestPermission
import com.zody.utils.checkPermissionAndRun
import me.dm7.barcodescanner.zxing.ZXingScannerView
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, October 29
 */
class ScanQrCodeActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityScanQrCodeBinding

    private val scanQrCodeViewModel: ScanQrCodeViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ScanQrCodeViewModel::class.java)
    }

    private val resultHandler = object : ZXingScannerView.ResultHandler {
        override fun handleResult(result: Result) {
            val code = result.text
            if (code?.isNotEmpty() == true) {
                scanQrCodeViewModel.sendCode(code)
            } else {
                binding.scannerView.resumeCameraPreview(this)
            }
        }
    }

    override val screenName: String?
        get() = "scan qr code"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_scan_qr_code)
        binding.btHeaderActionLeft.setOnClickListener { finish() }

        checkAndRequestPermission(Manifest.permission.CAMERA)

        binding.scannerView.setResultHandler(resultHandler)
        binding.btScanCodeFlash.setOnClickListener {
            try {
                binding.scannerView.toggleFlash()
            } catch (e: Exception) {
                Timber.e(e)
            }
        }

        scanQrCodeViewModel.result.observe(this, Observer {
            binding.loading = it.loading
            if (it.success == false) {
                when (it.code) {
                    110 -> {
                        ScanErrorDialog().apply {

                            var handledOnDismiss = false

                            icon = ContextCompat.getDrawable(this@ScanQrCodeActivity, R.drawable.ic_scan_qr_code_110)
                            title = this@ScanQrCodeActivity.getString(R.string.scan_qr_code_error_title_110)
                            message = it.message
                            onClickListener = DialogInterface.OnClickListener { _, which ->
                                handledOnDismiss = true
                                when (which) {
                                    AlertDialog.BUTTON_POSITIVE -> binding.scannerView.resumeCameraPreview(resultHandler)
                                    AlertDialog.BUTTON_NEGATIVE -> finish()
                                    AlertDialog.BUTTON_NEUTRAL -> {
                                        openSupport()
                                        finish()
                                    }
                                }
                            }
                            onDismissListener = DialogInterface.OnDismissListener {
                                if (!handledOnDismiss) {
                                    finish()
                                }
                            }
                        }.show(supportFragmentManager, null)
                    }
                    111 -> {
                        ScanErrorDialog().apply {

                            var handledOnDismiss = false

                            icon = ContextCompat.getDrawable(this@ScanQrCodeActivity, R.drawable.ic_scan_qr_code_111)
                            title = this@ScanQrCodeActivity.getString(R.string.scan_qr_code_error_title_111)
                            message = it.message
                            onClickListener = DialogInterface.OnClickListener { _, which ->
                                handledOnDismiss = true
                                when (which) {
                                    AlertDialog.BUTTON_POSITIVE -> binding.scannerView.resumeCameraPreview(resultHandler)
                                    AlertDialog.BUTTON_NEGATIVE -> finish()
                                    AlertDialog.BUTTON_NEUTRAL -> {
                                        openSupport()
                                        finish()
                                    }
                                }
                            }
                            onDismissListener = DialogInterface.OnDismissListener {
                                if (!handledOnDismiss) {
                                    finish()
                                }
                            }
                        }.show(supportFragmentManager, null)
                    }
                    else -> {
                        ScanErrorOtherDialog().apply {

                            var handledOnDismiss = false

                            onClickListener = DialogInterface.OnClickListener { _, which ->
                                handledOnDismiss = true
                                when (which) {
                                    AlertDialog.BUTTON_POSITIVE -> binding.scannerView.resumeCameraPreview(resultHandler)
                                    AlertDialog.BUTTON_NEGATIVE -> finish()
                                    AlertDialog.BUTTON_NEUTRAL -> {
                                        openSupport()
                                        finish()
                                    }
                                }
                            }
                            onDismissListener = DialogInterface.OnDismissListener {
                                if (!handledOnDismiss) {
                                    finish()
                                }
                            }
                            message = it.message
                        }.show(supportFragmentManager, null)
                    }
                }
            } else if (it.success == true) {
                ScanSuccessDialog().apply {
                    result = it.data
                    onDismissListener = DialogInterface.OnDismissListener {
                        finish()
                    }
                }.show(supportFragmentManager, null)
            }
//            if (!it.loading) {
//                ScanSuccessDialog().apply {
//                    result = ScanQrCodeResult(
//                            coin = 100,
//                            price = 10000.0,
//                            business = BusinessCompat(
//                                    id = "1",
//                                    name = "name",
//                                    address = "address",
//                                    distance = null
//
//                            ),
//                            summary = ScanQrCodeSummary(
//                                    bill = 100,
//                                    other = 50,
//                                    luckyDrawTurns = 2
//                            ),
//                            budweiserItems = listOf(
//                                    BudweiserItem(
//                                            name = "name",
//                                            coin = 10,
//                                            quantity = 20
//                                    ),
//                                    BudweiserItem(
//                                            name = "name",
//                                            coin = 10,
//                                            quantity = 20
//                                    ),
//                                    BudweiserItem(
//                                            name = "name",
//                                            coin = 10,
//                                            quantity = 20
//                                    ),
//                                    BudweiserItem(
//                                            name = "name",
//                                            coin = 10,
//                                            quantity = 20
//                                    )
//                            )
//                    )
//                    onDismissListener = DialogInterface.OnDismissListener {
//                        finish()
//                    }
//                }.show(supportFragmentManager, null)
//            }
        })
    }

    override fun onResume() {
        super.onResume()
        checkPermissionAndRun(Manifest.permission.CAMERA) {
            binding.scannerView.startCamera()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (permissions.contains(Manifest.permission.CAMERA) && grantResults.firstOrNull() != PackageManager.PERMISSION_GRANTED) {
            finish()
        }
    }

    override fun onPause() {
        super.onPause()
        binding.scannerView.stopCamera()
    }
}