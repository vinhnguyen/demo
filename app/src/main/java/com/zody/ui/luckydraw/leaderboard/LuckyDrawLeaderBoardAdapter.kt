package com.zody.ui.luckydraw.leaderboard

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.zody.BR
import com.zody.R
import com.zody.entity.luckydraw.LuckyDrawLeaderBoard
import com.zody.ui.base.DataBindingViewHolder
import com.zody.ui.base.DatabindingListAdapter

/**
 * Created by vinhnguyen.it.vn on 2018, December 03
 */
class LuckyDrawLeaderBoardAdapter(val type: String?) : DatabindingListAdapter<LuckyDrawLeaderBoard>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingViewHolder {
        val holder = super.onCreateViewHolder(parent, viewType)
        holder.binding.setVariable(BR.type, type)
        return holder
    }

    override fun onBindViewHolder(holder: DataBindingViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.binding.setVariable(BR.rank, position + 1)
    }

    override fun onBind(binding: ViewDataBinding, item: LuckyDrawLeaderBoard?) {
        binding.setVariable(BR.item, item)
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            null -> R.layout.item_list_lucky_draw_leader_board_place_holder
            else -> R.layout.item_list_lucky_draw_leader_board
        }
    }


    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<LuckyDrawLeaderBoard>() {
            override fun areItemsTheSame(oldItem: LuckyDrawLeaderBoard, newItem: LuckyDrawLeaderBoard): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: LuckyDrawLeaderBoard, newItem: LuckyDrawLeaderBoard): Boolean {
                return oldItem == newItem
            }
        }
    }
}