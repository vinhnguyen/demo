package com.zody.ui.luckydraw.dialog

import android.text.SpannableStringBuilder
import android.widget.TextView
import androidx.core.text.bold
import androidx.databinding.BindingAdapter
import com.zody.R

/**
 * Created by vinhnguyen.it.vn on 2018, December 10
 */
object BindingInterface {

    @JvmStatic
    @BindingAdapter("luckyDrawCode")
    fun bindLuckyDrawCode(view: TextView, code: String?) {
        if (code == null) {
            view.text = null
            return
        }
        view.text = SpannableStringBuilder()
                .append(view.context.getString(R.string.lucky_draw_code_1))
                .append(" ")
                .bold {
                    append(code)
                }
                .append(view.context.getString(R.string.lucky_draw_code_2))
    }
}