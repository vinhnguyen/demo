package com.zody.ui.luckydraw.prize

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.zody.entity.BusinessCompat

/**
 * Created by vinhnguyen.it.vn on 2018, December 04
 */
object BindingInterface {

    @JvmStatic
    @BindingAdapter("prizeBusinessNameAndAddress")
    fun bindPrizeBusinessNameAndAddress(view: TextView, business: BusinessCompat?) {
        view.text = when {
            business == null -> null
            business.address?.isNotEmpty() != true -> business.name
            else -> "${business.name}\n${business.address}"
        }
    }
}