package com.zody.ui.luckydraw.leaderboard

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zody.R
import com.zody.databinding.ActivityLuckyDrawLeaderBoardBinding
import com.zody.ui.base.BaseActivity
import com.zody.ui.base.Navigation
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2019, January 29
 */
class LuckyDrawLeaderBoardActivity : BaseActivity() {

    companion object {

        private val EXTRA_TYPE = "LuckyDrawLeaderBoardActivity.Type"

        fun start(context: Context, id: String, type: String?) {
            context.startActivity(Intent(context, LuckyDrawLeaderBoardActivity::class.java).apply {
                putExtra(Navigation.EXTRA_TARGET_VALUE, id)
                putExtra(EXTRA_TYPE, type)
            })
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val luckyDrawLeaderBoardViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(LuckyDrawLeaderBoardViewModel::class.java)
    }

    private lateinit var binding: ActivityLuckyDrawLeaderBoardBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_lucky_draw_leader_board)
        binding.headerBar.btHeaderActionLeft.setOnClickListener { finish() }

        val adapter = LuckyDrawLeaderBoardAdapter(intent.getStringExtra(EXTRA_TYPE))
        binding.recyclerView.adapter = adapter

        luckyDrawLeaderBoardViewModel.result.observe(this, Observer {
            adapter.submitList(it.data)
        })

        luckyDrawLeaderBoardViewModel.init(Navigation.getId(intent))
    }
}