package com.zody.ui.luckydraw.guide

import android.content.Context
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.zody.R
import com.zody.databinding.ActivityLuckyDrawGuideBinding
import com.zody.entity.luckydraw.LuckyDrawGuide
import com.zody.ui.base.BaseActivity
import com.zody.utils.px
import com.zody.utils.startActivityIfResolved
import com.zody.utils.toArrayList

/**
 * Created by vinhnguyen.it.vn on 2019, January 30
 */
class LuckyDrawGuideActivity : BaseActivity() {

    companion object {

        private const val EXTRA_TYPE = "LuckyDrawGuideActivity.Type"
        private const val EXTRA_DATA = "LuckyDrawGuideActivity.Data"

        fun start(context: Context, type: String?, data: List<LuckyDrawGuide>) {
            context.startActivity(Intent(context, LuckyDrawGuideActivity::class.java).apply {
                putExtra(EXTRA_TYPE, type)
                putParcelableArrayListExtra(EXTRA_DATA, data.toArrayList())
            })
        }
    }

    private lateinit var binding: ActivityLuckyDrawGuideBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_lucky_draw_guide)

        binding.headerBar.btHeaderActionLeft.setOnClickListener { finish() }

        binding.recyclerView.apply {
            addItemDecoration(object : RecyclerView.ItemDecoration() {
                override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                    outRect.top = 15.px
                    outRect.left = 10.px
                    outRect.right = 10.px
                }
            })
        }

        binding.recyclerView.adapter = LuckyDrawGuideAdapter(intent.getStringExtra(EXTRA_TYPE)).apply {
            submitList(intent.getParcelableArrayListExtra(EXTRA_DATA))
            onActionClick = { guide ->
                share(guide.extraData)
            }
        }
    }

    private fun share(text: String?) {
        if (text?.isNotEmpty() != true) return
        startActivityIfResolved(Intent(Intent.ACTION_SEND).apply {
            type = "text/plain"
            putExtra(Intent.EXTRA_TEXT, text)
        })
    }
}