package com.zody.ui.luckydraw.prize

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.zody.BR
import com.zody.R
import com.zody.databinding.ItemListLuckyDrawPrizeBinding
import com.zody.entity.luckydraw.LuckyDrawPrize
import com.zody.ui.base.DataBindingViewHolder
import com.zody.ui.base.DatabindingListAdapter

/**
 * Created by vinhnguyen.it.vn on 2018, December 03
 */
class LuckyDrawPrizeAdapter : DatabindingListAdapter<LuckyDrawPrize>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingViewHolder {
        val holder = super.onCreateViewHolder(parent, viewType)
        if (holder.binding is ItemListLuckyDrawPrizeBinding) {
            holder.binding.root.setOnClickListener { view ->
                holder.binding.prize?.let { prize ->
                    LuckyDrawPrizeDetailActivity.start(view.context, prize)
                }
            }
        }
        return holder
    }

    override fun onBind(binding: ViewDataBinding, item: LuckyDrawPrize?) {
        binding.setVariable(BR.prize, item)
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_list_lucky_draw_prize
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<LuckyDrawPrize>() {
            override fun areItemsTheSame(oldItem: LuckyDrawPrize, newItem: LuckyDrawPrize): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: LuckyDrawPrize, newItem: LuckyDrawPrize): Boolean {
                return oldItem == newItem
            }

        }
    }

}