package com.zody.ui.luckydraw.draw

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zody.R
import com.zody.databinding.FragmentLuckyDrawBinding
import com.zody.entity.luckydraw.LuckyDrawPrize
import com.zody.ui.base.BaseFragment
import com.zody.ui.luckydraw.LuckyDrawViewModel
import com.zody.ui.luckydraw.dialog.LuckyDrawResultDialog
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2019, January 25
 */
class LuckyDrawFragment : BaseFragment() {

    companion object {
        private const val LOOPS = 4
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentLuckyDrawBinding

    private val luckyDrawViewModel by lazy {
        ViewModelProviders.of(requireActivity(), viewModelFactory).get(LuckyDrawViewModel::class.java)
    }

    private var currentPrizePosition = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_lucky_draw, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        luckyDrawViewModel.luckyDraw.observe(this, Observer { luckyDraw ->
            binding.luckyDrawView.setLuckyDraw(luckyDraw)
        })

        luckyDrawViewModel.winPrizeAndBallot.observe(this, Observer {
            it?.prize?.let { prize -> rotateTo(prize) }
        })

        binding.btSpinStart.setOnClickListener {
            luckyDrawViewModel.draw()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.luckyDrawView.animate().setListener(null)
    }

    private fun rotateTo(prize: LuckyDrawPrize) {
        val prizes = luckyDrawViewModel.luckyDraw.value?.prizes?.data ?: return
        val size = prizes.size
        val step = 360 / size
        for (i in 0 until size) {
            if (prizes[i].id == prize.id) {
                val total = 720F * LOOPS + step * (size - i - currentPrizePosition)
                currentPrizePosition = size - i
                rotateTo(total, Runnable {
                    showReward()
                    luckyDrawViewModel.endDraw()
                })
                break
            }
        }
    }

    private fun rotateTo(degrees: Float, end: Runnable?) {
        binding.luckyDrawView.animate().rotationBy(degrees).setDuration(LOOPS * 1000L)
                .setInterpolator(FastOutSlowInInterpolator())
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        super.onAnimationEnd(animation)
                        end?.run()
                    }
                })
    }

    private fun showReward() {
        LuckyDrawResultDialog().show(fragmentManager, null)
    }
}