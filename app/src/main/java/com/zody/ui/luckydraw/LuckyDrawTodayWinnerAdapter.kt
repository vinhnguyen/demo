package com.zody.ui.luckydraw

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.zody.BR
import com.zody.R
import com.zody.entity.luckydraw.LuckyDrawWinner
import com.zody.ui.base.DatabindingListAdapter

/**
 * Created by vinhnguyen.it.vn on 2018, December 03
 */
class LuckyDrawTodayWinnerAdapter : DatabindingListAdapter<LuckyDrawWinner>(DIFF_CALLBACK) {

    override fun onBind(binding: ViewDataBinding, item: LuckyDrawWinner?) {
        binding.setVariable(BR.winner, item)
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_list_lucky_draw_winner
    }


    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<LuckyDrawWinner>() {
            override fun areItemsTheSame(oldItem: LuckyDrawWinner, newItem: LuckyDrawWinner): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: LuckyDrawWinner, newItem: LuckyDrawWinner): Boolean {
                return oldItem == newItem
            }
        }
    }
}