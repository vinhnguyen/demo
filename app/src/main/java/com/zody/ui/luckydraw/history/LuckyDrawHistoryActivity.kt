package com.zody.ui.luckydraw.history

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import com.zody.R
import com.zody.databinding.ActivityLuckyDrawHistoryBinding
import com.zody.ui.base.BaseActivity
import com.zody.ui.base.Navigation
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, December 03
 */
class LuckyDrawHistoryActivity : BaseActivity() {

    companion object {
        fun start(context: Context, id: String) {
            context.startActivity(Intent(context, LuckyDrawHistoryActivity::class.java).apply {
                putExtra(Navigation.EXTRA_TARGET_VALUE, id)
            })
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityLuckyDrawHistoryBinding

    override val screenName: String?
        get() = "lucky draw history"

    private val luckyDrawHistoryViewModel: LuckyDrawHistoryViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(LuckyDrawHistoryViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_lucky_draw_history)
        binding.headerBar.btHeaderActionLeft.setOnClickListener { finish() }

        binding.recyclerView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        val adapter = LuckyDrawHistoryAdapter()
        binding.recyclerView.adapter = adapter

        luckyDrawHistoryViewModel.init(Navigation.getId(intent))
        luckyDrawHistoryViewModel.data.observe(this, Observer {
            adapter.submitList(it.pagedList, it.endData)
        })

    }
}