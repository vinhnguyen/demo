package com.zody.ui.luckydraw.draw

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.text.Layout
import android.text.StaticLayout
import android.text.TextPaint
import android.util.AttributeSet
import android.view.View
import androidx.core.content.res.ResourcesCompat
import com.zody.R
import com.zody.entity.luckydraw.LuckyDraw
import com.zody.utils.px

/**
 * Created by VINHNGUYEN.IT.VN on 12/28/16
 */

class LuckyDrawView : View {

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private var luckyDraw: LuckyDraw? = null

    private val strokeWhite = Paint()
    private val strokeBlack = Paint()
    private val paint = Paint()
    private val rectF = RectF()
    private val textPaint = TextPaint()
    private val angleOfArrow: Int

    private var numberOfPrize = 1
    private var cornerStep = 360f


    init {
        strokeBlack.isAntiAlias = true
        strokeBlack.style = Paint.Style.STROKE
        strokeBlack.color = Color.BLACK
        strokeBlack.alpha = 25
        strokeBlack.strokeWidth = 16.px.toFloat()

        strokeWhite.isAntiAlias = true
        strokeWhite.style = Paint.Style.STROKE
        strokeWhite.color = Color.WHITE
        strokeWhite.strokeWidth = 8.px.toFloat()

        paint.isAntiAlias = true
        paint.style = Paint.Style.FILL

        textPaint.isAntiAlias = true
        textPaint.color = Color.WHITE
        textPaint.typeface = ResourcesCompat.getFont(context, R.font.semi_bold)
        textPaint.textAlign = Paint.Align.CENTER

        angleOfArrow = 5.px
    }

    public override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = View.MeasureSpec.getSize(widthMeasureSpec)
        val height = View.MeasureSpec.getSize(heightMeasureSpec)
        val size = if (width > height) height else width
        setMeasuredDimension(size, size)
    }

    fun setLuckyDraw(luckyDraw: LuckyDraw) {
        this.luckyDraw = luckyDraw
        this.numberOfPrize = luckyDraw.prizes?.totalPrize ?: 0
        this.cornerStep = 360.0f / numberOfPrize
        invalidate()
        rotation = -90f - cornerStep / 2 - (angleOfArrow / 2).toFloat()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        if (luckyDraw == null) return
        val size = Math.min(width, height)
        val strokeWidth = strokeWhite.strokeWidth.toInt()
        rectF.set(strokeWidth.toFloat(), strokeWidth.toFloat(), (size - strokeWidth).toFloat(), (size - strokeWidth).toFloat())
        val textWidth = (size.toDouble() * (1 - RATIO_SPIN_BUTTON_ON_WIDTH) * 2.0 / 5).toInt() // 4/5 * (radius - button spin radius)
        textPaint.textSize = Math.min(size.toFloat() / 2.2f / numberOfPrize.toFloat(), 23.px.toFloat())
        var current = 0f
        for (i in 0 until numberOfPrize) {
            val prize = luckyDraw?.prizes?.data?.get(i) ?: continue

            paint.color = Color.parseColor(prize.color)
            canvas.drawArc(rectF, current, cornerStep, true, paint)

            @SuppressLint("DrawAllocation")
            val textLayout = StaticLayout(prize.rewardTitle, textPaint, textWidth, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false)
            val corner = current + cornerStep / 2
            val textHeight = textLayout.height

            val childRadius = Math.sqrt(Math.pow(size * (1 + RATIO_SPIN_BUTTON_ON_WIDTH) / 4, 2.0) + Math.pow((textHeight / 2).toDouble(), 2.0))
            val textCorner = corner + Math.toDegrees(Math.atan(2.0 * textHeight / size.toDouble() / (1 + RATIO_SPIN_BUTTON_ON_WIDTH))).toFloat()
            val textCoordinates = getPoint(childRadius, textCorner)
            canvas.save()
            canvas.rotate(180 + corner, textCoordinates[0], textCoordinates[1])
            canvas.translate(textCoordinates[0], textCoordinates[1])
            textLayout.draw(canvas)
            canvas.restore()

            current += cornerStep
        }
        canvas.drawCircle((size / 2).toFloat(), (size / 2).toFloat(), size / 2 - strokeBlack.strokeWidth / 2, strokeBlack)
        canvas.drawCircle((size / 2).toFloat(), (size / 2).toFloat(), size / 2 - strokeWhite.strokeWidth / 2, strokeWhite)
    }

    private fun getPoint(childRadius: Double, angle: Float): FloatArray {
        val radius = (width / 2).toDouble()
        val radian = Math.toRadians((angle % 90).toDouble())
        val x: Double
        val y: Double
        if (angle == 0f) {
            x = radius + radius
            y = radius
        } else if (angle < 90) {
            x = radius + childRadius * Math.cos(radian)
            y = radius + childRadius * Math.sin(radian)
        } else if (angle == 90f) {
            x = radius
            y = radius + childRadius
        } else if (angle < 180) {
            x = radius - childRadius * Math.sin(radian)
            y = radius + childRadius * Math.cos(radian)
        } else if (angle == 180f) {
            x = radius - childRadius
            y = radius
        } else if (angle < 270) {
            x = radius - childRadius * Math.cos(radian)
            y = radius - childRadius * Math.sin(radian)
        } else if (angle == 270f) {
            x = radius
            y = radius - childRadius
        } else {
            x = radius + childRadius * Math.sin(radian)
            y = radius - childRadius * Math.cos(radian)
        }
        return floatArrayOf(x.toFloat(), y.toFloat())
    }

    companion object {

        val RATIO_SPIN_BUTTON_ON_WIDTH = (219.0f / 732).toDouble()
    }
}
