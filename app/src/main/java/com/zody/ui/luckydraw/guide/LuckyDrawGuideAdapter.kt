package com.zody.ui.luckydraw.guide

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.zody.BR
import com.zody.R
import com.zody.databinding.ItemListLuckyDrawGuideBinding
import com.zody.entity.luckydraw.LuckyDrawGuide
import com.zody.ui.base.DatabindingListAdapter

/**
 * Created by vinhnguyen.it.vn on 2019, January 30
 */
class LuckyDrawGuideAdapter(private val type: String?) : DatabindingListAdapter<LuckyDrawGuide>(DIFF_CALLBACK) {

    var onActionClick: ((guide: LuckyDrawGuide) -> Unit)? = null

    override fun onBind(binding: ViewDataBinding, item: LuckyDrawGuide?) {
        binding.setVariable(BR.guide, item)
        binding.setVariable(BR.luckyDrawType, type)

        if (binding is ItemListLuckyDrawGuideBinding) {
            binding.btAction.setOnClickListener {
                binding.guide?.let { guide ->
                    onActionClick?.invoke(guide)
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_list_lucky_draw_guide
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<LuckyDrawGuide>() {

            override fun areItemsTheSame(oldItem: LuckyDrawGuide, newItem: LuckyDrawGuide): Boolean {
                return false
            }

            override fun areContentsTheSame(oldItem: LuckyDrawGuide, newItem: LuckyDrawGuide): Boolean {
                return false
            }
        }
    }
}