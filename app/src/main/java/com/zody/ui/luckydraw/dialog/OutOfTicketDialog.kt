package com.zody.ui.luckydraw.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import com.zody.R
import com.zody.databinding.DialogLuckyDrawOutOfTicketBinding
import com.zody.ui.base.BaseDialogFragment
import com.zody.utils.openUrl

/**
 * Created by vinhnguyen.it.vn on 2018, December 04
 */
class OutOfTicketDialog : BaseDialogFragment() {

    companion object {
        private const val ARG_URL = "OutOfTicketDialog.Url"

        fun newInstance(url: String?): OutOfTicketDialog {
            return OutOfTicketDialog().apply {
                arguments = bundleOf(ARG_URL to url)
            }
        }
    }

    private lateinit var binding: DialogLuckyDrawOutOfTicketBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_lucky_draw_out_of_ticket, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btLuckyDrawGuide.setOnClickListener {
            arguments?.getString(ARG_URL)?.let { url ->
                context?.openUrl(url)
            }
            dismissAllowingStateLoss()
        }
    }
}