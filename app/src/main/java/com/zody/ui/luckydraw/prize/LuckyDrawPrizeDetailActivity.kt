package com.zody.ui.luckydraw.prize

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.zody.R
import com.zody.databinding.ActivityLuckyDrawPrizeDetailBinding
import com.zody.entity.luckydraw.LuckyDrawPrize
import com.zody.ui.base.BaseActivity
import com.zody.ui.base.Navigation
import com.zody.ui.business.BusinessDetailActivity

/**
 * Created by vinhnguyen.it.vn on 2018, December 04
 */
class LuckyDrawPrizeDetailActivity : BaseActivity() {

    companion object {
        fun start(context: Context, prize: LuckyDrawPrize) {
            context.startActivity(Intent(context, LuckyDrawPrizeDetailActivity::class.java).apply {
                putExtra(Navigation.EXTRA_TARGET_VALUE, prize)
            })
        }
    }

    private lateinit var binding: ActivityLuckyDrawPrizeDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_lucky_draw_prize_detail)
        binding.btHeaderActionLeft.setOnClickListener { finish() }

        val prize = intent.getParcelableExtra<LuckyDrawPrize>(Navigation.EXTRA_TARGET_VALUE)
        binding.prize = prize
        binding.viewPager.adapter = LuckyDrawPrizeCoverPageAdapter(prize.covers)

        prize.business?.let { business ->
            binding.tvPrizeDetailBusinessName.setOnClickListener {
                BusinessDetailActivity.start(this@LuckyDrawPrizeDetailActivity, business.id)
            }
        }
    }
}