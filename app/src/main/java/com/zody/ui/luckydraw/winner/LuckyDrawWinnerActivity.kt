package com.zody.ui.luckydraw.winner

import android.content.Context
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.zody.R
import com.zody.databinding.ActivityLuckyDrawWinnerBinding
import com.zody.ui.base.BaseActivity
import com.zody.ui.base.Navigation
import com.zody.utils.px
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, December 03
 */
class LuckyDrawWinnerActivity : BaseActivity() {

    companion object {
        fun start(context: Context, id: String) {
            context.startActivity(Intent(context, LuckyDrawWinnerActivity::class.java).apply {
                putExtra(Navigation.EXTRA_TARGET_VALUE, id)
            })
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityLuckyDrawWinnerBinding

    private val luckyDrawWinnerViewModel: LuckyDrawWinnerViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(LuckyDrawWinnerViewModel::class.java)
    }

    override val screenName: String?
        get() = "lucky draw winner"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_lucky_draw_winner)
        binding.headerBar.btHeaderActionLeft.setOnClickListener { finish() }

        val adapter = LuckyDrawWinnerAdapter()
        binding.recyclerView.adapter = adapter
        binding.recyclerView.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                outRect.bottom = 1.px
            }
        })

        luckyDrawWinnerViewModel.data.observe(this, Observer {
            adapter.submitList(it.pagedList, it.endData)
        })

        luckyDrawWinnerViewModel.init(Navigation.getId(intent))
    }
}