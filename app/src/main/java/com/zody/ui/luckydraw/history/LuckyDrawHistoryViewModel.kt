package com.zody.ui.luckydraw.history

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.zody.entity.ListItem
import com.zody.entity.luckydraw.LuckyDrawHistory
import com.zody.repository.Listing
import com.zody.repository.ListingData
import com.zody.repository.LuckyDrawRepository
import com.zody.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, December 03
 */
class LuckyDrawHistoryViewModel @Inject constructor(luckyDrawRepository: LuckyDrawRepository) : BaseViewModel() {

    private val luckyDrawId = MutableLiveData<String>()

    private val result: LiveData<Listing<ListItem<LuckyDrawHistory>>> = Transformations.map(luckyDrawId) {
        luckyDrawRepository.loadLuckyDrawHistory(it)
    }

    val data: LiveData<ListingData<ListItem<LuckyDrawHistory>>> = Transformations.switchMap(result) { it.data }!!

    fun init(luckyDrawId: String?) {
        if (luckyDrawId != null) {
            this.luckyDrawId.value = luckyDrawId
        }
    }

}