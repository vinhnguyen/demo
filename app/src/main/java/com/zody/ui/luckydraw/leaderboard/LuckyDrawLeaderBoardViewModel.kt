package com.zody.ui.luckydraw.leaderboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.zody.entity.luckydraw.LuckyDrawLeaderBoard
import com.zody.repository.LuckyDrawRepository
import com.zody.repository.Result
import com.zody.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2019, January 29
 */
class LuckyDrawLeaderBoardViewModel @Inject constructor(luckyDrawRepository: LuckyDrawRepository) : BaseViewModel() {

    private val luckyDrawId = MutableLiveData<String>()


    val result: LiveData<Result<List<LuckyDrawLeaderBoard>>> = Transformations.switchMap(luckyDrawId) {
        luckyDrawRepository.loadLeaderBoard(it)
    }

    fun init(luckyDrawId: String?) {
        if (luckyDrawId != null) this.luckyDrawId.value = luckyDrawId
    }


}