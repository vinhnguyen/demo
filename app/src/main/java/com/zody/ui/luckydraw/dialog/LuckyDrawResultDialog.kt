package com.zody.ui.luckydraw.dialog

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zody.R
import com.zody.databinding.DialogLuckyDrawResultBinding
import com.zody.di.Injectable
import com.zody.entity.luckydraw.LuckyDrawPrize
import com.zody.ui.base.BaseDialogFragment
import com.zody.ui.evoucher.EVoucherActivity
import com.zody.ui.luckydraw.LuckyDrawViewModel
import com.zody.utils.gone
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, December 04
 */
class LuckyDrawResultDialog : BaseDialogFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: DialogLuckyDrawResultBinding

    private val luckyDrawViewModel by lazy {
        ViewModelProviders.of(requireActivity(), viewModelFactory).get(LuckyDrawViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_lucky_draw_result, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.layoutPrize.btPrizeMyReward.setOnClickListener {
            startActivity(Intent(requireContext(), EVoucherActivity::class.java))
            dismissAllowingStateLoss()
        }
        binding.layoutPrize.btPrizeShare.setOnClickListener {
            share()
            dismissAllowingStateLoss()
        }
        binding.layoutGoodLuck.btPrizeShare.setOnClickListener {
            share()
            dismissAllowingStateLoss()
        }

        luckyDrawViewModel.luckyDraw.observe(this, Observer { luckyDraw ->
            binding.luckyDrawType = luckyDraw.type
            binding.shareBonus = luckyDraw.shareBonus
        })

        luckyDrawViewModel.showPrize.observe(this, Observer { prize ->
            binding.prize = prize
            binding.layoutGoodLuck.gone = prize.type != LuckyDrawPrize.TYPE_GOOD_LUCK
            binding.layoutPrize.gone = prize.type == LuckyDrawPrize.TYPE_GOOD_LUCK
        })
        luckyDrawViewModel.showBallot.observe(this, Observer { ballot ->
            binding.ballot = ballot
        })

        luckyDrawViewModel.countFailed.observe(this, Observer {
            binding.hasFailed = it > 1
        })
    }

    private fun share() {
        luckyDrawViewModel.share()
    }
}