package com.zody.ui.luckydraw.winner

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.zody.BR
import com.zody.R
import com.zody.entity.ListItem
import com.zody.entity.luckydraw.LuckyDrawWinner
import com.zody.ui.base.ListItemAdapter

/**
 * Created by vinhnguyen.it.vn on 2018, December 03
 */
class LuckyDrawWinnerAdapter : ListItemAdapter<LuckyDrawWinner>(DIFF_CALLBACK) {

    override fun onBind(binding: ViewDataBinding, item: ListItem<LuckyDrawWinner>?) {
        binding.setVariable(BR.winner, item?.data)
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is ListItem -> R.layout.item_list_lucky_draw_winner
            else -> R.layout.item_list_lucky_draw_winner_place_holder
        }
    }


    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ListItem<LuckyDrawWinner>>() {
            override fun areItemsTheSame(oldItem: ListItem<LuckyDrawWinner>, newItem: ListItem<LuckyDrawWinner>): Boolean {
                return oldItem.type == newItem.type && oldItem.data?.id == newItem.data?.id
            }

            override fun areContentsTheSame(oldItem: ListItem<LuckyDrawWinner>, newItem: ListItem<LuckyDrawWinner>): Boolean {
                return oldItem.data == newItem.data
            }
        }
    }
}