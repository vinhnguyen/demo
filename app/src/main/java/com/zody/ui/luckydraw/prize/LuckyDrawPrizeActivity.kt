package com.zody.ui.luckydraw.prize

import android.content.Context
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.zody.R
import com.zody.databinding.ActivityLuckyDrawPrizesBinding
import com.zody.entity.luckydraw.LuckyDrawPrize
import com.zody.ui.base.BaseActivity
import com.zody.ui.base.Navigation
import com.zody.utils.px

/**
 * Created by vinhnguyen.it.vn on 2018, December 03
 */
class LuckyDrawPrizeActivity : BaseActivity() {

    companion object {
        fun start(context: Context, prizes: ArrayList<LuckyDrawPrize>) {
            context.startActivity(Intent(context, LuckyDrawPrizeActivity::class.java).apply {
                putParcelableArrayListExtra(Navigation.EXTRA_TARGET_VALUE, prizes)
            })
        }
    }

    private lateinit var binding: ActivityLuckyDrawPrizesBinding

    override val screenName: String?
        get() = "lucky draw prize"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_lucky_draw_prizes)
        binding.headerBar.btHeaderActionLeft.setOnClickListener { finish() }

        binding.recyclerView.addItemDecoration(object : RecyclerView.ItemDecoration() {

            val width = 8.px

            override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                val position = parent.getChildAdapterPosition(view)
                outRect.left = if (position % 2 == 0) width else width / 2
                outRect.right = if (position % 2 == 0) width / 2 else width
            }
        })

        binding.recyclerView.adapter = LuckyDrawPrizeAdapter().apply {
            submitList(intent.getParcelableArrayListExtra(Navigation.EXTRA_TARGET_VALUE))
        }

    }
}