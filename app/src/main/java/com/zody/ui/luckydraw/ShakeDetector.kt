package com.zody.ui.luckydraw

import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Build
import timber.log.Timber

/**
 * Created by vinhnguyen.it.vn on 2019, January 29
 */
class ShakeDetector : SensorEventListener {

    var listener: ((Double) -> Unit)? = null

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
        //no thing
    }

    override fun onSensorChanged(event: SensorEvent?) {
        if (event == null || event.values.size < 3) return

        val x = (event.values[0] / SensorManager.GRAVITY_EARTH).toDouble()
        val y = (event.values[1] / SensorManager.GRAVITY_EARTH).toDouble()
        val z = (event.values[2] / SensorManager.GRAVITY_EARTH).toDouble()

        val force = Math.sqrt(x * x + y * y + z * z)

        if (force > 1) {
            Timber.tag("Daniel").d("force: $force")
        }
        if (Build.MANUFACTURER.contains("samsung", ignoreCase = true)) {
            listener?.invoke(force / 2)
        } else {
            listener?.invoke(force)
        }
    }
}