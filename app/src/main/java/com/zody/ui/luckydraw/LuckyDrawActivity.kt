package com.zody.ui.luckydraw

import android.content.Context
import android.content.Intent
import android.graphics.Rect
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.core.view.isGone
import androidx.core.widget.TextViewCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.transaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.share.Sharer
import com.facebook.share.model.ShareContent
import com.facebook.share.model.ShareHashtag
import com.facebook.share.model.ShareLinkContent
import com.facebook.share.widget.ShareDialog
import com.zody.R
import com.zody.databinding.ActivityLuckyDrawBinding
import com.zody.entity.luckydraw.LuckyDraw
import com.zody.ui.base.BaseFragmentActivity
import com.zody.ui.base.Navigation
import com.zody.ui.luckydraw.code.LuckyDrawCodeActivity
import com.zody.ui.luckydraw.dialog.OutOfTicketDialog
import com.zody.ui.luckydraw.draw.LuckyDrawFragment
import com.zody.ui.luckydraw.guide.LuckyDrawGuideActivity
import com.zody.ui.luckydraw.history.LuckyDrawHistoryActivity
import com.zody.ui.luckydraw.leaderboard.LuckyDrawLeaderBoardActivity
import com.zody.ui.luckydraw.money.LuckyMoneyFragment
import com.zody.ui.luckydraw.prize.LuckyDrawPrizeActivity
import com.zody.ui.luckydraw.prize.LuckyDrawPrizeAdapter
import com.zody.ui.luckydraw.winner.LuckyDrawWinnerActivity
import com.zody.utils.*
import kotlinx.android.synthetic.main.layout_lucky_draw_not_winner_yet.view.*
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, November 30
 */
class LuckyDrawActivity : BaseFragmentActivity() {

    companion object {
        fun start(context: Context, id: String?, title: String?) {
            context.startActivity(Intent(context, LuckyDrawActivity::class.java).apply {
                putExtra(Navigation.EXTRA_TARGET_VALUE, id)
                putExtra(Navigation.EXTRA_TARGET_TITLE, title)
            })
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityLuckyDrawBinding

    private val luckyDrawViewModel: LuckyDrawViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(LuckyDrawViewModel::class.java)
    }

    private val callbackManager by lazy {
        CallbackManager.Factory.create()
    }

    override val screenName: String?
        get() = "lucky draw"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_lucky_draw)
        binding.headerBar.btHeaderActionLeft.setOnClickListener { finish() }
        binding.headerBar.title = intent.getStringExtra(Navigation.EXTRA_TARGET_TITLE)

        luckyDrawViewModel.luckyDraw.observe(this, Observer { luckyDraw ->
            binding.headerBar.title = luckyDraw.name

            //Main View
            when (luckyDraw.type) {
                LuckyDraw.TYPE_LUCKY_DRAW -> {
                    supportFragmentManager.transaction {
                        replace(R.id.mainView, LuckyDrawFragment())
                    }
                }
                LuckyDraw.TYPE_LUCKY_MONEY -> {
                    supportFragmentManager.transaction {
                        replace(R.id.mainView, LuckyMoneyFragment())
                    }
                }
            }

            //User turn
            TextViewCompat.setCompoundDrawablesRelativeWithIntrinsicBounds(binding.layoutInfo.tvUserTicket,
                    0, 0,
                    when {
                        luckyDraw.type == LuckyDraw.TYPE_LUCKY_DRAW -> R.drawable.ic_lucky_draw_spin
                        luckyDraw.type == LuckyDraw.TYPE_LUCKY_MONEY -> R.drawable.ic_lucky_money
                        else -> 0
                    },
                    0)

            //Footer
            binding.layoutFooter.btLuckyDrawGuide.setOnClickListener {
                if (luckyDraw.guide?.isNotEmpty() == true) {
                    LuckyDrawGuideActivity.start(this, luckyDraw.type, luckyDraw.guide)
                } else {
                    openUrl(luckyDraw.introductionLink)
                }
            }
            binding.layoutFooter.btLuckyDrawHistory.setOnClickListener {
                LuckyDrawHistoryActivity.start(this@LuckyDrawActivity, luckyDraw.id)
            }
            if (luckyDraw.haveBallot == true) {
                binding.layoutFooter.btLuckyDrawCode.visibility = View.VISIBLE
                binding.layoutFooter.line2.visibility = View.VISIBLE
                binding.layoutFooter.btLuckyDrawCode.setOnClickListener {
                    LuckyDrawCodeActivity.start(this@LuckyDrawActivity, luckyDraw.id, luckyDraw.introductionLink)
                }
            }
            if (luckyDraw.haveLeaderBoard == true) {
                binding.layoutFooter.btLuckyDrawLeaderBoard.visibility = View.VISIBLE
                binding.layoutFooter.line3.visibility = View.VISIBLE
                binding.layoutFooter.btLuckyDrawLeaderBoard.setOnClickListener {
                    LuckyDrawLeaderBoardActivity.start(this@LuckyDrawActivity, luckyDraw.id, luckyDraw.type)
                }
            }

            //Winner
            if (luckyDraw.type == LuckyDraw.TYPE_LUCKY_MONEY) {
                binding.layoutWinner.ivWinner.setImageResource(R.drawable.ic_lucky_money_winner)
            }

            binding.layoutWinner.btLoadMore.setOnClickListener {
                LuckyDrawWinnerActivity.start(this@LuckyDrawActivity, luckyDraw.id)
            }

            binding.layoutNotWinnerYet.btStartSpin.setOnClickListener { binding.scrollView.smoothScrollTo(0, 0) }
            binding.layoutNotWinnerYet.btOtherWinner.setOnClickListener {
                LuckyDrawWinnerActivity.start(this@LuckyDrawActivity, luckyDraw.id)
            }


            //Prize
            binding.layoutPrize.apply {
                val list = luckyDraw.prizes?.data?.filter { it.isLuckyPrize != true }?.toArrayList()
                        ?: return@apply
                if (list.isEmpty()) return@apply
                gone = false
                btLoadMore.isGone = list.size <= 6
                btLoadMore.setOnClickListener {
                    LuckyDrawPrizeActivity.start(this@LuckyDrawActivity, list)
                }

                recyclerView.apply {
                    adapter = LuckyDrawPrizeAdapter().apply {
                        submitList(if (list.size > 6) list.subList(0, 6) else list)
                    }

                    addItemDecoration(object : RecyclerView.ItemDecoration() {

                        val width = 8.px

                        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                            val position = parent.getChildAdapterPosition(view)
                            outRect.left = if (position % 2 == 0) width else width / 2
                            outRect.right = if (position % 2 == 0) width / 2 else width
                        }
                    })
                }
            }

            //Some device make wrong scroll
            binding.scrollView.smoothScrollTo(0, 0)
        })
        luckyDrawViewModel.name.observe(this, Observer {
            binding.layoutInfo.tvUserName.text = it
        })
        luckyDrawViewModel.turn.observe(this, Observer {
            binding.layoutInfo.tvUserTicket.text = it.toString()
        })
        luckyDrawViewModel.outOfTicket.observe(this, Observer {
            showPopupOutOfTicket()
        })

        val adapter = LuckyDrawTodayWinnerAdapter()
        binding.layoutWinner.recyclerView.adapter = adapter
        luckyDrawViewModel.winner.observe(this, Observer {
            val list = it.subList(0, Math.min(it.size, 3))
            adapter.submitList(list)
            val hasWinner = it?.isNotEmpty() == true
            binding.layoutWinner.gone = !hasWinner
            binding.layoutNotWinnerYet.gone = hasWinner
        })
        luckyDrawViewModel.shareTrigger.observe(this, Observer {
            share()
        })
        luckyDrawViewModel.message.observe(this, Observer {
            showMessage(it)
        })
        luckyDrawViewModel.countFailed.observe(this, Observer {

        })

        luckyDrawViewModel.init(Navigation.getId(intent))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }

    private fun showPopupOutOfTicket() {
        OutOfTicketDialog.newInstance(luckyDrawViewModel.luckyDraw.value?.introductionLink)
                .show(supportFragmentManager, null)
    }

    private fun share() {
        val content = ShareLinkContent.Builder()
                .setShareHashtag(ShareHashtag.Builder().setHashtag("#zody").build())
                .setContentUrl(Uri.parse(luckyDrawViewModel.luckyDraw.value?.shareBonus?.link))
                .build()
        share(content)
    }

    private fun share(content: ShareContent<*, *>) {
        val shareDialog = ShareDialog(this)
        shareDialog.registerCallback(callbackManager, object : FacebookCallback<Sharer.Result> {
            override fun onSuccess(result: Sharer.Result) {
                luckyDrawViewModel.shareSuccess()
            }

            override fun onCancel() {

            }

            override fun onError(error: FacebookException) {

            }
        })
        shareDialog.show(content, ShareDialog.Mode.AUTOMATIC)
    }
}