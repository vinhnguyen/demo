package com.zody.ui.luckydraw.history

import android.content.Intent
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.zody.BR
import com.zody.R
import com.zody.databinding.ItemListLuckyDrawHistoryBinding
import com.zody.entity.ListItem
import com.zody.entity.luckydraw.LuckyDrawHistory
import com.zody.entity.luckydraw.LuckyDrawPrize
import com.zody.ui.base.DataBindingViewHolder
import com.zody.ui.base.ListItemAdapter
import com.zody.ui.evoucher.EVoucherActivity

/**
 * Created by vinhnguyen.it.vn on 2018, December 03
 */
class LuckyDrawHistoryAdapter : ListItemAdapter<LuckyDrawHistory>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingViewHolder {
        val holder = super.onCreateViewHolder(parent, viewType)
        if (holder.binding is ItemListLuckyDrawHistoryBinding) {
            holder.itemView.setOnClickListener { view ->
                holder.binding.history?.prize?.let {
                    if (it.type == LuckyDrawPrize.TYPE_VOUCHER) {
                        view.context.startActivity(Intent(view.context, EVoucherActivity::class.java))
                    }
                }
            }
        }
        return holder
    }

    override fun onBind(binding: ViewDataBinding, item: ListItem<LuckyDrawHistory>?) {
        binding.setVariable(BR.history, item?.data)
    }

    override fun getItemViewType(position: Int): Int {
        return if (getItem(position) != null)
            R.layout.item_list_lucky_draw_history
        else
            R.layout.item_list_lucky_draw_history_place_holder
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ListItem<LuckyDrawHistory>>() {
            override fun areItemsTheSame(oldItem: ListItem<LuckyDrawHistory>, newItem: ListItem<LuckyDrawHistory>): Boolean {
                return oldItem.data?.id == newItem.data?.id
            }

            override fun areContentsTheSame(oldItem: ListItem<LuckyDrawHistory>, newItem: ListItem<LuckyDrawHistory>): Boolean {
                return oldItem.data == newItem.data
            }
        }
    }
}