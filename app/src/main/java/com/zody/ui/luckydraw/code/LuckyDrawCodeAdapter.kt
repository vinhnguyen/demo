package com.zody.ui.luckydraw.code

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.zody.BR
import com.zody.R
import com.zody.entity.ListItem
import com.zody.entity.luckydraw.LuckyDrawBallot
import com.zody.ui.base.ListItemAdapter

/**
 * Created by vinhnguyen.it.vn on 2018, December 12
 */
class LuckyDrawCodeAdapter : ListItemAdapter<LuckyDrawBallot>(DIFF_CALLBACK) {

    override fun onBind(binding: ViewDataBinding, item: ListItem<LuckyDrawBallot>?) {
        binding.setVariable(BR.code, item?.data)
    }

    override fun getItemViewType(position: Int): Int {
        return if (getItem(position) == null)
            R.layout.item_list_lucky_draw_code_place_holder
        else
            R.layout.item_list_lucky_draw_code
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ListItem<LuckyDrawBallot>>() {
            override fun areItemsTheSame(oldItem: ListItem<LuckyDrawBallot>, newItem: ListItem<LuckyDrawBallot>): Boolean {
                return oldItem.type == newItem.type && oldItem.data?.id == newItem.data?.id
            }

            override fun areContentsTheSame(oldItem: ListItem<LuckyDrawBallot>, newItem: ListItem<LuckyDrawBallot>): Boolean {
                return oldItem.data == newItem.data
            }
        }
    }
}