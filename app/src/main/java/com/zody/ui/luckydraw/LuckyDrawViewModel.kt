package com.zody.ui.luckydraw

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.zody.entity.luckydraw.*
import com.zody.livedata.SingleLiveEvent
import com.zody.repository.LuckyDrawRepository
import com.zody.repository.ProfileRepository
import com.zody.repository.Result
import com.zody.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, November 30
 */
class LuckyDrawViewModel @Inject constructor(profileRepository: ProfileRepository,
                                             luckyDrawRepository: LuckyDrawRepository,
                                             val turn: TurnLiveData) : BaseViewModel() {

    private val luckyDrawId = MutableLiveData<String>()

    private val luckyDrawResult: LiveData<Result<LuckyDrawData>> = Transformations.switchMap(luckyDrawId) {
        luckyDrawRepository.loadLuckyDraw(it)
    }

    val luckyDraw = MediatorLiveData<LuckyDraw>().apply {
        addSource(luckyDrawResult) {
            it.data?.luckyDraw?.let { data ->
                value = data
            }
        }
    }

    val winner = MediatorLiveData<List<LuckyDrawWinner>>().apply {
        addSource(luckyDrawResult) { result ->
            result.data?.let {
                value = it.winners
            }
        }
    }

    private val drawTrigger = MutableLiveData<Unit>()

    private val callDrawTrigger = SingleLiveEvent<String>()
    private val drawResult: LiveData<Result<LuckyDrawResult>> = Transformations.switchMap(callDrawTrigger) {
        luckyDrawRepository.draw(it)
    }

    val outOfTicket = SingleLiveEvent<Unit>()
    val winPrizeAndBallot = SingleLiveEvent<LuckyDrawResult>()
    val showPrize = SingleLiveEvent<LuckyDrawPrize>()
    val showBallot = SingleLiveEvent<String>()
    val countFailed = MediatorLiveData<Int>()

    val shareTrigger = SingleLiveEvent<Unit>()
    private val shareSuccessTrigger = MutableLiveData<String>()
    private val rewardAfterShared = Transformations.switchMap(shareSuccessTrigger) {
        luckyDrawRepository.getRewardAfterShared(it)
    }

    val message = SingleLiveEvent<String>()

    private val profile = profileRepository.loadProfileFromDB()
    val name: LiveData<String?> = Transformations.map(profile) { it?.name }

    private val drawing = MutableLiveData<Boolean>()

    init {
        turn.apply {
            addSource(luckyDrawResult) {
                it.data?.userTurnLeft?.let { current ->
                    value = current
                }
            }
            addSource(drawResult) {
                if (it.success == true) {
                    value = value?.minus(1)
                } else if (it.code == 73) {
                    value = 0
                }
            }
            addSource(rewardAfterShared) { result ->
                result.data?.value?.let { x ->
                    value = value?.plus(x) ?: x
                }
            }
        }
        callDrawTrigger.apply {
            addSource(drawTrigger) {
                addSource(turn) { current ->
                    removeSource(turn)
                    if (current > 0) {
                        addSource(luckyDrawId) { id ->
                            removeSource(luckyDrawId)
                            value = id
                        }
                    }
                }
            }
        }
        outOfTicket.apply {
            addSource(drawTrigger) {
                addSource(turn) { current ->
                    removeSource(turn)
                    if (current <= 0) {
                        value = null
                        endDraw()
                    }
                }
            }

            addSource(drawResult) { result ->
                if (result.code == 73) {
                    value = null
                    endDraw()
                }
            }
        }
        winPrizeAndBallot.apply {
            addSource(drawResult) { result ->
                result.data?.let { value = it }
            }
        }
        showPrize.apply {
            addSource(winPrizeAndBallot) {
                it.prize?.let { prize -> value = prize }
            }
        }
        showBallot.apply {
            addSource(winPrizeAndBallot) {
                it.ballot?.code?.let { code -> value = code }
            }
        }
        message.apply {
            addSource(rewardAfterShared) { result ->
                if (result.success == false) {
                    value = result.message
                } else if (result.success == true) {
                    value = result.data?.reason
                }
            }
            addSource(drawResult) { result ->
                if (result.success == false && result.code != 73) {
                    value = result.message
                }
            }
        }
        countFailed.apply {
            value = 0
            addSource(drawResult) { result ->
                if (result.success == true) {
                    value = if (LuckyDrawPrize.TYPE_GOOD_LUCK == result.data?.prize?.type) value?.inc()
                            ?: 1 else 0
                }
            }
        }
    }

    fun init(luckyDrawId: String?) {
        if (luckyDrawId != null) {
            this.luckyDrawId.value = luckyDrawId
        }
    }

    fun draw() {
        if (drawing.value != true) {
            drawing.value = true
            drawTrigger.value = null
        }
    }

    fun share() {
        shareTrigger.value = null
    }

    fun shareSuccess() {
        shareSuccessTrigger.value = luckyDrawId.value
    }

    fun endDraw() {
        drawing.value = false
    }
}