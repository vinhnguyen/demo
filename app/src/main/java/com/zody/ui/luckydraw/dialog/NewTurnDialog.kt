package com.zody.ui.luckydraw.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.zody.R
import com.zody.databinding.DialogLuckyDrawNewTurnBinding
import com.zody.entity.luckydraw.NewTurnFromSocket
import com.zody.ui.base.BaseDialogFragment
import com.zody.ui.luckydraw.LuckyDrawActivity

/**
 * Created by vinhnguyen.it.vn on 2019, January 29
 */
class NewTurnDialog : BaseDialogFragment() {

    private lateinit var binding: DialogLuckyDrawNewTurnBinding

    var data: NewTurnFromSocket? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_lucky_draw_new_turn, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.data = data

        binding.btAction.setOnClickListener {
            LuckyDrawActivity.start(requireContext(), data?.luckyDraw?.id, data?.luckyDraw?.name)
            dismissAllowingStateLoss()
        }
    }
}