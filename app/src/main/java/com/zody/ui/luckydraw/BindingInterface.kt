package com.zody.ui.luckydraw

import android.text.SpannableStringBuilder
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.text.color
import androidx.databinding.BindingAdapter
import com.zody.R
import com.zody.entity.luckydraw.LuckyDraw
import com.zody.entity.luckydraw.LuckyDrawPrize
import com.zody.utils.dateFormat
import com.zody.utils.periodFormat
import java.util.*

/**
 * Created by vinhnguyen.it.vn on 2018, November 30
 */
object BindingInterface {

    @JvmStatic
    @BindingAdapter("luckyDrawTime")
    fun bindLuckyDrawTime(view: TextView, luckyDraw: LuckyDraw?) {
        if (luckyDraw?.start == null || luckyDraw.end == null || luckyDraw.start == luckyDraw.end) {
            view.text = null
            return
        }
        val context = view.context
        val now = Date()
        val status = when {
            now < luckyDraw.start -> context.getString(R.string.lucky_draw_status_1)
            now < luckyDraw.end -> context.getString(R.string.lucky_draw_status_2)
            else -> context.getString(R.string.lucky_draw_status_3)
        }

        view.text = SpannableStringBuilder()
                .color(ContextCompat.getColor(context, R.color.orange)) {
                    append(status)
                }
                .append(": ${luckyDraw.start.dateFormat()} - ${luckyDraw.end.dateFormat()}")
    }

    @JvmStatic
    @BindingAdapter("luckyDrawHistoryTime")
    fun bindLuckyDrawHistoryTime(view: TextView, time: Date?) {
        view.text = time?.periodFormat(view.context)
    }

    @JvmStatic
    @BindingAdapter("luckyDrawHistoryIcon")
    fun bindLuckyDrawHistoryIcon(view: ImageView, type: String?) {
        view.setImageResource(when (type) {
            LuckyDrawPrize.TYPE_COIN -> R.drawable.ic_lucky_draw_coin
            LuckyDrawPrize.TYPE_GOOD_LUCK -> R.drawable.ic_lucky_draw_goodluck
            LuckyDrawPrize.TYPE_VOUCHER -> R.drawable.ic_lucky_draw_voucher
            else -> 0
        })
    }
}