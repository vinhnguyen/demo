package com.zody.ui.luckydraw.code

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import com.zody.R
import com.zody.databinding.ActivityLuckyDrawCodeBinding
import com.zody.ui.base.BaseActivity
import com.zody.ui.base.Navigation
import com.zody.utils.openUrl
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, December 12
 */
class LuckyDrawCodeActivity : BaseActivity() {

    companion object {

        private const val EXTRA_GUIDE_LINK = "LuckyDrawCodeActivity.Guide"

        fun start(context: Context, luckyDrawId: String, guide: String?) {
            context.startActivity(Intent(context, LuckyDrawCodeActivity::class.java).apply {
                putExtra(Navigation.EXTRA_TARGET_VALUE, luckyDrawId)
                putExtra(EXTRA_GUIDE_LINK, guide)
            })
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val luckyDrawCodeViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(LuckyDrawCodeViewModel::class.java)
    }

    private lateinit var binding: ActivityLuckyDrawCodeBinding

    override val screenName: String?
        get() = "lucky draw code"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_lucky_draw_code)
        binding.headerBar.btHeaderActionLeft.setOnClickListener { finish() }
        binding.btRule.setOnClickListener {
            openUrl(intent.getStringExtra(EXTRA_GUIDE_LINK))
        }

        val adapter = LuckyDrawCodeAdapter()
        binding.recyclerView.apply {
            this.adapter = adapter
            this.addItemDecoration(DividerItemDecoration(this@LuckyDrawCodeActivity, DividerItemDecoration.VERTICAL))
        }
        luckyDrawCodeViewModel.data.observe(this, Observer {
            adapter.submitList(it.pagedList, it.endData)
        })

        luckyDrawCodeViewModel.init(Navigation.getId(intent))

    }
}