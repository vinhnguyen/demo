package com.zody.ui.luckydraw.money

import android.animation.AnimatorInflater
import android.animation.ObjectAnimator
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.hardware.Sensor
import android.hardware.SensorManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnticipateOvershootInterpolator
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.animation.doOnEnd
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.transition.ChangeBounds
import androidx.transition.TransitionManager
import com.zody.R
import com.zody.databinding.DialogLuckyMoneyShakeBinding
import com.zody.di.Injectable
import com.zody.entity.luckydraw.LuckyDrawPrize
import com.zody.ui.base.BaseDialogFragment
import com.zody.ui.evoucher.EVoucherActivity
import com.zody.ui.luckydraw.LuckyDrawViewModel
import com.zody.ui.luckydraw.ShakeDetector
import com.zody.utils.doOnEnd
import com.zody.utils.gone
import com.zody.utils.vibrate
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicInteger
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2019, January 28
 */
class LuckyMoneyShakeDialog : BaseDialogFragment(), Injectable {

    companion object {
        private const val EXTRA_DRAW_ON_OPEN = "LuckyMoneyShakeDialog.DrawOnOpen"

        fun newInstance(drawOnOpen: Boolean): LuckyMoneyShakeDialog {
            return LuckyMoneyShakeDialog().apply {
                arguments = bundleOf(EXTRA_DRAW_ON_OPEN to drawOnOpen)
            }
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val luckyDrawViewModel by lazy {
        ViewModelProviders.of(requireActivity(), viewModelFactory).get(LuckyDrawViewModel::class.java)
    }

    private lateinit var binding: DialogLuckyMoneyShakeBinding

    private val sensorManager by lazy { requireContext().getSystemService(Context.SENSOR_SERVICE) as SensorManager }
    private val sensor by lazy { sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) }
    private val countShake = AtomicInteger()
    private val countBigShake = AtomicInteger()
    private var lastTime: Long = 0L
    private val drawing = AtomicBoolean()
    private val listener = ShakeDetector().apply {
        listener = { value ->
            when {
                drawing.get() -> {
                    //skip
                }
                value > 3.5 -> {
                    val now = System.currentTimeMillis()
                    if (now - lastTime > 500) {
                        if (now - lastTime > 3000) {
                            countBigShake.set(0)
                        }
                        if (countBigShake.incrementAndGet() > 3) {
                            countBigShake.set(0)
                            countShake.set(0)
                            drawing.set(true)
                            requireContext().vibrate()
                            luckyDrawViewModel.draw()

                            binding.layoutWarning.gone = true
                        }
                    }
                }
                countShake.get() == 60 -> {
                    countBigShake.set(0)
                    countShake.set(0)
                    drawing.set(true)
                    requireContext().vibrate()
                    luckyDrawViewModel.draw()

                    binding.layoutWarning.gone = true
                }
                value > 1.2 -> {
                    shake(value)
                    if (countShake.incrementAndGet() == 30) {
                        binding.layoutWarning.gone = false
                    }
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_lucky_money_shake, container, false)
        return binding.root
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.layoutGoodLuck.btPrizeShare.setOnClickListener {
            share()
            dismissAllowingStateLoss()
        }
        binding.layoutPrize.btPrizeShare.setOnClickListener {
            share()
            dismissAllowingStateLoss()
        }
        binding.layoutPrize.btPrizeMyReward.setOnClickListener {
            startActivity(Intent(requireContext(), EVoucherActivity::class.java))
            dismissAllowingStateLoss()
        }
        binding.btClose.setOnClickListener {
            dismissAllowingStateLoss()
        }

        luckyDrawViewModel.luckyDraw.observe(this, Observer {
            binding.layoutPrize.luckyDrawType = it.type
            binding.layoutPrize.shareBonus = it.shareBonus

            binding.layoutGoodLuck.luckyDrawType = it.type
            binding.layoutGoodLuck.shareBonus = it.shareBonus
        })

        luckyDrawViewModel.countFailed.observe(this, Observer {
            binding.layoutGoodLuck.hasFailed = it > 1
        })

        luckyDrawViewModel.showPrize.observe(this, Observer { prize ->
            flip()

            binding.layoutPrize.gone = prize.type == LuckyDrawPrize.TYPE_GOOD_LUCK
            binding.layoutGoodLuck.gone = prize.type != LuckyDrawPrize.TYPE_GOOD_LUCK

            binding.layoutPrize.prize = prize
            binding.layoutGoodLuck.prize = prize
        })

        luckyDrawViewModel.showBallot.observe(this, Observer { ballot ->
            binding.layoutPrize.ballot = ballot
            binding.layoutGoodLuck.ballot = ballot
        })

        if (arguments?.getBoolean(EXTRA_DRAW_ON_OPEN) == true) {
            drawing.set(true)
            binding.root.postDelayed({
                luckyDrawViewModel.draw()
            }, 500)
        }
    }

    override fun onStart() {
        super.onStart()
        dialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
    }

    override fun onResume() {
        super.onResume()
        sensorManager.registerListener(listener, sensor, SensorManager.SENSOR_DELAY_UI)
    }

    override fun onPause() {
        super.onPause()
        sensorManager.unregisterListener(listener, sensor)
    }

    override fun onDismiss(dialog: DialogInterface?) {
        super.onDismiss(dialog)
        luckyDrawViewModel.endDraw()
    }

    private fun shake(value: Double) {
        ObjectAnimator.ofFloat(binding.layoutFront, "rotation", -10F * value.toFloat(), 10F * value.toFloat(), 0F)
                .setDuration(200)
                .start()
    }

    private fun flip() {
        if (context == null) return

        AnimatorInflater.loadAnimator(requireContext(), R.animator.lucky_money_front).apply {
            setTarget(binding.layoutFront)
            start()
        }

        AnimatorInflater.loadAnimator(requireContext(), R.animator.lucky_money_back).apply {
            setTarget(binding.constraintLayout)
            start()

            doOnEnd {
                moveToBottom()
            }
        }
    }

    private fun moveToBottom() {
        if (context == null) return

        val constraintSet = ConstraintSet()
        constraintSet.clone(requireContext(), R.layout.dialog_lucky_money_open)

        val transition = ChangeBounds()
        transition.interpolator = AnticipateOvershootInterpolator(1.0f)
        transition.duration = 500
        transition.doOnEnd {
            openLid()
        }

        TransitionManager.beginDelayedTransition(binding.constraintLayout, transition)
        constraintSet.applyTo(binding.constraintLayout)
    }

    private fun openLid() {
        if (context == null) return

        binding.lid.animate()
                .rotationX(180F)
                .setDuration(300)
                .withEndAction {
                    binding.lid.alpha = 0F
                    binding.lidOpened.alpha = 1F
                    movePrizeUp()
                }
                .start()
    }

    private fun movePrizeUp() {
        if (context == null) return

        binding.layoutResult.alpha = 1F
        binding.layoutResult.apply {
            animate().translationYBy(-1F * height)
                    .setDuration(300)
                    .withEndAction {
                        binding.layoutPrize.btPrizeMyReward.isEnabled = true
                        binding.layoutPrize.btPrizeShare.isEnabled = true
                        binding.layoutGoodLuck.btPrizeShare.isEnabled = true
                        showButtonClose()
                    }
                    .start()
        }
    }

    private fun showButtonClose() {
        if (context == null) return
        binding.btClose.animate()
                .scaleX(1F)
                .scaleY(1F)
                .setDuration(200)
                .start()
    }

    private fun share() {
        luckyDrawViewModel.share()
    }
}