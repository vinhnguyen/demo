package com.zody.ui.luckydraw.money

import android.content.Context
import android.content.DialogInterface
import android.hardware.Sensor
import android.hardware.SensorManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zody.R
import com.zody.databinding.FragmentLuckyMoneyBinding
import com.zody.ui.base.BaseFragment
import com.zody.ui.luckydraw.LuckyDrawViewModel
import com.zody.ui.luckydraw.ShakeDetector
import com.zody.utils.gone
import com.zody.utils.setOnDoubleClickListener
import java.util.concurrent.atomic.AtomicBoolean
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2019, January 25
 */
class LuckyMoneyFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val luckyDrawViewModel by lazy {
        ViewModelProviders.of(requireActivity(), viewModelFactory).get(LuckyDrawViewModel::class.java)
    }

    private lateinit var binding: FragmentLuckyMoneyBinding

    private val sensorManager by lazy { requireContext().getSystemService(Context.SENSOR_SERVICE) as SensorManager }
    private val sensor by lazy { sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) }
    private val openingShakingDialog = AtomicBoolean(false)
    private val listener = ShakeDetector().apply {
        listener = { value ->
            if (value > 1.5F) {
                openShakingDialog(false)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_lucky_money, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btSpinStart.setOnDoubleClickListener {
            openShakingDialog(true)
        }

        luckyDrawViewModel.turn.observe(this, Observer { ticket ->
            binding.btSpinStart.gone = ticket <= 0
            binding.tvOutOfTicket.gone = ticket > 0
        })
    }

    override fun onResume() {
        super.onResume()
        sensorManager.registerListener(listener, sensor, SensorManager.SENSOR_DELAY_UI)
    }

    override fun onPause() {
        super.onPause()
        sensorManager.unregisterListener(listener, sensor)
    }

    private fun openShakingDialog(drawOnOpen: Boolean) {
        if (luckyDrawViewModel.turn.value ?: 0 > 0 && openingShakingDialog.compareAndSet(false, true)) {
            LuckyMoneyShakeDialog.newInstance(drawOnOpen).apply {
                onDismissListener = DialogInterface.OnDismissListener {
                    openingShakingDialog.set(false)
                }
            }.show(fragmentManager, null)
        }
    }
}