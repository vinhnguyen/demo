package com.zody.ui.luckydraw.winner

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.zody.entity.ListItem
import com.zody.entity.luckydraw.LuckyDrawWinner
import com.zody.repository.Listing
import com.zody.repository.ListingData
import com.zody.repository.LuckyDrawRepository
import com.zody.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, December 03
 */
class LuckyDrawWinnerViewModel @Inject constructor(luckyDrawRepository: LuckyDrawRepository) : BaseViewModel() {

    private val luckyDrawId = MutableLiveData<String>()


    private val result: LiveData<Listing<ListItem<LuckyDrawWinner>>> = Transformations.map(luckyDrawId) {
        luckyDrawRepository.loadWinner(it)
    }

    val data: LiveData<ListingData<ListItem<LuckyDrawWinner>>> = Transformations.switchMap(result) { it.data }

    fun init(luckyDrawId: String?) {
        if (luckyDrawId != null) this.luckyDrawId.value = luckyDrawId
    }


}