package com.zody.ui.luckydraw

import androidx.lifecycle.MediatorLiveData
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by vinhnguyen.it.vn on 2019, January 30
 */
@Singleton
class TurnLiveData @Inject constructor() : MediatorLiveData<Int>()