package com.zody.ui.luckydraw

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.PagerAdapter
import com.zody.R
import com.zody.databinding.ItemPageLuckyDrawBinding
import com.zody.entity.luckydraw.LuckyDraw

/**
 * Created by vinhnguyen.it.vn on 2018, November 30
 */
class LuckyDrawPageAdapter(val data: List<LuckyDraw>) : PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val binding = DataBindingUtil.inflate<ItemPageLuckyDrawBinding>(LayoutInflater.from(container.context), R.layout.item_page_lucky_draw, container, false)
        val luckyDraw = data[position]
        binding.luckyDraw = luckyDraw
        container.addView(binding.root)

        binding.root.setOnClickListener { view ->
            LuckyDrawActivity.start(view.context, luckyDraw.id, luckyDraw.name)
        }

        return binding.root
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        container.removeView(obj as View)
    }

    override fun isViewFromObject(view: View, obj: Any): Boolean = view == obj

    override fun getCount(): Int = data.size
}
