package com.zody.ui.luckydraw.code

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.zody.repository.LuckyDrawRepository
import com.zody.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, December 12
 */
class LuckyDrawCodeViewModel @Inject constructor(luckyDrawRepository: LuckyDrawRepository) : BaseViewModel() {

    private val luckyDrawId = MutableLiveData<String>()

    private val result = Transformations.map(luckyDrawId) {
        luckyDrawRepository.loadCode(it)
    }

    val data = Transformations.switchMap(result) { it.data }

    fun init(luckyDrawId: String?) {
        if (luckyDrawId != null) {
            this.luckyDrawId.value = luckyDrawId
        }
    }

}