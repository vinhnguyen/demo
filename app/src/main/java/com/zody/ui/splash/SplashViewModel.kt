package com.zody.ui.splash

import android.content.SharedPreferences
import androidx.lifecycle.MediatorLiveData
import com.zody.repository.ConfigurationRepository
import com.zody.ui.base.BaseViewModel
import com.zody.utils.SPKey
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 06
 */
class SplashViewModel @Inject constructor(sp: SharedPreferences,
                                          configurationRepository: ConfigurationRepository) : BaseViewModel() {

    companion object {
        const val ACTION_GO_LOGIN = 1
        const val ACTION_GO_MAIN = 2
    }

    private val token = sp.getString(SPKey.PREF_TOKEN, null)

    val nextActionLiveData = MediatorLiveData<Int>().apply {
        if (token == null) {
            value = ACTION_GO_LOGIN
        } else {
            value = ACTION_GO_MAIN
        }
    }

    init {
        token?.let { configurationRepository.getConfig() }
    }
}