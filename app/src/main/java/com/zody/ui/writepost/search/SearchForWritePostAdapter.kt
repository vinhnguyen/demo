package com.zody.ui.writepost.search

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.DiffUtil
import com.zody.R
import com.zody.databinding.ItemListSearchForWritePostAddManualBinding
import com.zody.databinding.ItemListSearchForWritePostBinding
import com.zody.databinding.ItemListSearchForWritePostEmptyBinding
import com.zody.entity.BusinessCompat
import com.zody.ui.base.DataBindingViewHolder
import com.zody.ui.base.DatabindingListAdapter

/**
 * Created by vinhnguyen.it.vn on 2018, July 13
 */
class SearchForWritePostAdapter : DatabindingListAdapter<BusinessCompat?>(DIFF_CALLBACK) {

    private var initialing = false
    private var empty = false

    var onBusinessSelected: ((BusinessCompat) -> Unit)? = null

    var onOpenAddManual: (() -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingViewHolder {
        val holder = super.onCreateViewHolder(parent, viewType)
        when (holder.binding) {
            is ItemListSearchForWritePostBinding -> {
                holder.itemView.setOnClickListener { _ ->
                    holder.binding.business?.let {
                        onBusinessSelected?.invoke(it)
                    }
                }
            }
            is ItemListSearchForWritePostAddManualBinding -> {
                holder.binding.btAddManual.setOnClickListener {
                    onOpenAddManual?.invoke()
                }
            }
            is ItemListSearchForWritePostEmptyBinding -> {
                holder.binding.btAddManual.setOnClickListener {
                    onOpenAddManual?.invoke()
                }
            }
        }
        return holder
    }

    override fun onBind(binding: ViewDataBinding, item: BusinessCompat?) {
        when (binding) {
            is ItemListSearchForWritePostBinding -> {
                binding.setVariable(BR.business, item)
            }
        }
    }

    override fun submitList(list: List<BusinessCompat?>?) {
        val oldInitialing = initialing
        initialing = list == null

        if (oldInitialing != initialing) {
            notifyDataSetChanged()
        }

        val oldEmpty = empty
        empty = list?.isEmpty() == true
        if (oldEmpty != empty) {
            notifyDataSetChanged()
        }

        super.submitList(list)
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            initialing -> R.layout.item_list_search_for_write_post_place_holder
            empty -> R.layout.item_list_search_for_write_post_empty
            position == itemCount - 1 -> R.layout.item_list_search_for_write_post_add_manual
            else -> R.layout.item_list_search_for_write_post
        }
    }

    override fun getItemCount(): Int {
        return if (initialing) return NUMBER_OF_PLACE_HOLDER else super.getItemCount() + 1
    }

    override fun getItem(position: Int): BusinessCompat? {
        return when {
            initialing -> null
            empty -> null
            position == itemCount - 1 -> null
            else -> super.getItem(position)
        }
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<BusinessCompat?>() {
            override fun areItemsTheSame(oldItem: BusinessCompat, newItem: BusinessCompat): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: BusinessCompat, newItem: BusinessCompat): Boolean {
                return oldItem == newItem
            }
        }

        const val NUMBER_OF_PLACE_HOLDER = 10
    }
}