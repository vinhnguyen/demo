package com.zody.ui.writepost.edit

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Rect
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.zody.R
import com.zody.databinding.ActivityEditPostBinding
import com.zody.ui.base.BaseActivity
import com.zody.ui.base.Navigation
import com.zody.ui.writepost.WritePostFragment
import com.zody.ui.writepost.content.OnRatingSelectListener
import com.zody.ui.writepost.hashtag.AddHashTagActivity
import com.zody.ui.writepost.media.SelectMediaActivity
import com.zody.ui.writepost.media.SelectedMediaAdapter
import com.zody.utils.*
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, October 25
 */
class EditPostActivity : BaseActivity() {

    companion object {

        fun start(context: Context, postId: String) {
            context.startActivity(Intent(context, EditPostActivity::class.java).apply {
                putExtra(Navigation.EXTRA_TARGET_VALUE, postId)
            })
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityEditPostBinding

    private val editPostViewModel: EditPostViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(EditPostViewModel::class.java)
    }
    private val adapter = SelectedMediaAdapter()

    override val screenName: String?
        get() = "edit review"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_post)
        binding.headerBar.btHeaderActionLeft.setOnClickListener { finish() }
        binding.headerBar.btHeaderActionRight.setOnClickListener {
            editPostViewModel.send()
        }

        adapter.onMediaSelected = {
            startActivityForResult(Intent(this, SelectMediaActivity::class.java).apply {
                putParcelableArrayListExtra(SelectMediaActivity.EXTRA_SELECTED_MEDIA, ArrayList(adapter.items))
            }, WritePostFragment.REQUEST_CODE_SELECT_MEDIA)
        }

        binding.layoutPostContent.recyclerView.adapter = adapter
        binding.layoutPostContent.recyclerView.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                val position = parent.getChildAdapterPosition(view)
                outRect.left = if (position.rem(5) == 0) 0 else 1.px
                outRect.top = if (position < 5) 0 else 1.px
            }
        })
        binding.layoutPostContent.etReviewContent.apply {
            afterTextChanged { editPostViewModel.setFeedback(it) }
            requestFocus()
            showKeyboard()
        }

        binding.layoutPostContent.textView2.setOnClickListener {
            openAddHashTag()
        }
        binding.layoutPostContent.tvHashTag.setOnClickListener {
            openAddHashTag()
        }
        binding.layoutPostContent.layoutRatingPoint.listener = object : OnRatingSelectListener {
            override fun onRatingSelect(point: Int) {
                editPostViewModel.setRatePoint(point)
            }
        }

        editPostViewModel.init(intent.getStringExtra(Navigation.EXTRA_TARGET_VALUE))
        editPostViewModel.business.observe(this, Observer {
            binding.layoutPostContent.business = it
        })
        editPostViewModel.point.observe(this, Observer {
            binding.layoutPostContent.point = it ?: 0
        })
        editPostViewModel.feedback.observerOne(this) {
            binding.layoutPostContent.etReviewContent.append(it ?: "")
        }
        editPostViewModel.tags.observe(this, Observer { tags ->
            binding.layoutPostContent.tvHashTag.text = tags?.joinToString(separator = " ") { "#$it" }
        })
        editPostViewModel.photos.observe(this, Observer {
            adapter.initMedia = it
            adapter.notifyDataSetChanged()
        })
        editPostViewModel.media.observe(this, Observer { media ->
            adapter.selectedMedia = media
            adapter.notifyDataSetChanged()
        })
        editPostViewModel.checkContentResult.observe(this, Observer {
            showMessage(it)
            if (it.success == true) {
                Toast.makeText(this, getString(R.string.edit_post_message), Toast.LENGTH_LONG).show()
                finish()
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == WritePostFragment.REQUEST_CODE_SELECT_MEDIA && resultCode == Activity.RESULT_OK && data != null) {
            val result = data.getParcelableArrayListExtra<Uri>(SelectMediaActivity.EXTRA_SELECTED_MEDIA)
            editPostViewModel.setMedia(result)
        } else if (requestCode == WritePostFragment.REQUEST_CODE_HASH_TAG && resultCode == Activity.RESULT_OK && data != null) {
            val tags = data.getStringArrayListExtra(AddHashTagActivity.EXTRA_HASH_TAGS)
            editPostViewModel.setTags(tags)
        }
    }

    private fun openAddHashTag() {
        startActivityForResult(Intent(this, AddHashTagActivity::class.java).apply {
            putStringArrayListExtra(AddHashTagActivity.EXTRA_HASH_TAGS, editPostViewModel.tags.value?.toArrayList())
        }, WritePostFragment.REQUEST_CODE_HASH_TAG)
    }
}