package com.zody.ui.writepost.media

import android.net.Uri
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.zody.livedata.MediaLiveData
import com.zody.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 17
 */
class SelectMediaViewModel @Inject constructor(private val mediaLiveData: MediaLiveData) : BaseViewModel() {

    private val takenPhotos = MutableLiveData<MutableList<Uri>>()

    val media = MediatorLiveData<List<Uri>>().apply {
        fun updateValue() {
            val temp = mutableListOf<Uri>()
            takenPhotos.value?.let {
                temp.addAll(it)
            }
            mediaLiveData.value?.let {
                temp.addAll(it)
            }
            value = temp
        }

        addSource(mediaLiveData) {
            updateValue()
        }
        addSource(takenPhotos) {
            updateValue()
        }
    }

    fun addTakenPhoto(value: Uri?) {
        if (value != null) {
            val temp = takenPhotos.value ?: mutableListOf()
            temp.add(0, value)
            takenPhotos.value = temp
        }
    }
}