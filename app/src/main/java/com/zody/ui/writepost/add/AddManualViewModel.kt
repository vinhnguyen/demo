package com.zody.ui.writepost.add

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.google.android.gms.location.places.Place
import com.google.android.gms.maps.model.LatLng
import com.zody.R
import com.zody.entity.BusinessCompat
import com.zody.entity.explore.Category
import com.zody.livedata.CityLiveData
import com.zody.livedata.LiveDataProviders
import com.zody.livedata.LocationLiveData
import com.zody.livedata.SingleLiveEvent
import com.zody.repository.BusinessRepository
import com.zody.repository.ExploreRepository
import com.zody.repository.Result
import com.zody.ui.base.BaseViewModel
import com.zody.utils.valueIfDifferent
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 12
 */
class AddManualViewModel @Inject constructor(app: Application,
                                             val location: LocationLiveData,
                                             val city: CityLiveData,
                                             businessRepository: BusinessRepository,
                                             exploreRepository: ExploreRepository) : BaseViewModel() {

    companion object {
        const val MINUTES_PER_HOUR = 60
    }

    val currentLatLng = MediatorLiveData<LatLng>().apply {
        addSource(location) {
            if (it != null) {
                value = LatLng(it.latitude, it.longitude)
            } else {
                city.value?.location?.apply {
                    if (latitude != null && longitude != null) {
                        value = LatLng(latitude, longitude)
                    }
                }
            }
        }
    }

    val businessCategories = exploreRepository.loadCategories()

    val name = MutableLiveData<String>()

    val mapAddress = MutableLiveData<String>()

    val address = MutableLiveData<String>()

    val phone = MutableLiveData<String>()

    val desc = MutableLiveData<String>()

    val latLng = MutableLiveData<LatLng>()

    val categories = MutableLiveData<List<Category>>()

    val openHourMinute = MutableLiveData<Int>()

    val closeHourMinute = MutableLiveData<Int>()

    private val trigger = SingleLiveEvent<Unit>()

    val result: LiveData<Result<BusinessCompat>> = Transformations.switchMap(trigger) { _ ->
        return@switchMap when {
            name.value?.isNotEmpty() != true -> LiveDataProviders.createSingleValue(Result.error(
                    message = app.getString(R.string.add_manual_error_require_x, app.getString(R.string.add_manual_name_label))))
            latLng.value == null -> LiveDataProviders.createSingleValue(Result.error(
                    message = app.getString(R.string.add_manual_error_require_select_x, app.getString(R.string.add_manual_location_label))
            ))
            address.value?.isNotEmpty() != true -> LiveDataProviders.createSingleValue(Result.error(
                    message = app.getString(R.string.add_manual_error_require_x, app.getString(R.string.add_manual_address_label))
            ))
            categories.value?.isNotEmpty() != true -> LiveDataProviders.createSingleValue(Result.error(
                    message = app.getString(R.string.add_manual_error_require_x, app.getString(R.string.add_manual_category_label))
            ))
            else -> businessRepository.addBusiness(mapOf(
                    "name" to name.value,
                    "mapAddress" to mapAddress.value,
                    "address" to address.value,
                    "phone" to phone.value,
                    "desc" to desc.value,
                    "location" to if (latLng.value == null) null else mapOf(
                            "latitude" to latLng.value?.latitude,
                            "longitude" to latLng.value?.longitude),
                    "categories" to categories.value?.map { it.id },
                    "workingHours" to if (openHourMinute.value == null || closeHourMinute.value == null) null else listOf(mapOf(
                            "openHour" to openHourMinute.value?.div(MINUTES_PER_HOUR),
                            "openMinute" to openHourMinute.value?.rem(MINUTES_PER_HOUR),
                            "closeHour" to closeHourMinute.value?.div(MINUTES_PER_HOUR),
                            "closeMinute" to closeHourMinute.value?.rem(MINUTES_PER_HOUR)))
            ))
        }
    }

    fun updatePlace(place: Place) {
        address.valueIfDifferent = place.address?.toString()
        mapAddress.valueIfDifferent = place.address?.toString()
        phone.valueIfDifferent = place.phoneNumber?.toString()
        latLng.valueIfDifferent = place.latLng
    }

    fun updateName(value: String?) {
        name.valueIfDifferent = value
    }

    fun updateOpen(value: Int) {
        openHourMinute.valueIfDifferent = value
    }

    fun updateClose(value: Int) {
        closeHourMinute.valueIfDifferent = value
    }

    fun updateCategories(value: List<Category>) {
        categories.value = value
    }

    fun send(name: String?, address: String?, phone: String?, desc: String?) {
        this.name.valueIfDifferent = if (name?.isNotEmpty() == true) name else null
        this.address.valueIfDifferent = if (address?.isNotEmpty() == true) address else null
        this.phone.valueIfDifferent = if (phone?.isNotEmpty() == true) phone else null
        this.desc.valueIfDifferent = if (desc?.isNotEmpty() == true) desc else null
        trigger.call()
    }
}