package com.zody.ui.writepost.edit

import android.app.Application
import android.net.Uri
import androidx.lifecycle.*
import com.zody.R
import com.zody.entity.Photo
import com.zody.entity.Post
import com.zody.livedata.LiveDataProviders
import com.zody.livedata.SingleLiveEvent
import com.zody.repository.PostRepository
import com.zody.repository.Result
import com.zody.repository.WritePostRepository
import com.zody.ui.base.BaseViewModel
import com.zody.utils.valueIfDifferent
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 16
 */
class EditPostViewModel @Inject constructor(app: Application,
                                            postRepository: PostRepository,
                                            writePostRepository: WritePostRepository) : BaseViewModel() {

    val postId = MutableLiveData<String>()
    val postDetail = Transformations.switchMap(postId) {
        postRepository.loadPostDetail(it)
    }
    val post = MediatorLiveData<Post>().apply {
        addSource(postDetail) { result ->
            result.data?.let { value = it }
        }
    }

    val business = Transformations.map(post) { it.business }
    val point = MediatorLiveData<Int>().apply {
        addSource(post) {
            value = it.point
        }
    }
    val feedback = MediatorLiveData<String>().apply {
        addSource(post) {
            value = it.feedback
        }
    }
    val photos = MediatorLiveData<MutableList<Photo>>().apply {
        addSource(post) {
            value = it.photos?.toMutableList()
        }
    }
    val media = MutableLiveData<MutableList<Uri>>()
    val tags = MediatorLiveData<List<String>>().apply {
        addSource(post) {
            value = it.hashtags
        }
    }

    private val checkContentTrigger = SingleLiveEvent<Unit?>()
    private val writePostObserver = Observer<Result<Unit>> { result ->
        if (result.success == true) {
            writePostRepository.editPost(
                    postId.value!!,
                    point.value,
                    feedback.value,
                    photos.value?.map { it.id },
                    media.value,
                    tags.value).observeForever {}
        }
    }
    val checkContentResult: LiveData<Result<Unit>> = Transformations.switchMap(checkContentTrigger) {
        when {
            point.value == null ->
                LiveDataProviders.createSingleValue(Result.error(
                        message = app.getString(R.string.write_post_error_missing_point)
                ))
            feedback.value?.isNotEmpty() != true && media.value?.isNotEmpty() != true ->
                LiveDataProviders.createSingleValue(Result.error(
                        message = app.getString(R.string.write_post_error_missing_feedback_media)
                ))
            feedback.value?.isNotEmpty() == true ->
                writePostRepository.checkContent(feedback.value)
            else ->
                LiveDataProviders.createSingleValue(Result.success())
        }
    }

    init {
        checkContentResult.observeForever(writePostObserver)
    }

    fun init(postId: String) {
        this.postId.valueIfDifferent = postId
    }

    fun setRatePoint(value: Int) {
        this.point.value = value
    }

    fun setFeedback(value: String) {
        this.feedback.value = value
    }

    fun setMedia(value: List<Uri>) {
        this.media.value = value.toMutableList()
    }

    fun setTags(value: List<String>?) {
        this.tags.value = value
    }

    fun send() {
        checkContentTrigger.call()
    }

    override fun onCleared() {
        super.onCleared()
        checkContentResult.removeObserver(writePostObserver)
    }
}