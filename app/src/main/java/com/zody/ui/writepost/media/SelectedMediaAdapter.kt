package com.zody.ui.writepost.media

import android.net.Uri
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.zody.R
import com.zody.databinding.ItemListSelectedMediaBinding
import com.zody.entity.Photo
import com.zody.ui.base.DataBindingViewHolder
import com.zody.utils.px

/**
 * Created by vinhnguyen.it.vn on 2018, July 17
 */
class SelectedMediaAdapter : RecyclerView.Adapter<SelectedMediaAdapter.ViewHolder>() {

    var initMedia: MutableList<Photo>? = null
        set(value) {
            field = value
            items.clear()
            value?.asSequence()?.mapNotNull { it.url }?.map { Uri.parse(it) }?.let { items.addAll(it) }
            selectedMedia?.let { items.addAll(it) }
        }

    var selectedMedia: MutableList<Uri>? = null
        set(value) {
            field = value
            items.clear()
            initMedia?.asSequence()?.mapNotNull { it.url }?.map { Uri.parse(it) }?.let { items.addAll(it) }
            value?.let { items.addAll(it) }
        }

    var items: MutableList<Uri> = mutableListOf()

    var onMediaSelected: ((Uri?) -> Unit)? = null

    var onMediaDeleted: ((Uri) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount() = items.size + 1

    inner class ViewHolder(parent: ViewGroup) : DataBindingViewHolder(parent, R.layout.item_list_selected_media) {

        private val size: Int = (parent.width - 4.px) / 5
        private var item: Uri? = null

        init {
            (binding as ItemListSelectedMediaBinding).apply {
                itemView.setOnClickListener {
                    onMediaSelected?.invoke(item)
                }
                btSelectMediaDelete.setOnClickListener { _ ->
                    item?.let { uri ->
                        onMediaDeleted?.invoke(uri)
                        items.remove(uri)
                        if (selectedMedia?.remove(uri) != true) {
                            initMedia?.remove(initMedia?.find { it.url == uri.toString() })
                        }
                        notifyItemRemoved(adapterPosition)
                    }
                }
            }
        }

        fun onBind() {
            val position = adapterPosition
            item = if (position == 0) null else items[position - 1]
            if (binding is ItemListSelectedMediaBinding) {
                if (item != null) {
                    binding.imageView.scaleType = ImageView.ScaleType.CENTER_CROP
                    Glide.with(binding.imageView)
                            .load(item)
                            .centerCrop()
                            .apply(RequestOptions().override(size))
                            .into(binding.imageView)
                } else {
                    binding.imageView.scaleType = ImageView.ScaleType.CENTER_INSIDE
                    Glide.with(binding.imageView)
                            .load(R.drawable.ic_selected_media_add)
                            .centerInside()
                            .into(binding.imageView)
                }
            }
        }
    }
}