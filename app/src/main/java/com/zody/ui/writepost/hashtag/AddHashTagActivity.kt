package com.zody.ui.writepost.hashtag

import android.app.Activity
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.TextWatcher
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.zody.R
import com.zody.databinding.ActivityAddHashTagBinding
import com.zody.ui.base.BaseActivity
import com.zody.utils.px
import com.zody.utils.toArrayList
import java.util.concurrent.atomic.AtomicBoolean
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 18
 */
class AddHashTagActivity : BaseActivity() {

    companion object {
        const val EXTRA_HASH_TAGS = "AddHashTagActivity.HashTag"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityAddHashTagBinding

    private val addHashTagViewModel: AddHashTagViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(AddHashTagViewModel::class.java)
    }

    override val screenName: String?
        get() = "add hashtag for review"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_hash_tag)
        binding.headerBar.btHeaderActionLeft.setOnClickListener {
            finish()
        }
        binding.headerBar.btHeaderActionRight.setOnClickListener { _ ->
            setResult(Activity.RESULT_OK, Intent().apply {
                val text = binding.etHashTag.text
                val tags = text.split(" ", "#").asSequence().filter { it.isNotEmpty() }.distinct().toList().toArrayList()
                putStringArrayListExtra(EXTRA_HASH_TAGS, tags)
            })
            finish()
        }

        val adapter = HashTagSuggestionAdapter()
        binding.recyclerView.apply {
            addItemDecoration(object : RecyclerView.ItemDecoration() {
                override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                    outRect.bottom = 1.px
                }
            })
            this.adapter = adapter
        }
        addHashTagViewModel.suggestionHashTags.observe(this, Observer {
            adapter.submitList(it?.data)
        })
        binding.btClearText.setOnClickListener { _ ->
            binding.etHashTag.text = null
        }

        binding.etHashTag.apply {
            addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {

                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    if (s == null) return
                    removeTextChangedListener(this)

                    val selection = selectionStart

                    val builder = SpannableStringBuilder()
                    val needAddSharp = AtomicBoolean(true)
                    s.subSequence(0, selection).forEach { c ->
                        if (needAddSharp.getAndSet(false) && c != '#' && c != ' ') {
                            builder.append('#')
                        }
                        builder.append(c)
                        if (c == ' ') needAddSharp.set(true)
                    }

                    val newSelection = builder.length
                    builder.append(s.subSequence(selection, s.length))
                    text = builder
                    setSelection(newSelection)

                    addTextChangedListener(this)
                }
            })
            onQueryTextListener = { text ->
                addHashTagViewModel.search(text?.trimStart('#')?.toString() ?: "")
            }
            adapter.onItemClickListener = { tag ->
                replaceText("$tag")
            }
        }

        intent.getStringArrayListExtra(EXTRA_HASH_TAGS)?.let { list ->
            binding.etHashTag.append(list.joinToString(separator = "") { "#$it " })
        }
    }
}