package com.zody.ui.writepost.search

import android.os.Handler
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Transformations
import com.zody.entity.BusinessCompat
import com.zody.livedata.CityAndLocationForApi
import com.zody.livedata.LiveDataProviders
import com.zody.repository.BusinessRepository
import com.zody.repository.Result
import com.zody.ui.base.BaseViewModel
import com.zody.utils.valueIfDifferent
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 13
 */
class SearchForWritePostViewModel @Inject constructor(cityAndLocationForApi: CityAndLocationForApi,
                                                      businessRepository: BusinessRepository) : BaseViewModel() {

    private val keywordLiveData = MediatorLiveData<String>().apply {
        addSource(cityAndLocationForApi) {
            this.value = value ?: ""
        }
    }
    private val handler = Handler()
    private var runnable: Runnable? = null

    val searchResultLiveData: LiveData<Result<List<BusinessCompat>>> = Transformations.switchMap(keywordLiveData) {
        if (it == null) {
            LiveDataProviders.createAbsent()
        } else {
            businessRepository.searchBusiness(
                    keyword = it,
                    latitude = cityAndLocationForApi.value?.latitude,
                    longitude = cityAndLocationForApi.value?.longitude,
                    city = cityAndLocationForApi.value?.city)
        }
    }

    fun search(keyword: String) {
        handler.removeCallbacks(runnable)
        keyword.trim().apply {
            if (this.isEmpty()) {
                keywordLiveData.valueIfDifferent = ""
            } else {
                runnable = Runnable {
                    keywordLiveData.valueIfDifferent = this
                }
                keywordLiveData.valueIfDifferent = null
                handler.postDelayed(runnable, 500)
            }
        }
    }
}