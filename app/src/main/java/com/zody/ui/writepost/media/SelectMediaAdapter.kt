package com.zody.ui.writepost.media

import android.net.Uri
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zody.R
import com.zody.databinding.ItemListSelectMediaBinding
import com.zody.ui.base.DataBindingViewHolder
import com.zody.utils.px

/**
 * Created by vinhnguyen.it.vn on 2018, July 17
 */
class SelectMediaAdapter : RecyclerView.Adapter<SelectMediaAdapter.ViewHolder>() {

    private var items = emptyList<Uri>()
    private var itemsWithSelectedIndex = mutableMapOf<Uri, Int?>()
    private var itemsWithPosition = mutableMapOf<Uri, Int>()

    var selected = mutableListOf<Uri>()

    private var showingItem: Pair<Uri, Int>? = null
        set(value) {
            if (field != value) {
                field = value
                onShowingItemChanged?.invoke(value?.first)
            }
        }

    var onShowingItemChanged: ((Uri?) -> Unit)? = null

    fun submitList(list: List<Uri>) {
        itemsWithPosition.clear()
        val tempMap = mutableMapOf<Uri, Int?>()
        val tempList = mutableListOf<Uri>()

        list.forEachIndexed { index, uri ->
            //Add position for each uri
            itemsWithPosition[uri] = index
            //Copy selected index from old map to new map
            //The deleted uris are removed in new map too
            tempMap[uri] = itemsWithSelectedIndex[uri]
        }


        //Copy old selected uri to new selected list
        selected.forEach {
            //Only copy the uri exist
            if (tempMap.containsKey(it)) {
                tempList.add(it)
                //Update the selected index in new map too(it changed when some selected uri deleted)
                tempMap[it] = tempList.size
            }
        }

        items = list
        selected = tempList
        itemsWithSelectedIndex = tempMap

        if ((showingItem != null && !itemsWithPosition.containsKey(showingItem!!.first)) || showingItem == null) {
            //If showing item deleted or no item is showing, need to choose and show one
            getWillShowingItem()?.let {
                showingItem = Pair(it, itemsWithPosition[it]!!)
            }
        }

        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount() = items.size

    private fun isShowing(item: Uri) = item == showingItem?.first

    private fun isSelected(item: Uri) = itemsWithSelectedIndex[item] != null

    private fun selectItem(item: Uri) {
        //Add it to selected list
        selected.add(item)
        //The selected index is equal to selected list size
        itemsWithSelectedIndex[item] = selected.size
        //Only notify change this item
        notifyItemChanged(itemsWithPosition[item]!!)
    }

    private fun unSelectItem(item: Uri) {
        //An selected item must have non-null position
        val position = itemsWithPosition[item]!!
        //An selected item must have non-null selected index
        val selectedIndex = itemsWithSelectedIndex[item]!!
        //Clear selected index
        itemsWithSelectedIndex[item] = null
        //Notify to remove number on this item first
        notifyItemChanged(position)
        //Remove it from selected list
        selected.removeAt(selectedIndex - 1)
        //Update selected index, it only effect to larger index number
        for (i in selectedIndex - 1 until selected.size) {
            val tempUri = selected[i]
            itemsWithSelectedIndex[tempUri] = i + 1
            //Only notify change for the item be effected
            notifyItemChanged(itemsWithPosition[tempUri]!!)
        }
    }

    private fun hideAndShowOtherItem() {
        getWillShowingItem()?.let {
            //Do nothing if this item is showing
            if (!isShowing(it)) {
                showItem(it)
            }
        }
    }

    private fun showItem(item: Uri) {
        //backup current showing item position, it may be null
        val backup = showingItem?.second
        //Every item has position
        val position = itemsWithPosition[item]!!
        showingItem = Pair(item, position)
        //Notify to show showing mask
        notifyItemChanged(position)
        //Notify to hide the showing mask on backup item
        backup?.let { notifyItemChanged(backup) }
        //invoke notify change
        onShowingItemChanged?.invoke(item)
    }

    private fun getWillShowingItem(): Uri? {
        //Show the last selected index item, if no item selected, show first item
        return if (selected.isNotEmpty()) selected.last() else if (items.isNotEmpty()) items.first() else null
    }

    private fun onItemClick(item: Uri) {
        //If this item is showing
        if (isShowing(item)) {
            if (isSelected(item)) {
                //If item is selected, un-select it
                unSelectItem(item)
            } else {
                //Otherwise, select it
                selectItem(item)
            }
            //hide current item and show other item
            hideAndShowOtherItem()
        } else {
            showItem(item)
            if (isSelected(item)) {
                //If selected, do nothing
            } else {
                //Otherwise, select it
                selectItem(item)
            }
        }
    }

    inner class ViewHolder(parent: ViewGroup) : DataBindingViewHolder(parent, R.layout.item_list_select_media) {

        lateinit var item: Uri

        init {
            val size = (parent.width - 4.px) / 3
            itemView.layoutParams = RecyclerView.LayoutParams(size, size)

            (binding as ItemListSelectMediaBinding).imageSize = size

            itemView.setOnClickListener { onItemClick(item) }
        }

        fun onBind() {
            val position = adapterPosition
            item = items[position]
            (binding as ItemListSelectMediaBinding).apply {
                image = item
                showing = isShowing(item)
                index = itemsWithSelectedIndex[item] ?: -1
            }
        }
    }
}