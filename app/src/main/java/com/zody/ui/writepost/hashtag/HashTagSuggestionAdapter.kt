package com.zody.ui.writepost.hashtag

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.zody.BR
import com.zody.R
import com.zody.databinding.ItemListWritePostHashTagSuggestionBinding
import com.zody.ui.base.DataBindingViewHolder
import com.zody.ui.base.DatabindingListAdapter

/**
 * Created by vinhnguyen.it.vn on 2018, September 18
 */
class HashTagSuggestionAdapter : DatabindingListAdapter<String>(DIFF_CALLBACK) {

    var onItemClickListener: ((String?) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingViewHolder {
        val holder = super.onCreateViewHolder(parent, viewType)
        if (holder.binding is ItemListWritePostHashTagSuggestionBinding) {
            holder.itemView.setOnClickListener { _ ->
                onItemClickListener?.invoke(holder.binding.tag)
            }
        }
        return holder
    }

    override fun onBind(binding: ViewDataBinding, item: String?) {
        binding.setVariable(BR.tag, item)
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_list_write_post_hash_tag_suggestion
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<String>() {
            override fun areItemsTheSame(oldItem: String, newItem: String): Boolean {
                return false
            }

            override fun areContentsTheSame(oldItem: String, newItem: String): Boolean {
                return oldItem == newItem
            }
        }
    }
}