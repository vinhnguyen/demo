package com.zody.ui.writepost.hashtag

import android.content.Context
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.TextUtils
import android.widget.MultiAutoCompleteTextView.Tokenizer
import androidx.core.content.ContextCompat
import androidx.core.text.color
import com.zody.R

/**
 * Created by vinhnguyen.it.vn on 2018, October 02
 */
class HashTagTokenizer(private val context: Context) : Tokenizer {

    override fun findTokenStart(text: CharSequence, cursor: Int): Int {
        var i = cursor

        while (i > 0 && text[i - 1] != '#' && text[i - 1] != ' ') {
            i--
        }
        while (i < cursor && text[i] == ' ') {
            i++
        }

        return if (i > 0 && text[i - 1] == '#') i - 1 else i
    }

    override fun findTokenEnd(text: CharSequence, cursor: Int): Int {
        var i = cursor
        val len = text.length

        while (i < len) {
            if (text[i] == '#' || text[i] == ' ') {
                return i
            } else {
                i++
            }
        }

        return len
    }

    override fun terminateToken(text: CharSequence): CharSequence {
        var i = text.length

        while (i > 0 && (text[i - 1] == '#' || text[i - 1] == ' ')) {
            i--
        }

        return when {
            i > 0 && (text[i - 1] == '#' || text[i - 1] == ' ') -> text
            text is Spanned -> {
                val sp = SpannableString("#$text ")
                TextUtils.copySpansFrom(text, 0, text.length,
                        Any::class.java, sp, 0)
                sp
            }
            else -> SpannableStringBuilder().color(ContextCompat.getColor(context, R.color.orange)) {
                append("#$text ")
            }
        }
    }
}