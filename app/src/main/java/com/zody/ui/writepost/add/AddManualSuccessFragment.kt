package com.zody.ui.writepost.add

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.bold
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zody.R
import com.zody.databinding.FragmentAddManualSuccessBinding
import com.zody.ui.base.BaseFragment
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 20
 */
class AddManualSuccessFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentAddManualSuccessBinding

    private val addManualViewModel: AddManualViewModel by lazy {
        ViewModelProviders.of(activity!!, viewModelFactory).get(AddManualViewModel::class.java)
    }

    override val screenName: String?
        get() = "add manual success"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_manual_success, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        addManualViewModel.result.observe(this, Observer { result ->
            result.data?.let { business ->
                binding.business = business
                binding.textView2.text = SpannableStringBuilder(getString(R.string.add_manual_success_message_1))
                        .append(" ")
                        .bold {
                            append(business.name)
                        }
                        .append(" ")
                        .append(getString(R.string.add_manual_success_message_2))
                binding.btAddManualSuccessAction.setOnClickListener {
                    activity?.apply {
                        setResult(Activity.RESULT_OK, Intent().apply {
                            putExtra(AddManualActivity.EXTRA_RESULT, addManualViewModel.result.value?.data)
                        })
                        finish()
                    }
                }
            }
        })
    }

}