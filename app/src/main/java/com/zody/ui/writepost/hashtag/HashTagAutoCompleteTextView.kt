package com.zody.ui.writepost.hashtag

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.MultiAutoCompleteTextView

/**
 * Created by vinhnguyen.it.vn on 2018, October 02
 */
class HashTagAutoCompleteTextView : MultiAutoCompleteTextView {
    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    var onQueryTextListener: ((CharSequence?) -> Unit)? = null

    private val emptyFilter: Filter by lazy {
        object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                Handler(Looper.getMainLooper()).post {
                    onQueryTextListener?.invoke(constraint?.trimStart('#'))
                }
                return FilterResults()
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {

            }
        }
    }

    init {
        setTokenizer(HashTagTokenizer(context))
        setAdapter(object : ArrayAdapter<String>(context, 0) {
            override fun getFilter(): Filter {
                return emptyFilter
            }
        })
    }

    public override fun replaceText(text: CharSequence?) {
        super.replaceText(text)
    }
}