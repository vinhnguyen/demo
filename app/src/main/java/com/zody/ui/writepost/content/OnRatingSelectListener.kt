package com.zody.ui.writepost.content

/**
 * Created by vinhnguyen.it.vn on 2018, October 25
 */
interface OnRatingSelectListener {

    fun onRatingSelect(point: Int)
}