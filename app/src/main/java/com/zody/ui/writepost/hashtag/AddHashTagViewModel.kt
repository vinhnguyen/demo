package com.zody.ui.writepost.hashtag

import android.os.Handler
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.zody.repository.HashTagRepository
import com.zody.repository.Result
import com.zody.ui.base.BaseViewModel
import com.zody.utils.valueIfDifferent
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 18
 */
class AddHashTagViewModel @Inject constructor(hashTagRepository: HashTagRepository) : BaseViewModel() {

    private val saved = mutableMapOf<String, LiveData<Result<List<String>>>>()

    private val keyword = MutableLiveData<String>().apply {
        value = null
    }

    val suggestionHashTags: LiveData<Result<List<String>>> = Transformations.switchMap(keyword) {
        val keyword = it ?: ""
        val result = saved[keyword] ?: hashTagRepository.loadSuggestionHashTag(keyword)
        saved[keyword] = result
        result
    }

    private val handler = Handler()
    private var runnable: Runnable? = null

    fun search(value: String) {
        handler.removeCallbacks(runnable)
        value.trim().apply {
            if (this.isEmpty() || saved.containsKey(this)) {
                keyword.valueIfDifferent = this
            } else {
                runnable = Runnable {
                    keyword.value = this
                }
                (suggestionHashTags as MediatorLiveData).value = null
                handler.postDelayed(runnable, 500)
            }
        }
    }
}