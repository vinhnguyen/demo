package com.zody.ui.writepost

import android.app.Application
import android.net.Uri
import androidx.lifecycle.*
import com.zody.R
import com.zody.entity.BusinessCompat
import com.zody.livedata.LiveDataProviders
import com.zody.livedata.SingleLiveEvent
import com.zody.repository.BusinessRepository
import com.zody.repository.Result
import com.zody.repository.WritePostRepository
import com.zody.ui.base.BaseViewModel
import com.zody.utils.valueIfDifferent
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 16
 */
class WritePostViewModel @Inject constructor(app: Application,
                                             private val writePostData: WritePostData,
                                             writePostRepository: WritePostRepository,
                                             businessRepository: BusinessRepository) : BaseViewModel() {

    val businessId = MutableLiveData<String>()

    val business = MediatorLiveData<BusinessCompat>().apply {
        addSource(businessId) { id ->
            if (value?.id != id) {
                val temp = businessRepository.loadBusinessCompat(id)
                addSource(temp) {
                    removeSource(temp)
                    value = it
                }
            }
        }
    }

    val point = MutableLiveData<Int>()
    val feedback = MutableLiveData<String>()
    val media = MutableLiveData<MutableList<Uri>>()
    val tags = MutableLiveData<List<String>>()

    private val writePostTrigger = SingleLiveEvent<Unit?>()
    private val writePostObserver = Observer<Result<Unit>> {
        if (it.success == true) {
            writePostData.send(business.value, point.value, feedback.value, media.value, tags.value)
        }
    }
    val checkContentResult: LiveData<Result<Unit>> = Transformations.switchMap(writePostTrigger) {
        when {
            business.value == null ->
                LiveDataProviders.createSingleValue(Result.error(
                        message = app.getString(R.string.write_post_error_missing_business)
                ))
            point.value == null ->
                LiveDataProviders.createSingleValue(Result.error(
                        message = app.getString(R.string.write_post_error_missing_point)
                ))
            feedback.value?.isNotEmpty() != true && media.value?.isNotEmpty() != true ->
                LiveDataProviders.createSingleValue(Result.error(
                        message = app.getString(R.string.write_post_error_missing_feedback_media)
                ))
            feedback.value?.isNotEmpty() == true ->
                writePostRepository.checkContent(feedback.value)
            else ->
                LiveDataProviders.createSingleValue(Result.success())
        }
    }

    init {
        checkContentResult.observeForever(writePostObserver)

        business.value = writePostData.data.value?.business
        businessId.value = business.value?.id
        point.value = writePostData.data.value?.point
        feedback.value = writePostData.data.value?.feedback
        media.value = writePostData.data.value?.media?.toMutableList()
        tags.value = writePostData.data.value?.tags
    }

    fun init(business: BusinessCompat?, point: Int?) {
        if (business != null && point != null) {
            this.business.valueIfDifferent = business
            this.businessId.valueIfDifferent = business.id
            this.point.valueIfDifferent = point
        }
    }

    fun init(businessId: String?) {
        if (businessId != null) {
            this.businessId.valueIfDifferent = businessId
        }
    }

    fun setBusiness(business: BusinessCompat) {
        this.business.valueIfDifferent = business
        this.businessId.valueIfDifferent = business.id
    }

    fun setRatePoint(value: Int) {
        this.point.value = value
    }

    fun setFeedback(value: String) {
        this.feedback.value = value
    }

    fun setMedia(value: List<Uri>) {
        this.media.value = value.toMutableList()
    }

    fun setTags(value: List<String>?) {
        this.tags.value = value
    }

    fun send() {
        writePostTrigger.call()
    }

    override fun onCleared() {
        super.onCleared()
        checkContentResult.removeObserver(writePostObserver)
    }
}