package com.zody.ui.writepost.add

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.transaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zody.R
import com.zody.databinding.ActivityAddManualBinding
import com.zody.ui.base.BaseFragmentActivity
import com.zody.utils.showMessage
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 12
 */
class AddManualActivity : BaseFragmentActivity() {

    companion object {
        const val EXTRA_NAME = "AddManualActivity.Name"

        const val EXTRA_RESULT = "AddManualActivity.Result"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityAddManualBinding

    private val addManualViewModel: AddManualViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(AddManualViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_manual)
        binding.headerBar.btHeaderActionLeft.setOnClickListener { finish() }

        addManualViewModel.updateName(intent.getStringExtra(EXTRA_NAME))

        if (supportFragmentManager.findFragmentById(R.id.container) == null) {
            supportFragmentManager.transaction {
                add(R.id.container, AddManualFragment())
            }
        }

        addManualViewModel.result.observe(this, Observer {
            showMessage(it)
            if (it.success == true) {
                supportFragmentManager.transaction {
                    replace(R.id.container, AddManualSuccessFragment())
                }
            }
        })
    }
}