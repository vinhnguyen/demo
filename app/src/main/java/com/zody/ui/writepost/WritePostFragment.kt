package com.zody.ui.writepost

import android.app.Activity
import android.content.Intent
import android.graphics.Rect
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.zody.R
import com.zody.databinding.FragmentWritePostBinding
import com.zody.ui.base.BaseFragment
import com.zody.ui.writepost.content.OnRatingSelectListener
import com.zody.ui.writepost.hashtag.AddHashTagActivity
import com.zody.ui.writepost.media.SelectMediaActivity
import com.zody.ui.writepost.media.SelectedMediaAdapter
import com.zody.utils.*
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 16
 */
class WritePostFragment : BaseFragment() {

    companion object {
        const val REQUEST_CODE_SELECT_MEDIA = 1
        const val REQUEST_CODE_HASH_TAG = 2
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentWritePostBinding
    private val adapter = SelectedMediaAdapter()
    private val writeViewModel: WritePostViewModel by lazy {
        ViewModelProviders.of(activity!!, viewModelFactory).get(WritePostViewModel::class.java)
    }

    override val screenName: String?
        get() = "write review"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_write_post, container, false)
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter.onMediaSelected = {
            startActivityForResult(Intent(context, SelectMediaActivity::class.java).apply {
                putParcelableArrayListExtra(SelectMediaActivity.EXTRA_SELECTED_MEDIA, ArrayList(adapter.items))
            }, REQUEST_CODE_SELECT_MEDIA)
        }

        binding.recyclerView.adapter = adapter
        binding.recyclerView.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                val position = parent.getChildAdapterPosition(view)
                outRect.left = if (position.rem(5) == 0) 0 else 1.px
                outRect.top = if (position < 5) 0 else 1.px
            }
        })
        binding.etReviewContent.apply {
            afterTextChanged { writeViewModel.setFeedback(it) }
            requestFocus()
            showKeyboard()
        }

        binding.textView2.setOnClickListener {
            openAddHashTag()
        }
        binding.tvHashTag.setOnClickListener {
            openAddHashTag()
        }
        binding.layoutRatingPoint.listener = object : OnRatingSelectListener {
            override fun onRatingSelect(point: Int) {
                writeViewModel.setRatePoint(point)
            }
        }

        writeViewModel.business.observe(this, Observer {
            binding.business = it
        })
        writeViewModel.point.observe(this, Observer {
            binding.point = it ?: 0
        })
        writeViewModel.feedback.observerOne(this) {
            binding.etReviewContent.append(it ?: "")
        }
        writeViewModel.tags.observe(this, Observer { tags ->
            binding.tvHashTag.text = tags?.joinToString(separator = " ") { "#$it" }
        })
        writeViewModel.media.observe(this, Observer { media ->
            adapter.selectedMedia = media
            adapter.notifyDataSetChanged()
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_SELECT_MEDIA && resultCode == Activity.RESULT_OK && data != null) {
            val result = data.getParcelableArrayListExtra<Uri>(SelectMediaActivity.EXTRA_SELECTED_MEDIA)
            writeViewModel.setMedia(result)
        } else if (requestCode == REQUEST_CODE_HASH_TAG && resultCode == Activity.RESULT_OK && data != null) {
            val tags = data.getStringArrayListExtra(AddHashTagActivity.EXTRA_HASH_TAGS)
            writeViewModel.setTags(tags)
        }
    }

    private fun openAddHashTag() {
        startActivityForResult(Intent(context, AddHashTagActivity::class.java).apply {
            putStringArrayListExtra(AddHashTagActivity.EXTRA_HASH_TAGS, writeViewModel.tags.value?.toArrayList())
        }, REQUEST_CODE_HASH_TAG)
    }
}