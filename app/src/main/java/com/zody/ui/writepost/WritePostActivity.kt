package com.zody.ui.writepost

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.transaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zody.R
import com.zody.databinding.ActivityWritePostBinding
import com.zody.entity.BusinessCompat
import com.zody.ui.base.BaseFragmentActivity
import com.zody.ui.base.Navigation
import com.zody.ui.main.MainActivity
import com.zody.ui.writepost.search.SearchForWritePostFragment
import com.zody.utils.showMessage
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 13
 */
class WritePostActivity : BaseFragmentActivity() {

    companion object {
        private const val EXTRA_BUSINESS = "WritePostActivity.Business"
        private const val EXTRA_POINT = "WritePostActivity.Point"

        fun start(context: Context, business: BusinessCompat, point: Int) {
            context.startActivity(Intent(context, WritePostActivity::class.java).apply {
                putExtra(EXTRA_BUSINESS, business)
                putExtra(EXTRA_POINT, point)
            })
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityWritePostBinding
    private val writePostViewModel by lazy { ViewModelProviders.of(this, viewModelFactory).get(WritePostViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_write_post)
        binding.headerBar.btHeaderActionLeft.setOnClickListener { finish() }

        binding.headerBar.textRight = getString(R.string.write_post_action_send)
        binding.headerBar.btHeaderActionRight.setOnClickListener {
            writePostViewModel.send()
        }

        val businessId = Navigation.getId(intent)
        if (businessId != null) {
            writePostViewModel.init(businessId)
        } else {
            val business = intent.getParcelableExtra<BusinessCompat>(EXTRA_BUSINESS)
            if (business != null) {
                writePostViewModel.init(business, intent.getIntExtra(EXTRA_POINT, 0))
            } else {
                supportFragmentManager.apply {
                    if (findFragmentById(R.id.container) == null) {
                        transaction { add(R.id.container, SearchForWritePostFragment()) }
                    }
                }
            }
        }

        writePostViewModel.businessId.observe(this, Observer {
            if (it != null) {
                val needAddBackStack = supportFragmentManager.findFragmentById(R.id.container) != null
                supportFragmentManager.transaction {
                    replace(R.id.container, WritePostFragment())
                    if (needAddBackStack) addToBackStack(null)
                }
            }
        })
        writePostViewModel.checkContentResult.observe(this, Observer {
            showMessage(it)
            if (it.success == true) {
                MainActivity.goProfile(this)
            }
        })
    }
}