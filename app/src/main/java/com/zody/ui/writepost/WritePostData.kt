package com.zody.ui.writepost

import android.app.Application
import android.net.Uri
import androidx.lifecycle.MediatorLiveData
import com.zody.R
import com.zody.entity.BusinessCompat
import com.zody.entity.Post
import com.zody.livedata.SingleLiveEvent
import com.zody.repository.Result
import com.zody.repository.WritePostRepository
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by vinhnguyen.it.vn on 2018, October 12
 */
@Singleton
class WritePostData @Inject constructor(app: Application,
                                        private val writePostRepository: WritePostRepository) {

    val data = MediatorLiveData<Data>()

    val result = MediatorLiveData<Result<Post>>().apply {
        addSource(data) {
            if (it != null) {
                addSource(writePostRepository.writePost(
                        businessId = it.business?.id,
                        point = it.point,
                        feedback = it.feedback,
                        media = it.media,
                        tags = it.tags)) { result ->
                    value = result
                }
            }
        }
    }
    val error = SingleLiveEvent<String>().apply {
        addSource(result) {
            if (!it.loading && it.success == false && it.message?.isNotEmpty() == true) {
                value = app.getString(R.string.write_post_error_message)
                Timber.e(it.message)
            }
        }
    }

    init {
        result.observeForever {
            if (it.success == true) {
                data.value = null
            }
        }
    }

    fun send(business: BusinessCompat?, point: Int?, feedback: String?, media: List<Uri>?, tags: List<String>?) {
        data.value = Data(business, point, feedback, media, tags)
    }

    class Data(val business: BusinessCompat?,
               val point: Int?,
               val feedback: String?,
               val media: List<Uri>?,
               val tags: List<String>?)
}