package com.zody.ui.writepost.media

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Rect
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.zody.R
import com.zody.databinding.ActivitySelectMediaBinding
import com.zody.ui.base.BaseActivity
import com.zody.utils.checkAndRequestPermission
import com.zody.utils.px
import com.zody.utils.requestPermission
import com.zody.utils.screenWith
import java.io.File
import java.util.*
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 17
 */
class SelectMediaActivity : BaseActivity() {

    companion object {
        const val EXTRA_SELECTED_MEDIA = "SelectMediaActivity.SelectedMedia"

        const val REQUEST_CODE_TAKE_PHOTO = 1
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivitySelectMediaBinding
    private val adapter = SelectMediaAdapter()
    private val selectMediaViewModel: SelectMediaViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(SelectMediaViewModel::class.java)
    }

    private var tempFile: Uri? = null

    override val screenName: String?
        get() = "select media"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_select_media)
        binding.headerBar.btHeaderActionLeft.setOnClickListener {
            finish()
        }
        binding.headerBar.btHeaderActionRight.setOnClickListener {
            returnResult()
        }
        binding.imageSize = screenWith

        adapter.selected = intent.getParcelableArrayListExtra(EXTRA_SELECTED_MEDIA)
        adapter.onShowingItemChanged = {
            binding.selectedUri = it
        }

        binding.recyclerView.adapter = adapter
        binding.recyclerView.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                super.getItemOffsets(outRect, view, parent, state)
                val position = parent.getChildAdapterPosition(view)
                outRect.right = if (position % 5 == 4) 0 else 1.px
                outRect.bottom = 1.px
            }
        })

        binding.btTakeNewImage.setOnClickListener {
            takePhoto()
        }

        binding.showRequestPermission = true
        checkAndRequestPermission(Manifest.permission.READ_EXTERNAL_STORAGE) {
            binding.showRequestPermission = false
            loadAndReceiveMedia()
        }
        binding.btRequestPermission.setOnClickListener {
            requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (permissions.contains(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults.firstOrNull() == PackageManager.PERMISSION_GRANTED) {
            binding.showRequestPermission = false
            loadAndReceiveMedia()
        } else if (permissions.contains(Manifest.permission.CAMERA) && grantResults.firstOrNull() == PackageManager.PERMISSION_GRANTED) {
            takePhoto()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
            binding.showRequestPermission = false
            selectMediaViewModel.addTakenPhoto(tempFile)
        }
    }

    private fun loadAndReceiveMedia() {
        selectMediaViewModel.media.observe(this, Observer {
            adapter.submitList(it)
        })
    }

    private fun returnResult() {
        val selected = adapter.selected
        val data = Intent().apply {
            putParcelableArrayListExtra(EXTRA_SELECTED_MEDIA, ArrayList(selected))
        }
        setResult(Activity.RESULT_OK, data)
        finish()
    }

    private fun takePhoto() {
        tempFile = createTempFile()
        try {
            startActivityForResult(Intent(MediaStore.ACTION_IMAGE_CAPTURE).apply {
                putExtra(MediaStore.EXTRA_OUTPUT, tempFile)
            }, REQUEST_CODE_TAKE_PHOTO)
        } catch (e: SecurityException) {
            checkAndRequestPermission(Manifest.permission.CAMERA)
        }
    }

    private fun createTempFile(): Uri {
        val file = File.createTempFile(UUID.randomUUID().toString(), ".jpg", filesDir)
        return FileProvider.getUriForFile(this, getString(R.string.provider_authorities), file)
    }
}