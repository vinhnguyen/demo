package com.zody.ui.writepost.search

import android.app.Activity
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.zody.R
import com.zody.databinding.FragmentSearchForWritePostBinding
import com.zody.entity.BusinessCompat
import com.zody.ui.base.BaseFragment
import com.zody.ui.writepost.WritePostViewModel
import com.zody.ui.writepost.add.AddManualActivity
import com.zody.utils.afterTextChanged
import com.zody.utils.hideKeyboard
import com.zody.utils.px
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 16
 */
class SearchForWritePostFragment : BaseFragment() {

    companion object {
        const val REQUEST_ADD_MANUAL = 1
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentSearchForWritePostBinding

    private val searchForWritePostViewModel: SearchForWritePostViewModel by lazy { ViewModelProviders.of(this, viewModelFactory).get(SearchForWritePostViewModel::class.java) }

    private val writePosViewModel: WritePostViewModel by lazy {
        ViewModelProviders.of(activity!!, viewModelFactory).get(WritePostViewModel::class.java)
    }

    override val screenName: String?
        get() = "search business for write review"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search_for_write_post, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val adapter = SearchForWritePostAdapter().apply {
            onOpenAddManual = {
                startActivityForResult(Intent(context, AddManualActivity::class.java).apply {
                    putExtra(AddManualActivity.EXTRA_NAME, binding.etWritePostSearch.text.toString())
                }, REQUEST_ADD_MANUAL)
            }
            onBusinessSelected = { business ->
                writePosViewModel.setBusiness(business)
            }
        }
        binding.recyclerView.adapter = adapter
        binding.recyclerView.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                super.getItemOffsets(outRect, view, parent, state)
                outRect.set(0, 0, 0, 4.px)
            }
        })

        searchForWritePostViewModel.searchResultLiveData.observe(this, Observer {
            adapter.submitList(it?.data)
        })
        binding.etWritePostSearch.afterTextChanged { text -> searchForWritePostViewModel.search(text) }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_ADD_MANUAL && resultCode == Activity.RESULT_OK && data != null) {
            data.getParcelableExtra<BusinessCompat>(AddManualActivity.EXTRA_RESULT)?.let {
                writePosViewModel.init(it, 0)
            }
        }
    }

    override fun onDestroyView() {
        view?.hideKeyboard()
        super.onDestroyView()
    }

}