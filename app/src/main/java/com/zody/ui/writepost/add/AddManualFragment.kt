package com.zody.ui.writepost.add

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.location.places.AutocompleteFilter
import com.google.android.gms.location.places.ui.PlaceAutocomplete
import com.google.android.gms.location.places.ui.PlacePicker
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.MarkerOptions
import com.zody.R
import com.zody.databinding.FragmentAddManualBinding
import com.zody.ui.base.BaseFragment
import com.zody.ui.views.SingleChoiceDialogFragment
import com.zody.utils.observerOne
import javax.inject.Inject


/**
 * Created by vinhnguyen.it.vn on 2018, September 20
 */
class AddManualFragment : BaseFragment() {

    companion object {
        private const val REQUEST_CODE_PICK_PLACE = 1
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentAddManualBinding

    private var map: GoogleMap? = null

    private val addManualViewModel: AddManualViewModel by lazy {
        ViewModelProviders.of(activity!!, viewModelFactory).get(AddManualViewModel::class.java)
    }

    override val screenName: String?
        get() = "add manual"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_manual, container, false)
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.vm = addManualViewModel

        binding.btAddManualSelectOnMap.setOnClickListener {
            startActivityForResult(
                    PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .setFilter(AutocompleteFilter.Builder()
                                    .setCountry("VN")
                                    .build())
                            .build(activity),
                    REQUEST_CODE_PICK_PLACE)
        }
        binding.tvAddManualCategory.setOnClickListener { _ ->
            addManualViewModel.businessCategories.observerOne(this) { categories ->
                val options = categories.map {
                    SingleChoiceDialogFragment.Option(key = it, text = it.name ?: "")
                }
                SingleChoiceDialogFragment.newInstance(options).apply {
                    onItemSelected = { option, _ ->
                        addManualViewModel.updateCategories(listOf(option.key))
                    }
                    show(this@AddManualFragment.fragmentManager, null)
                }
            }
        }
        binding.tvAddManualOpenHour.setOnClickListener { _ ->
            pickTime {
                addManualViewModel.updateOpen(it)
            }
        }
        binding.tvAddManualCloseHour.setOnClickListener { _ ->
            pickTime {
                addManualViewModel.updateClose(it)
            }
        }

        binding.btAddManualNext.setOnClickListener {
            addManualViewModel.send(name = binding.etAddManualName.text.toString(),
                    address = binding.tvAddManualAddress.text.toString(),
                    phone = binding.tvAddManualPhone.text.toString(),
                    desc = binding.tvAddManualDesc.text.toString())
        }
        (childFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment)?.getMapAsync { map ->
            this.map = map
            initMap()
        }

        addManualViewModel.categories.observe(this, Observer { categories ->
            binding.tvAddManualCategory.text = categories.mapNotNull { it.name }.joinToString()
        })
        addManualViewModel.latLng.observe(this, Observer { latLng ->
            map?.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17F))
            map?.addMarker(MarkerOptions()
                    .position(latLng)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker))
            )
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_PICK_PLACE && resultCode == Activity.RESULT_OK) {
            PlacePicker.getPlace(activity, data)?.let {
                addManualViewModel.updatePlace(it)
            }
        }
    }

    private fun initMap() {
        addManualViewModel.currentLatLng.observe(this, Observer {
            map?.moveCamera(CameraUpdateFactory.newLatLngZoom(it, 17F))
        })
    }

    private fun pickTime(callback: (Int) -> Unit) {
        val minutePerHour = 60
        val step = arrayOf(0, 30)

        val options = mutableListOf<SingleChoiceDialogFragment.Option<Int>>()
        repeat(24) { hour ->
            step.forEach { minute ->
                options.add(SingleChoiceDialogFragment.Option(key = hour * minutePerHour + minute,
                        text = String.format("%1$02d:%2$02d", hour, minute)))
            }
        }
        SingleChoiceDialogFragment.newInstance(options).apply {
            onItemSelected = { option, _ ->
                callback(option.key)
            }
            show(this@AddManualFragment.fragmentManager, null)
        }
    }
}