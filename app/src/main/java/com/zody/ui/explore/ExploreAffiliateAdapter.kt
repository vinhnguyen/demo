package com.zody.ui.explore

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.zody.R
import com.zody.databinding.ItemListExploreAffiliateProgramBinding
import com.zody.entity.explore.AffiliateProgram
import com.zody.ui.base.DataBindingViewHolder
import com.zody.ui.base.DatabindingListAdapter
import com.zody.ui.explore.online.OnlineShoppingActivity

/**
 * Created by vinhnguyen.it.vn on 2018, November 17
 */
class ExploreAffiliateAdapter : DatabindingListAdapter<AffiliateProgram>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingViewHolder {
        val holder = super.onCreateViewHolder(parent, viewType)
        when (holder.binding) {
            is ItemListExploreAffiliateProgramBinding -> {
                holder.itemView.setOnClickListener { view ->
                    holder.binding.program?.let {
                        OnlineShoppingActivity.start(view.context, it.id)
                    }
                }
            }
        }
        return holder
    }

    override fun onBind(binding: ViewDataBinding, item: AffiliateProgram?) {
        if (binding is ItemListExploreAffiliateProgramBinding) {
            binding.program = item
        }
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_list_explore_affiliate_program
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<AffiliateProgram>() {
            override fun areItemsTheSame(oldItem: AffiliateProgram, newItem: AffiliateProgram): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: AffiliateProgram, newItem: AffiliateProgram): Boolean {
                return oldItem == newItem
            }
        }
    }
}