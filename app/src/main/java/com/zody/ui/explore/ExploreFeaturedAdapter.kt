package com.zody.ui.explore

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.zody.BR
import com.zody.R
import com.zody.entity.explore.BusinessFeatured
import com.zody.ui.base.DatabindingListAdapter
import com.zody.ui.business.BusinessDetailActivity
import com.zody.ui.explore.chain.ChainActivity

/**
 * Created by vinhnguyen.it.vn on 2018, September 06
 */
class ExploreFeaturedAdapter : DatabindingListAdapter<BusinessFeatured>(DIFF_CALLBACK) {

    override fun onBind(binding: ViewDataBinding, item: BusinessFeatured?) {
        binding.setVariable(BR.featured, item)

        binding.root.setOnClickListener { view ->
            item?.let {
                if (it.chain?.isNotEmpty() == true) {
                    ChainActivity.start(view.context, it.chain)
                } else if (it.business?.isNotEmpty() == true) {
                    BusinessDetailActivity.start(view.context, it.business)
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_list_explore_featured
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<BusinessFeatured>() {
            override fun areItemsTheSame(oldItem: BusinessFeatured, newItem: BusinessFeatured): Boolean {
                return oldItem.business == newItem.business && oldItem.chain == newItem.chain
            }

            override fun areContentsTheSame(oldItem: BusinessFeatured, newItem: BusinessFeatured): Boolean {
                return oldItem.logo == newItem.logo
            }

        }
    }
}