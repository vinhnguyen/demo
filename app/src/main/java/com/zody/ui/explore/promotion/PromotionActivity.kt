package com.zody.ui.explore.promotion

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zody.R
import com.zody.databinding.ActivityPromotionBinding
import com.zody.ui.base.BaseActivity
import com.zody.ui.explore.ExplorePromotionAdapter
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 06
 */
class PromotionActivity : BaseActivity() {

    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, PromotionActivity::class.java))
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityPromotionBinding

    private val promotionViewModel: PromotionViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(PromotionViewModel::class.java)
    }

    override val screenName: String?
        get() = "promotion list"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_promotion)
        binding.headerBar.btHeaderActionLeft.setOnClickListener {
            finish()
        }

        val adapter = ExplorePromotionAdapter()
        binding.recyclerView.apply {
            this.adapter = adapter
        }

        promotionViewModel.data.observe(this, Observer {
            adapter.submitList(it.pagedList)
        })
    }
}