package com.zody.ui.explore.online

import android.os.Bundle
import android.os.Handler
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.core.os.bundleOf
import androidx.core.text.bold
import androidx.databinding.DataBindingUtil
import com.zody.R
import com.zody.databinding.DialogOnlineShoppingRedirectBinding
import com.zody.ui.base.BaseDialogFragment
import com.zody.utils.openUrl


/**
 * Created by vinhnguyen.it.vn on 2018, September 08
 */
class OnlineShoppingRedirectDialog : BaseDialogFragment() {

    companion object {
        private const val ARG_NAME = "OnlineShoppingRedirectDialog.Name"
        private const val ARG_DISCOUNT = "OnlineShoppingRedirectDialog.Discount"
        private const val ARG_URL = "OnlineShoppingRedirectActivity.Url"

        fun newInstance(name: String?, discount: String?, url: String?): OnlineShoppingRedirectDialog {
            return OnlineShoppingRedirectDialog().apply {
                arguments = bundleOf(
                        ARG_NAME to name,
                        ARG_DISCOUNT to discount,
                        ARG_URL to url
                )
            }
        }
    }

    private lateinit var binding: DialogOnlineShoppingRedirectBinding

    private val handler = Handler()
    private val runnable = Runnable {
        try {
            context?.openUrl(arguments?.getString(ARG_URL))
            dismissAllowingStateLoss()
        } catch (ignored: Exception) {

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_online_shopping_redirect, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.tvInstructionDiscount.text = arguments?.getString(ARG_DISCOUNT)

        arguments?.getString(ARG_NAME)?.let { name ->
            binding.tvInstructionRedirectMessage.text = SpannableStringBuilder()
                    .append(getString(R.string.online_shopping_redirect_message_1))
                    .append(" ")
                    .bold {
                        append(name.toUpperCase())
                    }
                    .append(getString(R.string.online_shopping_redirect_message_2))
        }

        binding.ivProgress.startAnimation(RotateAnimation(0.0f, 360.0f,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f)
                .apply {
                    interpolator = LinearInterpolator()

                    duration = 1500
                    fillAfter = true
                    repeatCount = Animation.INFINITE
                })

        handler.postDelayed(runnable, 3000)
    }

    override fun onDestroy() {
        super.onDestroy()
        handler.removeCallbacks(runnable)
    }
}