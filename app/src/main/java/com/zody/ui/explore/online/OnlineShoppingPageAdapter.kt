package com.zody.ui.explore.online

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

/**
 * Created by vinhnguyen.it.vn on 2018, September 07
 */
class OnlineShoppingPageAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> OnlineShoppingIntroduceFragment()
            1 -> OnlineShoppingOrderFragment()
            else -> OnlineShoppingFaqFragment()
        }
    }

    override fun getCount(): Int = 3
}