package com.zody.ui.explore.offline

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.zody.entity.BusinessList
import com.zody.livedata.CityAndLocationForApi
import com.zody.repository.ExploreRepository
import com.zody.repository.Listing
import com.zody.repository.ListingData
import com.zody.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 06
 */
class BusinessViewModel @Inject constructor(cityAndLocationForApi: CityAndLocationForApi,
                                            private val exploreRepository: ExploreRepository) : BaseViewModel() {

    val category = MutableLiveData<String>()

    private val query = MediatorLiveData<Query>().apply {
        addSource(category) {
            removeSource(cityAndLocationForApi)
            addSource(cityAndLocationForApi) { cityAndLocation ->
                value = Query(it, cityAndLocation?.city, cityAndLocation?.latitude, cityAndLocation?.longitude)
            }
        }
    }

    private val businessesResult: LiveData<Listing<BusinessList>> = Transformations.map(query) {
        exploreRepository.loadBusinessOfCategory(it.categoryId, it.city, it.latitude, it.longitude)
    }

    val businesses: LiveData<ListingData<BusinessList>> = Transformations.switchMap(businessesResult) { it.data }

    fun initCategory(id: String?) {
        this.category.value = id
    }

    private class Query(val categoryId: String,
                        val city: String?,
                        val latitude: Double?,
                        val longitude: Double?)

}