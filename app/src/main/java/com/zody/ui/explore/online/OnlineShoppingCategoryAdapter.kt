package com.zody.ui.explore.online

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import com.zody.BR
import com.zody.R
import com.zody.databinding.ItemListOnlineShoppingCategoryBinding
import com.zody.entity.explore.OnlineShoppingCategory
import com.zody.ui.base.DataBindingViewHolder
import com.zody.ui.base.DatabindingAdapter

/**
 * Created by vinhnguyen.it.vn on 2018, September 08
 */
class OnlineShoppingCategoryAdapter(private val items: List<OnlineShoppingCategory>?) : DatabindingAdapter() {

    var onItemClickListener: ((OnlineShoppingCategory) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingViewHolder {
        val holder = super.onCreateViewHolder(parent, viewType)
        if (holder.binding is ItemListOnlineShoppingCategoryBinding) {
            holder.itemView.setOnClickListener { _ ->
                holder.binding.category?.let { onItemClickListener?.invoke(it) }
            }
        }
        return holder
    }

    override fun onBind(binding: ViewDataBinding, position: Int) {
        binding.setVariable(BR.category, items?.get(position))
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_list_online_shopping_category
    }

    override fun getItemCount(): Int {
        return items?.size ?: 0
    }
}