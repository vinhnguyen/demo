package com.zody.ui.explore

import android.graphics.Color
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DiffUtil
import com.zody.R
import com.zody.databinding.*
import com.zody.ui.base.DatabindingListAdapter
import com.zody.ui.business.BusinessDetailActivity
import com.zody.ui.explore.chain.ChainActivity
import com.zody.ui.explore.online.OnlineShoppingActivity
import com.zody.ui.explore.promotion.PromotionActivity
import com.zody.ui.explore.qrcode.QrCodeBusinessActivity
import com.zody.ui.explore.trending.TrendingActivity

/**
 * Created by vinhnguyen.it.vn on 2018, November 23
 */
class ExploreAdapter(val activity: FragmentActivity) : DatabindingListAdapter<ExploreItem>(DIFF_CALLBACK) {

    override fun onBind(binding: ViewDataBinding, item: ExploreItem?) {
        if (item == null) return
        when (binding) {
            is LayoutExploreBannersBinding -> {
                val banners = (item as BannerItem).banners
                if (binding.root.tag != banners) {
                    binding.root.tag = banners
                    binding.viewPager.adapter = BannerPageAdapter(banners)
                }
            }
            is LayoutExploreCategoryBinding -> {
                val categories = (item as CategoryItem).categories
                if (binding.root.tag != categories) {
                    binding.root.tag = categories
                    binding.recyclerView.adapter = ExploreCategoryAdapter(categories)
                }
            }
            is ItemListExploreHeaderBinding -> {
                binding.tvTopLabel.text = (item as HeaderItem).header
                binding.root.setBackgroundColor(if (item.whiteBackground) Color.WHITE else Color.TRANSPARENT)
            }
            is ItemListExploreBusinessBinding -> {
                val business = (item as BusinessItem).business
                binding.business = business
                binding.root.setOnClickListener {
                    if (business.chain?.isNotEmpty() == true) {
                        ChainActivity.start(activity, business.chain)
                    } else {
                        BusinessDetailActivity.start(activity, business.id)
                    }
                }
                binding.root.setBackgroundColor(if (item.whiteBackground) Color.WHITE else Color.TRANSPARENT)
            }
            is ItemListExploreViewMoreBinding -> {
                binding.tvViewMore.setOnClickListener {
                    when ((item as ViewMoreItem).kind) {
                        ViewMoreItem.KIND_QR_CODE -> QrCodeBusinessActivity.start(activity)
                        ViewMoreItem.KIND_TRENDING -> TrendingActivity.start(activity)
                        ViewMoreItem.KIND_PROMOTION -> PromotionActivity.start(activity)
                    }
                }
                binding.root.setBackgroundColor(if ((item as ViewMoreItem).whiteBackground) Color.WHITE else Color.TRANSPARENT)
            }
            is LayoutExploreFeaturedBinding -> {
                val business = (item as FeaturedItem).businesses
                if (binding.root.tag != business) {
                    binding.root.tag = business
                    binding.recyclerView.adapter = ExploreFeaturedAdapter().apply {
                        submitList(business)
                    }
                }
            }
            is ItemListExplorePromotionBinding -> {
                val business = (item as PromotionItem).business
                binding.business = business
                binding.root.setOnClickListener {
                    if (business.chain?.isNotEmpty() == true) {
                        ChainActivity.start(activity, business.chain)
                    } else {
                        BusinessDetailActivity.start(activity, business.id)
                    }
                }
                binding.root.setBackgroundColor(Color.WHITE)
            }
            is ItemListExploreAffiliateProgramBinding -> {
                val program = (item as AffiliateItem).affiliate
                binding.program = program
                binding.root.setOnClickListener {
                    OnlineShoppingActivity.start(activity, program.id)
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)?.type) {
            ExploreItem.TYPE_BANNER -> R.layout.layout_explore_banners
            ExploreItem.TYPE_CATEGORY -> R.layout.layout_explore_category
            ExploreItem.TYPE_HEADER -> R.layout.item_list_explore_header
            ExploreItem.TYPE_BUSINESS -> R.layout.item_list_explore_business
            ExploreItem.TYPE_VIEW_MORE -> R.layout.item_list_explore_view_more
            ExploreItem.TYPE_PROMOTION -> R.layout.item_list_explore_promotion
            ExploreItem.TYPE_AFFILIATE -> R.layout.item_list_explore_affiliate_program
            ExploreItem.TYPE_FEATURED -> R.layout.layout_explore_featured
            else -> 0
        }
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ExploreItem>() {
            override fun areItemsTheSame(oldItem: ExploreItem, newItem: ExploreItem): Boolean {
                return oldItem.type == newItem.type
            }

            override fun areContentsTheSame(oldItem: ExploreItem, newItem: ExploreItem): Boolean {
                return oldItem == newItem
            }

        }
    }
}