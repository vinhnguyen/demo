package com.zody.ui.explore.offline

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zody.R
import com.zody.databinding.ActivityBusinessBinding
import com.zody.ui.base.BaseActivity
import com.zody.ui.base.Navigation
import com.zody.ui.explore.ExploreBusinessAdapter
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 06
 */
class BusinessActivity : BaseActivity() {

    companion object {

        fun start(context: Context, categoryId: String?, name: String?) {
            context.startActivity(Intent(context, BusinessActivity::class.java).apply {
                putExtra(Navigation.EXTRA_TARGET_VALUE, categoryId)
                putExtra(Navigation.EXTRA_TARGET_TITLE, name)
            })
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityBusinessBinding

    private val businessViewModel: BusinessViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(BusinessViewModel::class.java)
    }

    override val screenName: String?
        get() = "business list"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_business)
        binding.headerBar.btHeaderActionLeft.setOnClickListener {
            finish()
        }
        binding.headerBar.title = intent.getStringExtra(Navigation.EXTRA_TARGET_TITLE)

        val adapter = ExploreBusinessAdapter()
        binding.recyclerView.adapter = adapter
        businessViewModel.businesses.observe(this, Observer {
            adapter.submitList(it.pagedList)
        })

        businessViewModel.initCategory(intent.getStringExtra(Navigation.EXTRA_TARGET_VALUE))
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.keep, R.anim.present_out)
    }
}