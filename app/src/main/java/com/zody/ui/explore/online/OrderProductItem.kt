package com.zody.ui.explore.online

import com.zody.entity.ListItem
import com.zody.entity.explore.OnlineShoppingOrder
import com.zody.entity.explore.OnlineShoppingOrderProduct

/**
 * Created by vinhnguyen.it.vn on 2018, September 10
 */
class OrderProductItem(val product: OnlineShoppingOrderProduct) : ListItem<OnlineShoppingOrder>(data = null, type = -1, next = null, order = -1)