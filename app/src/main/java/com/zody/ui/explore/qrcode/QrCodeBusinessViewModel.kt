package com.zody.ui.explore.qrcode

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.zody.entity.BusinessList
import com.zody.livedata.CityAndLocationForApi
import com.zody.repository.ExploreRepository
import com.zody.repository.Listing
import com.zody.repository.ListingData
import com.zody.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 06
 */
class QrCodeBusinessViewModel @Inject constructor(cityAndLocationForApi: CityAndLocationForApi,
                                                  private val exploreRepository: ExploreRepository) : BaseViewModel() {

    private val result: LiveData<Listing<BusinessList>> = Transformations.map(cityAndLocationForApi) {
        exploreRepository.loadQrCodeBusiness( it.city, it.latitude, it.longitude)
    }

    val data: LiveData<ListingData<BusinessList>> = Transformations.switchMap(result) { it.data }
}