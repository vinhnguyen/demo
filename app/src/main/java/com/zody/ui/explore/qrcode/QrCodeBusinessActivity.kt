package com.zody.ui.explore.qrcode

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zody.R
import com.zody.databinding.ActivityQrCodeBusinessBinding
import com.zody.ui.base.BaseActivity
import com.zody.ui.explore.ExploreBusinessAdapter
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 06
 */
class QrCodeBusinessActivity : BaseActivity() {

    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, QrCodeBusinessActivity::class.java))
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityQrCodeBusinessBinding

    private val qrCodeBusinessViewModel: QrCodeBusinessViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(QrCodeBusinessViewModel::class.java)
    }

    override val screenName: String?
        get() = "qr code business"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_qr_code_business)
        binding.headerBar.btHeaderActionLeft.setOnClickListener {
            finish()
        }

        val adapter = ExploreBusinessAdapter()
        binding.recyclerView.apply {
            this.adapter = adapter
        }

        qrCodeBusinessViewModel.data.observe(this, Observer {
            adapter.submitList(it.pagedList)
        })
    }
}