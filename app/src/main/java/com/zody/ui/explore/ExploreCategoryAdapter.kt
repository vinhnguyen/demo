package com.zody.ui.explore

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import com.zody.BR
import com.zody.R
import com.zody.databinding.ItemListExploreCategoryBinding
import com.zody.entity.explore.Category
import com.zody.ui.base.DataBindingViewHolder
import com.zody.ui.base.DatabindingAdapter
import com.zody.ui.explore.offline.BusinessActivity
import com.zody.utils.toActivity

/**
 * Created by vinhnguyen.it.vn on 2018, September 04
 */
open class ExploreCategoryAdapter(private val categories: List<Category>) : DatabindingAdapter() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingViewHolder {
        val holder = super.onCreateViewHolder(parent, viewType)
        if (holder.binding is ItemListExploreCategoryBinding) {
            holder.itemView.setOnClickListener { view ->
                holder.binding.category?.let { category ->
                    BusinessActivity.start(view.context, category.id, category.name)
                    view.context.toActivity()?.overridePendingTransition(R.anim.present_in, R.anim.keep)
                }
            }
        }
        return holder
    }

    override fun onBind(binding: ViewDataBinding, position: Int) {
        binding.setVariable(BR.category, categories[position])
    }

    override fun getItemCount(): Int {
        return categories.size
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_list_explore_category
    }

}