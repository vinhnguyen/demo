package com.zody.ui.explore.online

import com.zody.entity.ListItem
import com.zody.entity.explore.OnlineShoppingOrder

/**
 * Created by vinhnguyen.it.vn on 2018, September 10
 */
class OrderItem(data: OnlineShoppingOrder) : ListItem<OnlineShoppingOrder>(data, next = null, order = -1, type = -1) {
}