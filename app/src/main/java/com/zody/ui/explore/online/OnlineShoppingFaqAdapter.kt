package com.zody.ui.explore.online

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.appbar.AppBarLayout
import com.zody.BR
import com.zody.R
import com.zody.databinding.ItemListOnlineShoppingFaqQuestionBinding
import com.zody.entity.explore.OnlineShopping
import com.zody.entity.explore.OnlineShoppingFaq
import com.zody.ui.base.DataBindingViewHolder
import com.zody.ui.base.DatabindingAdapter
import com.zody.utils.toActivity


/**
 * Created by vinhnguyen.it.vn on 2018, September 07
 */
class OnlineShoppingFaqAdapter(onlineShopping: OnlineShopping) : DatabindingAdapter() {

    companion object {
        private const val TYPE_HEADER = 0
        private const val TYPE_QUESTION = 1
        private const val TYPE_ANSWER = 2
    }

    val items: MutableList<Pair<Int, OnlineShoppingFaq?>> = onlineShopping.faq?.map {
        TYPE_QUESTION to (it as OnlineShoppingFaq?)
    }?.toMutableList() ?: mutableListOf()

    init {
        items.add(0, TYPE_HEADER to null)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingViewHolder {
        val holder = super.onCreateViewHolder(parent, viewType)
        if (holder.binding is ItemListOnlineShoppingFaqQuestionBinding) {
            holder.itemView.setOnClickListener {
                val position = holder.adapterPosition
                val item = items[position]
                val nextItem = if (items.size > position + 1) items[position + 1] else null
                when (nextItem?.first) {
                    TYPE_ANSWER -> {
                        items.removeAt(position + 1)
                        notifyItemRemoved(position + 1)
                    }
                    else -> {
                        items.add(position + 1, TYPE_ANSWER to item.second)
                        notifyItemInserted(position + 1)

                        holder.itemView.context.toActivity()?.findViewById<AppBarLayout>(R.id.appBarLayout)?.setExpanded(false, true)
                        ((holder.itemView.parent as RecyclerView).layoutManager as LinearLayoutManager).scrollToPositionWithOffset(position, 0)
                    }
                }
            }
        }
        return holder
    }

    override fun onBind(binding: ViewDataBinding, position: Int) {
        binding.setVariable(BR.faq, items[position].second)
    }

    override fun getItemViewType(position: Int): Int {
        return when (items[position].first) {
            TYPE_QUESTION -> R.layout.item_list_online_shopping_faq_question
            TYPE_ANSWER -> R.layout.item_list_online_shopping_faq_answer
            else -> R.layout.item_list_online_shopping_faq_header
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }
}