package com.zody.ui.explore.online

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.zody.entity.explore.OnlineShopping
import com.zody.repository.ExploreRepository
import com.zody.repository.Result
import com.zody.ui.base.BaseViewModel
import com.zody.utils.valueIfDifferent
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 07
 */
class OnlineShoppingViewModel @Inject constructor(exploreRepository: ExploreRepository) : BaseViewModel() {

    private val service = MutableLiveData<String>()

    private val onlineShoppingResult: LiveData<Result<OnlineShopping>> = Transformations.switchMap(service) {
        exploreRepository.loadOnlineShopping(it)
    }

    val onlineShopping = MediatorLiveData<OnlineShopping>().apply {
        addSource(onlineShoppingResult) { result ->
            result.data?.let {
                value = it
            }
        }
    }

    private val orderListing = Transformations.map(onlineShopping) {
        exploreRepository.loadOnlineShoppingOrder("affiliate-accesstrade", it.id)
    }

    val orders = Transformations.switchMap(orderListing) { it.data }

    fun init(service: String) {
        this.service.valueIfDifferent = service
    }

}