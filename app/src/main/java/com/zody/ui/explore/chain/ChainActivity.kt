package com.zody.ui.explore.chain

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import com.zody.R
import com.zody.databinding.ActivityChainBinding
import com.zody.ui.base.BaseActivity
import com.zody.ui.base.Navigation
import javax.inject.Inject


/**
 * Created by vinhnguyen.it.vn on 2018, September 07
 */
class ChainActivity : BaseActivity() {

    companion object {

        fun start(context: Context, chainId: String) {
            context.startActivity(Intent(context, ChainActivity::class.java).apply {
                putExtra(Navigation.EXTRA_TARGET_VALUE, chainId)
            })
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityChainBinding

    private val chainViewModel: ChainViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ChainViewModel::class.java)
    }

    override val screenName: String?
        get() = "chain"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val chainId = Navigation.getId(intent)
        if (chainId == null) {
            finish()
            return
        }

        binding = DataBindingUtil.setContentView(this, R.layout.activity_chain)
        binding.headerBar.btHeaderActionLeft.setOnClickListener {
            finish()
        }

        binding.recyclerView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        chainViewModel.init(chainId)

        chainViewModel.chain.observe(this, Observer { result ->
            result.data?.let {
                binding.chain = it
                binding.recyclerView.adapter = ChainAdapter(it)
            }
        })
    }

}