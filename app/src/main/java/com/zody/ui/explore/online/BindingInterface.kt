package com.zody.ui.explore.online

import android.graphics.Color
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.widget.TextViewCompat
import androidx.databinding.BindingAdapter
import com.zody.R
import com.zody.entity.explore.OnlineShoppingOrderStatus
import com.zody.utils.dateFormat
import java.util.*

/**
 * Created by vinhnguyen.it.vn on 2018, September 11
 */
object BindingInterface {

    @JvmStatic
    @BindingAdapter("orderTime")
    fun bindOnlineShoppingOrderTime(view: TextView, time: Date?) {
        if (time == null) {
            view.text = null
            return
        }
        view.text = view.resources.getString(R.string.online_shopping_order_id, time.dateFormat())
    }

    @JvmStatic
    @BindingAdapter("orderStatus")
    fun bindOnlineShoppingOrderStatus(view: TextView, status: OnlineShoppingOrderStatus?) {
        view.text = status?.title

        val color = try {
            Color.parseColor(status?.color)
        } catch (e: Exception) {
            ContextCompat.getColor(view.context, R.color.blue)
        }

        view.setTextColor(color)
        view.background = ContextCompat.getDrawable(view.context, R.drawable.background_order_status)?.apply {
            DrawableCompat.setTint(this, color)
        }
        TextViewCompat.setCompoundDrawablesRelativeWithIntrinsicBounds(view, null, null,
                ContextCompat.getDrawable(view.context, R.drawable.ic_order_status)?.apply {
                    DrawableCompat.setTint(this, color)
                }
                , null)

    }
}