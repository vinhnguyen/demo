package com.zody.ui.explore.online

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.paging.PagedList
import androidx.recyclerview.widget.DiffUtil
import com.zody.BR
import com.zody.R
import com.zody.databinding.ItemListOnlineShoppingOrderBinding
import com.zody.entity.ListItem
import com.zody.entity.explore.OnlineShoppingOrder
import com.zody.ui.base.DataBindingViewHolder
import com.zody.ui.base.ListItemAdapter
import com.zody.ui.base.MessageDialog
import com.zody.utils.toActivity

/**
 * Created by vinhnguyen.it.vn on 2018, September 08
 */
class OnlineShoppingOrderAdapter : ListItemAdapter<OnlineShoppingOrder>(DIFF_CALLBACK) {

    private var empty = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingViewHolder {
        val holder = super.onCreateViewHolder(parent, viewType)
        if (holder.binding is ItemListOnlineShoppingOrderBinding) {
            holder.itemView.setOnClickListener { view ->
                holder.binding.order?.let { order ->
                    if (order.status?.desc?.isNotEmpty() == true) {
                        view.context.toActivity()?.supportFragmentManager?.let { fm ->
                            MessageDialog.instantiate(title = order.status.title,
                                    message = order.status.desc
                            ).show(fm, null)
                        }
                    }
                }
            }
        }
        return holder
    }

    override fun onBind(binding: ViewDataBinding, item: ListItem<OnlineShoppingOrder>?) {
        when (item) {
            is OrderItem -> binding.setVariable(BR.order, item.data)
            is OrderProductItem -> binding.setVariable(BR.product, item.product)
            is OrderCoinItem -> binding.setVariable(BR.coin, item.coin)
        }
    }

    override fun getItem(position: Int): ListItem<OnlineShoppingOrder>? {
        return when {
            empty -> null
            else -> super.getItem(position)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (empty)
            R.layout.item_list_online_shopping_order_empty
        else
            when (getItem(position)) {
                is OrderItem -> R.layout.item_list_online_shopping_order
                is OrderProductItem -> R.layout.item_list_online_shopping_order_product
                is OrderCoinItem -> R.layout.item_list_online_shopping_order_coin
                else -> R.layout.item_list_online_shopping_order_place_holder
            }
    }

    override fun getItemCount(): Int {
        return if (empty) 1 else super.getItemCount()
    }

    override fun submitList(pagedList: PagedList<ListItem<OnlineShoppingOrder>>?, endData: Boolean) {
        super.submitList(pagedList, endData)

        val oldEmpty = empty
        empty = pagedList?.isNotEmpty() != true && endData
        if (oldEmpty != empty) {
            notifyDataSetChanged()
        }
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ListItem<OnlineShoppingOrder>>() {
            override fun areItemsTheSame(oldItem: ListItem<OnlineShoppingOrder>, newItem: ListItem<OnlineShoppingOrder>): Boolean {
                return oldItem.type == newItem.type && oldItem.data?.id == newItem.data?.id
            }

            override fun areContentsTheSame(oldItem: ListItem<OnlineShoppingOrder>, newItem: ListItem<OnlineShoppingOrder>): Boolean {
                return oldItem.data == newItem.data
            }
        }
    }

}