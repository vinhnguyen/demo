package com.zody.ui.explore.online

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.ViewGroup
import androidx.core.view.children
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.tabs.TabLayout
import com.zody.R
import com.zody.databinding.ActivityOnlineShoppingBinding
import com.zody.ui.base.BaseFragmentActivity
import com.zody.ui.base.Navigation
import com.zody.ui.business.openSupport
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 07
 */
class OnlineShoppingActivity : BaseFragmentActivity() {

    companion object {
        fun start(context: Context, id: String?) {
            context.startActivity(Intent(context, OnlineShoppingActivity::class.java).apply {
                putExtra(Navigation.EXTRA_TARGET_VALUE, id)
            })
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityOnlineShoppingBinding

    private val onlineShoppingViewModel: OnlineShoppingViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(OnlineShoppingViewModel::class.java)
    }

    override val screenName: String?
        get() = "online shopping"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val onlineShoppingId = Navigation.getId(intent)
        if (onlineShoppingId == null) {
            finish()
            return
        }

        binding = DataBindingUtil.setContentView(this, R.layout.activity_online_shopping)
        binding.btHeaderActionLeft.setOnClickListener {
            finish()
        }
        binding.tabLayout.apply {
            for (tabView in (getChildAt(0) as ViewGroup).children) {
                tabView.apply { alpha = if (isSelected) 1F else 0.5F }
            }
            addOnTabSelectedListener(object : TabLayout.ViewPagerOnTabSelectedListener(binding.viewPager) {
                private fun setAlphaForView(tab: TabLayout.Tab?, alpha: Float) {
                    tab?.let {
                        (it.parent.getChildAt(0) as ViewGroup).getChildAt(it.position).alpha = alpha
                    }
                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {
                    super.onTabUnselected(tab)
                    setAlphaForView(tab, 0.5F)
                }

                override fun onTabSelected(tab: TabLayout.Tab?) {
                    super.onTabSelected(tab)
                    setAlphaForView(tab, 1F)
                }
            })
        }
        binding.viewPager.apply {
            offscreenPageLimit = 3
            addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(binding.tabLayout))
            setPageTransformer(false) { view, float ->
                if (view == getChildAt(2)) {
                    binding.btChatWithZody.translationX = float * width
                }
            }
        }
        binding.btChatWithZody.setOnClickListener {
            openSupport()
        }

        onlineShoppingViewModel.init(onlineShoppingId)
        onlineShoppingViewModel.onlineShopping.observe(this, Observer {
            binding.onlineShopping = it
            binding.viewPager.adapter = OnlineShoppingPageAdapter(supportFragmentManager)
        })

    }
}