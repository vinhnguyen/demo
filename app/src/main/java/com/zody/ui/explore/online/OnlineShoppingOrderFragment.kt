package com.zody.ui.explore.online

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.zody.R
import com.zody.databinding.FragmentOnlineShoppingOrderBinding
import com.zody.ui.base.BaseFragment
import com.zody.utils.px
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 07
 */
class OnlineShoppingOrderFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentOnlineShoppingOrderBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_online_shopping_order, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = OnlineShoppingOrderAdapter()
        binding.recyclerView.apply {
            this.adapter = adapter
            addItemDecoration(object : RecyclerView.ItemDecoration() {
                override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                    val holder = parent.getChildViewHolder(view)
                    outRect.top = when (holder.itemViewType) {
                        R.layout.item_list_online_shopping_order_place_holder -> 10.px
                        R.layout.item_list_online_shopping_order -> 10.px
                        R.layout.item_list_online_shopping_order_product -> 1.px
                        R.layout.item_list_online_shopping_order_coin -> 1.px
                        else -> 0
                    }
                }
            })
        }

        val onlineShoppingViewModel = ViewModelProviders.of(activity!!, viewModelFactory).get(OnlineShoppingViewModel::class.java)
        onlineShoppingViewModel.orders.observe(this, Observer {
            adapter.submitList(it.pagedList, it.endData)
        })
    }
}