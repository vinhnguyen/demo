package com.zody.ui.explore

import com.zody.entity.Banner
import com.zody.entity.BusinessList
import com.zody.entity.BusinessPromotion
import com.zody.entity.explore.AffiliateProgram
import com.zody.entity.explore.BusinessFeatured
import com.zody.entity.explore.Category

/**
 * Created by vinhnguyen.it.vn on 2018, November 23
 */
open class ExploreItem(val type: Int) {
    companion object {
        const val TYPE_BANNER = 1
        const val TYPE_CATEGORY = 2
        const val TYPE_HEADER = 3
        const val TYPE_BUSINESS = 4
        const val TYPE_VIEW_MORE = 5
        const val TYPE_PROMOTION = 6
        const val TYPE_FEATURED = 7
        const val TYPE_AFFILIATE = 8
    }
}

class BannerItem(val banners: List<Banner>) : ExploreItem(TYPE_BANNER) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is BannerItem) return false
        if (!super.equals(other)) return false

        if (banners != other.banners) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + banners.hashCode()
        return result
    }
}

class CategoryItem(val categories: List<Category>) : ExploreItem(TYPE_CATEGORY) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is CategoryItem) return false
        if (!super.equals(other)) return false

        if (categories != other.categories) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + categories.hashCode()
        return result
    }
}

class HeaderItem(val header: String, val whiteBackground: Boolean) : ExploreItem(TYPE_HEADER) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is HeaderItem) return false
        if (!super.equals(other)) return false

        if (header != other.header) return false
        if (whiteBackground != other.whiteBackground) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + header.hashCode()
        result = 31 * result + whiteBackground.hashCode()
        return result
    }
}

open class BusinessItem(val business: BusinessList, val whiteBackground: Boolean) : ExploreItem(TYPE_BUSINESS) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is BusinessItem) return false
        if (!super.equals(other)) return false

        if (business != other.business) return false
        if (whiteBackground != other.whiteBackground) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + business.hashCode()
        result = 31 * result + whiteBackground.hashCode()
        return result
    }
}

@Suppress("EqualsOrHashCode")
class QrCodeBusinessItem(business: BusinessList, whiteBackground: Boolean) : BusinessItem(business, whiteBackground) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is QrCodeBusinessItem) return false
        if (!super.equals(other)) return false
        return true
    }
}

@Suppress("EqualsOrHashCode")
class TrendingBusinessItem(business: BusinessList, whiteBackground: Boolean) : BusinessItem(business, whiteBackground) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is TrendingBusinessItem) return false
        if (!super.equals(other)) return false
        return true
    }

}

class ViewMoreItem(val kind: Int, val whiteBackground: Boolean) : ExploreItem(TYPE_VIEW_MORE) {

    companion object {
        const val KIND_QR_CODE = 1
        const val KIND_PROMOTION = 2
        const val KIND_TRENDING = 3
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ViewMoreItem) return false
        if (!super.equals(other)) return false

        if (whiteBackground != other.whiteBackground) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + whiteBackground.hashCode()
        return result
    }
}

class PromotionItem(val business: BusinessPromotion) : ExploreItem(TYPE_PROMOTION) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is PromotionItem) return false
        if (!super.equals(other)) return false

        if (business != other.business) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + business.hashCode()
        return result
    }
}

class FeaturedItem(val businesses: List<BusinessFeatured>) : ExploreItem(TYPE_FEATURED) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is FeaturedItem) return false
        if (!super.equals(other)) return false

        if (businesses != other.businesses) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + businesses.hashCode()
        return result
    }
}

class AffiliateItem(val affiliate: AffiliateProgram) : ExploreItem(TYPE_AFFILIATE) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is AffiliateItem) return false
        if (!super.equals(other)) return false

        if (affiliate != other.affiliate) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + affiliate.hashCode()
        return result
    }
}