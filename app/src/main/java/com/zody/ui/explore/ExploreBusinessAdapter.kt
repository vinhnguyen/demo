package com.zody.ui.explore

import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.DiffUtil
import com.zody.R
import com.zody.entity.BusinessList
import com.zody.ui.base.DatabindingPagedListAdapter
import com.zody.ui.business.BusinessDetailActivity
import com.zody.ui.explore.chain.ChainActivity

/**
 * Created by vinhnguyen.it.vn on 2018, September 04
 */
class ExploreBusinessAdapter : DatabindingPagedListAdapter<BusinessList>(DIFF_CALLBACK) {

    override fun onBind(binding: ViewDataBinding, item: BusinessList?) {
        binding.setVariable(BR.business, item)

        binding.root.setOnClickListener { view ->
            item?.let {
                if (it.chain?.isNotEmpty() == true) {
                    ChainActivity.start(view.context, it.chain)
                } else {
                    BusinessDetailActivity.start(view.context, it.id)
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_list_explore_business
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<BusinessList>() {
            override fun areItemsTheSame(oldItem: BusinessList, newItem: BusinessList): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: BusinessList, newItem: BusinessList): Boolean {
                return oldItem == newItem
            }

        }
    }
}