package com.zody.ui.explore

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.PagerAdapter
import com.zody.R
import com.zody.databinding.ItemPageBannerBinding
import com.zody.entity.Banner
import com.zody.ui.base.Navigation

/**
 * Created by vinhnguyen.it.vn on 2018, September 04
 */
class BannerPageAdapter(private val banners: List<Banner>) : PagerAdapter() {

    override fun getCount(): Int {
        return banners.size
    }

    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view == obj
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val binding: ItemPageBannerBinding = DataBindingUtil.inflate(LayoutInflater.from(container.context), R.layout.item_page_banner, container, false)
        val banner = banners[position]
        binding.photo = banner.photo
        val view = binding.root
        container.addView(view)

        view.setOnClickListener {
            Navigation.start(view.context, banner.type, banner.target, banner.url, banner.name, null)
        }
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        container.removeView(obj as View)
    }
}