package com.zody.ui.explore.newbusiness

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zody.R
import com.zody.databinding.ActivityNewBusinessBinding
import com.zody.ui.base.BaseActivity
import com.zody.ui.explore.ExploreBusinessAdapter
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 06
 */
class NewBusinessActivity : BaseActivity() {

    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, NewBusinessActivity::class.java))
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityNewBusinessBinding

    private val newBusinessViewModel: NewBusinessViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(NewBusinessViewModel::class.java)
    }

    override val screenName: String?
        get() = "new business"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_new_business)
        binding.headerBar.btHeaderActionLeft.setOnClickListener {
            finish()
        }

        val adapter = ExploreBusinessAdapter()
        binding.recyclerView.apply {
            this.adapter = adapter
        }

        newBusinessViewModel.data.observe(this, Observer {
            adapter.submitList(it.pagedList)
        })

    }
}