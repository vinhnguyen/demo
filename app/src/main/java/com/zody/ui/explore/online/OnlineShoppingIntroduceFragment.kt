package com.zody.ui.explore.online

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zody.R
import com.zody.databinding.FragmentOnlineShoppingIntroduceBinding
import com.zody.ui.base.BaseFragment
import com.zody.utils.openUrl
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 07
 */
class OnlineShoppingIntroduceFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentOnlineShoppingIntroduceBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_online_shopping_introduce, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val onlineShoppingViewModel = ViewModelProviders.of(activity!!, viewModelFactory).get(OnlineShoppingViewModel::class.java)
        onlineShoppingViewModel.onlineShopping.observe(this, Observer { onlineShopping ->
            binding.onlineShopping = onlineShopping
            binding.btGetZcoinWithBrowser.setOnClickListener {
                if (onlineShopping.discount != null) {
                    OnlineShoppingRedirectDialog.newInstance(onlineShopping.title, onlineShopping.discount, onlineShopping.url)
                            .show(fragmentManager, null)
                } else {
                    context?.openUrl(onlineShopping.url)
                }
            }
        })
    }
}