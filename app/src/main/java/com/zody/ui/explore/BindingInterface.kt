package com.zody.ui.explore

import android.text.SpannableStringBuilder
import android.widget.TextView
import androidx.core.text.bold
import androidx.databinding.BindingAdapter
import com.zody.R
import com.zody.utils.dateFormat
import com.zody.utils.decimalFormat
import java.util.*

/**
 * Created by vinhnguyen.it.vn on 2018, September 04
 */
object BindingInterface {
    @JvmStatic
    @BindingAdapter("promotionCategories")
    fun bindPromotionCategories(view: TextView, categories: List<String>?) {
        view.text = categories?.joinToString()
    }

    @JvmStatic
    @BindingAdapter("trendingNumberBill")
    fun bindTrendingNumberBill(view: TextView, numberBill: Int?) {
        if (numberBill == null) {
            view.text = null
            return
        }
        view.text = SpannableStringBuilder()
                .bold {
                    append(numberBill.decimalFormat)
                }
                .append(" ")
                .append(view.resources.getString(R.string.explore_number_bill))
    }

    @JvmStatic
    @BindingAdapter("newBusinessNumberRate")
    fun bindNewBusinessNumberRate(view: TextView, numberRate: Int?) {
        if (numberRate == null) {
            view.text = null
            return
        }
        view.text = SpannableStringBuilder()
                .bold {
                    append(numberRate.decimalFormat)
                }
                .append(" ")
                .append(view.resources.getString(R.string.new_business_number_rate))
    }

    @JvmStatic
    @BindingAdapter("promotionEndTime")
    fun bindPromotionEndTime(view: TextView, created: Date?) {
        view.text = created?.dateFormat()
    }
}