package com.zody.ui.explore

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import com.zody.BR
import com.zody.R
import com.zody.databinding.ItemListExploreMenuBinding
import com.zody.entity.explore.ExploreMenu
import com.zody.ui.base.DataBindingViewHolder
import com.zody.ui.base.DatabindingAdapter

/**
 * Created by vinhnguyen.it.vn on 2018, September 04
 */
class MenuAdapter(menu: List<ExploreMenu>) : DatabindingAdapter() {

    private val items = mutableListOf<ExploreMenu?>().apply {
        menu.forEachIndexed { index, exploreMenu ->
            add(exploreMenu)
            if (index.rem(3) == 2 && index < menu.size - 1) {
                add(null)
            }
        }
    }

    var onItemClickListener: ((ExploreMenu) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingViewHolder {
        val holder = super.onCreateViewHolder(parent, viewType)
        if (holder.binding is ItemListExploreMenuBinding) {
            holder.itemView.setOnClickListener { _ ->
                holder.binding.menu?.let {
                    onItemClickListener?.invoke(it)
                }
            }
        }
        return holder
    }

    override fun onBind(binding: ViewDataBinding, position: Int) {
        binding.setVariable(BR.menu, items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (items[position] == null) R.layout.item_list_explore_menu_line_horizontal else R.layout.item_list_explore_menu
    }

}