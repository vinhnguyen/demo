package com.zody.ui.explore.trending

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zody.R
import com.zody.databinding.ActivityTrendingBinding
import com.zody.ui.base.BaseActivity
import com.zody.ui.explore.ExploreBusinessAdapter
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 06
 */
class TrendingActivity : BaseActivity() {

    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, TrendingActivity::class.java))
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityTrendingBinding

    private val trendingViewModel: TrendingViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(TrendingViewModel::class.java)
    }

    override val screenName: String?
        get() = "trending list"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_trending)
        binding.headerBar.btHeaderActionLeft.setOnClickListener {
            finish()
        }

        val adapter = ExploreBusinessAdapter()
        binding.recyclerView.apply {
            this.adapter = adapter
        }

        trendingViewModel.data.observe(this, Observer {
            adapter.submitList(it.pagedList)
        })
    }
}