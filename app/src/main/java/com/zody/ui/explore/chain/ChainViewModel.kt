package com.zody.ui.explore.chain

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.zody.entity.business.BusinessOfChain
import com.zody.repository.ExploreRepository
import com.zody.repository.Result
import com.zody.ui.base.BaseViewModel
import com.zody.utils.valueIfDifferent
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 07
 */
class ChainViewModel @Inject constructor(exploreRepository: ExploreRepository) : BaseViewModel() {

    private val chainId = MutableLiveData<String>()

    val chain: LiveData<Result<BusinessOfChain>> = Transformations.switchMap(chainId) {
        exploreRepository.loadChain(it)
    }

    fun init(chainId: String) {
        this.chainId.valueIfDifferent = chainId
    }

}