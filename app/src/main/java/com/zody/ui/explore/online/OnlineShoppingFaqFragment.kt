package com.zody.ui.explore.online

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import com.zody.R
import com.zody.databinding.FragmentOnlineShoppingFaqBinding
import com.zody.ui.base.BaseFragment
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 07
 */
class OnlineShoppingFaqFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentOnlineShoppingFaqBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_online_shopping_faq, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerView.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))

        val onlineShoppingViewModel = ViewModelProviders.of(activity!!, viewModelFactory).get(OnlineShoppingViewModel::class.java)
        onlineShoppingViewModel.onlineShopping.observe(this, Observer {
            binding.recyclerView.adapter = OnlineShoppingFaqAdapter(it)
        })
    }
}