package com.zody.ui.explore

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zody.R
import com.zody.databinding.FragmentExploreBinding
import com.zody.ui.base.BaseFragment
import com.zody.ui.header.HeaderViewModel
import com.zody.ui.search.SearchActivity
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 04
 */
class ExploreFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentExploreBinding

    private val headerViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(HeaderViewModel::class.java)
    }
    private val exploreViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ExploreViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_explore, container, false)
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override val screenName: String?
        get() = "explore"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.headerView.init(headerViewModel, this)
        binding.headerView.binding.btHeaderActionLeft.setOnClickListener {
            startActivity(Intent(context, SearchActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            })
        }

        val adapter = ExploreAdapter(requireActivity())
        binding.recyclerView.adapter = adapter
        exploreViewModel.data.observe(this, Observer {
            adapter.submitList(it)
        })
    }
}