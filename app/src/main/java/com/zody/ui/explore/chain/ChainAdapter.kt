package com.zody.ui.explore.chain

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.zody.BR
import com.zody.R
import com.zody.databinding.ItemListBusinessOfChainBinding
import com.zody.entity.BusinessCompat
import com.zody.entity.business.BusinessOfChain
import com.zody.ui.base.DataBindingViewHolder
import com.zody.ui.base.DatabindingListAdapter
import com.zody.ui.business.BusinessDetailActivity

/**
 * Created by vinhnguyen.it.vn on 2018, September 07
 */
class ChainAdapter(private val chain: BusinessOfChain) : DatabindingListAdapter<BusinessCompat>(DIFF_CALLBACK) {

    init {
        submitList(chain.businesses)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingViewHolder {
        val holder = super.onCreateViewHolder(parent, viewType)
        if (holder.binding is ItemListBusinessOfChainBinding) {
            holder.itemView.setOnClickListener { view ->
                holder.binding.business?.id?.let {
                    BusinessDetailActivity.start(view.context, it)
                }
            }
        }
        return holder
    }

    override fun onBind(binding: ViewDataBinding, item: BusinessCompat?) {
        binding.setVariable(BR.chain, chain)
        binding.setVariable(BR.business, item)
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) R.layout.item_list_chain_cover else R.layout.item_list_business_of_chain
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + 1
    }

    override fun getItem(position: Int): BusinessCompat? {
        return if (position == 0) null else super.getItem(position - 1)
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<BusinessCompat>() {
            override fun areItemsTheSame(oldItem: BusinessCompat, newItem: BusinessCompat): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: BusinessCompat, newItem: BusinessCompat): Boolean {
                return oldItem == newItem
            }

        }
    }
}