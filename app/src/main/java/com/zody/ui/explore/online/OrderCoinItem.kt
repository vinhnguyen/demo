package com.zody.ui.explore.online

import com.zody.entity.ListItem
import com.zody.entity.explore.OnlineShoppingOrder

/**
 * Created by vinhnguyen.it.vn on 2018, September 10
 */
class OrderCoinItem(val coin: Int) : ListItem<OnlineShoppingOrder>(data = null, type = -1, next = null, order = -1)