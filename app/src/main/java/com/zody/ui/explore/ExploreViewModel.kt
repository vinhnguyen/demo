package com.zody.ui.explore

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Transformations
import com.zody.R
import com.zody.api.ApiPaging
import com.zody.entity.BusinessList
import com.zody.entity.BusinessPromotion
import com.zody.entity.explore.Category
import com.zody.entity.explore.ExploreData
import com.zody.livedata.CityAndLocationForApi
import com.zody.repository.ExploreRepository
import com.zody.repository.Result
import com.zody.ui.base.BaseViewModel
import com.zody.utils.AppExecutors
import java.util.concurrent.atomic.AtomicInteger
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 04
 */
class ExploreViewModel @Inject constructor(private val app: Application,
                                           private val appExecutors: AppExecutors,
                                           cityAndLocationForApi: CityAndLocationForApi,
                                           exploreRepository: ExploreRepository) : BaseViewModel() {

    private val categories: LiveData<List<Category>> = exploreRepository.loadCategories()

    private val exploreData: LiveData<Result<ExploreData>> = Transformations.switchMap(cityAndLocationForApi) {
        exploreRepository.loadExplorerData(it.city, it.latitude, it.longitude)
    }

    private val qrCodeBusiness: LiveData<Result<ApiPaging<BusinessList>>> = Transformations.switchMap(cityAndLocationForApi) {
        exploreRepository.loadExploreQrCodeBusiness(it.city, it.latitude, it.longitude)
    }

    private val trendingBusiness: LiveData<Result<ApiPaging<BusinessList>>> = Transformations.switchMap(cityAndLocationForApi) {
        exploreRepository.loadExploreTrending(it.city, it.latitude, it.longitude)
    }

    private val promotionBusiness: LiveData<Result<ApiPaging<BusinessPromotion>>> = Transformations.switchMap(cityAndLocationForApi) {
        exploreRepository.loadExplorePromotion(it.city, it.latitude, it.longitude)
    }

    val data = MediatorLiveData<List<ExploreItem>>().apply {
        addSource(exploreData) { result ->
            removeSource(categories)
            removeSource(qrCodeBusiness)
            removeSource(trendingBusiness)
            removeSource(promotionBusiness)

            if (result.success == true) {
                addSource(categories) { update() }

                addSource(qrCodeBusiness) {
                    if (it.success == true) {
                        update()
                    }
                }

                addSource(trendingBusiness) {
                    if (it.success == true) {
                        update()
                    }
                }

                addSource(promotionBusiness) {
                    if (it.success == true) {
                        update()
                    }
                }
            }
        }
    }

    private val id = AtomicInteger()

    private fun update() {
        val backup = id.incrementAndGet()
        appExecutors.diskIO.execute {
            val list = initData()
            if (backup == id.get()) {
                appExecutors.mainThread.execute {
                    data.value = list
                }
            }
        }
    }

    private fun initData(): List<ExploreItem> {

        val list = mutableListOf<ExploreItem>()
        val banners = exploreData.value?.data?.banners
        if (banners?.isNotEmpty() == true) {
            list.add(BannerItem(banners))
        }
        val categories = categories.value
        if (categories?.isNotEmpty() == true) {
            list.add(CategoryItem(categories))
        }
        val qrCodeBusiness = qrCodeBusiness.value?.data
        if (qrCodeBusiness?.data?.isNotEmpty() == true) {
            list.add(HeaderItem(app.getString(R.string.explore_qr_code_business_label), true))
            qrCodeBusiness.data?.forEach { list.add(QrCodeBusinessItem(it, true)) }
            if (qrCodeBusiness.endData == false) {
                list.add(ViewMoreItem(ViewMoreItem.KIND_QR_CODE, true))
            }
        }
        val promotionBusiness = promotionBusiness.value?.data
        if (promotionBusiness?.data?.isNotEmpty() == true) {
            list.add(HeaderItem(app.getString(R.string.explore_promotion_label), true))
            promotionBusiness.data?.forEach { list.add(PromotionItem(it)) }
            if (promotionBusiness.endData == false) {
                list.add(ViewMoreItem(ViewMoreItem.KIND_PROMOTION, true))
            }
        }
        val featuredBusiness = exploreData.value?.data?.featuredBrands
        if (featuredBusiness?.isNotEmpty() == true) {
            list.add(HeaderItem(app.getString(R.string.explore_featured_business_label), true))
            list.add(FeaturedItem(featuredBusiness))
        }
        val trendingBusiness = trendingBusiness.value?.data
        if (trendingBusiness?.data?.isNotEmpty() == true) {
            list.add(HeaderItem(app.getString(R.string.explore_trending_label), true))
            trendingBusiness.data?.forEach { list.add(TrendingBusinessItem(it, true)) }
            if (trendingBusiness.endData == false) {
                list.add(ViewMoreItem(ViewMoreItem.KIND_TRENDING, true))
            }
        }
        val affiliatePrograms = exploreData.value?.data?.affiliatePrograms
        if (affiliatePrograms?.isNotEmpty() == true) {
            list.add(HeaderItem(app.getString(R.string.explore_affiliate_label), false))
            affiliatePrograms.forEach { list.add(AffiliateItem(it)) }
        }
        return list
    }

}