package com.zody.ui.search

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.zody.R
import com.zody.databinding.FragmentSearchBusinessBinding
import com.zody.ui.base.BaseFragment
import com.zody.ui.business.BusinessDetailActivity
import com.zody.utils.px
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, October 15
 */
class SearchBusinessFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentSearchBusinessBinding

    private val searchViewModel: SearchViewModel by lazy {
        ViewModelProviders.of(requireActivity(), viewModelFactory).get(SearchViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search_business, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerView.apply {
            val adapter = SearchBusinessAdapter()
            adapter.onBusinessSelected = { business ->
                BusinessDetailActivity.start(requireContext(), business.id)
            }

            this.adapter = adapter
            addItemDecoration(object : RecyclerView.ItemDecoration() {
                override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                    outRect.set(0, 4.px, 0, 0)
                }
            })
            searchViewModel.business.observe(this@SearchBusinessFragment, Observer {
                adapter.submitList(it)
            })
        }
    }
}