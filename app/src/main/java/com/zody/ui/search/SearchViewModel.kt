package com.zody.ui.search

import android.os.Handler
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Transformations
import com.zody.entity.Author
import com.zody.entity.BusinessCompat
import com.zody.entity.SearchResult
import com.zody.entity.voucher.Voucher
import com.zody.livedata.CityAndLocationForApi
import com.zody.livedata.LiveDataProviders
import com.zody.repository.Result
import com.zody.repository.SearchRepository
import com.zody.ui.base.BaseViewModel
import com.zody.utils.valueIfDifferent
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 13
 */
class SearchViewModel @Inject constructor(cityAndLocationForApi: CityAndLocationForApi,
                                          searchRepository: SearchRepository) : BaseViewModel() {

    private val keywordLiveData = MediatorLiveData<String>().apply {
        addSource(cityAndLocationForApi) {
            this.value = value ?: ""
        }
    }
    private val handler = Handler()
    private var runnable: Runnable? = null

    private val searchResult: LiveData<Result<SearchResult>> = Transformations.switchMap(keywordLiveData) {
        if (it == null) {
            LiveDataProviders.createAbsent()
        } else {
            searchRepository.search(
                    keyword = it,
                    latitude = cityAndLocationForApi.value?.latitude,
                    longitude = cityAndLocationForApi.value?.longitude,
                    city = cityAndLocationForApi.value?.city)
        }
    }

    val business: LiveData<List<BusinessCompat>?> = Transformations.map(searchResult) { it?.data?.businesses }

    val author: LiveData<List<Author>?> = Transformations.map(searchResult) { it?.data?.users }

    val vouchers: LiveData<List<Voucher>?> = Transformations.map(searchResult) { it?.data?.vouchers }

    fun search(keyword: String) {
        handler.removeCallbacks(runnable)
        keyword.trim().apply {
            if (this.isEmpty()) {
                keywordLiveData.valueIfDifferent = ""
            } else {
                runnable = Runnable {
                    keywordLiveData.valueIfDifferent = this
                }
                keywordLiveData.valueIfDifferent = null
                handler.postDelayed(runnable, 500)
            }
        }
    }
}