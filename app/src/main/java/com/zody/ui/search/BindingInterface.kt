package com.zody.ui.search

import android.graphics.Typeface
import android.text.SpannableStringBuilder
import android.text.style.ImageSpan
import android.widget.TextView
import androidx.core.text.inSpans
import androidx.core.text.toSpanned
import androidx.databinding.BindingAdapter
import com.zody.R
import com.zody.entity.Author
import com.zody.utils.decimalFormat

/**
 * Created by vinhnguyen.it.vn on 2018, October 12
 */
object BindingInterface {

    @JvmStatic
    @BindingAdapter("searchTypeSelected")
    fun bindSearchTypeSelected(view: TextView, type: String?) {
        val selected = type?.equals(view.text.toString(), ignoreCase = true)
        if (selected == true) {
            view.setTypeface(null, Typeface.BOLD)
            view.isAllCaps = true
        } else {
            view.setTypeface(null, Typeface.NORMAL)
            view.isAllCaps = false
        }
    }

    @JvmStatic
    @BindingAdapter("searchAuthorStatistic")
    fun bindSearchAuthorStatistic(view: TextView, author: Author?) {
        val builder = SpannableStringBuilder()
        author?.statistic?.followers?.let {
            builder.append(view.resources.getString(R.string.search_author_number_follower, it.decimalFormat))
        }
        author?.statistic?.reviews?.let {
            if (builder.isNotEmpty()) {
                builder.append("  ")
                builder.inSpans(ImageSpan(view.context, R.drawable.dot_author_statistic)) {
                    append(" ")
                }
                builder.append("  ")
            }
            builder.append(view.resources.getString(R.string.search_author_number_review, it.decimalFormat))
        }
        view.text = builder.toSpanned()
    }
}