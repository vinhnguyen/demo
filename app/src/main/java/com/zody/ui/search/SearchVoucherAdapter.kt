package com.zody.ui.search

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.zody.BR
import com.zody.R
import com.zody.databinding.ItemListSearchVoucherBinding
import com.zody.entity.voucher.Voucher
import com.zody.ui.base.DataBindingViewHolder
import com.zody.ui.base.DatabindingListAdapter

/**
 * Created by vinhnguyen.it.vn on 2018, October 12
 */
class SearchVoucherAdapter : DatabindingListAdapter<Voucher?>(DIFF_CALLBACK) {

    private var initialing = false

    var onVoucherSelected: ((Voucher) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingViewHolder {
        val holder = super.onCreateViewHolder(parent, viewType)
        when (holder.binding) {
            is ItemListSearchVoucherBinding -> {
                holder.itemView.setOnClickListener { _ ->
                    holder.binding.voucher?.let {
                        onVoucherSelected?.invoke(it)
                    }
                }
            }
        }
        return holder
    }

    override fun onBind(binding: ViewDataBinding, item: Voucher?) {
        binding.setVariable(BR.voucher, item)
    }

    override fun submitList(list: List<Voucher?>?) {
        val oldInitialing = initialing
        initialing = list == null
        if (oldInitialing != initialing) {
            notifyDataSetChanged()
        }
        super.submitList(list)
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            initialing -> R.layout.item_list_search_voucher_place_holder
            else -> R.layout.item_list_search_voucher
        }
    }

    override fun getItemCount(): Int {
        return if (initialing) return NUMBER_OF_PLACE_HOLDER else super.getItemCount()
    }

    override fun getItem(position: Int): Voucher? {
        return when {
            initialing -> null
            else -> super.getItem(position)
        }
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Voucher?>() {
            override fun areItemsTheSame(oldItem: Voucher, newItem: Voucher): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Voucher, newItem: Voucher): Boolean {
                return oldItem == newItem
            }
        }

        const val NUMBER_OF_PLACE_HOLDER = 10
    }
}