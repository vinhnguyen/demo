package com.zody.ui.search

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.DiffUtil
import com.zody.R
import com.zody.databinding.ItemListSearchForWritePostBinding
import com.zody.entity.BusinessCompat
import com.zody.ui.base.DataBindingViewHolder
import com.zody.ui.base.DatabindingListAdapter

/**
 * Created by vinhnguyen.it.vn on 2018, July 13
 */
class SearchBusinessAdapter : DatabindingListAdapter<BusinessCompat?>(DIFF_CALLBACK) {

    private var initialing = false

    var onBusinessSelected: ((BusinessCompat) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingViewHolder {
        val holder = super.onCreateViewHolder(parent, viewType)
        when (holder.binding) {
            is ItemListSearchForWritePostBinding -> {
                holder.itemView.setOnClickListener { _ ->
                    holder.binding.business?.let {
                        onBusinessSelected?.invoke(it)
                    }
                }
            }
        }
        return holder
    }

    override fun onBind(binding: ViewDataBinding, item: BusinessCompat?) {
        when (binding) {
            is ItemListSearchForWritePostBinding -> {
                binding.setVariable(BR.business, item)
            }
        }
    }

    override fun submitList(list: List<BusinessCompat?>?) {
        val oldInitialing = initialing
        initialing = list == null
        if (oldInitialing != initialing) {
            notifyDataSetChanged()
        }
        super.submitList(list)
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            initialing -> R.layout.item_list_search_for_write_post_place_holder
            else -> R.layout.item_list_search_for_write_post
        }
    }

    override fun getItemCount(): Int {
        return if (initialing) return NUMBER_OF_PLACE_HOLDER else super.getItemCount()
    }

    override fun getItem(position: Int): BusinessCompat? {
        return when {
            initialing -> null
            else -> super.getItem(position)
        }
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<BusinessCompat?>() {
            override fun areItemsTheSame(oldItem: BusinessCompat, newItem: BusinessCompat): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: BusinessCompat, newItem: BusinessCompat): Boolean {
                return oldItem == newItem
            }
        }

        const val NUMBER_OF_PLACE_HOLDER = 10
    }
}