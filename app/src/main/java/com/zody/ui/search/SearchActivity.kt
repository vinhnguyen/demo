package com.zody.ui.search

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.ViewGroup
import androidx.core.view.children
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.tabs.TabLayout
import com.zody.R
import com.zody.databinding.ActivitySearchBinding
import com.zody.ui.base.BaseFragmentActivity
import com.zody.utils.afterTextChanged
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, August 14
 */
class SearchActivity : BaseFragmentActivity() {

    companion object {
        const val TAB_VOUCHER = 2

        const val EXTRA_SELECTED_TAB = "SearchActivity.SelectedTab"

        fun start(context: Context, tab: Int) {
            context.startActivity(Intent(context, SearchActivity::class.java).apply {
                putExtra(EXTRA_SELECTED_TAB, tab)
            })
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivitySearchBinding

    private val searchViewModel: SearchViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(SearchViewModel::class.java)
    }

    override val screenName: String?
        get() = "search"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search)
        binding.btHeaderActionLeft.setOnClickListener {
            finish()
            overridePendingTransition(0, 0)
        }
        binding.btClearText.setOnClickListener {
            binding.etSearch.text = null
        }
        binding.etSearch.afterTextChanged { text -> searchViewModel.search(text) }
        binding.etSearch.requestFocus()

        binding.tabLayout.apply {
            for (tabView in (getChildAt(0) as ViewGroup).children) {
                tabView.apply { alpha = if (isSelected) 1F else 0.5F }
            }
            addOnTabSelectedListener(object : TabLayout.ViewPagerOnTabSelectedListener(binding.viewPager) {
                private fun setAlphaForView(tab: TabLayout.Tab?, alpha: Float) {
                    tab?.let {
                        (it.parent.getChildAt(0) as ViewGroup).getChildAt(it.position).alpha = alpha
                    }
                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {
                    super.onTabUnselected(tab)
                    setAlphaForView(tab, 0.5F)
                }

                override fun onTabSelected(tab: TabLayout.Tab?) {
                    super.onTabSelected(tab)
                    setAlphaForView(tab, 1F)
                }
            })
        }
        binding.viewPager.apply {
            addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(binding.tabLayout))
            adapter = SearchPageAdapter(supportFragmentManager)

            currentItem = intent.getIntExtra(EXTRA_SELECTED_TAB, 0)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(0, 0)
    }
}