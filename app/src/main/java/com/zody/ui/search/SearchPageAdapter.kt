package com.zody.ui.search

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

/**
 * Created by vinhnguyen.it.vn on 2018, October 15
 */
class SearchPageAdapter(fm: FragmentManager?) : FragmentStatePagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> SearchBusinessFragment()
            1 -> SearchAuthorFragment()
            else -> SearchVoucherFragment()
        }
    }

    override fun getCount(): Int {
        return 3
    }
}