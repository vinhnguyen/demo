package com.zody.ui.search

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.zody.BR
import com.zody.R
import com.zody.databinding.ItemListSearchAuthorBinding
import com.zody.entity.Author
import com.zody.ui.base.DataBindingViewHolder
import com.zody.ui.base.DatabindingListAdapter

/**
 * Created by vinhnguyen.it.vn on 2018, October 12
 */
class SearchAuthorAdapter : DatabindingListAdapter<Author?>(DIFF_CALLBACK) {

    private var initialing = false

    var onAuthorSelected: ((Author) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingViewHolder {
        val holder = super.onCreateViewHolder(parent, viewType)
        when (holder.binding) {
            is ItemListSearchAuthorBinding -> {
                holder.itemView.setOnClickListener { _ ->
                    holder.binding.author?.let {
                        onAuthorSelected?.invoke(it)
                    }
                }
            }
        }
        return holder
    }

    override fun onBind(binding: ViewDataBinding, item: Author?) {
        binding.setVariable(BR.author, item)
    }

    override fun submitList(list: List<Author?>?) {
        val oldInitialing = initialing
        initialing = list == null
        if (oldInitialing != initialing) {
            notifyDataSetChanged()
        }
        super.submitList(list)
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            initialing -> R.layout.item_list_search_author_place_holder
            else -> R.layout.item_list_search_author
        }
    }

    override fun getItemCount(): Int {
        return if (initialing) return NUMBER_OF_PLACE_HOLDER else super.getItemCount()
    }

    override fun getItem(position: Int): Author? {
        return when {
            initialing -> null
            else -> super.getItem(position)
        }
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Author?>() {
            override fun areItemsTheSame(oldItem: Author, newItem: Author): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Author, newItem: Author): Boolean {
                return oldItem == newItem
            }
        }

        const val NUMBER_OF_PLACE_HOLDER = 10
    }
}