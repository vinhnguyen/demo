package com.zody.ui.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zody.R
import com.zody.databinding.FragmentSearchVoucherBinding
import com.zody.ui.base.BaseFragment
import com.zody.ui.voucher.detail.VoucherDetailActivity
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, October 15
 */
class SearchVoucherFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentSearchVoucherBinding

    private val searchViewModel: SearchViewModel by lazy {
        ViewModelProviders.of(requireActivity(), viewModelFactory).get(SearchViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search_voucher, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerView.apply {
            val adapter = SearchVoucherAdapter()
            adapter.onVoucherSelected = { voucher ->
                VoucherDetailActivity.start(requireContext(), voucher.id)
            }

            this.adapter = adapter
            searchViewModel.vouchers.observe(this@SearchVoucherFragment, Observer {
                adapter.submitList(it)
            })
        }
    }
}