package com.zody.ui.author

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.paging.PagedList
import com.zody.R
import com.zody.databinding.ItemListPostAuthorBinding
import com.zody.entity.ListItem
import com.zody.entity.Post
import com.zody.ui.expert.AuthorViewModel
import com.zody.ui.post.PostActionViewModel
import com.zody.ui.post.list.PostListAdapter

/**
 * Created by vinhnguyen.it.vn on 2018, August 03
 */
class AuthorProfilePostAdapter(authorViewModel: AuthorViewModel, postActionViewModel: PostActionViewModel) : PostListAdapter(authorViewModel, postActionViewModel) {

    private var empty = false

    override fun initViewHolder(binding: ViewDataBinding, parent: ViewGroup) {
        super.initViewHolder(binding, parent)
        when (binding) {
            is ItemListPostAuthorBinding -> {
                binding.tvUserName.setOnClickListener(null)
                binding.ivUserAvatar.setOnClickListener(null)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            initialing -> super.getItemViewType(position)
            empty -> R.layout.item_list_post_in_author_profile_empty
            else -> super.getItemViewType(position)
        }
    }

    override fun getItem(position: Int): ListItem<Post>? {
        return when {
            initialing -> super.getItem(position)
            empty -> null
            else -> super.getItem(position)
        }
    }

    override fun getItemCount(): Int {
        return if (empty) 1 else super.getItemCount()
    }

    override fun submitList(pagedList: PagedList<ListItem<Post>>?, endData: Boolean) {
        super.submitList(pagedList, endData)

        val oldEmpty = empty
        empty = pagedList?.isNotEmpty() != true && endData
        if (oldEmpty != empty) {
            notifyDataSetChanged()
        }
    }

}