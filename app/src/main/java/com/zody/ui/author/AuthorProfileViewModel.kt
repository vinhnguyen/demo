package com.zody.ui.author

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.zody.entity.Author
import com.zody.entity.ListItem
import com.zody.entity.Post
import com.zody.repository.*
import com.zody.ui.base.BaseViewModel
import com.zody.utils.valueIfDifferent
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, August 08
 */
class AuthorProfileViewModel @Inject constructor(authorProfileRepository: AuthorProfileRepository,
                                                 private val authorRepository: AuthorRepository,
                                                 postRepository: PostRepository) : BaseViewModel() {


    private val id = MutableLiveData<String>()
    private val profileResult: LiveData<Result<Author>> = Transformations.switchMap(id) {
        authorProfileRepository.loadProfile(it)
    }
    val profile: LiveData<Author> = MediatorLiveData<Author>().apply {
        addSource(profileResult) { result ->
            result.data?.let { postValue(it) }
        }
    }

    private val userPosts: LiveData<Listing<ListItem<Post>>> = Transformations.map(id) {
        postRepository.loadAuthorPost(it)
    }

    private val userPhotos: LiveData<Listing<ListItem<Post>>> = Transformations.map(id) {
        postRepository.loadAuthorPhoto(it)
    }

    val posts: LiveData<ListingData<ListItem<Post>>> = Transformations.switchMap(userPosts) {
        it.data
    }

    val photos: LiveData<ListingData<ListItem<Post>>> = Transformations.switchMap(userPhotos) {
        it.data
    }

    val result = MediatorLiveData<Result<Unit>>()

    fun initAuthorId(id: String) {
        this.id.valueIfDifferent = id
    }

    fun toggleFollow() {
        profile.value?.let { author ->
            val followResult: LiveData<Result<Unit>> = if (author.isFollowed == true)
                authorRepository.unFollowAuthor(author.id)
            else authorRepository.followAuthor(author.id)
            result.addSource(followResult) {
                if (!it.loading) result.removeSource(followResult)
                result.value = it
            }
        }
    }
}