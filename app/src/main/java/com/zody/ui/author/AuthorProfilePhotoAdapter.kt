package com.zody.ui.author

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.zody.BR
import com.zody.R
import com.zody.entity.ListItem
import com.zody.entity.Post
import com.zody.entity.PostPhotoListItem
import com.zody.ui.base.ListItemAdapter
import com.zody.ui.post.detail.PostDetailActivity

/**
 * Created by vinhnguyen.it.vn on 2018, July 30
 */
class AuthorProfilePhotoAdapter : ListItemAdapter<Post>(diffCallback = DIFF_CALLBACK) {

    override fun onBind(binding: ViewDataBinding, item: ListItem<Post>?) {
        binding.setVariable(BR.photo, (item as? PostPhotoListItem)?.photo)
        binding.root.setOnClickListener { view ->
            item?.data?.id?.let {
                PostDetailActivity.start(view.context, it)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_list_profile_photo
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ListItem<Post>>() {
            override fun areItemsTheSame(oldItem: ListItem<Post>, newItem: ListItem<Post>): Boolean {
                return (oldItem as PostPhotoListItem).photo.id == (newItem as PostPhotoListItem).photo.id
            }

            override fun areContentsTheSame(oldItem: ListItem<Post>, newItem: ListItem<Post>): Boolean {
                return (oldItem as PostPhotoListItem).photo == (newItem as PostPhotoListItem).photo
            }
        }
    }
}