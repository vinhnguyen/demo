package com.zody.ui.author

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.children
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.tabs.TabLayout
import com.zody.R
import com.zody.databinding.FragmentAuthorProfileBinding
import com.zody.entity.Author
import com.zody.ui.base.BaseFragmentWithChildFragment
import com.zody.ui.base.MessageDialog
import com.zody.ui.expert.AuthorViewModel
import com.zody.ui.user.UserActivity
import com.zody.utils.observerOne
import com.zody.utils.showMessage
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, August 08
 */
class AuthorProfileFragment : BaseFragmentWithChildFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentAuthorProfileBinding

    private val authorProfileViewModel: AuthorProfileViewModel by lazy {
        ViewModelProviders.of(activity!!, viewModelFactory).get(AuthorProfileViewModel::class.java)
    }

    private val authorViewModel: AuthorViewModel by lazy {
        ViewModelProviders.of(activity!!, viewModelFactory).get(AuthorViewModel::class.java)
    }

    override val screenName: String?
        get() = "Other profile"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_author_profile, container, false)
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.layoutInfo.btAuthorFollow.setOnClickListener { _ ->
            authorProfileViewModel.profile.observerOne(this) {
                if (it.isFollowed == false) {
                    authorViewModel.follow(it.id)
                } else {
                    confirmUnFollow(it)
                }
            }
        }

        authorProfileViewModel.profile.observe(this, Observer {
            if (activity is UserActivity) {
                activity?.findViewById<TextView>(R.id.tvHeaderTitle)?.text = it.nickname
            }
        })

        authorViewModel.message.observe(this, Observer {
            showMessage(it)
        })

        authorProfileViewModel.result.observe(this, Observer {
            showMessage(it)
        })
        binding.vm = authorProfileViewModel

        binding.tabLayout.apply {
            for (tabView in (getChildAt(0) as ViewGroup).children) {
                tabView.apply { alpha = if (isSelected) 1F else 0.5F }
            }
            addOnTabSelectedListener(object : TabLayout.ViewPagerOnTabSelectedListener(binding.viewPager) {
                private fun setAlphaForView(tab: TabLayout.Tab?, alpha: Float) {
                    tab?.let {
                        (it.parent.getChildAt(0) as ViewGroup).getChildAt(it.position).alpha = alpha
                    }
                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {
                    super.onTabUnselected(tab)
                    setAlphaForView(tab, 0.5F)
                }

                override fun onTabSelected(tab: TabLayout.Tab?) {
                    super.onTabSelected(tab)
                    setAlphaForView(tab, 1F)
                }
            })
        }
        binding.viewPager.apply {
            addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(binding.tabLayout))
            adapter = AuthorProfilePageAdapter(childFragmentManager)
        }
    }

    private fun confirmUnFollow(author: Author) {
        MessageDialog.instantiate(title = author.name,
                message = "${getString(R.string.author_profile_action_unfollow)} ${author.name}?",
                action = getString(R.string.author_profile_action_unfollow),
                onActionClick = {
                    authorViewModel.unFollow(author.id)
                }).show(fragmentManager, null)
    }
}