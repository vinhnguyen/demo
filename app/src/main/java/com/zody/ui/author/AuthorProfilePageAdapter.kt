package com.zody.ui.author

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

/**
 * Created by vinhnguyen.it.vn on 2018, July 28
 */
class AuthorProfilePageAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return if (position == 0) AuthorProfilePostFragment() else AuthorProfilePhotoFragment()
    }

    override fun getCount(): Int = 2
}