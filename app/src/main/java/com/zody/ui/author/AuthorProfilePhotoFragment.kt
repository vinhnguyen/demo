package com.zody.ui.author

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.zody.R
import com.zody.databinding.FragmentProfilePhotoBinding
import com.zody.ui.base.BaseFragment
import com.zody.utils.px
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 28
 */
class AuthorProfilePhotoFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentProfilePhotoBinding
    private val authorProfileViewModel: AuthorProfileViewModel by lazy {
        ViewModelProviders.of(activity!!, viewModelFactory).get(AuthorProfileViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile_photo, container, false)
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = AuthorProfilePhotoAdapter()
        binding.recyclerView.apply {
            binding.recyclerView.adapter = adapter
            addItemDecoration(object : RecyclerView.ItemDecoration() {

                private val dividerSize = 1.px
                private val spanCount = (layoutManager as GridLayoutManager).spanCount

                override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                    val position = parent.getChildAdapterPosition(view)
                    outRect.bottom = dividerSize
                    outRect.left = if (position.rem(spanCount) == 0) 0 else dividerSize
                    outRect.right = 0
                    outRect.top = 0
                }
            })
        }

        authorProfileViewModel.photos.observe(this, Observer {
            adapter.submitList(it.pagedList, it.endData)
        })
    }
}