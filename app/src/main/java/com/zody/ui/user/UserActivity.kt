package com.zody.ui.user

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.transaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zody.R
import com.zody.databinding.ActivityUserBinding
import com.zody.ui.author.AuthorProfileFragment
import com.zody.ui.author.AuthorProfileViewModel
import com.zody.ui.base.BaseFragmentActivity
import com.zody.ui.base.Navigation
import com.zody.ui.profile.ProfileFragment
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, August 13
 */
class UserActivity : BaseFragmentActivity() {

    companion object {

        private const val EXTRA_USER_ID = "AuthorProfileActivity.UserId"
        fun start(context: Context, id: String?) {
            context.startActivity(Intent(context, UserActivity::class.java).apply {
                putExtra(EXTRA_USER_ID, id)
            })
        }

    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityUserBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val userId = intent.getStringExtra(EXTRA_USER_ID) ?: Navigation.getId(intent)
        if (userId == null) {
            finish()
            return
        }

        binding = DataBindingUtil.setContentView(this, R.layout.activity_user)
        binding.headerBar.btHeaderActionLeft.setOnClickListener {
            finish()
        }

        val userViewModel = ViewModelProviders.of(this, viewModelFactory).get(UserViewModel::class.java)
        userViewModel.init(userId)
        userViewModel.isMe.observe(this, Observer {
            if (it == true) {
                supportFragmentManager.transaction {
                    replace(R.id.container, ProfileFragment())
                }
            } else {
                val authorProViewModel = ViewModelProviders.of(this, viewModelFactory).get(AuthorProfileViewModel::class.java)
                authorProViewModel.initAuthorId(userId)
                supportFragmentManager.transaction {
                    replace(R.id.container, AuthorProfileFragment())
                }
            }
        })
    }
}