package com.zody.ui.user

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.zody.repository.ProfileRepository
import com.zody.ui.base.BaseViewModel
import com.zody.utils.valueIfDifferent
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, August 13
 */
class UserViewModel @Inject constructor(profileRepository: ProfileRepository) : BaseViewModel() {

    private val userId = MutableLiveData<String>()

    val isMe = Transformations.switchMap(userId) { profileRepository.isMe(it) }

    fun init(userId: String) {
        this.userId.valueIfDifferent = userId
    }
}