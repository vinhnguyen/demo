package com.zody.ui.tch

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.zody.R
import com.zody.entity.TheCoffeeHouseLinkedProgram
import com.zody.livedata.LiveDataProviders
import com.zody.livedata.SingleLiveEvent
import com.zody.repository.Result
import com.zody.repository.TheCoffeeHouseRepository
import com.zody.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, October 02
 */
class TheCoffeeHouseViewModel @Inject constructor(app: Application,
                                                  theCoffeeHouseRepository: TheCoffeeHouseRepository) : BaseViewModel() {

    private val updateId = MutableLiveData<String>()
    private val deleteTrigger = SingleLiveEvent<Unit>()

    val data: LiveData<Result<TheCoffeeHouseLinkedProgram>> = theCoffeeHouseRepository.loadTheCoffeeHouseLinkedProgram()

    val updateResult: LiveData<Result<Unit>> = Transformations.switchMap(updateId) {
        if (it?.isNotEmpty() != true) {
            LiveDataProviders.createSingleValue(Result.error(message = app.getString(R.string.tch_error_empty)))
        } else {
            theCoffeeHouseRepository.update(it)
        }
    }

    val deleteResult: LiveData<Result<Unit>> = Transformations.switchMap(deleteTrigger) {
        theCoffeeHouseRepository.delete()
    }

    val tchId = MediatorLiveData<String>().apply {
        addSource(data) {
            value = it?.data?.tchId
        }
        addSource(updateResult) {
            if (it.success == true) {
                value = updateId.value
            }
        }
        addSource(deleteResult) {
            if (it.success == true) {
                value = null
            }
        }
    }

    fun send(id: String) {
        this.updateId.value = id
    }

    fun delete() {
        deleteTrigger.call()
    }
}