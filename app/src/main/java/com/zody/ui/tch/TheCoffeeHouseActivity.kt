package com.zody.ui.tch

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zody.R
import com.zody.databinding.ActivityTheCoffeeHouseBinding
import com.zody.ui.base.BaseActivity
import com.zody.ui.business.openStore
import com.zody.utils.showMessage
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, October 02
 */
class TheCoffeeHouseActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityTheCoffeeHouseBinding

    private val theCoffeeHouseViewModel: TheCoffeeHouseViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(TheCoffeeHouseViewModel::class.java)
    }

    override val screenName: String?
        get() = "the coffee house"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_the_coffee_house)
        binding.headerBar.btHeaderActionLeft.setOnClickListener {
            finish()
        }

        binding.btTCHSend.setOnClickListener {
            theCoffeeHouseViewModel.send(binding.etTCHId.text.toString().trim())
        }
        binding.btTCHDelete.setOnClickListener {
            theCoffeeHouseViewModel.delete()
        }
        binding.layoutTCHDownload.setOnClickListener {
            val packageName = "com.thecoffeehouse.guestapp"
            val intent = packageManager.getLaunchIntentForPackage(packageName)
            if (intent != null) {
                startActivity(intent)
            } else {
                openStore(packageName)
            }
        }

        theCoffeeHouseViewModel.tchId.observe(this, Observer {
            binding.id = it
        })
        theCoffeeHouseViewModel.data.observe(this, Observer {
            showMessage(it)
            binding.tch = it.data
        })
        theCoffeeHouseViewModel.updateResult.observe(this, Observer {
            showMessage(it)
        })
        theCoffeeHouseViewModel.deleteResult.observe(this, Observer {
            showMessage(it)
        })
    }

}