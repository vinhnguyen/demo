package com.zody.ui.tch

import android.text.SpannableStringBuilder
import android.widget.TextView
import androidx.core.text.bold
import androidx.databinding.BindingAdapter
import com.zody.R

/**
 * Created by vinhnguyen.it.vn on 2018, October 02
 */
object BindingInterface {

    @JvmStatic
    @BindingAdapter(value = ["step", "content"], requireAll = true)
    fun bindTchStepContent(view: TextView, step: Int?, content: String?) {
        if (step == null || content == null) {
            view.text = null
            return
        }
        view.text = SpannableStringBuilder()
                .bold {
                    append(view.resources.getString(R.string.tch_step_x))
                    append(" ")
                    append(step.toString())
                    append(": ")
                }
                .append(" $content")
    }
}