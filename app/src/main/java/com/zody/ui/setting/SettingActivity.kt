package com.zody.ui.setting

import android.content.Intent
import android.os.Bundle
import androidx.annotation.DrawableRes
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import com.zody.BuildConfig
import com.zody.R
import com.zody.databinding.ActivitySettingBinding
import com.zody.ui.base.BaseActivity
import com.zody.ui.business.openStore
import com.zody.ui.business.openSupport
import com.zody.ui.help.ZcoinCentreActivity
import com.zody.ui.profile.edit.EditProfileActivity
import com.zody.ui.tch.TheCoffeeHouseActivity
import com.zody.ui.verify.VerifyPhoneNumberActivity
import com.zody.utils.decimalFormat
import com.zody.utils.openUrl
import javax.inject.Inject

class SettingActivity : BaseActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivitySettingBinding

    private val settingViewModel: SettingViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(SettingViewModel::class.java)
    }

    override val screenName: String?
        get() = "settings"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_setting)
        binding.headerBar.btHeaderActionLeft.setOnClickListener { finish() }
        binding.version = "${BuildConfig.VERSION_NAME} (${BuildConfig.VERSION_CODE})"

        val items = mutableListOf(
                Item(Item.TYPE_EDIT_PROFILE, getString(R.string.setting_edit_profile), R.drawable.ic_settings_profile),
                Item(Item.TYPE_RATE, getString(R.string.setting_rate), R.drawable.ic_settings_rate),
                Item(Item.TYPE_INVITE_FRIEND, getString(R.string.setting_invite_friend), R.drawable.ic_settings_referral),
                Item(Item.TYPE_POLICY, getString(R.string.setting_policy), R.drawable.ic_settings_privacy),
                Item(Item.TYPE_CONTACT, getString(R.string.setting_contact), R.drawable.ic_settings_feedback),
                Item(Item.TYPE_TCH, getString(R.string.setting_tch), R.drawable.ic_setting_the_coffee_house)
        )
        val adapter = SettingAdapter(items)
        adapter.onItemClickListener = { item ->
            when (item.type) {
                Item.TYPE_EDIT_PROFILE -> goEditProfile()
                Item.TYPE_UPDATE_PHONE -> goUpdatePhone()
                Item.TYPE_RATE -> goGooglePlay()
                Item.TYPE_INVITE_FRIEND -> inviteFriend()
                Item.TYPE_POLICY -> policy()
                Item.TYPE_CONTACT -> chatWithSupport()
                Item.TYPE_TCH -> goTch()
            }
        }
        binding.recyclerView.adapter = adapter
        binding.recyclerView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        binding.tvLogout.setOnClickListener {
            settingViewModel.logout()
        }

        settingViewModel.needUpdatePhoneNumber.observe(this, Observer { needUpdatePhoneNumber ->
            val item = items.firstOrNull { it.type == Item.TYPE_UPDATE_PHONE }
            if (needUpdatePhoneNumber) {
                if (item != null) {
                } else {
                    val title = StringBuilder(getString(R.string.setting_update_phone))
                    val bonusCoin = settingViewModel.coinForVerifyPhoneSuccess
                    if (bonusCoin > 0) {
                        title.append(" ").append(getString(R.string.setting_bonus_coin, bonusCoin.decimalFormat))
                    }
                    items.add(1, Item(Item.TYPE_UPDATE_PHONE, title.toString(), R.drawable.ic_setting_update_phone))
                }
            } else if (item != null) {
                items.remove(item)
            }
            adapter.notifyDataSetChanged()
        })
    }

    private fun goEditProfile() {
        startActivity(Intent(this, EditProfileActivity::class.java))
    }

    private fun goUpdatePhone() {
        startActivity(Intent(this, VerifyPhoneNumberActivity::class.java))
    }

    private fun goGooglePlay() {
        openStore()
    }

    private fun inviteFriend() {
        startActivity(Intent(this, ZcoinCentreActivity::class.java))
    }

    private fun policy() {
        openUrl(getString(R.string.term_of_use_url))
    }

    private fun chatWithSupport() {
        openSupport()
    }

    private fun goTch() {
        startActivity(Intent(this, TheCoffeeHouseActivity::class.java))
    }

    data class Item(val type: Int,
                    val title: String,
                    @DrawableRes val icon: Int) {

        companion object {
            const val TYPE_EDIT_PROFILE = 1
            const val TYPE_UPDATE_PHONE = 2
            const val TYPE_RATE = 3
            const val TYPE_INVITE_FRIEND = 4
            const val TYPE_POLICY = 5
            const val TYPE_CONTACT = 6
            const val TYPE_TCH = 7
        }
    }

}