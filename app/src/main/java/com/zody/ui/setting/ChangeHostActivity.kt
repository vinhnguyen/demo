package com.zody.ui.setting

import android.content.SharedPreferences
import android.os.Bundle
import androidx.core.content.edit
import androidx.databinding.DataBindingUtil
import com.zody.R
import com.zody.databinding.ActivityChangeHostBinding
import com.zody.ui.base.BaseActivity
import com.zody.utils.SPKey
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, August 09
 */
class ChangeHostActivity : BaseActivity() {

    @Inject
    lateinit var sp: SharedPreferences

    private lateinit var binding: ActivityChangeHostBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_host)
        binding.headerBar.btHeaderActionLeft.setOnClickListener {
            finish()
        }

        val oldHost = sp.getString(SPKey.PREF_OLD_HOST, getString(R.string.host_old_version))
        val host = sp.getString(SPKey.PREF_HOST, getString(R.string.host))
        val trackingHost = sp.getString(SPKey.PREF_TRACKING_HOST, getString(R.string.host_tracking))

        binding.tilOldHost.editText?.append(oldHost)
        binding.tilNewHost.editText?.append(host)
        binding.tilTrackingHost.editText?.append(trackingHost)

        binding.btSave.setOnClickListener {
            sp.edit {
                putString(SPKey.PREF_OLD_HOST, binding.tilOldHost.editText?.text?.toString())
                putString(SPKey.PREF_HOST, binding.tilNewHost.editText?.text?.toString())
                putString(SPKey.PREF_TRACKING_HOST, binding.tilTrackingHost.editText?.text?.toString())
            }
            finish()
        }
    }
}