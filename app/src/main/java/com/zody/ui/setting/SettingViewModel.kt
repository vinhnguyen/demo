package com.zody.ui.setting

import com.zody.repository.ConfigurationRepository
import com.zody.repository.ProfileRepository
import com.zody.ui.base.BaseViewModel
import javax.inject.Inject

class SettingViewModel @Inject constructor(private val profileRepository: ProfileRepository,
                                           configurationRepository: ConfigurationRepository) : BaseViewModel() {

    val needUpdatePhoneNumber = profileRepository.needUpdatePhoneNumber()

    val coinForVerifyPhoneSuccess = configurationRepository.getCoinWhenVerifyPhoneSuccess()

    fun logout() {
        profileRepository.logout()
    }
}