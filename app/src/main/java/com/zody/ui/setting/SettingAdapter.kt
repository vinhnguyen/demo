package com.zody.ui.setting

import androidx.core.content.ContextCompat
import androidx.databinding.ViewDataBinding
import com.zody.R
import com.zody.databinding.ItemListSettingBinding
import com.zody.ui.base.DatabindingAdapter

class SettingAdapter(val items: List<SettingActivity.Item>) : DatabindingAdapter() {


    var onItemClickListener: ((SettingActivity.Item) -> Unit)? = null

    override fun onBind(binding: ViewDataBinding, position: Int) {
        (binding as ItemListSettingBinding).apply {
            val context = root.context
            val item = items[position]
            text = item.title
            icon = ContextCompat.getDrawable(context, item.icon)
            root.setOnClickListener {
                onItemClickListener?.invoke(item)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_list_setting
    }

    override fun getItemCount(): Int {
        return items.size
    }
}