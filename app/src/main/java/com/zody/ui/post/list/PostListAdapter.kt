package com.zody.ui.post.list

import android.content.Intent
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.zody.BR
import com.zody.R
import com.zody.databinding.*
import com.zody.entity.ListItem
import com.zody.entity.Post
import com.zody.entity.PostListItem
import com.zody.ui.base.ListItemAdapter
import com.zody.ui.business.BusinessDetailActivity
import com.zody.ui.business.shareLink
import com.zody.ui.expert.AuthorViewModel
import com.zody.ui.post.PostActionViewModel
import com.zody.ui.post.detail.PostDetailActivity
import com.zody.ui.post.tip.PostTippedActivity
import com.zody.ui.user.UserActivity

/**
 * Created by vinhnguyen.it.vn on 2018, July 14
 */
open class PostListAdapter(private val authorViewModel: AuthorViewModel,
                           private val postActionViewModel: PostActionViewModel) : ListItemAdapter<Post>(diffCallback = DIFF_CALLBACK) {

    var onButtonMoreClick: ((Post) -> Unit)? = null

    override fun initViewHolder(binding: ViewDataBinding, parent: ViewGroup) {
        when (binding) {
            is ItemListPostPhotosMultiHorizontalBinding,
            is ItemListPostPhotosMultiVerticalBinding -> binding.root.layoutParams = ViewGroup.LayoutParams(parent.width, parent.width)
            is ItemListPostAuthorBinding -> {
                binding.tvUserActionFollow.setOnClickListener { _ ->
                    binding.post?.author?.let {
                        authorViewModel.follow(it.id)
                    }
                }
                binding.btPostActionMore.setOnClickListener {
                    onButtonMoreClick?.invoke(binding.post!!)
                }
                binding.ivUserAvatar.setOnClickListener { view ->
                    binding.post?.let {
                        UserActivity.start(view.context, it.author?.id)
                    }
                }
                binding.tvUserName.setOnClickListener { view ->
                    binding.post?.let {
                        UserActivity.start(view.context, it.author?.id)
                    }
                }
            }
            is ItemListPostBusinessBinding -> {
                binding.tvPostBusinessName.setOnClickListener { view ->
                    binding.post?.business?.let {
                        BusinessDetailActivity.start(view.context, it.id)
                    }
                }
            }
            is ItemListPostStatisticBinding -> {
                binding.tvPostStatisticCoin.setOnClickListener {
                    it.context.startActivity(Intent(it.context, PostTippedActivity::class.java).apply {
                        putExtra(PostTippedActivity.EXTRA_POST_ID, binding.post?.id)
                    })
                }
            }
            is ItemListPostActionBinding -> {
                binding.btPostActionTip.setOnClickListener {
                    postActionViewModel.tip(binding.post!!.id)
                }
                binding.btPostActionComment.setOnClickListener { view ->
                    binding.post?.let {
                        PostDetailActivity.startAndShowComment(view.context, it.id)
                    }
                }
                binding.btPostActionShare.setOnClickListener { view ->
                    view.context.shareLink(binding.post?.shareUrl)
                    binding.post?.id?.let { postActionViewModel.view(it) }
                }
            }
        }
    }

    override fun onBind(binding: ViewDataBinding, item: ListItem<Post>?) {
        binding.root.setOnClickListener { view ->
            item?.data?.let {
                PostDetailActivity.start(view.context, it.id)
            }
        }
        binding.setVariable(BR.post, item?.data)
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)?.type) {
            PostListItem.TYPE_AUTHOR_AND_TIME -> R.layout.item_list_post_author
            PostListItem.TYPE_BUSINESS_AND_POINT -> R.layout.item_list_post_business
            PostListItem.TYPE_FEEDBACK -> R.layout.item_list_post_feedback
            PostListItem.TYPE_STATISTIC -> R.layout.item_list_post_statistic
            PostListItem.TYPE_ACTION -> R.layout.item_list_post_action
            PostListItem.TYPE_PHOTO_ONE -> R.layout.item_list_post_photo_one
            PostListItem.TYPE_PHOTO_MULTI -> R.layout.item_list_post_photos_multi
            PostListItem.TYPE_PHOTO_TWO_SPECIAL -> R.layout.item_list_post_photos_two_special
            PostListItem.TYPE_PHOTO_MULTI_VERTICAL -> R.layout.item_list_post_photos_multi_vertical
            PostListItem.TYPE_PHOTO_MULTI_HORIZONTAL -> R.layout.item_list_post_photos_multi_horizontal
            PostListItem.TYPE_PHOTO_FIVE_AND_OVER -> R.layout.item_list_post_photos_five
            else -> R.layout.item_list_post_place_holder
        }
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ListItem<Post>>() {
            override fun areItemsTheSame(oldItem: ListItem<Post>, newItem: ListItem<Post>): Boolean {
                return oldItem.type == newItem.type
                        && oldItem.data?.id == newItem.data?.id
            }

            override fun areContentsTheSame(oldItem: ListItem<Post>, newItem: ListItem<Post>): Boolean {
                return when (oldItem.type) {
                    PostListItem.TYPE_AUTHOR_AND_TIME -> oldItem.data?.author == newItem.data?.author
                            && oldItem.data?.statistic?.views == newItem.data?.statistic?.views
                            && oldItem.data?.createdAt == newItem.data?.createdAt
                    PostListItem.TYPE_BUSINESS_AND_POINT -> oldItem.data?.business?.name == newItem.data?.business?.name
                            && oldItem.data?.point == newItem.data?.point
                    PostListItem.TYPE_FEEDBACK -> oldItem.data?.feedback == newItem.data?.feedback
                            && oldItem.data?.hashtags == newItem.data?.hashtags
                    PostListItem.TYPE_STATISTIC -> oldItem.data?.statistic == newItem.data?.statistic
                    PostListItem.TYPE_ACTION -> oldItem.data?.currentUserTipped == newItem.data?.currentUserTipped
                    PostListItem.TYPE_PHOTO_ONE, PostListItem.TYPE_PHOTO_MULTI, PostListItem.TYPE_PHOTO_TWO_SPECIAL,
                    PostListItem.TYPE_PHOTO_MULTI_VERTICAL, PostListItem.TYPE_PHOTO_MULTI_HORIZONTAL,
                    PostListItem.TYPE_PHOTO_FIVE_AND_OVER -> oldItem.data?.photos == newItem.data?.photos
                    else -> false
                }
            }
        }
    }
}