package com.zody.ui.post

import android.graphics.Typeface
import android.text.SpannableStringBuilder
import android.text.TextPaint
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.text.color
import androidx.core.text.inSpans
import androidx.core.text.toSpanned
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.zody.R
import com.zody.databinding.ItemListPostPhotosMultiBinding
import com.zody.entity.Photo
import com.zody.entity.Post
import com.zody.ui.base.BindingInterface
import com.zody.ui.post.hashtag.PostOfHashTagActivity
import com.zody.ui.views.CustomTypefaceSpan
import com.zody.utils.*

/**
 * Created by vinhnguyen.it.vn on 2018, August 06
 */
object BindingInterafce {

    @JvmStatic
    @BindingAdapter("businessAndPoint")
    fun bindBusiness(view: TextView, post: Post?) {
        if (post?.business == null && post?.point == null) {
            view.text = null
            return
        }
        view.text = SpannableStringBuilder().apply {
            append(view.resources.getString(R.string.post_business_label))
            append(" ")
            post.business?.name?.let {
                inSpans(StyleSpan(Typeface.BOLD), ForegroundColorSpan(ContextCompat.getColor(view.context, R.color.black))) {
                    append(it)
                }
            }
            post.point?.let {
                append(" -   ")
                color(ContextCompat.getColor(view.context, R.color.orange)) { append("$it ★") }
            }
        }.toSpanned()
    }

    @JvmStatic
    @BindingAdapter("createdAndView")
    fun bindCreatedAndView(view: TextView, post: Post?) {
        view.text = SpannableStringBuilder().apply {
            post?.createdAt?.let {
                inSpans(CustomTypefaceSpan(ResourcesCompat.getFont(view.context, R.font.material))) { append(view.resources.getString(R.string.post_ic_time)) }
                append(" ")
                append(it.periodFormat(view.context))
            }
            post?.statistic?.views?.let {
                val minViewToDisplay = view.context.sp.getLong(SPKey.PREF_REVIEW_MIN_VIEW_TO_DISPLAY, 10)
                if (it > minViewToDisplay) {
                    if (isNotEmpty()) {
                        append("  .  ")
                    }
                    append(view.context.getString(R.string.post_statistic_views, it.decimalFormat))
                }
            }
            post?.distance?.let {
                if (it >= 0) {
                    append("  .  ")
                    append(it.distanceFormat)
                }
            }
        }.toSpanned()
    }

    @JvmStatic
    @BindingAdapter("photo", "onClick")
    fun bindPhoto(view: ImageView, photo: Photo?, onClick: OnPhotoClickListener?) {
        Glide.with(view).load(photo?.url).into(view)
        view.setOnClickListener {
            photo?.let { onClick?.onClick(view.context, it) }
        }
    }

    @JvmStatic
    @BindingAdapter(value = ["feedbackWithHashTag", "maxLine", "expandWhenClicked"], requireAll = false)
    fun bindFeedbackWithHashTag(view: TextView, post: Post?, maxLine: Int?, expandWhenClicked: Boolean = false) {
        if (post == null) {
            view.text = null
            return
        }

        val builder = SpannableStringBuilder(post.feedback ?: "")
                .append("\n")
        post.hashtags?.forEach { tag ->
            builder.inSpans(object : ClickableSpan() {
                override fun updateDrawState(ds: TextPaint?) {
                    ds?.color = ContextCompat.getColor(view.context, R.color.orange)
                    ds?.isUnderlineText = false
                }

                override fun onClick(widget: View?) {
                    PostOfHashTagActivity.start(view.context, tag)
                }
            }) {
                append("#$tag")
            }.append("  ")
        }
        BindingInterface.bindTextWithViewMore(view, builder.toSpanned().trim(), maxLine, expandWhenClicked)
    }

    @JvmStatic
    @BindingAdapter("photosOnPost")
    fun bindPhotoOnPost(view: View, post: Post?) {
        if (post == null) {
            return
        }
        val photos = post.photos ?: emptyList()
        val binding = DataBindingUtil.getBinding<ItemListPostPhotosMultiBinding>(view)
        binding?.textView?.text = "1/${photos.size}"

        binding?.viewPager?.apply {
            adapter = PhotoOnPostPageAdapter(post)
            (tag as? ViewPager.OnPageChangeListener)?.let {
                removeOnPageChangeListener(it)
            }
            val listener = object : ViewPager.SimpleOnPageChangeListener() {
                override fun onPageSelected(position: Int) {
                    binding.textView.text = "${position + 1}/${photos.size}"

                    findViewWithTag<View>(position)?.findViewById<ImageView>(R.id.imageView)?.apply {
                        val url = photos[position].url
                        Glide.with(this)
                                .load(url)
                                .priority(Priority.IMMEDIATE)
                                .into(this)
                    }
                }
            }
            addOnPageChangeListener(listener)
            tag = listener
        }
    }
}