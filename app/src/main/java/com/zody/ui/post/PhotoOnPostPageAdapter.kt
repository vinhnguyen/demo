package com.zody.ui.post

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.PagerAdapter
import com.zody.R
import com.zody.databinding.ItemPagePhotoOfPostBinding
import com.zody.entity.Post
import com.zody.ui.photo.PhotoActivity


/**
 * Created by vinhnguyen.it.vn on 2018, August 07
 */
class PhotoOnPostPageAdapter(private val post: Post) : PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        return DataBindingUtil.inflate<ItemPagePhotoOfPostBinding>(
                LayoutInflater.from(container.context),
                R.layout.item_page_photo_of_post,
                container, false
        ).apply {
            image = post.photos?.get(0)?.url
            root.tag = position
            container.addView(root)
            root.setOnClickListener { view ->
                PhotoActivity.viewPostPhoto(view.context, post, post.photos!![position])
            }
        }.root
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        container.removeView(obj as View)
    }

    override fun isViewFromObject(view: View, obj: Any): Boolean = view == obj

    override fun getCount(): Int = post.photos?.size ?: 0
}