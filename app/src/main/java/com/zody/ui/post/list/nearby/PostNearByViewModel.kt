package com.zody.ui.post.list.nearby

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.zody.entity.ListItem
import com.zody.entity.Post
import com.zody.livedata.CityLiveData
import com.zody.livedata.LocationLiveData
import com.zody.repository.Listing
import com.zody.repository.ListingData
import com.zody.repository.NetworkState
import com.zody.repository.PostRepository
import com.zody.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 20
 */
class PostNearByViewModel @Inject constructor(val city: CityLiveData,
                                              val location: LocationLiveData,
                                              postRepository: PostRepository) : BaseViewModel() {

    private val result: LiveData<Listing<ListItem<Post>>> = Transformations.map(location) {
        if (it == null) {
            null
        } else {
            postRepository.loadNearByPost(it.latitude, it.longitude)
        }
    }

    val data: LiveData<ListingData<ListItem<Post>>> = Transformations.switchMap(result) {
        it?.data
    }

    val refreshState: LiveData<NetworkState> = Transformations.switchMap(result) {
        it?.refreshState
    }

    fun refresh() {
        result.value?.refresh?.invoke()
    }
}