package com.zody.ui.post.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.zody.entity.Comment
import com.zody.entity.ListItem
import com.zody.entity.Post
import com.zody.livedata.SingleLiveEvent
import com.zody.repository.*
import com.zody.ui.base.BaseViewModel
import com.zody.utils.valueIfDifferent
import java.util.concurrent.atomic.AtomicBoolean
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 27
 */
class PostDetailViewModel @Inject constructor(private val postRepository: PostRepository) : BaseViewModel() {

    private val postId = MutableLiveData<String>()
    private val postResult: LiveData<Result<Post>> = Transformations.switchMap(postId) {
        postRepository.loadPostDetail(it)
    }
    private val commentContent = MutableLiveData<String>()
    val post: MediatorLiveData<Post> = MediatorLiveData<Post>().apply {
        addSource(postResult) {
            it.data?.let { postValue(it) }
        }
    }

    private val loadCommentResult: LiveData<Listing<ListItem<Comment>>> = Transformations.map(post) {
        postRepository.loadComment(it.id)
    }
    val commentResult: LiveData<Result<Comment>> = Transformations.switchMap(commentContent) {
        postRepository.sendComment(postId = postId.value!!, content = it)
    }
    val comments: LiveData<ListingData<ListItem<Comment>>> = Transformations.switchMap(loadCommentResult) {
        it.data
    }
    val networkState: LiveData<NetworkState> = Transformations.switchMap(loadCommentResult) {
        it.networkState
    }
    val refreshState: LiveData<NetworkState> = Transformations.switchMap(loadCommentResult) {
        it.refreshState
    }
    val onNewCommentAppear = SingleLiveEvent<Unit>().apply {
        addSource(commentResult) {
            if (it.success == true && waitingForNewCommentAppear.get()) {
                waitingForNewCommentAppear.set(false)
                this.call()
            }
        }
    }

    private val waitingForNewCommentAppear = AtomicBoolean(false)

    fun initPostId(id: String) {
        postId.valueIfDifferent = id
        postRepository.increaseView(id)
    }

    fun sendComment(content: String) {
        commentContent.value = content.trim()
        waitingForNewCommentAppear.set(true)
    }

    fun refresh() {
        loadCommentResult.value?.refresh?.invoke()
    }

    fun retry() {
        loadCommentResult.value?.retry?.invoke()
    }
}