package com.zody.ui.post.hashtag

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.Transformations
import com.zody.entity.ListItem
import com.zody.entity.Post
import com.zody.livedata.CityAndLocationForApi
import com.zody.livedata.LocationEnableLiveData
import com.zody.repository.Listing
import com.zody.repository.ListingData
import com.zody.repository.PostRepository
import com.zody.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 22
 */
class PostOfHashTagViewModel @Inject constructor(val locationEnable: LocationEnableLiveData,
                                                 private val cityAndLocationForApi: CityAndLocationForApi,
                                                 private val postRepository: PostRepository) : BaseViewModel() {

    private val hashTagAndSort = MutableLiveData<Pair<String, String>>()

    private val result: LiveData<Listing<ListItem<Post>>> = Transformations.map(hashTagAndSort) {
        postRepository.loadPostOfHashTag(
                tag = it.first,
                latitude = cityAndLocationForApi.value?.latitude, longitude = cityAndLocationForApi.value?.longitude,
                city = cityAndLocationForApi.value?.city,
                sort = it.second)
    }
    val data: LiveData<ListingData<ListItem<Post>>> = Transformations.switchMap(result) {
        it.data
    }

    private val observer = Observer<CityAndLocationForApi.Data> {

    }

    init {
        cityAndLocationForApi.observeForever(observer)
    }

    fun init(hashTag: String, sort: String) {
        this.hashTagAndSort.value = hashTag to sort
    }

    override fun onCleared() {
        super.onCleared()
        postRepository.clearPostOfTag(hashTagAndSort.value?.first)
        cityAndLocationForApi.removeObserver(observer)
    }
}