package com.zody.ui.post.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.zody.R
import com.zody.databinding.ActivityPostDetailBinding
import com.zody.ui.base.BaseFragmentActivity
import com.zody.ui.base.Navigation
import com.zody.ui.expert.AuthorViewModel
import com.zody.ui.post.NotEnoughCoinObserver
import com.zody.ui.post.PostActionViewModel
import com.zody.ui.post.PostOptionsDialogFragment
import com.zody.utils.hideKeyboard
import com.zody.utils.showKeyboard
import com.zody.utils.showMessage
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 27
 */
class PostDetailActivity : BaseFragmentActivity() {

    companion object {
        private const val EXTRA_SHOW_COMMENT = "PostDetailActivity.ShowComment"

        fun start(context: Context, postId: String) {
            context.startActivity(Intent(context, PostDetailActivity::class.java).apply {
                putExtra(Navigation.EXTRA_TARGET_VALUE, postId)
            })
        }

        fun startAndShowComment(context: Context, postId: String) {
            context.startActivity(Intent(context, PostDetailActivity::class.java).apply {
                putExtra(Navigation.EXTRA_TARGET_VALUE, postId)
                putExtra(EXTRA_SHOW_COMMENT, true)
            })
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityPostDetailBinding
    private val postDetailViewModel: PostDetailViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(PostDetailViewModel::class.java)
    }
    private val authorViewModel: AuthorViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(AuthorViewModel::class.java)
    }
    private val postActionViewModel: PostActionViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(PostActionViewModel::class.java)
    }

    override val screenName: String?
        get() = "review detail"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val postId = Navigation.getId(intent)
        if (postId == null) {
            finish()
            return
        }

        binding = DataBindingUtil.setContentView(this, R.layout.activity_post_detail)
        binding.headerBar.btHeaderActionLeft.setOnClickListener {
            finish()
        }
        if (intent.getBooleanExtra(EXTRA_SHOW_COMMENT, false)) {
            binding.etCommentContent.showKeyboard()
        }

        val adapter = PostDetailAdapter(authorViewModel, postActionViewModel)
        adapter.onCommentButtonClick = {
            binding.etCommentContent.showKeyboard()
        }
        adapter.onMoreButtonClick = {
            PostOptionsDialogFragment.show(supportFragmentManager, it)
        }
        binding.recyclerView.adapter = adapter

        binding.swipeRefreshLayout.setOnRefreshListener {
            postDetailViewModel.refresh()
        }
        postDetailViewModel.initPostId(postId)
        postDetailViewModel.post.observe(this, Observer {
            adapter.post = it
        })
        postDetailViewModel.refreshState.observe(this, Observer {
            binding.swipeRefreshLayout.isRefreshing = it.loading
        })

        postDetailViewModel.comments.observe(this, Observer {
            adapter.submitList(it.pagedList, it.endData)
        })
        postDetailViewModel.commentResult.observe(this, Observer {
            showMessage(it)
        })
        postDetailViewModel.onNewCommentAppear.observe(this, Observer {
            (binding.recyclerView.layoutManager as LinearLayoutManager).scrollToPositionWithOffset(adapter.postItemSize - 1, 0)
        })

        binding.btCommentSend.setOnClickListener {
            val content = binding.etCommentContent.text.toString()
            if (content.isNotEmpty()) {
                postDetailViewModel.sendComment(content)
                binding.etCommentContent.text = null
                binding.etCommentContent.hideKeyboard()
            }
        }

        authorViewModel.message.observe(this, Observer {
            showMessage(it)
        })
        postActionViewModel.tipResult.observe(this, Observer {
            showMessage(it)
        })
        postActionViewModel.deleteResult.observe(this, Observer {
            showMessage(it)
            if (it.success == true) {
                finish()
            }
        })
        postActionViewModel.showPopupNotEnoughCoin.observe(this, NotEnoughCoinObserver(this, supportFragmentManager))

    }
}