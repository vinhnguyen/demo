package com.zody.ui.post.list

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableField
import androidx.databinding.ViewDataBinding
import androidx.paging.PagedList
import com.zody.R
import com.zody.databinding.ItemListPostListInCityEmptyBinding
import com.zody.databinding.ItemListPostListTrendingHashTagBinding
import com.zody.databinding.LayoutChipHashTagBinding
import com.zody.entity.ListItem
import com.zody.entity.Post
import com.zody.ui.base.DataBindingViewHolder
import com.zody.ui.expert.AuthorViewModel
import com.zody.ui.hashtag.SearchHashTagActivity
import com.zody.ui.post.PostActionViewModel
import com.zody.ui.writepost.WritePostActivity

/**
 * Created by vinhnguyen.it.vn on 2018, July 26
 */
class PostInCityAdapter(authorViewModel: AuthorViewModel, postActionViewModel: PostActionViewModel,
                        private val city: ObservableField<String>) : PostListAdapter(authorViewModel, postActionViewModel) {

    var hashTag: List<String>? = null
        set(value) {
            field = value
            if (itemCount > 1) {
                notifyItemChanged(0)
            }
        }

    private var empty = false
    private val startPositionOfRealList: Int
        get() = if (hashTag?.isNotEmpty() == true) 1 else 0

    var onTagClick: ((String) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingViewHolder {
        val holder = super.onCreateViewHolder(parent, viewType)
        when (holder.binding) {
            is ItemListPostListInCityEmptyBinding -> holder.binding.city = city
        }
        return holder
    }

    override fun onBind(binding: ViewDataBinding, item: ListItem<Post>?) {
        when (binding) {
            is ItemListPostListInCityEmptyBinding -> {
                binding.btEmptyAction.setOnClickListener {
                    it.context.startActivity(Intent(it.context, WritePostActivity::class.java))
                }
            }
            is ItemListPostListTrendingHashTagBinding -> {
                if (binding.flHashTag.tag == hashTag) return
                binding.flHashTag.tag = hashTag
                binding.flHashTag.removeAllViews()
                val inflater = LayoutInflater.from(binding.root.context)
                hashTag?.forEach { tag ->
                    DataBindingUtil.inflate<LayoutChipHashTagBinding>(inflater, R.layout.layout_chip_hash_tag, binding.flHashTag, false).apply {
                        this.tag = tag
                        binding.flHashTag.addView(root)
                        root.setOnClickListener {
                            onTagClick?.invoke(tag)
                        }
                    }
                }

                DataBindingUtil.inflate<LayoutChipHashTagBinding>(inflater, R.layout.layout_chip_hash_tag, binding.flHashTag, false).apply {
                    executePendingBindings()
                    chip.typeface = ResourcesCompat.getFont(chip.context, R.font.material)
                    chip.setText(R.string.ic_hash_tag_search)
                    binding.flHashTag.addView(root)
                    root.setOnClickListener { view ->
                        view.context.startActivity(Intent(view.context, SearchHashTagActivity::class.java))
                    }
                }
            }
            else -> super.onBind(binding, item)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            position == 0 && hashTag?.isNotEmpty() == true -> R.layout.item_list_post_list_trending_hash_tag
            initialing -> super.getItemViewType(position)
            empty -> R.layout.item_list_post_list_in_city_empty
            else -> super.getItemViewType(position)
        }
    }

    override fun getItem(position: Int): ListItem<Post>? {
        return when {
            position == 0 -> null
            initialing -> super.getItem(position)
            empty -> null
            else -> super.getItem(position - startPositionOfRealList)
        }
    }

    override fun getItemCount(): Int {
        return (if (empty) 1 else super.getItemCount()) + startPositionOfRealList
    }

    override fun submitList(pagedList: PagedList<ListItem<Post>>?, endData: Boolean) {
        super.submitList(pagedList, endData)

        val oldEmpty = empty
        empty = pagedList?.isNotEmpty() != true && endData
        if (oldEmpty != empty) {
            notifyDataSetChanged()
        }
    }

    override fun onInserted(position: Int, count: Int) {
        super.onInserted(position + startPositionOfRealList, count)
    }

    override fun onRemoved(position: Int, count: Int) {
        super.onRemoved(position + startPositionOfRealList, count)
    }

    override fun onMoved(fromPosition: Int, toPosition: Int) {
        super.onMoved(fromPosition + startPositionOfRealList, toPosition + startPositionOfRealList)
    }

    override fun onChanged(position: Int, count: Int, payload: Any?) {
        super.onChanged(position + startPositionOfRealList, count, payload)
    }
}