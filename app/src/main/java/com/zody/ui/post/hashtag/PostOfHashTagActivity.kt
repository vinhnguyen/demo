package com.zody.ui.post.hashtag

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.ViewGroup
import androidx.core.view.children
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.tabs.TabLayout
import com.zody.R
import com.zody.databinding.ActivityPostOfHashTagBinding
import com.zody.ui.base.BaseFragmentActivity
import com.zody.ui.base.Navigation
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 22
 */
class PostOfHashTagActivity : BaseFragmentActivity() {

    companion object {
        private const val EXTRA_FILTER = "PostOfHashTagActivity.Filter"

        fun start(context: Context, tag: String?, filter: String? = null) {
            context.startActivity(Intent(context, PostOfHashTagActivity::class.java).apply {
                putExtra(Navigation.EXTRA_TARGET_TITLE, tag)
                putExtra(Navigation.EXTRA_TARGET_VALUE, tag)
                putExtra(EXTRA_FILTER, filter)
            })
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityPostOfHashTagBinding

    override val screenName: String?
        get() = "review of hashtag"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_post_of_hash_tag)
        binding.headerBar.btHeaderActionLeft.setOnClickListener {
            finish()
        }
        binding.headerBar.tvHeaderTitle.isAllCaps = false
        binding.headerBar.title = "#${intent.getStringExtra(Navigation.EXTRA_TARGET_TITLE)}"

        binding.tabLayout.apply {
            for (tabView in (getChildAt(0) as ViewGroup).children) {
                tabView.apply { alpha = if (isSelected) 1F else 0.5F }
            }
            addOnTabSelectedListener(object : TabLayout.ViewPagerOnTabSelectedListener(binding.viewPager) {
                private fun setAlphaForView(tab: TabLayout.Tab?, alpha: Float) {
                    tab?.let {
                        (it.parent.getChildAt(0) as ViewGroup).getChildAt(it.position).alpha = alpha
                    }
                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {
                    super.onTabUnselected(tab)
                    setAlphaForView(tab, 0.5F)
                }

                override fun onTabSelected(tab: TabLayout.Tab?) {
                    super.onTabSelected(tab)
                    setAlphaForView(tab, 1F)
                }
            })
        }
        binding.viewPager.apply {
            addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(binding.tabLayout))
            val adapter = PostOfHashTagPageAdapter(intent.getStringExtra(Navigation.EXTRA_TARGET_VALUE), supportFragmentManager)
            this.adapter = adapter
            offscreenPageLimit = adapter.count

            intent.getStringExtra(EXTRA_FILTER)?.let { filter ->
                currentItem = PostOfHashTagPageAdapter.filters.indexOfFirst { it == filter }
            }
        }
    }
}