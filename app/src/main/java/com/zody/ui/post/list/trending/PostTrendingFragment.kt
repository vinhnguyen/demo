package com.zody.ui.post.list.trending

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableField
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zody.R
import com.zody.databinding.FragmentPostListBinding
import com.zody.entity.PostItem
import com.zody.ui.base.BaseFragment
import com.zody.ui.expert.AuthorViewModel
import com.zody.ui.home.HomeViewModel
import com.zody.ui.post.PostActionViewModel
import com.zody.ui.post.PostOptionsDialogFragment
import com.zody.ui.post.hashtag.PostOfHashTagActivity
import com.zody.ui.post.list.PostInCityAdapter
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 12
 */
class PostTrendingFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentPostListBinding

    private val homeViewModel: HomeViewModel by lazy {
        ViewModelProviders.of(parentFragment!!, viewModelFactory).get(HomeViewModel::class.java)
    }
    private val postTrendingViewModel: PostTrendingViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(PostTrendingViewModel::class.java)
    }
    private val authorViewModel: AuthorViewModel by lazy {
        ViewModelProviders.of(activity!!, viewModelFactory).get(AuthorViewModel::class.java)
    }
    private val postActionViewModel: PostActionViewModel by lazy {
        ViewModelProviders.of(activity!!, viewModelFactory).get(PostActionViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_post_list, container, false)
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.swipeRefreshLayout.setOnRefreshListener {
            postTrendingViewModel.refresh()
        }

        val city = ObservableField<String>()
        postTrendingViewModel.city.observe(this@PostTrendingFragment, Observer {
            city.set(it?.name)
        })

        val adapter = PostInCityAdapter(authorViewModel, postActionViewModel, city)
        adapter.onButtonMoreClick = {
            PostOptionsDialogFragment.show(fragmentManager, it)
        }
        adapter.onTagClick = { tag ->
            PostOfHashTagActivity.start(requireContext(), tag, PostItem.SORT_TRENDING)
        }
        binding.recyclerView.adapter = adapter
        homeViewModel.trendingHashTag.observe(this, Observer {
            adapter.hashTag = it.data
        })
        postTrendingViewModel.data.observe(this, Observer {
            adapter.submitList(it.pagedList, it.endData)
        })
        postTrendingViewModel.refreshState.observe(this, Observer {
            binding.swipeRefreshLayout.isRefreshing = it.loading
        })
    }
}