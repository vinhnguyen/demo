package com.zody.ui.post.list.newest

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.zody.entity.ListItem
import com.zody.entity.Post
import com.zody.livedata.CityLiveData
import com.zody.repository.Listing
import com.zody.repository.ListingData
import com.zody.repository.NetworkState
import com.zody.repository.PostRepository
import com.zody.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 20
 */
class PostNewestViewModel @Inject constructor(val city: CityLiveData,
                                              postRepository: PostRepository) : BaseViewModel() {

    private val result: LiveData<Listing<ListItem<Post>>> = Transformations.map(city) {
        postRepository.loadNewestPost(it?.id)
    }

    val data: LiveData<ListingData<ListItem<Post>>> = Transformations.switchMap(result) {
        it.data
    }

    val refreshState: LiveData<NetworkState> = Transformations.switchMap(result) {
        it.refreshState
    }

    fun refresh() {
        result.value?.refresh?.invoke()
    }
}