package com.zody.ui.post

import android.content.Context
import android.content.Intent
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import com.zody.R
import com.zody.ui.base.MessageDialog
import com.zody.ui.help.ZcoinCentreActivity
import com.zody.utils.SPKey
import com.zody.utils.sp

/**
 * Created by vinhnguyen.it.vn on 2018, August 24
 */
class NotEnoughCoinObserver(private val context: Context,
                            private val fm: FragmentManager) : Observer<Unit> {

    override fun onChanged(t: Unit?) {
        val minCoin = context.sp.getLong(SPKey.PREF_TIP_MIN_COIN, 50)
        MessageDialog.instantiate(
                icon = ContextCompat.getDrawable(context, R.drawable.ic_not_enough_coin_to_tip),
                title = context.getString(R.string.post_list_not_enough_coin_to_tip_title),
                message = context.getString(R.string.post_list_not_enough_coin_to_tip_message, minCoin),
                action = context.getString(R.string.post_list_not_enough_coin_to_tip_action),
                onActionClick = {
                    context.startActivity(Intent(context, ZcoinCentreActivity::class.java))
                }).show(fm, null)
    }
}