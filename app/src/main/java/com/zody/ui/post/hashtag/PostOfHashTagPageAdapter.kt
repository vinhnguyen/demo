package com.zody.ui.post.hashtag

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.zody.entity.PostItem

/**
 * Created by vinhnguyen.it.vn on 2018, November 12
 */
class PostOfHashTagPageAdapter(private val hashTag: String, fm: FragmentManager?) : FragmentStatePagerAdapter(fm) {

    companion object {
        val filters = arrayOf(PostItem.SORT_TRENDING, PostItem.SORT_NEARBY, PostItem.SORT_NEWEST)
    }

    override fun getItem(position: Int): Fragment {
        return PostOfHashTagFragment.newInstance(hashTag, filters[position])
    }

    override fun getCount() = filters.size

}