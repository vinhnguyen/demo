package com.zody.ui.post.hashtag

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zody.R
import com.zody.databinding.FragmentPostOfHashTagBinding
import com.zody.entity.PostItem
import com.zody.ui.base.BaseFragment
import com.zody.ui.expert.AuthorViewModel
import com.zody.ui.permission.AskLocationActivity
import com.zody.ui.post.PostActionViewModel
import com.zody.ui.post.PostOptionsDialogFragment
import com.zody.utils.gone
import com.zody.utils.showMessage
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, November 12
 */
class PostOfHashTagFragment : BaseFragment() {

    companion object {

        private const val ARG_HASH_TAG = "PostOfHashTagFragment.HashTag"
        private const val ARG_FILTER = "PostOfHashTagFragment.Filter"

        fun newInstance(hashTag: String, filter: String): PostOfHashTagFragment {
            return PostOfHashTagFragment().apply {
                arguments = bundleOf(ARG_HASH_TAG to hashTag, ARG_FILTER to filter)
            }
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentPostOfHashTagBinding

    private val hashTag: String by lazy {
        arguments?.getString(ARG_HASH_TAG)!!
    }

    private val filter: String by lazy {
        arguments?.getString(ARG_FILTER) ?: PostItem.SORT_NEWEST
    }

    private val postOfHashTagViewModel: PostOfHashTagViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(PostOfHashTagViewModel::class.java)
    }

    private val authorViewModel: AuthorViewModel by lazy {
        ViewModelProviders.of(requireActivity(), viewModelFactory).get(AuthorViewModel::class.java)
    }
    private val postActionViewModel: PostActionViewModel by lazy {
        ViewModelProviders.of(requireActivity(), viewModelFactory).get(PostActionViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_post_of_hash_tag, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = PostOfHashTagAdapter(authorViewModel, postActionViewModel)
        adapter.onButtonMoreClick = {
            PostOptionsDialogFragment.show(fragmentManager, it)
        }
        binding.recyclerView.adapter = adapter

        postOfHashTagViewModel.init(hashTag, filter)
        postOfHashTagViewModel.data.observe(this, Observer {
            adapter.submitList(it.pagedList, it.endData)
        })

        authorViewModel.message.observe(this, Observer {
            showMessage(it)
        })
        postActionViewModel.tipResult.observe(this, Observer {
            showMessage(it)
        })
        postActionViewModel.deleteResult.observe(this, Observer {
            showMessage(it)
        })

        if (filter == PostItem.SORT_NEARBY) {
            postOfHashTagViewModel.locationEnable.observe(this, Observer {
                binding.recyclerView.gone = !it
                binding.layoutNoLocation.gone = it
            })

            binding.layoutNoLocation.btActionPositive.setOnClickListener {
                AskLocationActivity.start(requireContext(), true)
            }
        }
    }
}