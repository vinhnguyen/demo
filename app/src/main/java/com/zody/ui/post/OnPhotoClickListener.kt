package com.zody.ui.post

import android.content.Context
import com.zody.entity.Photo

/**
 * Created by vinhnguyen.it.vn on 2018, August 07
 */
interface OnPhotoClickListener {
    fun onClick(context: Context, photo: Photo)
}