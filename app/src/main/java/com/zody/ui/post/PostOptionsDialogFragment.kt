package com.zody.ui.post

import android.os.Bundle
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zody.R
import com.zody.di.Injectable
import com.zody.entity.Post
import com.zody.ui.base.MessageDialog
import com.zody.ui.business.copy
import com.zody.ui.expert.AuthorViewModel
import com.zody.ui.views.SingleChoiceDialogFragment
import com.zody.ui.writepost.edit.EditPostActivity
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 28
 */
class PostOptionsDialogFragment : SingleChoiceDialogFragment<Int>(), Injectable {

    companion object {
        const val REQUEST_CODE_EDIT = 0
        const val REQUEST_CODE_FOLLOW = 1
        const val REQUEST_CODE_UNFOLLOW = 2
        const val REQUEST_CODE_COPY = 3
        const val REQUEST_CODE_REPORT = 4
        const val REQUEST_CODE_DELETE = 5

        fun show(fragmentManager: FragmentManager?, post: Post) {
            val fragment = PostOptionsDialogFragment()
            fragment.post = post
            fragment.show(fragmentManager, null)
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val authorViewModel: AuthorViewModel by lazy {
        ViewModelProviders.of(activity!!, viewModelFactory).get(AuthorViewModel::class.java)
    }
    private val postActionViewModel: PostActionViewModel by lazy {
        ViewModelProviders.of(activity!!, viewModelFactory).get(PostActionViewModel::class.java)
    }
    lateinit var post: Post

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val options = mutableListOf<Option<Int>>()
        if (post.isAuthor == true) {
            options.add(Option(REQUEST_CODE_EDIT, getString(R.string.post_list_action_edit), R.drawable.ic_post_action_edit))
        }
        if (post.isAuthor != true) {
            if (post.author?.isFollowed != true) {
                options.add(Option(REQUEST_CODE_FOLLOW, getString(R.string.post_list_action_follow, post.author?.name), R.drawable.ic_post_action_follow))
            } else {
                options.add(Option(REQUEST_CODE_UNFOLLOW, getString(R.string.post_list_action_unfollow, post.author?.name), R.drawable.ic_post_action_unfollow))
            }
        }
        options.add(Option(REQUEST_CODE_COPY, getString(R.string.post_list_action_copy), R.drawable.ic_post_action_copy))
        options.add(Option(REQUEST_CODE_REPORT, getString(R.string.post_list_action_report), R.drawable.ic_post_action_report))
        if (post.isAuthor == true) {
            options.add(Option(REQUEST_CODE_DELETE, getString(R.string.post_list_action_delete), R.drawable.ic_post_action_delete))
        }
        this@PostOptionsDialogFragment.options = options
    }

    override var onItemSelected: ((Option<Int>, Int) -> Unit)? = { option, _ ->
        when (option.key) {
            REQUEST_CODE_FOLLOW -> post.author?.id?.let { authorViewModel.follow(it) }
            REQUEST_CODE_UNFOLLOW -> post.author?.id?.let { authorViewModel.unFollow(it) }
            REQUEST_CODE_DELETE -> postActionViewModel.delete(post.id)
            REQUEST_CODE_COPY -> context?.copy(post.shareUrl)
            REQUEST_CODE_EDIT -> EditPostActivity.start(requireContext(), post.id)
            REQUEST_CODE_REPORT -> showComingSoon()
        }
    }

    private fun showComingSoon() {
        MessageDialog.instantiate(title = getString(R.string.app_name), message = "Coming soon")
                .show(activity!!.supportFragmentManager, null)
    }
}