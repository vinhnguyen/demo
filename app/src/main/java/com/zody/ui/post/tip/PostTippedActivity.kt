package com.zody.ui.post.tip

import android.graphics.Rect
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.zody.R
import com.zody.databinding.ActivityPostTippedBinding
import com.zody.ui.base.BaseActivity
import com.zody.utils.px
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 28
 */
class PostTippedActivity : BaseActivity() {

    companion object {
        const val EXTRA_POST_ID = "PostTippedActivity.PostId"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityPostTippedBinding

    override val screenName: String?
        get() = "who tip the review"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_post_tipped)
        binding.headerBar.btHeaderActionLeft.setOnClickListener { finish() }

        val adapter = PostTippedAdapter()
        binding.recyclerView.adapter = adapter
        binding.recyclerView.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                outRect.bottom = 1.px
            }
        })

        val postTippedViewModel = ViewModelProviders.of(this, viewModelFactory).get(PostTippedViewModel::class.java)
        postTippedViewModel.pagedList.observe(this, Observer {
            adapter.submitList(it.pagedList)
        })
        postTippedViewModel.initPostId(intent.getStringExtra(EXTRA_POST_ID))
    }
}