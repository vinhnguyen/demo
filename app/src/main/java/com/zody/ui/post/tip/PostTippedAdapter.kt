package com.zody.ui.post.tip

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.zody.R
import com.zody.databinding.ItemListPostTippedBinding
import com.zody.entity.Tip
import com.zody.ui.base.DataBindingViewHolder
import com.zody.ui.base.DatabindingPagedListAdapter
import com.zody.ui.user.UserActivity

/**
 * Created by vinhnguyen.it.vn on 2018, July 28
 */
class PostTippedAdapter : DatabindingPagedListAdapter<Tip>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingViewHolder {
        val holder = super.onCreateViewHolder(parent, viewType)
        when (holder.binding) {
            is ItemListPostTippedBinding -> holder.itemView.setOnClickListener { view ->
                UserActivity.start(view.context, holder.binding.tip?.author?.id)
            }
        }
        return holder
    }

    override fun onBind(binding: ViewDataBinding, item: Tip?) {
        (binding as ItemListPostTippedBinding).tip = item
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_list_post_tipped
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Tip>() {
            override fun areItemsTheSame(oldItem: Tip, newItem: Tip): Boolean {
                return oldItem.author?.id == newItem.author?.id
            }

            override fun areContentsTheSame(oldItem: Tip, newItem: Tip): Boolean {
                return oldItem == newItem
            }
        }
    }
}