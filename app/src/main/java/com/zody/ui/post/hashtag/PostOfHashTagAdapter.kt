package com.zody.ui.post.hashtag

import androidx.paging.PagedList
import com.zody.R
import com.zody.entity.ListItem
import com.zody.entity.Post
import com.zody.ui.expert.AuthorViewModel
import com.zody.ui.post.PostActionViewModel
import com.zody.ui.post.list.PostListAdapter

/**
 * Created by vinhnguyen.it.vn on 2018, September 27
 */
class PostOfHashTagAdapter(authorViewModel: AuthorViewModel, postActionViewModel: PostActionViewModel) : PostListAdapter(authorViewModel, postActionViewModel) {

    private var empty = false

    override fun getItemViewType(position: Int): Int {
        return when {
            empty -> R.layout.item_list_post_list_of_hash_tag_empty
            else -> super.getItemViewType(position)
        }
    }

    override fun getItem(position: Int): ListItem<Post>? {
        return when {
            empty -> null
            else -> super.getItem(position)
        }
    }

    override fun getItemCount(): Int {
        return when {
            empty -> 1
            else -> super.getItemCount()
        }
    }

    override fun submitList(pagedList: PagedList<ListItem<Post>>?, endData: Boolean) {
        super.submitList(pagedList, endData)

        val oldEmpty = empty
        empty = pagedList?.isNotEmpty() != true && endData
        if (oldEmpty != empty) {
            notifyDataSetChanged()
        }
    }
}