package com.zody.ui.post.detail

import android.content.Intent
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.zody.BR
import com.zody.R
import com.zody.databinding.*
import com.zody.entity.*
import com.zody.repository.toItemTypes
import com.zody.ui.base.ListItemAdapter
import com.zody.ui.business.BusinessDetailActivity
import com.zody.ui.business.shareLink
import com.zody.ui.expert.AuthorViewModel
import com.zody.ui.post.PostActionViewModel
import com.zody.ui.post.tip.PostTippedActivity
import com.zody.ui.user.UserActivity

/**
 * Created by vinhnguyen.it.vn on 2018, July 27
 */
class PostDetailAdapter(private val authorViewModel: AuthorViewModel, private val postActionViewModel: PostActionViewModel) : ListItemAdapter<Comment>(DIFF_CALLBACK) {

    var onCommentButtonClick: (() -> Unit)? = null
    var onMoreButtonClick: ((Post) -> Unit)? = null

    var post: Post? = null
        set(value) {
            if (field == value) return
            field = value
            val oldSize = postItems.size

            postItems = value?.toItemTypes() ?: emptyList()

            val newSize = postItems.size
            if (oldSize != newSize) {
                notifyItemRangeRemoved(0, oldSize)
                notifyItemRangeInserted(0, newSize)
            } else {
                notifyItemRangeChanged(0, newSize)
            }
        }

    private var postItems = emptyList<Int>()
    val postItemSize: Int
        get() = postItems.size


    override fun initViewHolder(binding: ViewDataBinding, parent: ViewGroup) {
        when (binding) {
            is ItemListPostPhotosMultiHorizontalBinding,
            is ItemListPostPhotosMultiVerticalBinding -> binding.root.layoutParams = ViewGroup.LayoutParams(parent.width, parent.width)
            is ItemListPostFeedbackBinding -> binding.maxLine = 0
            is ItemListPostAuthorBinding -> {
                binding.tvUserActionFollow.setOnClickListener { _ ->
                    post?.author?.let {
                        authorViewModel.follow(it.id)
                    }
                }
                binding.btPostActionMore.setOnClickListener { _ ->
                    post?.let {
                        onMoreButtonClick?.invoke(it)
                    }
                }
                binding.ivUserAvatar.setOnClickListener { view ->
                    binding.post?.let {
                        UserActivity.start(view.context, it.author?.id)
                    }
                }
                binding.tvUserName.setOnClickListener { view ->
                    binding.post?.let {
                        UserActivity.start(view.context, it.author?.id)
                    }
                }
            }
            is ItemListPostBusinessBinding -> {
                binding.tvPostBusinessName.setOnClickListener { view ->
                    binding.post?.business?.let {
                        BusinessDetailActivity.start(view.context, it.id)
                    }
                }
            }
            is ItemListPostStatisticBinding -> {
                binding.tvPostStatisticCoin.setOnClickListener {
                    it.context.startActivity(Intent(it.context, PostTippedActivity::class.java).apply {
                        putExtra(PostTippedActivity.EXTRA_POST_ID, post?.id)
                    })
                }
            }
            is ItemListPostActionBinding -> {
                binding.btPostActionTip.setOnClickListener {
                    postActionViewModel.tip(post!!.id)
                }
                binding.btPostActionComment.setOnClickListener {
                    onCommentButtonClick?.invoke()
                }
                binding.btPostActionShare.setOnClickListener { view ->
                    view.context.shareLink(binding.post?.shareUrl)
                    binding.post?.id?.let { postActionViewModel.view(it) }
                }
            }
            is ItemListCommentBinding -> {
                binding.ivUserAvatar.setOnClickListener { view ->
                    binding.comment?.let {
                        UserActivity.start(view.context, it.author?.id)
                    }
                }
                binding.tvUserName.setOnClickListener { view ->
                    binding.comment?.let {
                        UserActivity.start(view.context, it.author?.id)
                    }
                }
            }
        }
    }

    override fun onBind(binding: ViewDataBinding, item: ListItem<Comment>?) {
        binding.setVariable(BR.post, post)
        item?.data?.let { binding.setVariable(BR.comment, it) }
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            position < postItems.size -> {
                when (postItems[position]) {
                    PostListItem.TYPE_AUTHOR_AND_TIME -> R.layout.item_list_post_author
                    PostListItem.TYPE_BUSINESS_AND_POINT -> R.layout.item_list_post_business
                    PostListItem.TYPE_FEEDBACK -> R.layout.item_list_post_feedback
                    PostListItem.TYPE_STATISTIC -> R.layout.item_list_post_statistic
                    PostListItem.TYPE_ACTION -> R.layout.item_list_post_action
                    PostListItem.TYPE_PHOTO_ONE -> R.layout.item_list_post_photo_one
                    PostListItem.TYPE_PHOTO_MULTI -> R.layout.item_list_post_photos_multi
                    PostListItem.TYPE_PHOTO_TWO_SPECIAL -> R.layout.item_list_post_photos_two_special
                    PostListItem.TYPE_PHOTO_MULTI_VERTICAL -> R.layout.item_list_post_photos_multi_vertical
                    PostListItem.TYPE_PHOTO_MULTI_HORIZONTAL -> R.layout.item_list_post_photos_multi_horizontal
                    PostListItem.TYPE_PHOTO_FIVE_AND_OVER -> R.layout.item_list_post_photos_five
                    else -> super.getItemViewType(position)
                }
            }
            else -> when (getItem(position)?.type) {
                CommentListItem.TYPE_NORMAL -> R.layout.item_list_comment
                else -> R.layout.item_list_comment_place_holder
            }
        }
    }

    override fun getItem(position: Int): ListItem<Comment>? {
        return when {
            position < postItemSize -> null
            position - postItemSize >= super.getItemCount() -> null
            else -> super.getItem(position - postItemSize)
        }
    }

    override fun getItemCount(): Int {
        return if (post == null) 0 else super.getItemCount() + postItemSize
    }

    override fun onInserted(position: Int, count: Int) {
        super.onInserted(position + postItemSize, count)
    }

    override fun onRemoved(position: Int, count: Int) {
        super.onRemoved(position + postItemSize, count)
    }

    override fun onMoved(fromPosition: Int, toPosition: Int) {
        super.onMoved(fromPosition + postItemSize, toPosition)
    }

    override fun onChanged(position: Int, count: Int, payload: Any?) {
        super.onChanged(position + postItemSize, count, payload)
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ListItem<Comment>>() {
            override fun areItemsTheSame(oldItem: ListItem<Comment>, newItem: ListItem<Comment>): Boolean {
                return oldItem.type == newItem.type
                        && oldItem.data?.id == newItem.data?.id
            }

            override fun areContentsTheSame(oldItem: ListItem<Comment>, newItem: ListItem<Comment>): Boolean {
                return oldItem.data == newItem.data
            }
        }
    }
}