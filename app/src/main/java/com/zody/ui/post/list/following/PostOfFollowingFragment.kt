package com.zody.ui.post.list.following

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zody.R
import com.zody.databinding.FragmentPostFromFollowingBinding
import com.zody.ui.base.BaseFragment
import com.zody.ui.expert.AuthorViewModel
import com.zody.ui.post.PostActionViewModel
import com.zody.ui.post.PostOptionsDialogFragment
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 19
 */
class PostOfFollowingFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentPostFromFollowingBinding
    private val postOfFollowingViewModel: PostOfFollowingViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(PostOfFollowingViewModel::class.java)
    }
    private val authorViewModel: AuthorViewModel by lazy {
        ViewModelProviders.of(activity!!, viewModelFactory).get(AuthorViewModel::class.java)
    }
    private val postActionViewModel: PostActionViewModel by lazy {
        ViewModelProviders.of(activity!!, viewModelFactory).get(PostActionViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_post_from_following, container, false)
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = PostOfFollowingAdapter(authorViewModel, postActionViewModel)
        binding.recyclerView.adapter = adapter
        adapter.onButtonMoreClick = {
            PostOptionsDialogFragment.show(fragmentManager, it)
        }

        binding.swipeRefreshLayout.setOnRefreshListener {
            postOfFollowingViewModel.refresh()
        }

        postOfFollowingViewModel.localExperts.observe(this, Observer {
            adapter.suggestedLocalExpert = it
        })
        postOfFollowingViewModel.data.observe(this, Observer {
            adapter.submitList(it.pagedList, it.endData)
        })
        postOfFollowingViewModel.refreshState.observe(this, Observer {
            binding.swipeRefreshLayout.isRefreshing = it.loading
        })
    }
}