package com.zody.ui.post.list.following

import androidx.lifecycle.MediatorLiveData
import com.zody.entity.ListItem
import com.zody.entity.Post
import com.zody.repository.Listing
import com.zody.repository.NetworkState
import com.zody.repository.PostRepository
import com.zody.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 20
 */
class PostOfFollowingViewModel @Inject constructor(postRepository: PostRepository) : BaseViewModel() {

    private val result: Listing<ListItem<Post>> = postRepository.loadFollowingPost()

    private val localExpertsResult = postRepository.loadLocalExpertOnPost()

    val localExperts = localExpertsResult.data
    val data = result.data
    val refreshState = MediatorLiveData<NetworkState>().apply {
        addSource(result.refreshState) {
            value = it
        }
        addSource(localExpertsResult.refreshState) {
            value = it
        }
    }

    fun refresh() {
        result.refresh.invoke()
        localExpertsResult.refresh.invoke()
    }
}