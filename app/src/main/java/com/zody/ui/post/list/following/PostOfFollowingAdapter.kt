package com.zody.ui.post.list.following

import android.content.Intent
import android.graphics.Rect
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.paging.PagedList
import androidx.recyclerview.widget.RecyclerView
import com.zody.R
import com.zody.databinding.ItemListPostListFollowingEmptyBinding
import com.zody.databinding.ItemListPostListLeaderboardBinding
import com.zody.databinding.ItemListPostSuggestedLocalExpertBinding
import com.zody.entity.Author
import com.zody.entity.ListItem
import com.zody.entity.Post
import com.zody.repository.ListingData
import com.zody.ui.base.DataBindingViewHolder
import com.zody.ui.expert.AuthorViewModel
import com.zody.ui.expert.LocalExpertActivity
import com.zody.ui.leaderboard.LeaderBoardActivity
import com.zody.ui.post.PostActionViewModel
import com.zody.ui.post.list.PostListAdapter
import com.zody.ui.post.list.PostListLocalExpertAdapter
import com.zody.utils.px

/**
 * Created by vinhnguyen.it.vn on 2018, July 26
 */
class PostOfFollowingAdapter(authorViewModel: AuthorViewModel, postActionViewModel: PostActionViewModel) : PostListAdapter(authorViewModel, postActionViewModel) {

    var suggestedLocalExpert: ListingData<ListItem<Author>>? = null
        set(value) {
            field = value
            if (itemCount > 2) {
                notifyItemChanged(1)
            }
        }

    private var empty = false
    private val startPositionOfRealList: Int
        get() = if (initialing || empty) 1 else 2

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingViewHolder {
        val holder = super.onCreateViewHolder(parent, viewType)
        when (holder.binding) {
            is ItemListPostSuggestedLocalExpertBinding ->
                holder.binding.recyclerView.apply {
                    this.addItemDecoration(object : RecyclerView.ItemDecoration() {
                        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                            outRect.right = 10.px
                        }
                    })
                    this.adapter = PostListLocalExpertAdapter()
                }

        }
        return holder
    }

    override fun onBind(binding: ViewDataBinding, item: ListItem<Post>?) {
        super.onBind(binding, item)
        when (binding) {
            is ItemListPostListFollowingEmptyBinding -> {
                binding.btEmptyAction.setOnClickListener {
                    it.context.startActivity(Intent(it.context, LocalExpertActivity::class.java))
                }
            }
            is ItemListPostSuggestedLocalExpertBinding -> {
                suggestedLocalExpert?.let {
                    (binding.recyclerView.adapter as PostListLocalExpertAdapter).submitList(it.pagedList, it.endData)
                }
            }
            is ItemListPostListLeaderboardBinding ->
                binding.root.setOnClickListener { view ->
                    view.context.startActivity(Intent(view.context, LeaderBoardActivity::class.java))

                }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            position == 0 -> R.layout.item_list_post_list_leaderboard
            initialing -> super.getItemViewType(position)
            empty -> R.layout.item_list_post_list_following_empty
            position == 1 -> R.layout.item_list_post_suggested_local_expert
            else -> super.getItemViewType(position)
        }
    }

    override fun getItem(position: Int): ListItem<Post>? {
        return when {
            position == 0 -> null
            initialing -> super.getItem(position)
            empty -> null
            position == 1 -> null
            else -> super.getItem(position - startPositionOfRealList)
        }
    }

    override fun getItemCount(): Int {
        return if (empty) 2 else super.getItemCount() + startPositionOfRealList
    }

    override fun submitList(pagedList: PagedList<ListItem<Post>>?, endData: Boolean) {
        super.submitList(pagedList, endData)

        val oldEmpty = empty
        empty = pagedList?.isNotEmpty() != true && endData
        if (oldEmpty != empty) {
            notifyDataSetChanged()
        }
    }

    override fun onInserted(position: Int, count: Int) {
        super.onInserted(position + startPositionOfRealList, count)
    }

    override fun onRemoved(position: Int, count: Int) {
        super.onRemoved(position + startPositionOfRealList, count)
    }

    override fun onMoved(fromPosition: Int, toPosition: Int) {
        super.onMoved(fromPosition + startPositionOfRealList, toPosition + startPositionOfRealList)
    }

    override fun onChanged(position: Int, count: Int, payload: Any?) {
        super.onChanged(position + startPositionOfRealList, count, payload)
    }
}