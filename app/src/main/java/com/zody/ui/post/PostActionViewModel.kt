package com.zody.ui.post

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.Transformations
import com.zody.livedata.SingleLiveEvent
import com.zody.repository.ConfigurationRepository
import com.zody.repository.PostRepository
import com.zody.repository.ProfileRepository
import com.zody.repository.Result
import com.zody.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 27
 */
class PostActionViewModel @Inject constructor(postRepository: PostRepository,
                                              profileRepository: ProfileRepository,
                                              configurationRepository: ConfigurationRepository) : BaseViewModel() {

    private val minCoinForTip = configurationRepository.getMinCoinForTip()
    private val coin = Transformations.map(profileRepository.loadProfileFromDB()) {
        it?.statistic?.coin ?: 0
    }

    private val tipToPost = MutableLiveData<String>()
    private val deletePost = MutableLiveData<String>()
    private val viewPost = MutableLiveData<String>()

    private val coinObserver = Observer<Int> {

    }

    val tipResult: LiveData<Result<Unit>> = Transformations.switchMap(tipToPost) {
        postRepository.sendTip(postId = it)
    }

    val deleteResult = Transformations.switchMap(deletePost) {
        postRepository.deletePost(postId = it)
    }

    val showPopupNotEnoughCoin = SingleLiveEvent<Unit>()

    init {
        viewPost.observeForever {
            postRepository.increaseView(it)
        }
        coin.observeForever(coinObserver)
    }

    fun tip(postId: String) {
        view(postId)
        if (coin.value ?: 0 > minCoinForTip.value ?: 0) {
            tipToPost.value = postId
        } else {
            showPopupNotEnoughCoin.call()
        }
    }

    fun delete(postId: String) {
        deletePost.value = postId
    }

    fun view(postId: String) {
        viewPost.value = postId
    }

    override fun onCleared() {
        super.onCleared()
        coin.removeObserver(coinObserver)
    }
}