package com.zody.ui.post.list

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.zody.BR
import com.zody.R
import com.zody.databinding.ItemListSuggestedLocalExpertBinding
import com.zody.entity.Author
import com.zody.entity.ListItem
import com.zody.ui.base.ListItemAdapter
import com.zody.ui.user.UserActivity

/**
 * Created by vinhnguyen.it.vn on 2018, July 26
 */
class PostListLocalExpertAdapter : ListItemAdapter<Author>(DIFF_CALLBACK) {

    override fun initViewHolder(binding: ViewDataBinding, parent: ViewGroup) {
        when (binding) {
            is ItemListSuggestedLocalExpertBinding -> binding.root.setOnClickListener { view ->
                binding.author?.let {
                    UserActivity.start(view.context, it.id)
                }
            }
        }
    }

    override fun onBind(binding: ViewDataBinding, item: ListItem<Author>?) {
        binding.setVariable(BR.author, item?.data)
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_list_suggested_local_expert
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ListItem<Author>>() {
            override fun areItemsTheSame(oldItem: ListItem<Author>, newItem: ListItem<Author>): Boolean {
                return oldItem.data?.id == newItem.data?.id
            }

            override fun areContentsTheSame(oldItem: ListItem<Author>, newItem: ListItem<Author>): Boolean {
                return oldItem.data?.avatar == newItem.data?.avatar
            }

        }
    }
}