package com.zody.ui.post.tip

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.zody.entity.Tip
import com.zody.repository.Listing
import com.zody.repository.ListingData
import com.zody.repository.NetworkState
import com.zody.repository.PostRepository
import com.zody.ui.base.BaseViewModel
import com.zody.utils.valueIfDifferent
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, July 28
 */
class PostTippedViewModel @Inject constructor(postRepository: PostRepository) : BaseViewModel() {

    private val postId = MutableLiveData<String>()

    private val listing: LiveData<Listing<Tip>> = Transformations.map(postId) {
        postRepository.loadPostTipped(it)
    }

    val pagedList: LiveData<ListingData<Tip>> = Transformations.switchMap(listing) {
        it.data
    }

    val networkState: LiveData<NetworkState> = Transformations.switchMap(listing) {
        it.networkState
    }

    fun initPostId(id: String) {
        postId.valueIfDifferent = id
    }

}