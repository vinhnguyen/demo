package com.zody.ui.verify

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.zody.repository.ProfileRepository
import com.zody.repository.Result
import com.zody.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 24
 */
class VerifyPhoneViewModel @Inject constructor(private val profileRepository: ProfileRepository) : BaseViewModel() {

    private val tokenAndPhone = MutableLiveData<Pair<String, String>>()

    val result: LiveData<Result<Unit>> = Transformations.switchMap(tokenAndPhone) {
        profileRepository.updatePhone(it.first, it.second)
    }

    fun verify(token: String, phone: String) {
        this.tokenAndPhone.value = token to phone
    }
}