package com.zody.ui.verify

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.facebook.accountkit.*
import com.facebook.accountkit.ui.AccountKitActivity
import com.facebook.accountkit.ui.AccountKitConfiguration
import com.facebook.accountkit.ui.LoginType
import com.zody.ui.base.BaseActivity
import javax.inject.Inject


/**
 * Created by vinhnguyen.it.vn on 2018, September 24
 */
class VerifyPhoneNumberActivity : BaseActivity() {

    companion object {
        private const val REQUEST_CODE_ACCOUNT_KIT = 100
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val verifyPhoneViewModel: VerifyPhoneViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(VerifyPhoneViewModel::class.java)
    }

    override val screenName: String?
        get() = "verify phone number"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startActivityForResult(Intent(this, AccountKitActivity::class.java).apply {
            putExtra(AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                    AccountKitConfiguration.AccountKitConfigurationBuilder(LoginType.PHONE, AccountKitActivity.ResponseType.TOKEN)
                            .setDefaultCountryCode("VN")
                            .build())
        }, REQUEST_CODE_ACCOUNT_KIT)

        verifyPhoneViewModel.result.observe(this, Observer {
            if (!it.loading) {
                finish()
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_ACCOUNT_KIT && resultCode == Activity.RESULT_OK && data != null) {
            data.getParcelableExtra<AccountKitLoginResult>(AccountKitLoginResult.RESULT_KEY)
                    ?.accessToken?.let {
                AccountKit.getCurrentAccount(object : AccountKitCallback<Account> {
                    override fun onSuccess(account: Account) {
                        verifyPhoneViewModel.verify(account.phoneNumber.toString(), it.token)
                    }

                    override fun onError(p0: AccountKitError?) {
                        finish()
                    }
                })
                return
            }
        }
        finish()
    }
}