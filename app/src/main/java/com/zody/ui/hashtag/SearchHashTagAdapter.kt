package com.zody.ui.hashtag

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.zody.BR
import com.zody.R
import com.zody.databinding.ItemListSearchHashTagBinding
import com.zody.entity.HashTagAndCounter
import com.zody.ui.base.DataBindingViewHolder
import com.zody.ui.base.DatabindingListAdapter
import com.zody.ui.post.hashtag.PostOfHashTagActivity

/**
 * Created by vinhnguyen.it.vn on 2018, September 25
 */
class SearchHashTagAdapter : DatabindingListAdapter<HashTagAndCounter>(DIFF_CALLBACK) {

    private var empty = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingViewHolder {
        val holder = super.onCreateViewHolder(parent, viewType)
        if (holder.binding is ItemListSearchHashTagBinding) {
            holder.itemView.setOnClickListener { view ->
                PostOfHashTagActivity.start(view.context, holder.binding.tagAndCount?.name)
            }
        }
        return holder
    }

    override fun onBind(binding: ViewDataBinding, item: HashTagAndCounter?) {
        binding.setVariable(BR.tagAndCount, item)
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            empty -> R.layout.item_list_search_hash_tag_empty
            else -> R.layout.item_list_search_hash_tag
        }
    }

    override fun getItem(position: Int): HashTagAndCounter? {
        return when {
            empty -> null
            else -> super.getItem(position)
        }
    }

    override fun getItemCount(): Int {
        return when {
            empty -> 1
            else -> super.getItemCount()
        }
    }

    override fun submitList(list: List<HashTagAndCounter>?) {
        super.submitList(list)

        val oldEmpty = empty
        empty = list?.isNotEmpty() == false
        if (oldEmpty != empty) {
            notifyDataSetChanged()
        }
    }


    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<HashTagAndCounter>() {
            override fun areItemsTheSame(oldItem: HashTagAndCounter, newItem: HashTagAndCounter): Boolean {
                return oldItem.name == newItem.name
            }

            override fun areContentsTheSame(oldItem: HashTagAndCounter, newItem: HashTagAndCounter): Boolean {
                return oldItem == newItem
            }
        }
    }
}