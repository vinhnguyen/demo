package com.zody.ui.hashtag

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import com.zody.R
import com.zody.databinding.ActivitySearchHashTagBinding
import com.zody.ui.base.BaseActivity
import com.zody.utils.afterTextChanged
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 19
 */
class SearchHashTagActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivitySearchHashTagBinding

    private val searchHashTagViewModel: SearchHashTagViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(SearchHashTagViewModel::class.java)
    }

    override val screenName: String?
        get() = "search hashtag"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search_hash_tag)
        binding.headerBar.btHeaderActionLeft.setOnClickListener {
            finish()
        }

        val adapter = SearchHashTagAdapter()
        binding.recyclerView.adapter = adapter
        binding.recyclerView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        searchHashTagViewModel.result.observe(this, Observer {
            adapter.submitList(it?.data)
        })

        binding.etSearchHashTag.afterTextChanged {
            searchHashTagViewModel.search(it)
        }
    }
}