package com.zody.ui.hashtag

import android.os.Handler
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.zody.entity.HashTagAndCounter
import com.zody.livedata.LiveDataProviders
import com.zody.repository.HashTagRepository
import com.zody.repository.Result
import com.zody.ui.base.BaseViewModel
import com.zody.utils.valueIfDifferent
import javax.inject.Inject

/**
 * Created by vinhnguyen.it.vn on 2018, September 19
 */
class SearchHashTagViewModel @Inject constructor(hashTagRepository: HashTagRepository) : BaseViewModel() {

    private val keyword = MutableLiveData<String>()

    val result: LiveData<Result<List<HashTagAndCounter>>> = Transformations.switchMap(keyword) {
        if (it == null) {
            LiveDataProviders.createAbsent()
        } else {
            hashTagRepository.searchHashTag(it)
        }
    }

    init {
        keyword.value = ""
    }

    private val handler = Handler()
    private var runnable: Runnable? = null

    fun search(value: String) {
        handler.removeCallbacks(runnable)
        value.trim().apply {
            if (this.isEmpty()) {
                keyword.valueIfDifferent = ""
            } else {
                runnable = Runnable {
                    keyword.valueIfDifferent = this
                }
                keyword.valueIfDifferent = null
                handler.postDelayed(runnable, 500)
            }
        }
    }
}